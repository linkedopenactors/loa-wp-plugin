<?php
/* Uninstall für livinglines-tools
/  Löscht die Indiv-Verzeichnisse
/  Löscht die Indiv-Variablen
/  Wird auch verwendet für das Löschen alter Versionen von CiviCRM.
*/
define('LL_PLUGIN_NAME',basename(__FILE__,".php"));
require_once('admin/ll-tools-core-functions.php');
require_once('admin/ll-tools-admin-functions.php');

// if uninstall.php is not called by WordPress, die
if (!defined('LL_UNINSTALL_PLUGIN') and !defined('WP_UNINSTALL_PLUGIN')) {
	ll_uninstall_log("Uninstall-Versuch für ".$ll_uninstall_plugin,$ll_uninstall_plugin);
	//    die("bye,bye");
}	

if (defined('WP_UNINSTALL_PLUGIN') and !defined('LL_UNINSTALL_PLUGIN')) {
	define('LL_UNINSTALL_PLUGIN',basename(dirname(__FILE__)));
	ll_uninstall_sites(basename(dirname(__FILE__)));
} 

function ll_uninstall_sites($plugins_dir) {
	ll_uninstall_log("uninstall.php ".$plugins_dir,$plugins_dir);
	if(function_exists('get_sites')) {
		if($plugins_dir == basename(dirname(__FILE__))) {
			$mu_file = WP_CONTENT_DIR."/mu-plugins/livinglines-tools-mu.php";
			ll_uninstall_log($mu_file." wird gelöscht");
			if(is_file($mu_file)) unlink($mu_file);				
		}
		$ll_tools_sites = get_sites();
		ll_uninstall_log($ll_tools_sites);
		foreach($ll_tools_sites as $ll_tools_site) {
			ll_uninstall_log($ll_tools_site->domain);
			switch_to_blog($ll_tools_site->blog_id);
			ll_uninstall_log($ll_tools_site->domain);
			ll_uninstall("_".$ll_tools_site->blog_id,$plugins_dir); 
			restore_current_blog();
			ll_uninstall_log($log);
		}
	} else {
		ll_uninstall("",$plugins_dir);
	}	
	ll_uninstall_log("Abgeschlossen",false);
}

function ll_uninstall($blog_id = "",$plugins_dir) {
	ll_uninstall_log(get_bloginfo( 'name' ));
	$llIndivDir = wp_get_upload_dir()['basedir']."/".$plugins_dir."/";
	ll_uninstall_log($llIndivDir." wird geprüft");
	if(is_dir($llIndivDir)) {
		ll_uninstall_log($llIndivDir." wird gelöscht");
		llToolsZip($llIndivDir,wp_get_upload_dir()['basedir']."/".$plugins_dir.$blog_id);
		$dirlist = ll_uninstall_deldir($llIndivDir);
		if(defined('LL_UNINSTALL_PLUGIN')) if(rmdir($llIndivDir));					
		ll_uninstall_log($dirlist);
	}
	if($plugins_dir == basename(dirname(__FILE__))) {
		$llAllOptions = wp_load_alloptions();
		ll_uninstall_log("options werden gelöscht");
		foreach($llAllOptions as $option => $value) {
			if(substr($option,0,strlen('ll_tools_')) == 'll_tools_') {
				ll_uninstall_log($option." wird gelöscht");
				if(defined('WP_UNINSTALL_PLUGIN')) delete_option($option);			
			}
		}					
	}
}

function ll_uninstall_deldir($dir,$dirlist = array()) {
	$files = scandir($dir);
	foreach($files as $file) {
		if(count_chars($file,3) != ".") {
			if(is_dir($dir.$file)) {
				$dirlist = ll_uninstall_deldir($dir.$file."/",$dirlist);
				if(defined('LL_UNINSTALL_PLUGIN')) {
					if(rmdir($dir.$file));					
				}
			}
			if(is_file($dir.$file)) {
				$dirlist[] = $dir.$file;
				if(defined('LL_UNINSTALL_PLUGIN')) unlink($dir.$file);
			}				
		}
	}
	return $dirlist;
}

if(!function_exists('llToolsZip')) {
	function llToolsZip($dirname, $filename = "wcZIPfile") {
		ll_uninstall_log(array("ZIP-START",$dirname,$filename));
		$dirlist = new RecursiveDirectoryIterator($dirname);
		$filelist = new RecursiveIteratorIterator($dirlist);

		$filelimit = 250;
		$emptydir = array();
		$useddir = array();
		$alldir = array();
		$allfile = array();


		$zip = new ZipArchive();
		$i = 1;
		$basename = $filename;
		while (file_exists($filename.".zip")) {
			$filename = $basename."-".strval($i);
			$i++;
		}
		$filename = "{$filename}.zip";
		if(file_exists($filename)) {
			ll_uninstall_log("File existiert bereits, Vorgang abgebrochen!");
			return false;
		}
		if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
			ll_uninstall_log("cannot open <$filename>");
			return false;
		}
		foreach ($filelist as $key => $value) {
			$patharr = pathinfo($key);
			$pathtest = $patharr['dirname'];
			if(!in_array($pathtest,$alldir)) {
				$alldir[] = $pathtest;			
			}
			if(substr($key,-1) == ".") {
				if(!array_key_exists($pathtest,$useddir)) {
					if (!array_key_exists($pathtest,$emptydir)) {
						$emptydir[$pathtest] = "";				
				 	} 
				}
			} elseif(strpos($key,".~") === false) {
				ll_uninstall_log($key);
				if(!array_key_exists($pathtest,$useddir)) $useddir[$pathtest] = "" ;
				if(array_key_exists($pathtest,$emptydir)) unset($emptydir[$pathtest]);
				$path = str_replace(dirname($dirname)."/", "", $key); 

			    if ($zip->numFiles == $filelimit) {$zip->close(); $zip->open($filename) or die ("Error: Could not reopen Zip");}
			    if (!file_exists($key)) { 
					$out.= $key.' does not exist. Please contact your administrator or try again later.'; 
				} elseif (!is_readable($key)) { 
					$out.= $key.' not readable. Please contact your administrator or try again later.'; 
				} else {
					$zip->addFile($key, $path) or die ("ERROR: Could not add file: $key </br> numFile:".$zip->numFiles);
					$allfile[] = $key;
				}

			}
		}
		foreach ($emptydir as $key => $value) {
			$path = str_replace(dirname($dirname)."/", "", $key); 		
			$zip->addEmptyDir( $path );
		}
		return $zip->close();
	}	
}

function ll_uninstall_log($params,$plugins_dir="") {
	static $fp;
	if(!isset($fp)) {
		if (!($fp = fopen(ABSPATH."wp-content/uploads/ll-tools-uninstall_log_".$plugins_dir.".txt", 'a'))) {
		    return;
		}		
	}
	ob_start();
	print_r($params);
	fprintf($fp,ob_get_contents());
	ob_clean();
	fprintf($fp,chr(10));	
	if($plugins_dir === false) fclose($fp);
}


?>
