<?php
/**
 * Speichert und liefert Listformate
 * 
 * @since 4.2.7
 *
 * @param string $key Prefix der Felder
 */


class LL_classes_rendering {
	/* Prefix der Felder in denen die Defintion hinterlegt ist */
	protected $field_key;

	/* Array aller Felder mit dem Prefix $field_key */
	protected $field_defs;

	private static $_singleton;

	function __construct($key) {
		$this->field_key = $key;
		$field_defs = get_post_meta(get_the_ID());
		//Hier werden nur die Definitionen für $field_key übernommen (sparql)
		foreach($field_defs as $key => $value) 
			if(strpos($key,$this->field_key."-") === 0) $this->field_defs[str_replace($this->field_key."-","",$key)] = $value[0];

	}
		
	public static function singleton($key){
		if(!isset(self::$_singleton[$key]))
			self::$_singleton[$key] = new LL_classes_rendering($key);
		return self::$_singleton[$key];
	}

	/**
	 * Wandelt übergebene Wertepaare zur Abspeicherung um.
	 * 
	 * @since 4.2.7, ab 4.2.8 werden vorhandene Werte ergänzt 
	 *
	 * Es entstehen keine doppelten Einträge, vorhandene Werte überschreiben neue.
	 *
	 * @param array $values Wertepaare
	 * header: LOA Felder im Header
	 *        'cols'   max. Anzahl der Spalten im Grid (nur für Table)
	 *	fields 'label' Bezeichnung
	 *			 'row' Zeile
	 *			 'col' Spalte (fehlende Nummern werden an die Folgespalte gehängt)
	 *			 'link_url' LinkURL	
	 *			 'field' Feld, aus der die Daten kommen sollen (für Felder nicht direkt aus den Daten kommen)
	 *			 'name'  Name des Feldes im ZielSet, wenn ein für einen Wert ein bestimmter Felname erwartet wird.
	 *			 
	 *			 $type+Wert ermöglicht die Übersteuerung des Wertes für eine bestimmte Abfrage (alle Basiswerte gelten)
	 * 			 map+name bedeutet, dass für die Abfrage map das Feld mit name im Set steht.
	 *
	 *        string $field Feldname (ohne Prefix)
	 */

	
	private function make_field_values($values,$field) {
		$field_values = array();
		$values = array_merge($values,$this->get_field_values($field,'raw'));
		foreach($values as $var => $value) {
			if(is_array($value)) {
				foreach($value as $key => $v) $field_values[] = $var."=".$key."::".$v;
			}else
				$field_values[] = $var."=".$value;
		}
		ll_crm_debug(implode(";\n",$field_values));

		$this->field_defs[$field] = implode(";\n",$field_values);
		update_post_meta(get_the_ID(),$this->field_key.'-'.$field,$this->field_defs[$field]);
	}
				

	/**
	 * Bereitet die Felder für eine übergebene Feldliste vor.
	 * 
	 * @since 4.2.7
	 *
	 * @param string $key Prefix zur Abspeicherung
	 * @param array $rows Feldliste
	 * @param string $header Headerdaten zur Abspeicherung
	 */

		
	public static function make_row_def($key,$rows, $header) {
		$rendering = self::singleton($key);
  		$rendering->make_field_values($header,'-header');
		
		$row = 1;
		$col = 1;
		foreach( $rows as $field ) {
			$field_def = array("label" => $field,"row" => $row, "col" => $col);
	  		$rendering->make_field_values($field_def,$field);
	  		if($col>5) {
				$col = 1;
				$row++;	  		
	  		}else
		  		$col++;		
		}	
	}

	/**
	 * Steuert die Ausgabe
	 * 
	 * @since 4.2.7
	 *
	 * @param array $result Ergebnisdaten
	 * @param string $key Prefix der Feldliste
	 * @param string $output Art der Aufbereitung
	 */


	public static function rendering($result,$key,$output = 'table') {
		$rendering = self::singleton($key);
		if($output == 'table')
			return $rendering->rendering_table($result);		
		elseif($output == 'map')
			return $rendering->rendering_map($result);
		elseif($output == 'fields')
			return $rendering->get_rendering_def($result);
	}

	/**
	 * Gibt $field_key--header zurück oder false, wenn er nicht existiert.
	 * 
	 * @since 4.2.9
	 *
	 * @param string $key Prefix zur Abspeicherung
	 */

	public static function check_field_def($key) {
		$rendering = self::singleton($key);
		if(isset($rendering->field_defs['-header'])) return $rendering->get_field_values('-header');
		return false;
	}
		
	/**
	 * Auslesen der Felddefinition und Rückgabe der gewünschten Parameter
	 * 
	 * @since 4.2.7, überarbeitet 4.2.9 (Parametergruppen)
	 *
	 * @param string $field Feldname ohne $field_key und erstem Trennzeichen.
	 * @param string $type Paramtergruppe, 'raw' = alle, 'base' = ohne Prefix, xxx = Prefix (wird um + ergänzt)
	 */

	private function get_field_values($field,$type = 'base') {
		static $fields_values;
		if(!isset($fields_values[$field])) {
			$field_values = array('raw' => array(), 'base' => array());
		
			if(!empty($this->field_defs[$field])) {
				$values = str_replace("\n","",$this->field_defs[$field]);

				//Name ist immer mit dem Feldnamen belegt.
				$field_values['base']['name'] = $field;									

				$pars = explode(";",$values);
				foreach($pars as $par => $value) {
					$el = explode("=",$value);
					$el[0] = trim($el[0]);
					if(!isset($el[1])) $el[1] = "";
					if(strpos($el[1],"::") != 0) {
						$ele = explode("::",$el[1]);			
						$field_values['raw'][$el[0]][$ele[0]] = $ele[1];		
					}else
						$field_values['raw'][$el[0]] = trim($el[1]);					

					if(strpos($el[0],"+") === false) $field_values['base'][$el[0]] = $field_values['raw'][$el[0]];
					else {
						$ele = explode("+",$el[0]);
						$field_values[trim($ele[1])][trim($ele[0])] = $field_values['raw'][$el[0]];
					}
				}
			}
			$fields_values[$field] = $field_values;		
		}
		ll_crm_debug($fields_values);
		if(empty($fields_values[$field][$type])) $type = 'base'; 
		if(in_array($type,array('raw','base')))
			return $fields_values[$field][$type];
		else
			return array_merge($fields_values[$field]['base'],$fields_values[$field]['type']);
	}
	
	/**
	 * Liest die Felddefinition aus und organisiert sie nach Zeilen und Spalten
	 * 
	 * @since 4.2.7, überarbeitet 4.2.9 (Parametergruppen)
	 *
	 * @param string $field Feldname ohne $field_key und erstem Trennzeichen.
	 * @param string $type Paramtergruppe, 'raw' = alle, 'base' = ohne Prefix, xxx = Prefix (wird um + ergänzt)
	 */

	
	protected function get_rendering_def($data,$type = 'base') {
	  	$data_def = array();
		$error = "";
		$data_def['header'] = $this->get_field_values('-header',$type);
		$data_def['header']['cols'] = 0;
		ll_crm_debug($data_def);

		foreach($this->field_defs as $field => $dummy) {
			if(strpos($field,'-') === 0) continue; //OHne Header
			$field_def = $this->get_field_values($field,$type);
			if(!empty($field_def)) {
		 	  	if(empty($field_def['row'])) $field_def['row'] = "001";
		 	  	else $field_def['row'] = str_pad($field_def['row'],3,"0",STR_PAD_LEFT);
		 	  	if(empty($field_def['col'])) $field_def['col'] = "001";
		 	  	else $field_def['col'] = str_pad($field_def['col'],3,"0",STR_PAD_LEFT);	 	  	
		 	  	if(!isset($field_def['label'])) $field_def['label'] = "";
			 	  	$data_def['header'][$field_def['row']."-".$field_def['col']] = $field_def['label']; 
		 	  	$data_def['fields'][$field_def['row']."-".$field_def['col']][$field] = $field_def;
		 	  	$data_def['header']['cols'] = max($data_def['header']['cols'],$field_def['col']);
		 	  	ll_crm_debug($data_def['header']['cols']);		  
			}
		}
		if(empty($data_def['fields'])) $this->error("Keine Felddefinition eingetragen (".__FUNCTION__.",".__LINE__.")");
		ll_crm_debug($data_def);
		return $data_def;
	}
	
	/**
	 * Aufbereitung der Tabelle auf Basis der Definitionsparameter (Der Type wurde bereits in der Definition übergeben)
	 * 
	 * @since 4.2.7
	 *
	 * @param array $data_def Definition des Grids
	 *
	 */


	private function prepare_grid($data_def) {
		ksort($data_def['fields']);
		ksort($data_def['header']);
		ll_crm_debug($data_def);
		$grid = array();
		$row = 0;
		foreach($data_def['fields'] as $cell => $dummy) {
			$pos = explode("-",$cell);
			$grid[$pos[0]][$pos[1]] = "";
			if(intval($pos[0]) > $row) {
			 if($row > 0) {
			 	$grid[$pos[0]][$pos[1]] .= "</td></tr>";
	 			if($data_def['header']['cols']-intval($col) > 0)
					$grid[$row][$col] = 
						str_replace("<td>","<td colspan='".($data_def['header']['cols']-intval($col)+1)."'>",$grid[$row][$col]);
			 }	
			 $grid[$pos[0]][$pos[1]] .= "<tr><td>";
			 $row = str_pad($pos[0],3,"0",STR_PAD_LEFT);
			 $col = "001";
			}
			if($pos[1] > $col) {
				if(intval($pos[1])-intval($col) > 1)
					$grid[$row][$col] = 
						str_replace("<td>","<td colspan='".(intval($pos[1])-intval($col))."'>",$grid[$row][$col]);
				$grid[$pos[0]][$pos[1]] = "";
				if($col > 0) $grid[$pos[0]][$pos[1]] .= "</td>";
				$grid[$pos[0]][$pos[1]] .= "<td>";
				$col = str_pad($pos[1],3,"0",STR_PAD_LEFT);;
			}			
		}
		ll_crm_debug($pos);
		if($data_def['header']['cols']-intval($col) > 0)
				$grid[$pos[0]][$pos[1]] = 
					str_replace("<td>","<td colspan='".($data_def['header']['cols']-intval($col)+1)."'>",$grid[$pos[0]][$pos[1]]);
							
		ll_crm_debug($grid);
		return $grid;
	}

	/**
	 * Aufbereitung der Daten für die Karte
	 * 
	 * @since 4.2.9
	 *
	 * @param array $data Daten
	 *
	 */

	private function rendering_map($data) {
		$map_fields = array();
		$result = array();
		foreach($this->field_defs as $field => $dummy) {
			if(strpos($field,'-') === 0) continue;
			$map_fields[$field] = $this->get_field_values($field,'map')['name'];
		}
	   foreach($data as $key => $data_set) {
	   	foreach($map_fields as $field => $map_field) {
	   		$result[$key][$map_field] = $data_set[$field];
	   	}
	   }
		return $result;
	}

	/**
	 * Aufbereitung der Daten als Tabelle
	 * 
	 * @since 4.2.7
	 *
	 * @param array $data Daten
	 *
	 */

	private function rendering_table($data) {
		$data_def = $this->get_rendering_def($data);
//		if(!is_array($data_def)) return $data_def;	sollte durch error abgefangen sein!	
		$grid = $this->prepare_grid($data_def);
		ll_crm_debug($data_def);
		ll_crm_debug($data);
		ll_crm_debug($grid);
		$out = "<table class='wpsparql_result_list'>";
		if(!empty($data_def['header']['label']))
			$out .= "<tr><td colspan='".$data_def['header']['cols']."'>".$data_def['header']['label']."</td></tr>";
		$header_out = "";
		$header_result = false;
		foreach($grid as $row => $row_data) {
			$cell_out = "";
			$row_result = false;
			foreach($row_data as $col => $col_data) {
				$key = $row."-".$col;
				if(!$row_result) $row_result = !empty($data_def['header'][$key]);
				$cell_out .= $col_data.$data_def['header'][$key];
			}
			$header_out .= $cell_out;
			if(!$header_result) $header_result = $row_result;
		}
		if($header_result) $out .= $header_out."</td></tr>";
		$i = 1;		
		foreach( $data as $data_set ) {
			foreach($grid as $row => $row_data) {
				$cell_out = "";
				$row_result = false;
				foreach($row_data as $col => $col_data) {
//					$col_data .= $i." ";
					$key = $row."-".$col;
					$cell_data = array();
					$link_data = array();
					foreach($data_def['fields'][$key] as $field => $field_def) {
						ll_crm_debug($field);
						if(!$row_result) $row_result = !empty($data_set[$field]);
						$cell_data[$field] = $data_set[$field];

						$link = $field;
						if(isset($field_def['link_url']) and isset($data_set[$field_def['link_url']]))
							$link = $field_def['link_url'];
						if(isset($data_set[$link.".type"]) and ($data_set[$link.".type"] == 'uri'))
							$cell_data[$field] = 
								"<a target='_blank' href='".$data_set[$link]."'>".$data_set[$field]."</a>";
					}
					$cell_out .= $col_data.implode("<br>",$cell_data);
				}
				if($row_result) $out .= $cell_out;					
			}
			$i++;
		}
		$out .= "</td></tr>";

		$out .= "</td></tr>"."</table>";

		return $out;		
	}

	protected function error($text) {
	   throw new Exception('<p class="error">'.$text.'</p>');
	}


}
?>
