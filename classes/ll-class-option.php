<?php
/**
* Funktionen zur Darstellung der Adminseiten
*/

################### Funktionen für die Adminseiten ####################

class LL_classes_option {
	private $multi_set;
	private $option;
	private $field;
	private $args;
	private $multiple;
	
	private static $_singleton;

	function __construct() {

	}

	private function set_vars($args) {
		$this->args = $args;
		$this->multi_set = isset($args['multi_set']);
		$this->option = $args['field'];
		$this->field = str_replace(LL_TOOLS_OPTION,"",$args['field']);
		$this->multiple = array();
		//Hier kann es für MultiSet eine Änderung geben!
		$this->setting = ll_tools_get_options($this->option);
	}
	
	public static function singleton($args = array()){
		if(!isset(self::$_singleton[$args[0]]))
			self::$_singleton[$args[0]] = new LL_classes_option();
		return self::$_singleton[$args[0]];
	}

	public static function default_field_cb($args) {
		$args[0] = 'all';
		$section = self::singleton($args);
		$section->set_vars($args);

		//HIer muß noch gearbeitet und überlegt werden.
		//Dies ist notwendig, weil derzeit davon ausgegangen wird, dass immer der default eingetragen wird
		//Wird verwendet um neue Werte mit Haken erzeugen zu können, Haken immer false.		
		$args['setting'] = $section->setting;
		if(isset($args['default'])) $args['setting'] = $args['default'];

		//Kompatibilitätsfunktion
		foreach(array('type','step','max','min') as $key) {
			if(isset($args[$key])) $section->args['format'][$key] = $args[$key];		
		}
	     echo "<p>".apply_filters('ll_tools_field_pre_text_'.$section->field,$args['pre_text'],$args)."</p>";
		if(!$section->default_suboptions_cb($args)) {
			$section->args['format']['value'] = (isset($args['setting'])) ? $args['setting'] : "";
			$section->args['format']['description'] = $section->args['description'];
			$section->make_output($section->args['format'],$section->field);
		}

	   echo "<p>".apply_filters('ll_tools_field_post_text_'.$section->field,$args['post_text'],$args)."</p>";
	}
	
	public static function set_multiple() {
		$args[0] = 'all';
		$section = self::singleton($args);
		ll_crm_debug($section->multiple);
		if(!empty($section->multiple)) {
			echo '<input type="hidden" id="ll_tools_multiple" name="ll_tools_multiple" value='.json_encode($section->multiple).' />';		
		}	
	}	
		
		
	private function default_suboptions_cb($options) {
		if(empty($options['options'])) return false;
		$options = $options['options'];
		ll_crm_debug($options);
		if(!empty($this->args['description'])) echo "<p>".$this->args['description']."</p>";
		ll_crm_debug($options);
		//Hier wird die Description der Option nicht angezeigt); 
		//Nur für Konvertierung bei Umstellung ????
		if(!empty($options) and !is_array($this->setting)) $this->setting = array($this->setting);
		foreach($options as $value => $option) {
			$opt = (is_array($option)) ? $option : array('description' => $option);
			$opt['field'] = (is_int($value)) ? $option : $value;
			if(empty($opt['description'])) $opt['description'] = (isset($opt['title'])) ? $opt['title'] : "";
//			ll_crm_debug($option,($opt['field'] == 'aktiv1'));

			$opt = array_merge($this->args['format'],$opt);
			ll_crm_debug(array($opt,$options,$this->args));
		//),array('valuekey' => $value)),$args['setting']
			$opt['value'] = false;

			//Die beiden folgenden Bedingungen sind für OptionOptions bedeutungslos, gelten aber für Options
			if(isset($this->setting[$opt['field']])) $opt['value'] = $this->setting[$opt['field']];
			if(in_array($opt['field'],$this->setting,true)) $opt['value'] = $opt['field']; 

			//Hier werden die Daten für OptionOptions eingetragen, die bereits dort ins default übernommen wurden.
			if(empty($opt['value']) and isset($opt['default'])) {
				$opt['value'] = $opt['default'];
				unset($opt['default']);			
			} 
//			ll_crm_debug($opt,($opt['field'] == 'aktiv1'));
			$this->make_output($opt,$opt['field'],true); 
		}
		return true;
		
	}

	private function make_output($option,$field,$sub=false) {
			$id = $this->option."-".str_replace(array("+","."),"-",$field);

			if(substr($field,0,1) == "*") {
				echo '<p>'.$option['description'].'<p>';
			}elseif(substr($field,0,4) == "_div") {
				echo $option['description'];
			}elseif($option['type'] == 'textarea') {
				$add = $this->make_format($option,$field,$sub);
				echo '<p>'.'<textarea name="'.esc_attr($this->option).$add.' id="'.$id.'" >'.$option['value'].'</textarea>';
				echo esc_html($option['description']).'</p>';		
			}elseif($this->make_select_field($option,$field));
			elseif(!empty($option['multiple'])) {
				ll_crm_debug($this->field);
		   	$set = str_replace($option['multiple'],"",$field);
				$multiple = 'multiple_'.$this->field.'_'.$option['multiple'];
		   	
				//Mustervorlage gibt es nur einmal für Option/Feld
		   	if(!isset($this->multiple[$this->field][$option['multiple']])) {
					$add = $this->make_format($option,$field,$sub);
					echo '<p id="'.$multiple.'" style="display:none;"><label id="label_'.$multiple.'">';
					echo '<input '.$add.' id="id_'.$multiple.'" >';
					echo '</label></p>';
				}

				// Gibt es für jedes Set des Feldes in der Option
		   	if(!isset($this->multiple[$this->field][$option['multiple']][$set])) {
					echo '<div id="base_'.$multiple.'_'.$set.'">';
					$par = "'".implode("','",array($this->field,$set,$option['multiple'],0,""))."'";						
					echo '<a id="add_namespace" href="#" onClick = "addRow('.$par.')">Add</a></div>';
				}

		   	$this->multiple[$this->field][$option['multiple']][$set] = array();
				if(is_array($option['value'])) {
					foreach($option['value'] as $key => $value) {
						if(!empty($value)) $this->multiple[$this->field][$option['multiple']][$set][] = $value;
					}
				}

			} else {
				$add = $this->make_format($option,$field,$sub);
				echo '<p><label><input name="'.esc_attr($this->option).$add.' id="'.esc_attr($id).'" >'.
						$option['description'].'</label></p>';				
			}	
	}

	private function make_select_field($option,$suboption) {
			if(($option['type'] != 'select') or
			    !isset($option['options']) or
			    !is_array($option['options'])) return false;
			echo '<p><label>';
			echo '<select name="'.$this->option.'['.$suboption.']"'.' size="1" style="margin-right:30px;">';
			$found = false;
			foreach($option['options'] as $key => $select) {
				if(is_int($select)) $option['value'] = intval($option['value']);
				$selected = "";
				if($select === $option['value']) {
					$selected = ' selected';
					$found = true;
				} 
	//			ll_crm_debug(array($key,$select,$args['setting'][$value],$selected,$value),'print');
				$select_value = "";
				if(!is_int($key)) {
					$select_value = ' value="'.$select.'"';
					$select = $key;
				} 
				ll_crm_debug(array($option));
				echo '<option'.$select_value.$selected.'>'.$select.'</option>';    
			}
			//Damit nicht Werte verloren gehen, die nicht mehr im Select sind!
			if(!$found and !empty($option['value'])) {
				echo '<option'.$selected.'>'.$option['value'].'</option>';    			
			}
		   echo '</select>'.$option['title'].'</label></p>';
		   return true;   		
	}	

	private function make_format($options,$field,$sub) {
		ll_crm_debug(array($options,$field,$this->option));
		$add = '"';
		if($sub) {
			if($options['type'] == 'radio')
				$add = '[]"'; //notwendig, weil die Felder gleich heißen müssen, sonst Mehrfachauswahl!
			else
				$add = '['.$field.']"';
		}
		if(!isset($options['style'])) $options['style'] = "";
		$options['style'] .= "margin-right:30px;";
//		$add .= ' style="margin-right:30px;" ';
		if(in_array($options['type'],array('checkbox','radio'))) {
			if($sub) { //Variante mit Optionen
				//bvalue kann in die Options eingetragen werden, um einen bestimmten Wert in die Checkbox einzutragen.
				$value = (isset($options['bvalue'])) ? $options['bvalue'] : $field;
				ll_crm_debug(array($options['value'],$value));
				//Neu eingeführt, weil sonst bei Umsortierung der Inhalt nicht stimmt.
				ll_crm_debug($options['value']);
				if(!empty($options['value']) and ($options['value'] != '-'))
					$add .= $this->set_checked($value,$value)." ";
//				$add .= $this->set_checked($value,$options['value'])." ";
				$options['value'] = $value;				
			} else {
				$add .= $this->set_checked($options['value'],'on')." ";
				unset($options['value']);			
			}
		}
		if(!empty($options['multiple'])) {
			$add = "";
			unset($options['value']);
		}
		unset($options['field']);
		unset($options['description']);
		foreach($options as $key => $value) $add .= $key."='".esc_attr($value)."' ";
		return $add;

	}
	
	private function set_checked($setting,$value = true) {
		ll_crm_debug(array($setting,$value));
		if(($setting === $value) or (is_array($setting) and in_array($value,$setting))) return " checked = 'checked' ";
		return "";
	}


}

?>
