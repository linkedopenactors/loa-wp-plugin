<?php
/**
* Funktionen zur Darstellung der Adminseiten
*/

class LL_classes_optionOptions {
	/* Array mit allen Defaults field => default */
	private $def_default;

	/* Array mit allen Defintionen field => array()
	 * Erweitert um die Sprachfelder und die Multiset-Felder 
	 * in beiden (auch oben) sind die Subarrays (+) aufgelöst */
	private $def_data_set;

	/* Array für alle Instanzen von Datasets ($element_type)
	 * modifiziert für individuelle Sets. */	 
	private $defaults;

	/* Set der Basisdefinition in das dann die definierten Werte gemergt werden */
	private $def_definition;

	/* Enhält die mit get_option ausgelesenen Werte */
	private $data;

	/* Enthält das Array set aus $data */
	private $data_sets;

	/* Name des OptionSets */
	private $option;

	/* Name des BasisOptionSets, wenn es mehrere Sets davon gibt, sonst ident mit $option */
	private $option_group;

	/* Rückgabeset für die Ausgabe */
	private $options;

//	private $save_mode;

	/* Feld, welches die Datasets differenziert (optional) */
	private $element_group;

	/* Array der möglichen Werte für $element_group, Array der unterschiedlichen DataSets */
	private $element_type;

	/* True gibt an, dass alle Variablen sofort ausgebildet und über Javascript ein und ausgeschaltet werden können.
	 * False Default, bedeutet, dass die Änderung für ein Dataset gewählt werden muß.
	 * True wird gesetzt, wenn definition['style'] = 'include' ist */
	private $style_inc;

	private static $_singleton;


	/**
	 * Konstruktur
	 * 
	 * 
	 * @since
	 *
	 * @param array $args option, optiongroup
	 *
	 * Die Klasse wird auch beim Speichern erzeugt wird, damit der Filter angestossen werden kann!
	 * Es müssen nicht alle Variablen auch beim Speichern aufgebaut werden.
	 * Daher sollten Variablen, die fürs Speichern nicht gebraucht werden nicht aufgebaut werden.
	 */


	function __construct($args) {
		$this->option = $args[0];
		$this->option_group = (isset($args[1])) ? $args[1]: $args[0];
		$def = apply_filters('ll_tools_get_standardSet_'.$this->option_group,array(),'all');
		$this->def_definition = array('element_group' => "",
						    'del' => array("-","ändern","löschen","kopieren"),
						    'style' => "include",
			//			    'option' => $this->option_group,
				   		 'detail_header' => "<detail_header> nicht definiert ",
				     	    'element_name' => "<element_name nicht definiert>",
						    'element_new' => "Neues Set hinzufügen",
						    'element_group' => "",
						    'new' => array('-',"Neues Element hinzufügen","type" => "checkbox")
							);		
		if(isset($def['default'])) {
			$this->def_data_set = $def['default'];
			$this->def_default = $this->set_default();
		} else ll_crm_debug("StandardSet ist nicht definiert",true);

		if(isset($def['definition']))
			$this->def_definition = array_merge($this->def_definition,$def['definition']);

		$this->style_inc = ($this->def_definition['style'] == 'include');

		if($this->style_inc) unset($this->def_definition['del'][array_search('ändern',$this->def_definition['del'])]);

		$this->element_group = $this->def_definition['element_group'];

		$this->element_type = array();
		if(!empty($this->element_group)) { 
			foreach(array_slice($this->def_definition['new']['options'],1) as $key => $type)
				$this->element_type[$type] = (is_int($key)) ? $type : $key;
		}
		$this->data = get_option(LL_TOOLS_OPTION.$this->option,array('check' => array(0)));
		$this->data_sets = (isset($this->data['sets'])) ? $this->data['sets'] : array();
	}
	
	
	public static function singleton($args = array()){
		if(!isset(self::$_singleton[$args[0]]))
			self::$_singleton[$args[0]] = new LL_classes_optionOptions($args);
		return self::$_singleton[$args[0]];
	}
	
	/**
	 * Gibt die Defaults zurück field => Default
	 * 
	 * 
	 * @since
	 *
	 * @param array $args
	 *
	 */

	public static function get_default($args) {
		$optionOptions = self::singleton($args);
		return $optionOptions->def_default;
	}
	
	/**
	 * Erstellt das Array "options" welches dann für die Ausgabe von Subelementen verwendet wird.
	 * Standardaufruf dieser Klasse. 
	 * Für den Aufruf beim Speichern wird davor noch der Filter aktiviert.
	 *
	 * @since
	 *
	 * @param array $args 
	 *
	 */
	
	public static function get_suboption_set($args) {
		$optionOptions = self::singleton($args);
		if(LL_SETTINGS_SAVE) {
			add_filter('pre_update_option_'.LL_TOOLS_OPTION.$args[0],array($optionOptions,'update_data_sets'),10,2);
			return array();
		}
		if(!isset($optionOptions->options)) $optionOptions->make_suboptions_setting();
		ll_crm_debug($optionOptions->options);
		return $optionOptions->options;	
	}
	
	
	/**
	 * Erweiter die Standarddefinition um 
	 * 	Sprachabhängige Felder
	 *		Multisets
	 *		Löst die SubArrays (+) auf
	 * Erzeugt damit eine erweiterete Defintion und die Default 
	 *
	 * @since
	 *
	 * @param 
	 *
	 * MultiSet muß noch um eine automatische Anpassung ergänzt werden siehe => ll_sparql_settings_logic.js
	 */

	private function set_default() {
		$default = array();
		$i=0;
		foreach($this->def_data_set as $option => $values) {
			$opt = explode("+",$option);
			if(isset($opt[1])) 
				$default[$opt[0]][$opt[1]] = $values[0];
			else  
				$default[$option] = $values[0];
			if(isset($values['language'])) {
				$languages = apply_filters('ll_tools_languages',array());
				if(!empty($languages)) {
					$default[$option] = array();
					$new = array();				
					foreach($languages as $language) {
						$default[$option][$language] = $values[0];
						$new[$option."+".$language] = $values;
						$new[$option."+".$language][1] = $values[1]." (".substr($language,3).")";
					}
					$first = array_splice($this->def_data_set,0,$i);
					$this->def_data_set = array_merge($first,$new,array_splice($this->def_data_set,1));
				}		
			}
			
			$i++;
 		}
		return $default;
	}

	/**
	 * Löst Subarray in den Daten für die Anzeige auf
	 * 
	 * @since
	 *
	 * @param string $key = field aus der Definition 
	 * @param array $set = Dataset 
	 *
	 */

	private function set_subDefault($key,$set) {
			$opt = explode("+",$key);
			if(!isset($set[$opt[0]])) return "";
//			ll_crm_debug(array($key,$test),isset($opt[1]));
			return (isset($opt[1])) ? $set[$opt[0]][$opt[1]] : $set[$opt[0]];
	}


	/**
	 * Falls es mehrere Instanzen gibt, werden hier die Defaults je Instanz erzeugt
	 * 
	 * @since
	 *
	 * @param array $set = Dataset 
	 *
	 * 4.2.5.5 => $def[2] enthält nun die ausgeschlossenen Felder (bis dahin die angezeigten ...)
	 * 4.2.5.5 => Filter ergänzt, $set[$this->element_group] ergänzt.
	 * 4.2.11 neuer Filter 'll_tools_default_data_'.$this->option_group für die Select-Options von CiviCRM_API (Hack), 
	 *        damit das Default-Set auch noch abhängig von den Daten verändert werden kann
	 *
	 */

	private function make_suboptions_defaults($set) {
		if(empty($this->element_group)) return $this->def_data_set;
		if(!isset($this->defaults)) {	
			$this->defaults = array();
			foreach($this->element_type as $type => $dummy) {		
				//ll_crm_debug($type,true);
				$this->defaults[$type] = apply_filters('ll_tools_default_'.$this->option_group,
						$this->def_data_set,$type,$this->data_sets,$this->option);
				$this->defaults[$type] = apply_filters('ll_tools_default_'.$this->option_group."_".$type,
						$this->defaults[$type],$this->data_sets,$this->option);
				foreach($this->def_data_set as $element => $def) {
					ll_crm_debug(array($element,$def,$type));
					if(!empty($def[2]) and is_array($def[2]) and in_array($type,$def[2]))
						unset($this->defaults[$type][$element]);					
				}
			}
			ll_crm_debug($this->defaults); 
		}
		ll_crm_debug($this->defaults);
		ll_crm_debug($this->element_group);
		ll_crm_debug($set[$this->element_group]);
		ll_crm_debug($set);
		return apply_filters('ll_tools_default_data_'.$this->option_group."_".$set[$this->element_group],
														$this->defaults[$set[$this->element_group]],$set);
	}

	/**
	 * Baut die Defintion für die Suboptions auf
	 * 
	 * @since
	 *
	 * @param int $setNR = Nummer des Datasets
	 *	       array $default_options - üblicherweise leer, wird nur für "new" übergeben 
	 *        array $set - üblicherweise leer, wird nur für New übergeben
	 */


	private function make_suboptionGroup_setting($setNr,$default_options = NULL, $set = NULL) {
		ll_crm_debug(array($setNr,$this->option));
		if(!isset($set)) $set = (isset($this->data_sets[$setNr])) ? $this->data_sets[$setNr] : $this->def_default;
		if(!isset($default_options)) $default_options = $this->make_suboptions_defaults($set);
		
		ll_crm_debug(array($default_options,$setNr,$set));
		$description = "";
		foreach($default_options as $key => $params) {
			$options_key = $key.$setNr;
			$default = $this->set_subDefault($key,$set);
			ll_crm_debug(array($options_key,$key,$params,$default_options));
//			ll_crm_debug($default,($options_key == 'aktiv1'));
			foreach($params as $arg => $param) {
				if($arg == 1) {
					$this->options[$options_key]['title'] = $param;
					$this->options[$options_key]['default'] = $params[0];									
					if(!empty($default)) {
						$this->options[$options_key]['default'] = $default;
						//Element die in der Zeile angezeigt/nicht angezeigt werden.
						if(is_array($default)) $default = implode(",",$default); //für multiple
						if(!in_array($key,array('name',$this->element_group))) 
							$description .= $key." = ".$default.", ";
					}
//					ll_crm_debug($this->options[$options_key],($options_key == 'aktiv1'));
				} elseif(is_string($arg)) 
					$this->options[$options_key][$arg] = $param;				
			}
		}
		return substr($description,0,-2);
	}


	/**
	 * Erstellt das Array "options" welches dann für die Ausgabe von Subelementen verwendet wird.
	 * 
	 * 
	 * @since
	 *
	 * @param 
	 *
	 */

	private function make_suboptions_setting() {
		$this->options = array();		
		
		$def = $this->def_definition;
		if((!$this->style_inc) and isset($this->data['check'])) {
			foreach($this->data['check'] as $setNr) {
				$this->options["*".$def['detail_header'].$setNr] = $def['detail_header'].$setNr;
				$this->make_suboptiongroup_setting($setNr);
			}
			$this->options['save'] = array('default' => implode(",",$this->data['check']), 'type' => 'hidden');
			unset($this->data['check']);
			update_option(LL_TOOLS_OPTION.$this->option,$this->data);
		} else {
			foreach($this->data_sets as $setNr => $set) {
				$pre = "";
				if(!empty($this->element_group)) {
//					ll_crm_debug($set[$this->element_group],true);
					$pre = $this->element_type[$set[$this->element_group]];
					$pre = "<b>".strtoupper($pre)."</b> - ";
				}
				$description = $pre.$def['element_name'].$setNr;
				if(isset($set['name']) and is_array($set['name'])) {
					if(!empty($set['name'][get_locale()])) $description = $pre.$setNr.": ".$set['name'][get_locale()];				
				} else 			   				   	
					if(!empty($set['name'])) $description = $pre.$setNr.": ".$set['name'];
				$description = "<b>".$description.":</b>&nbsp;&nbsp;";
				if($this->style_inc) {
					$this->options['*set_'.$setNr] = "<span id='set_".$this->option.$setNr.
								"' class='ll-option-icon' onClick = 'showGroupDetails(".$this->option.$setNr.")'/>";
					$this->options['*set_'.$setNr] .= $description;				
					$this->options['_div_'.$setNr] = "<div id='".$this->option.$setNr."' style='padding-left:100px;display:none'>";
					$this->options['set_'.$setNr] = array('title' => 'Werte löschen/kopieren', 
											'type' => 'select', 'options' => $def['del']);
					$this->options['*set_'.$setNr] .= $this->make_suboptionGroup_setting($setNr);
					$this->options['_div/'.$setNr] = "</div>";
				}
				else {
					$description2 = "";
					foreach($this->def_data_set as $key => $params) {
						$default = $this->set_subDefault($key,$set);
						if(($key != 'name') and !empty($params[1]) and !empty($default)) 
							$description2 .= $key." = ".$default.", ";
					}
					$this->options['set_'.$setNr] = array('title' => $description.substr($description2,0,-2), 
											'type' => 'select', 'options' => $def['del']);
				
				} 				
			}
			$def['new'] = apply_filters('ll_tools_newOptionSet_'.$this->option_group,$def['new'],$this->defaults);
			$this->make_suboptionGroup_setting('',array('new' => $def['new']),array('new' => '-'));
		}
	}
	
	public function update_data_sets($value,$oldvalue) {
	
		$debug = false; //
//		$debug = ($this->option == 'sparql_sets');
//		$debug = (isset($value[0]) and ($value[0] == 29));
//		$debug = true;
		ll_crm_debug(array($value,$oldvalue),$debug);
		set_transient(LL_TOOLS_OPTION.$this->option."_bak",$oldvalue,60*60);
		ll_crm_debug(array($value,$oldvalue),'print','update_data_sets');	
		if(isset($value['full'])) {
			unset($value['full']);
			return $value;
		}
		if(isset($value['delete'])) {
			unset($oldvalue[$value['delete']]);
			return $oldvalue;
		}
		if(empty($oldvalue)) {
			if(!is_array($oldvalue)) $oldvalue = array();
			$oldvalue = array('check' => array(0));
		}
		ll_crm_debug(array($value,$oldvalue));
		if(isset($value['save']) or ($this->style_inc and isset($oldvalue['sets']))) {
			if(isset($value['save']))		
				$save = explode(",",$value['save']);
			else 
				$save = array_keys($oldvalue['sets']);
			ll_crm_debug(array($oldvalue,$value,$save,$this->def_default),$debug);

			foreach($save as $setNr) {
				//soll verhindern, dass vorhandene Sets gelöscht werden, wenn beispielsweise ein direktes Update gemacht wird.
				//macht möglicherweise "full" unnötig! => Nein, denn ohne full wird nichts gespeichert!
				if(isset($value["set_".$setNr]) and ($value["set_".$setNr] == "-")) { 
					unset($value["set_".$setNr]);
					$new_set = array(); //Ausleeren.
					foreach($this->def_default as $option => $default) {
						if(is_array($default)) {
							ll_crm_debug($option,$debug);
							foreach($default as $suboption => $sub_default) {
								$new_set[$option][$suboption] = $sub_default;
								if(!empty($value[$option."+".$suboption.$setNr]))
									$new_set[$option][$suboption] = $value[$option."+".$suboption.$setNr];
							}
						} else {
							ll_crm_debug($option,$debug);
							$new_set[$option] = $default;
							if(!empty($value[$option.$setNr])) {
								$new_set[$option] = $value[$option.$setNr];						
							}
							ll_crm_debug(array($option,$new_set));
						}
						unset($value[$option.$setNr]);
					}
					ll_crm_debug($new_set,$debug,$debug);
					if(!isset($oldvalue['sets'][$setNr])) $oldvalue['sets'][$setNr] = array(); //nur für updade und Filter.
					$oldvalue['sets'][$setNr] = 
						apply_filters('ll_tools_modify_'.$this->option_group,$new_set,$this->options,$oldvalue['sets'][$setNr]);
				}
			}
			ll_crm_debug($oldvalue['sets'],$debug,$debug);
		}
		if(!isset($value['save'])) {
			ll_crm_debug($value);
			if(!empty($value))
				foreach($value as $key => $action) {
					if(strpos($key,"set_") === 0) {
						$setNr = substr($key,4);
						ll_crm_debug($setNr);
						if($action == "löschen")  {
							ll_crm_debug(array($setNr,$key));
							if(apply_filters('ll_tools_delete_'.$this->option_group,true,$this->option,$setNr))
								unset($oldvalue['sets'][$setNr]);
						}
						if($action == "ändern") 	 $oldvalue['check'][] = $setNr;
						if($action == "kopieren") $this->add_data_set($oldvalue,$setNr);						
					}
				}	
			}
			if(isset($value['new']) and ($value['new'] != '-')) {
				$this->add_data_set($oldvalue,NULL,$value['new']);
			}
		ll_crm_debug($oldvalue,$debug,$debug);
		return apply_filters('ll_tools_modify_all_'.$this->option_group,$oldvalue,$value);
//		return $oldvalue;
	}

	private function add_data_set(&$oldvalue,$setNr = NULL,$new = NULL) {
		ll_crm_debug(array($oldvalue,$setNr));
		if(!isset($setNr)) {
			$defaults = array();
			foreach($this->def_default as $option => $default) {
				$opt = explode("+",$option);
				if(isset($opt[1]))
					$defaults[$opt[0]][$opt[1]] = $default;	
				else 
					$defaults[$opt[0]] = $default;	
			}
			if(!empty($this->element_group)) 
				$defaults[$this->element_group] = $new;
		} else
			$defaults = $oldvalue['sets'][$setNr];
		$oldvalue['sets'][] = apply_filters('ll_tools_new_'.$this->option_group,$defaults,$this->options,$setNr);
		ll_crm_debug($defaults);
		end($oldvalue['sets']);
		$oldvalue['check'] = key($oldvalue['sets']);
//		$oldvalue['check'] = array(array_key_last($oldvalue['sets'])); //erst ab php 7.3
		ll_crm_debug($oldvalue['sets']);
	}
	
}

?>
