<?php
/* ll-Tools, hier werden die internen Funktionen für Updates gesammelt.
*  - UpdateCheck dass immer dann ausgeführt wird, wenn auch WP auf Updates prüft, damit das Array vollständig ist.
*    Wenn CiviCRM installiert und aktiviert ist, dann wird auch CiviCRM auf updates geprüft
*  - Es wird ein Link auf Livinglines-Settings im Plugin-Verzeichnis eingefügt, wenn das Plugin aktiviert ist.
*  - Die uploads-Verzeichnisstruktur für Livinglines-tools wird wenn notwendig erzeugt (ev. aus ll_crm kopiert) wenn das Plugin aktiviert wird.
*  - Nachdem Aktivieren von CiviCRM wird die aktivierte Version in eine Option geschrieben, damit LivingLInes-Tools auf das richtige Plugin prüft.
*  - PreUpdate-Funktion, die die alte Installation archiviert, dies ist auch für andere Plugins automatisierbar
*  - PostUpdate-Funktion, die die Durchführung von Datenkorrekturen ermöglcht.
*  - Zip-Funktionalität 
*  Stand 21.03.2020
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


############ UpdateHandling ###################

class LL_classes_upgrader {
	protected $upgrader;
	protected $backup_list;
	protected $base_path;
	public $upgrader_skin;
	protected $protFile; //derzeit nur für Checkmodified
	
	private static $_singleton;

	private function __construct($object) {
		if($object !== NULL) {
			$this->upgrader = $object;
			$this->upgrader_skin = $object->skin;			
		}
		$this->backup_list['plugin'] = ll_tools_get_options(LL_TOOLS_OPTION.'plug_bak',true);
		if(!is_array($this->backup_list['plugin'])) $this->backup_list['plugin'] = array();
    	$this->backup_list['theme'] = ll_tools_get_options(LL_TOOLS_OPTION.'updates',true);
		if(!is_array($this->backup_list['theme'])) $this->backup_list['theme'] = array();
		$this->backup_list['plugin'] = array_merge($this->backup_list['plugin'],$this->backup_list['theme']);
    	$this->backup_list['theme'] = ll_tools_get_options(LL_TOOLS_OPTION.'theme_bak',true);
		if(!is_array($this->backup_list['theme'])) $this->backup_list['theme'] = array();
		
		$this->base_path['plugin'] = str_replace(plugin_basename(LL_PLUGIN_FILE),"",LL_PLUGIN_FILE);
		$this->base_path['theme'] = get_theme_root()."/";
		
		$this->current_response = get_site_transient( 'update_plugins' )->response;
	}
	
	private function message($text) {
		if(isset($this->upgrader_skin))
			$this->upgrader_skin->feedback($text);			
	}

	public static function singleton($object = NULL){
		if(!isset(self::$_singleton)) {
			self::$_singleton = new LL_classes_upgrader($object);
			self::$_singleton->message("LL-Upgrader gestartet.");
		}
		return self::$_singleton;
	}

    public static function llpre_download($return = false, $package = NULL, $object = NULL) {
		$Upgrader = self::singleton($object);
		if(isset($object->skin->plugin_info['Name'])) {
			foreach($Upgrader->current_response as $plugin => $value) {
				if($value->package == $package) {
					if($Upgrader->ll_checkElement($plugin,'plugin')) {
						if($plugin == "civicrm/civicrm.php") {
							return ll_update_civicrm::ll_civi_PreDownload(false,array('plugin' => $plugin,"skin" => $Upgrader->upgrader_skin));
						} else {
							return $Upgrader->make_backup($return,dirname($plugin),$Upgrader->upgrader_skin->plugin_info['Version'],
																$Upgrader->base_path['plugin']);
							//Hier werden die Aktionen aufgeführt ...							
						}
					}
					break;
				}
			}
		}
		if(isset($object->skin->theme_info)) {
			if($Upgrader->ll_checkElement($object->skin->theme_info->get_template(),'theme')) {
				$dirname = $args['theme'];
				$version = $Upgrader->upgrader_skin->theme_info->get('Version'); //THEME
				return $Upgrader->make_backup($return,$object->skin->theme_info->get_template(),$Upgrader->upgrader_skin->theme_info->get('Version'),
													$Upgrader->base_path['theme']);
				//Hier werden die Aktionen aufgeführt ...				
			}
		}
//		return new WP_Error('dbexists',"Datenbankduplikat konnte nicht erstellt werden<br>Update wird nicht durchgeführt");
		return $return;
	}

	private function ll_checkElement($element,$type) {
		ll_crm_debug($element);
		return in_array($element,$this->backup_list[$type]);
	}
	
	private function make_backup($bool,$dirname,$version,$base_path) {
		if(!is_dir(wp_upload_dir()['basedir'].'/'.LL_PLUGIN_NAME.'/'.$dirname))
			 mkdir(wp_upload_dir()['basedir'].'/'.LL_PLUGIN_NAME.'/'.$dirname);
		$this->check_modified_path(array('dirname' => $base_path."/".$dirname, 'destination' => $dirname, 'save' => true ));
		if(llToolsZip($base_path.$dirname,wp_upload_dir()['basedir'].'/'.LL_PLUGIN_NAME.'/'.$dirname.'/'.$dirname."-".$version) === false)
			return new WP_Error('dbcopy_fail',"LL-Sicherheitkopie nicht erfolgreich!");
		$this->message("LL-Sicherheitskopie erfolgreich erstellt.");	
		return $bool;			
	}
	
	public static function check_modified($dirname,$destination = "",$save = false) {
		if(!is_dir(ABSPATH.'wp-content/'.$dirname)) return false;
		$Upgrader = self::singleton();
		$args['dirname'] = realpath(ABSPATH.'wp-content/'.$dirname);
		$args['destination'] = (empty($destination)) ? basename($dirname) : $destination;
		$args['save'] = $save;
		return $Upgrader->check_modified_path($args);
	}

	private function check_modified_path($args) {
		$this->protFile = wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs/FileCheck-".current_time('Y-m-d H:i').".txt";
		$this->ll_update_prot(chr(10)."FileCheck: ".$args['dirname']."=>".$args['destination']);
		$dirlist = new RecursiveDirectoryIterator($args['dirname']);
		$args['filelist'] = new RecursiveIteratorIterator($dirlist);
		$args['basetime'] = round(filectime($args['dirname'])/60);
		$args['basedir'] = wp_upload_dir()['basedir'].'/'.LL_PLUGIN_NAME.'/'.$args['destination'];
		do {
			$args['basetime'] = $this->check_modified_files($args);
		} while (!is_bool($args['basetime']));
		return $args['basetime'];
	}
	
	private function check_modified_files($args) {
		extract($args);
		$modified = false;
		foreach ($filelist as $key => $value) {
			if(is_file($key)) {
				$filetime = round(filectime($key)/60);
				if($filetime < $basetime) return $filetime;				
				if(round($filetime - $basetime) != 0) {
					if($save) {
						$modfile = str_replace($dirname,"",$key);
						$paths = dirname($modfile);
						if(!empty($paths)) {
							$paths = explode("/",$paths);
							foreach($paths as $path) {
								if(!is_dir($basedir.$path)) mkdir($basedir.$path);
								$basedir .= $path."/";
							}						
						}
						copy($key,wp_upload_dir()['basedir'].'/'.LL_PLUGIN_NAME.'/'.$destination.str_replace($dirname,"",$modfile));
					}
					$this->ll_update_prot($key);
					$modified = true;
				}
			}
		}
		return $modified;
	}
	

	public static function llPreUpdate($bool,$args,$result = array()) {
		$Upgrader = self::singleton();
		if(isset($args['plugin'])) {
			if($Upgrader->ll_checkElement($args['plugin'],'plugin')) {
				if($args['plugin'] == "civicrm/civicrm.php") {
					//include_once('ll-tools-civi-update.php');
					ll_tools_file_loader("components/Civi","ll-tools-civi-update");
					//CiviCRM wird gesondert gesichert!
					return ll_update_civicrm::ll_civi_PreUpdate($bool);
				}
//				return $Upgrader->make_backup($bool,dirname($args['plugin']),$Upgrader->upgrader_skin->plugin_info['Version'],$Upgrader->base_path['plugin']);			
			}
		} 
		if(isset($args['theme'])) {
			if($Upgrader->ll_checkElement($args['theme'],'theme')) {
//				return $Upgrader->make_backup($bool,$args['theme'],$Upgrader->upgrader_skin->theme_info->get('Version'),$Upgrader->base_path['theme']);
			}
		} 
		return $bool;
	}

	public static function llPostUpdate($bool,$args,$result = array()) {
		$Upgrader = self::singleton();
		if(isset($args['plugin']) and ($args['plugin'] == plugin_basename(LL_PLUGIN_FILE))) {
			$update_list = scandir(dirname(LL_PLUGIN_FILE)."/admin/updates");
			$version_list = array();
			$old_version = "0.0";
			if(isset($Upgrader->upgrader_skin)) $old_version = $Upgrader->upgrader_skin->plugin_info['Version'];
			
			foreach($update_list as $update_file) {
				if(substr($update_file,0,strlen('ll-tools-update-')) == 'll-tools-update-') {
					$version = substr($update_file,strlen('ll-tools-update-'),-4);
					if(version_compare($old_version,$version) < 0) {
						$versionparts = explode(".",$version);
						$Upgrader->message("LL-Tools Update für ".$version." ist vorbereitet.");	
	//					ll_crm_debug($version,'print');							
						$version_list[$versionparts[0]*10000+$versionparts[1]*100+$versionparts[2]] = $version; 
					}
				}
			}
			ksort($version_list);
			ll_crm_debug($version_list);
			$Upgrader->ll_update_sites($version_list);
		}
		if(isset($args['plugin']) and ($args['plugin'] == "civicrm/civicrm.php")) {
//			include_once('ll-tools-civi-update.php');
			ll_tools_file_loader("components/Civi","ll-tools-civi-update");
			$bool = ll_update_civicrm::ll_civi_PostUpdate($bool);
		}
		return $bool;
	}

	private function ll_update_sites($version_list = array()) {
		ll_tools_file_loader("admin","ll-tools-admin-functions");
		if(is_multisite()) {
			$ll_tools_sites = get_sites();
			foreach($ll_tools_sites as $ll_tools_site) {
				switch_to_blog($ll_tools_site->blog_id);
				if(is_plugin_active("livinglines-tools/livinglines-tools.php"))
					if(!add_option('ll_tools_update_list',$version_list))
						update_option('ll_tools_update_list',$version_list);
				restore_current_blog();
			}
			ll_crm_debug($version_list,'print');
			$this->message("LL-Tools Updateliste für Multisite ist erstellt.");
		} else {
			$this->ll_update_site($version_list);
			$this->message("LL-Tools Updates sind durchgeführt.");
		}		
	}

	public static function ll_update_site($version_list) {
		ll_crm_debug($version_list,'print');
		foreach($version_list as $version) {
			ll_crm_debug("Update ".$version." wird gestartet!",'print',false,true);
			if(file_exists(dirname(LL_PLUGIN_FILE)."/admin/updates/ll-tools-update-".$version.".php")) {
				ll_crm_debug(dirname(LL_PLUGIN_FILE)."/admin/updates/ll-tools-update-".$version.".php",'print');
				include_once(dirname(LL_PLUGIN_FILE)."/admin/updates/ll-tools-update-".$version.".php");
			} else
				ll_crm_debug(dirname(LL_PLUGIN_FILE)."/admin/updates/ll-tools-update-".$version.".php -> NICHT GEFUNDEN",'print');
		}
		delete_option('ll_tools_update_list');		
		if(LL_TOOLS_CIVICRM) {
			ll_crm_debug("Cache Variablen gelöscht",'print',false,true);
			ll_del_trans(); //löscht die Cache-Variablen, immer aber nur einmal für alle Updates ..
		}
	}	
	
	public static function llUpgrade_complete($object,$hook_options) {
		$Upgrader = self::singleton();
		ll_crm_debug($hook_options);
		if($hook_options['type'] != 'translation') $Upgrader->message("LL-Upgrader geschlossen.");		
	}

	private function ll_update_prot($text) {
		file_put_contents($this->protFile, print_r($text,true).chr(10), FILE_APPEND | LOCK_EX);
	} 


}


?>
