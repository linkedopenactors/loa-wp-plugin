<?php
/* Enthält die Klasse für die Karte
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


	/**
	 * sparql Klasse zur Datenaufbereitung ll-tools Karten
	 * Singleton 
	 * 
	 * @since
	 *
	 * @param object $maps LL_Map_map 
	 *
	 */

class LL_LOA_Map { 
	/* Übergebene LL_Map_map */
	protected $view_map;

	/* URL-Parameter für Projekt */
	protected $par;
			
	private static $_singleton;
	
	function __construct($map) {
		$this->view_map = $map;

//		parent::__construct(array());
		if(!is_numeric($this->view_map->response['response'])) $this->view_map->response['response'] = 0;	
	}

	/**
	 * Aufruf zum Aufbau der Daten für die Darstellung auf der Karte
	 * Aufbau der Klasse, wenn Daten aufzubauen sind. 
	 * Aufruf der Datenaufbereitung für jedes DataSet.
	 * 
	 * @since
	 *
	 * @param object $map
	 *
	 * Der Aufbau der Daten wird wiederholt, wenn in einer Sequenz Project und Map abgeholt wird.
	 * Wenn Map nach Projekt kommt, muß ohnehin zweimal aufgebaut werden ...
	 */

	
	public static function make_map_data($map) {	
		$par = array();
		
		if(isset($_GET['set_id']))
			$par = explode("_",$_GET['set_id']);

		$map_sets = $map->map_sets;
		foreach($map_sets as $map_key => $set){
			if(isset($par[0]) and ($par[0] != $map_key)) continue;

			//Hier dürfen nur jene map_sets verarbeitet werden, die auch sparql sind.
			if($set['post'] != 'sparql_file') continue;

			//Dies darf nur gemacht werden, wenn es tatsächlich eine DataSet gibt
			//ansonsten gibt es einen Fehler, weil die field_def fehlt und die wird derzeit im _construct der Api verwendet.
			if(!isset($sparql_map)) {
				if(!isset(self::$_singleton))
					self::$_singleton = new LL_LOA_Map($map);
				$sparql_map = self::$_singleton;			
			}
			try{
				$sparql_map->make_CustomFlag_Groups($map_key,$set,$par);				
			}catch(Exception $e) {
				$sparql_map->view_map->response['error'] = new WP_Error( 'll_tools_map', $e->getMessage());
			}
		}
	}

	/**
	 * Auslesen der Daten aus dem XML-File für ein DataSet
	 * 
	 * @since
	 *
	 * @param string $map_key ID des Datasets
	 * @param array $set DataSet
	 */

	private function make_CustomFlag_Groups($map_key,$set,$par = NULL) {
	
		ll_crm_debug($this->view_map->response['response']);
		//Diese Variable muß aus dem Map_set betückt werden.
		$data = LL_LOA_Show::get_data(array('set' => $set['data_set']),'map');		

		ll_crm_debug($data);

		//Diese Bedingung gilt für die Projektseite		
		if(isset($par[1])) $data = array($par[1] => $data[$par[1]]);

//		$i = 1;
		foreach( $data as $key => $contact ) {
			if(!empty($contact['lon']) and !empty($contact['lat'])) {

				$Set[0] = $contact;
				$Set[0]['id'] = $set['data_set']."-".$key;
				$Set[0]['type'] = $map_key; //hier steht normalerweise der tag-flag!
				$Set[0]['point_data'][] =array($contact['lat'],$contact['lon']);
				$Set[0]['group'] = 'main';

				if($this->view_map->output['page'] == 'project') {
					foreach($data_def as $cell => $cell_def) {
						foreach($cell_def as $field => $field_def) {
							if(!empty($field_def['label'])) {
							   if(!isset($Set[0]['data'][$cell]))
									$Set[0]['data'][$cell] = array('type' => $cell, 'title' => $field_def['label'], 'html' => "");
								
								if(!empty($field_def['link_url']))
									$data_set[$field] = 
										"<a target='_blank'	href='".$data_set[$field_def['link_url']]."'>".$data_set[$field]."</a>";
									
								$Set[0]['data'][$cell]['html'] .= "<p id='".$field."'>".$data_set[$field]."</p>";
										
							}
						}
					}
				}
				
				$this->view_map->makePointObjects($Set,'main');

				$this->view_map->response['response']++;
				ll_crm_debug($this->view_map->response['response']);				
			}
/*
			if($i == 300000) {
				ll_crm_debug($contact);
				ll_crm_debug(count_chars($contact['city'],1));
				break;
			}
			$i++;
*/
		}
		ll_crm_debug($this->view_map->response['response']);
	}
		
}
?>
