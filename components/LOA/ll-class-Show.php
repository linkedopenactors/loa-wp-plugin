<?php
/**
 * Liest LOA Daten und
 * Erzeugt/aktualisiert die zugehörige Feldefinition
 * 
 * @since 4.2.7
 *
 * @param array $args 
 */


class LL_LOA_Show extends LL_LOA_Api {
	/*prefix für die Felder */
	protected $field_key;

	/*Nummer des LOA-DataSets */
	public $set;

	/*Werte des LOA-DataSets */
	protected $default_args;

	/* Endpoint URL */
	protected $url;

	function __construct($args) {
		$this->field_key = 'llsparql';				
		$this->set = (empty($args['set'])) ? 0 : $args['set'];
		
		
		$default_args = get_option(LL_TOOLS_OPTION."sparql_sets");
		ll_crm_debug($default_args);
		if(isset($default_args['sets'][$this->set])) {
			$this->default_args = $default_args['sets'][$this->set];
			foreach($this->default_args['namespaces'] as $key => $value) {
				if(!empty($value)) {
					$parts = explode("::",$value);
					$this->default_args['namespaces'][$parts[0]] = $parts[1];
				}					
				unset($this->default_args['namespaces'][$key]);
			}
		} 
		if(empty($args['url']) and empty($this->default_args['url'])) {
			$this->error("Keine Endpoint-Url definiert und Parameter url fehlt");		
		}
		$this->url = (empty($args['url'])) ? $this->default_args['url'] : $args['url'];

	}
	
	public static function get_data($args,$output = 'table') {
		try{
			$showSparql = new LL_LOA_Show($args);
			$result = $showSparql->get_api_data();
			if($output != 'raw')
				$result = LL_classes_rendering::rendering($result,$showSparql->field_key,$output);
		}catch(Exception $e) {
			return $e->getMessage();		
		}
		return $result;
	}

	protected function get_api_data($args = array()) {
		if(isset($args['set'])) $this->set = $args['set'];

		$option_key = $this->field_key."_".get_the_ID()."_".$this->set;
		$result = get_transient(LL_TOOLS_OPTION.$option_key);

		if(!is_object($result) or ($this->default_args['buffer'] == 0) or isset($_GET['sparql_buffer'])) {
		
			$header = LL_classes_rendering::check_field_def($this->field_key);
			if(empty($header)) {
				$this->api_ping($this->url);
				if(empty($args['query'])) {
					$query = get_post_meta(get_the_ID(),$this->field_key,true);
					if(!empty($query))
						$args['query'] = $query;
					else
						$this->error("Kein Query übergeben oder in llsparql (".__FUNCTION__.",".__LINE__.")");	
				}
				ll_crm_debug($args);
		 		$header = $this->make_rendering_def($this->explode_query($args));	 	
			}
			$result = $this->get($header);		
			if(!is_object($result)) return $this->error($result);
			set_transient(LL_TOOLS_OPTION.$option_key,$result,$this->default_args['buffer']*60*60);		
		}
		return $result;
	}

	private function explode_query($args) {
		$args['query'] = str_replace("<br />"," ",$args['query']);			
		$args['query'] = str_replace(array("<br />","<",">","\n"),"",$args['query']);			
		
		$limit = strpos($args['query'],"LIMIT");
		if($limit != 0) {
			$header['limit'] = intval(substr($args['query'],$limit+strlen('LIMIT ')));
			$args['query'] = substr($args['query'],0,$limit);		
		}
		ll_crm_debug($header);
		if(isset($args['limit'])) $header['limit'] = $args['limit'];
		$prefix =substr($args['query'],0,strpos($args['query'],"SELECT"));
		$header['query'] = str_replace($prefix,"",$args['query']);
		if(!empty($prefix)) {
			ll_crm_debug($prefix);
			ll_crm_debug($args['query']);
			$sets = explode("PREFIX",$prefix);
			unset($sets[0]);
			foreach($sets as $set) {
				$set = explode(": ",$set);
				ll_crm_debug(array($sets,$set));
				$header['namespaces'][trim($set[0])] = trim($set[1]);
			}
		}
		//einzelne Namespaces können nicht überschrieben werden!
		if(isset($args['namespaces'])) $header['namespaces'] = $args['namespaces'];
		return $header;
	}

	private function make_rendering_def($header) {
 		$data = $this->get($header,1);
		LL_classes_rendering::make_row_def($this->field_key,$data->getFields(),$header);
		return $header;
	}


    private function get($args, $limit = 0, $timeout = 0, $query = "") {
	  	$args = array_merge($this->default_args,$args);
    	$this->set_vars($args);
      $result = $this->query($limit, $timeout, $query);
      if (!$result) {
          return null;
      }
      return $result->fetchAll();
    }
/*
	protected function error($text) {
	   throw new Exception('<p class="error">'.$text.'</p>');
	}
*/
}

?>
