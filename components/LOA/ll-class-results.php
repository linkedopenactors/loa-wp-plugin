<?php
/*
Dies muß ein eigener File sein, weil hierauf aus den Ergebnissen mit getFields() zugegriffen wird.
Nur als eigener File kann der autoloader die Klasse bei Bedarf laden.
Diese Klasse hat alle Ergebnisse gespeichert und wird als Instanz im Transient abgelegt.
Könnte natürlich auch anders gelöst werden, in dem auch die Felder direkt in ein Array eingelesen werden.
Ist natürlich alles noch genau zu prüfen.

*/
class LL_LOA_results extends \ArrayIterator
{

    protected $fields;

    public function setFields($fields)
    {
        $this->fields = $fields;
    }

    public function getFields()
    {
        return $this->fields;
    }

}

?>
