<?php

// functions that create our options page
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
* Adminseiten für Sparql als Ergänzung zu Livinglines-Tools
Component Name: SPARQL Adapter für Wordpress
Components Group: SPARQL
Components Order: 10
* 
*/


//ACHTUNG, hier muß es noch einen Ausstieg geben, wenn CIVI nicht aktiv ist!
ll_tools_make_menu(array('site_title' => 'WP SPARQL', 'page' => 'sparql'));

add_action('load-ll-tools_page_ll_tools_sparql','ll_tools_sparql_settings_init');
add_action('load-options.php','ll_tools_sparql_settings_init');


function ll_tools_settings_sparql_cb() {
//	ll_crm_debug(get_option(LL_TOOLS_OPTION."sparql_sets"),true);
}


function ll_tools_sparql_settings_init() {
	#### Test
	$default_args = array('group' => 'sparql', 'type' => 'checkbox','title' => 'WP SPARQL');
	$opt_values = array();
	$opt_values['sparql_sets'] = array('title' => "Endpoint-Sets", 'type' => 'text', 'format' => array('size' => 50),
													'options' => LL_classes_optionOptions::get_suboption_set(array('sparql_sets')));
	ll_tools_make_section($default_args,$opt_values);		
}

	/**
	 * Definiert die OptionOptions für Sparql EntdpointSets
	 * Filter aus LL_classes_optionOptions
	 * 
	 * @since
	 *
	 * @param string $typ Gewünschtes Rückgabearray
	 *
	 * Anmerkung: Hier sollte an erster Stelle das Gesamtarray übergeben werden,
	 * damit die Filter hintereinander geschaltet werden können (4.2.5.6)
	 */


add_filter('ll_tools_get_standardSet_sparql_sets','ll_sparql_standard_set',10,2);
function ll_sparql_standard_set($options = array(),$typ = "default") {
	$options = array('default' => array(
		'name' => array("","Name der Verbindung"),
		'url' => array("","Endpoint URL"),
		'*namepsaces' => array("","<b>Namespace:IRI </b>(z.B.: schema::http://schema.org/)"),
		'namespaces' => array("","","multiple" => 'namespaces'),
		'checked' => array("","Test",'type' => 'text', 'disabled' => true),
		'buffer' => array(0,"Zwischenspeicher in Stunden, 0 = ohne, der Speicher kann mit ?sparql_buffer in der URL gelöscht werden.",'type' => 'number','size' => 10),
		'aktiv' => array("","Aktiv",'type' => 'checkbox')		
	), 'definition' => array(
		'detail_header' => "Sparql-Sets ",
		'element_name' => "Set ",
	));
	if($typ == 'all') return $options;
	return($options[$typ]);
}

add_filter('ll_tools_modify_sparql_sets',function($set,$subOptions,$oldvalue){
  try{
  	 ll_crm_debug($set);
		 $sparql = new LL_LOA_Api($set);
		 $set['checked'] = $sparql->api_ping();   
  } catch(Exception $e) {
  	 $set['checked'] = $e->getMessage(); 
	add_settings_error('ll_tools',esc_attr('settings_updated'),$e->getMessage());
  }
  return $set;
},10,5);


################Shortcode-Funktionen

function LL_tools_sparql($args) {
	return LL_LOA_Show::get_data($args);		
}

################Map-Settings

add_action('ll_tools_get_map_data', array('LL_LOA_Map','make_map_data'),10,1);

add_filter('ll_tools_default_option_set_sparql_file',function($data_set){
	$data_set['set_tag'][1] = "Gewünschtes Endpoint-Set auswählen";
	$data_set['set_tag']['options'] = ll_tools_map_sets('sparql');	
	$styles = array('line','line_style+color','point','point_style+icon','point_style+iconClass','point_style+iconColor','line_box'); 
	foreach($styles as $style) unset($data_set[$style]);
	return $data_set;
},10,1);

add_filter('ll_map_optionset_type',function($types) {
	$types = $types + array("SPARQL" => 'sparql_file');
	return $types;
},15,1);

add_filter('ll_tools_modify_option_set',function($set,$defaults,$oldvalue) {
	if($set['post'] == 'sparql_file') {
		$set['data_set'] = str_replace('sparql','',$set['set_tag']);			
	}
	ll_crm_debug($set);
	return $set;
},10,3);

		
?>
