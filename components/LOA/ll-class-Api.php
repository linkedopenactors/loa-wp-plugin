<?php


class LL_LOA_Api {
	protected $url;
	protected $query;
	protected $limit;
	protected $prefixes;
	protected $timeout;
	protected $namespaces;
		
    # capabilities are either true, false or null if not yet tested.

    public function __construct($args) {
    	$this->set_vars($args);
    }

	 protected function set_vars($args) {
	 	foreach(array('url','query','namespaces','limit','timeout') as $par) {
	 		if(!empty($args['url']) and ($args['url'] != $this->url)) $this->$par = "";
	      if(!empty($args[$par])) $this->$par = $args[$par];	 		
	 	}
   	if(empty($this->url)) $this->error("Keine Endpoint-Url definiert (".__FUNCTION__.",".__LINE__.")");	 
	 }

    private function namespaces() {
		$this->prefixes = "";
    	if(is_array($this->namespaces)) {
    		foreach($this->namespaces as $key => $namespace) {
    			if(strpos($namespace,"::") != 0) {
					$parts = explode("::",$namespace);
	            $this->prefixes .= "PREFIX $parts[0]: <$parts[1]>\n";					
				}elseif(is_string($key)) {
	            $this->prefixes .= "PREFIX $key: <$namespace>\n";									
				}elseif(strpos($namespace,"PREFIX") === 0) {
					$this->prefixes .= $namespace;
				}else
					$this->prefixes .= "PREFIX ".$namespace;    		
    		}    	
    	}else
    		$this->prefixes = $this->namespaces;
    }

    protected function query($limit, $timeout, $query, $prefixes = NULL) {
    	$limit = (empty($limit)) ? $this->limit : $limit;
    	$timeout = (empty($timeout)) ? $this->timeout : $timeout;
    	$query = (empty($query)) ? $this->query : $query;
        
	   if(empty($query)) $this->error("Kein query definiert (".__FUNCTION__.",".__LINE__.")");
 		if(!empty($limit)) $query .= " LIMIT ".$limit;
 		$this->namespaces();

    	$prefixes = (isset($prefixes)) ? $prefixes : $this->prefixes;

      $output = $this->dispatchQuery($prefixes.$query,$timeout);

      $parser = new LL_LOA_parseXml($output);
      return new LL_LOA_result($parser->rows, $parser->fields);
    }

    /**
     * @param int $timeout
     * @return bool
     */
    public function api_ping($timeout = 0) {
      $this->query(1,max($timeout,3000),"SELECT * WHERE { ?s ?p ?o }","");
      return "API getestet und OK";
    }

    /**
     *
     * @param string $sparql
     * @param int $timeout Timeout in mileseconds
     * @return string
     * @throws ConnectionException
     * @throws \SparQL\Exception
     */
    public function dispatchQuery($sparql, $timeout = null)
    {
        $url = $this->url . "?query=" . urlencode($sparql);

	     $request_args = array(
		  		'headers' => "Accept: application/xhtml+xml,application/xml",
		  );
		  
		  if(!empty($timeout))
		  		$request_args['timeout'] = $timeout;
			  		
		  $result = wp_remote_request($url,$request_args);
		  if(is_object($result)) $this->error($result->get_error_message()." sparqlAPI (89)");

		  $output = $result['body'];	

        ll_crm_debug($output);
 
        return $output;
    }

    ###################################
    # Endpoint Capability Testing
    ####################################
    # This section is very limited right now. I plan, in time, to
    # caching so it can save results to a cache to save re-doing them
    # and many more capability options (suggestions to cjg@ecs.soton.ac.uk)

    protected $caps = array();
    protected $capsDesc = array(
        "select" => "Basic SELECT",
        "constant_as" => "SELECT (\"foo\" AS ?bar)",
        "math_as" => "SELECT (2+3 AS ?bar)",
        "count" => "SELECT (COUNT(?a) AS ?n) ?b ... GROUP BY ?b",
        "max" => "SELECT (MAX(?a) AS ?n) ?b ... GROUP BY ?b",
        "sample" => "SELECT (SAMPLE(?a) AS ?n) ?b ... GROUP BY ?b",
        "load" => "LOAD <...>",
    );
    protected $capsCache;
    protected $capsAnysubject;

    /**
     * @param $filename
     * @param string $dbaType
     */
    public function capabilityCache($filename, $dbaType = 'db4')
    {
        $handlers = dba_handlers(true);
        if (array_key_exists($dbaType, $handlers)) { // it is possible to save cache?
            $this->capsCache = array($filename, $dbaType);
        }
    }

    public function capabilityCodes()
    {
        return array_keys($this->capsDesc);
    }

    public function capabilityDescription($code)
    {
        return $this->capsDesc[$code];
    }
    # return true if the endpoint supports a capability
    # nb. returns false if connecion isn't authoriased to use the feature, eg LOAD

    /**
     * @param $code
     * @return bool|mixed|null
     * @throws \SparQL\Exception
     */
    public function supports($code)
    {
        if (isset($this->caps[$code])) {
            return $this->caps[$code];
        }
        $wasCached = false;
        if (isset($this->capsCache)) {
            $dbaCache = dba_open($this->capsCache[0], "c", $this->capsCache[1]);

            $cacheTimeoutSeconds = 7 * 24 * 60 * 60;
            $dbKey = $this->endpoint . ";" . $code;
            $dbVal = dba_fetch($dbKey, $dbaCache);
            if ($dbVal !== false) {
                list( $result, $when ) = preg_split('/;/', $dbVal);
                if ($when + $cacheTimeoutSeconds > time()) {
                    return $result;
                }
                $wasCached = true;
            }

            dba_close($dbaCache);
        }

        $result = null;
        
        $data = [
            "select" => function () {
                 return $this->testSelect();
            },
            "constant_as" => function () {
                 return $this->testConstantAs();
            },
            "math_as" => function () {
                 return $this->testMathAs();
            },
            "count" => function () {
                 return $this->testCount();
            },
            "max" => function () {
                 return $this->testMax();
            },
            "load" => function () {
                 return $this->testLoad();
            },
            "sample" => function () {
                 return $this->testSample();
            }
        ];

        if (!isset($data[$code])) {
            throw new Exception("Unknown capability code: '$code'");
        }

        $this->caps[$code] = $data[$code]();
        if (isset($this->capsCache)) {
            $dbaCache = dba_open($this->capsCache[0], "c", $this->capsCache[1]);

            $dbKey = $this->endpoint . ";" . $code;
            $dbVal = $result . ";" . time();
            if ($wasCached) {
                dba_replace($dbKey, $dbVal, $dbaCache);
            } else {
                dba_insert($dbKey, $dbVal, $dbaCache);
            }

            dba_close($dbaCache);
        }
        return $this->caps[$code];
    }

    /**
     * @return mixed
     * @throws \SparQL\ConnectionException
     * @throws \SparQL\Exception
     */
    public function anySubject()
    {
        if (!isset($this->capsAnysubject)) {
            $results = $this->query(
                "SELECT * WHERE { ?s ?p ?o } LIMIT 1"
            );
            if ($results->numRows() > 0) {
                $row = $results->fetchArray();
                $this->capsAnysubject = $row["s"];
            }
        }
        return $this->capsAnysubject;
    }
    # return true if the endpoint supports SELECT

    /**
     * @return bool
     * @throws \SparQL\ConnectionException
     */
    public function testSelect()
    {
        try {
            $this->dispatchQuery("SELECT ?s ?p ?o WHERE { ?s ?p ?o } LIMIT 1");
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    # return true if the endpoint supports AS

    /**
     * @return bool
     * @throws \SparQL\ConnectionException
     */
    public function testMathAs()
    {
        try {
            $this->dispatchQuery("SELECT (1+2 AS ?bar) WHERE { ?s ?p ?o } LIMIT 1");
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    # return true if the endpoint supports AS

    /**
     * @return bool
     * @throws \SparQL\ConnectionException
     */
    public function testConstantAs()
    {
        try {
            $this->dispatchQuery("SELECT (\"foo\" AS ?bar) WHERE { ?s ?p ?o } LIMIT 1");
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    # return true if the endpoint supports SELECT (COUNT(?x) as ?n) ... GROUP BY

    /**
     * @return bool
     * @throws \SparQL\ConnectionException
     */
    public function testCount()
    {
        try {
            # assumes at least one rdf:type predicate
            $subject = $this->anySubject();
            if (!isset($subject)) {
                return false;
            }
            $this->dispatchQuery("SELECT (COUNT(?p) AS ?n) ?o WHERE { <$subject> ?p ?o } GROUP BY ?o");
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @return bool
     * @throws \SparQL\ConnectionException
     */
    public function testMax()
    {
        try {
            $subject = $this->anySubject();
            if (!isset($subject)) {
                return false;
            }
            $this->dispatchQuery("SELECT (MAX(?p) AS ?max) ?o WHERE { <$subject> ?p ?o } GROUP BY ?o");
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @return bool
     * @throws \SparQL\ConnectionException
     */
    public function testSample()
    {
        try {
            $subject = $this->anySubject();
            if (!isset($subject)) {
                return false;
            }
            $this->dispatchQuery("SELECT (SAMPLE(?p) AS ?sam) ?o WHERE { <$subject> ?p ?o } GROUP BY ?o");
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }

    /**
     * @return bool
     * @throws \SparQL\ConnectionException
     */
    public function testLoad()
    {
        try {
            $this->dispatchQuery("LOAD <http://graphite.ecs.soton.ac.uk/sparqllib/examples/loadtest.rdf>");
            return true;
        } catch (Exception $ex) {
            return false;
        }
    }
    
   protected function error($text = "Es ist ein Fehler aufgetreten ...") {
	   throw new Exception('<p class="error">'.$text.'</p>');
	}

    
}
?>
