<?php

// functions that create our options page
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
* Adminseiten für Livinglines-Tools
Component Name: Liste alle Plugin und Themes für ev. Sicherungen vor Upate, internes Updates
Component Multisite: nicht anzeigen Standardcomponente
*/

ll_tools_make_menu(array('site_title' => 'Updates', 'page' => 'upd', 'network' => true));

add_action('load-ll-tools_page_ll_tools_upd','ll_tools_upd_settings_init');
add_action('load-options.php','ll_tools_upd_settings_init');



function ll_tools_settings_upd_cb() {
//	ll_crm_debug(gettype(get_site_option(LL_TOOLS_OPTION.'plug_bak')),true);
//	echo get_theme_root();
//	ll_crm_debug(get_option(LL_TOOLS_OPTION.'upd_server'),true);
//	ll_crm_debug(dirname(get_option(LL_TOOLS_OPTION.'theme_bak')[0]),true);
//	ll_crm_debug(get_plugins(),true);
//	ll_crm_debug(wp_get_theme('hathor')->get('Version'),true);
//	ll_crm_debug(get_option('ll_tools_plug_bak'),true);
//	ll_crm_debug(get_option(LL_TOOLS_OPTION.'check_modified_files'),true);
}


function ll_tools_upd_settings_init() {
	#### Updates
	$opt_values = array();
	$default_args = array('group' => 'upd', 'title' => "Updates");
//	delete_option(LL_TOOLS_OPTION.'check_modified_files');
	$modified = get_site_option(LL_TOOLS_OPTION.'check_modified_files',array());
	$modified_org = $modified;
	$plugins = get_plugins();
	foreach($plugins as $pluginkey => $plugin) {
		if(in_array(dirname($pluginkey),array("civicrm","livinglines-tools"))) {
			$options_upd[$pluginkey] = $plugin['Name']; 			
		} elseif(strpos($pluginkey,"civicrm") === false) {
			$options_bak[$pluginkey] = $plugin['Name'];
			if(!empty($modified['plugins'][dirname($pluginkey)])) $options_bak[$pluginkey] .= " ...........GEÄNDERT?";
			else $modified['plugins'][dirname($pluginkey)] = "";
		}	
	}
	$themes = wp_get_themes();
	foreach($themes as $themekey => $theme){
		$options_theme[$themekey] = $theme->get('Name');
		if(!empty($modified['themes'][$themekey])) $options_theme[$themekey] .= " ...........GEÄNDERT?";
		else $modified['themes'][$themekey] = "";
	}
	ll_crm_debug($options_theme);
	$opt_values['upd_server'] = array('title' => 'Update Server', 'options' => array(
											'server' => array('title' => 'URL für den externen Update-Server','type' => 'text', 'size' => '30'),
											'beta' => 'Beta Version installieren?')); 
	$opt_values['updates'] = array('title' => 'Plugins updaten', 'description' => 'Folgende Plugins werden von extern upgedatet','options' => $options_upd);
	$opt_values['plug_bak']	= array('title' => 'Plugins sichern', 'description' => 'Folgende Plugins sind vor Update zu sichern:','options' => $options_bak);
	$opt_values['theme_bak'] = array('title' => 'Themes sichern', 'description' => 'Folgende Themes sind vor Update zu sichern:','options' => $options_theme);
	$opt_values['check_modified_files'] = array('title' => 'Fileprüfung', 'description' => 'Prüft abweichende Änderungsdati in allen Plugins und Themes','default' => false);
	if($modified != $modified_org) update_option(LL_TOOLS_OPTION.'check_modified_files',$modified);
	ll_tools_make_section($default_args,$opt_values);

}


//Dies ist eine neue Funktion zum Ermitteln von Änderungen in StandardThemes/Plugins

add_filter('pre_update_option_'.LL_TOOLS_OPTION.'check_modified_files', function($value,$oldvalue) {
	if(is_array($value)) return $value;
	if($value === 'on') {
		foreach($oldvalue as $type => $paths)
			foreach($paths as $path => $dummy)		
				$oldvalue[$type][$path] = LL_classes_upgrader::check_modified($type."/".$path);

	}
	ll_crm_debug($oldvalue);
	return $oldvalue;
},10,2);


?>
