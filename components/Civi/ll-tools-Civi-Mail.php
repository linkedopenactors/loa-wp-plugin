<?php

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*
 * Funktionen für die Anzeige Newsletter, die mit CiviCRM versandt wurden
 * sowie Unterstützung des Versandes mit MailPoet
 * 
 * Component Name: AddOns für CiviMail, Newsletterliste ..
 * Components Group: Civi
 * Components Order: 40
 * Components Parent: Civi-Data
 * 
*/

function LL_crm_MailingList($params) {
	if(isset($_REQUEST['email_id'])) {
		return LL_crm_Mailing(array_merge($params,array('id' => $_REQUEST['email_id'])));
	}
	$request['entity'] = 'Mailing';
	$request['params'] = array(
		'return' => "id,name,scheduled_date,subject",
		'is_archived' => true
	);
	$result = LL_Civi_Data::get_request($request);
	ll_crm_debug($result);
	if(!is_array($result)) return $result;

	$result_list = array_reverse($result);
	if (in_array('last',$params)) return LL_crm_Mailing(array_merge($params,array('id' => $result_list[0]['id'])));
	if (!isset($params['permalink'])) 
		$params['permalink'] = get_permalink();

	$LL_crm_out = '<div class="ll_mail_box">';
//	$LL_crm_out .= '<div class="ll_mail_line" id="ll_mail_header">';
//	$LL_crm_out .= "</div>";			

	foreach($result_list as $Newsletter) {
		$permalink = $params['permalink']."?email_id=".$Newsletter['id'];
		$LL_crm_out .= '<div class="ll_mail_line" >';

		$LL_crm_out .= '<div class="ll_mail_line_link">'.'<a href="'.esc_url($permalink).'"> '.esc_html($Newsletter['subject']).'</a></div>';

		$LL_crm_out .= '<div class="ll_mail_line_date">';
		$LL_crm_out .= esc_html(wp_date('d. F Y',strtotime($Newsletter['scheduled_date'])));
		$LL_crm_out .= '</div>';		

		$LL_crm_out .= '</div>';		
	}
	$LL_crm_out .= '</div>';		
	return $LL_crm_out;
}


function LL_crm_Mailing($params) {
    if(isset($_REQUEST['email_id'])) $params['id'] = $_REQUEST['email_id'];
	
    if(!isset($params['id'])) return LL_crm_MailingList();

	$request['entity'] = 'Mailing';
	$request['params'] = array(
		'id' => $params['id']
	);
	$result = LL_Civi_Data::get_request($request);

	$out = "";
	if(isset($params['title'])) {
		$out = "<".$params['title'].">".$result[0]['subject']."</".$params['title'].">";
	}
	return $out.$result[0]['body_html'];
}
	
?>
