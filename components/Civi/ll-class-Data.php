<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

	/**
	 * Basisklasse für den Zugriff auf die CiviCRM-Daten direkt oder über Json.
	 * Singleton 
	 * 
	 * @since
	 *
	 * @param array $args 
	 *
	 */

class LL_Civi_Data {

	/* Enthält das Array mit den Ergebnissen */
	private $ll_result;

	/* array Enthält die Zugriffsdaten je Set (key), leer wenn keine gefunden */
	protected $civi_api;
	
	/* enthält die Abfragedaten */
	protected $request;
	
	/* Enthält die Fehler, leer */
	protected $error;
	
	private static $_singleton;
	
	function __construct($args) {
		$this->set_vars($args);
		$this->set_api_vars();
		$this->error = array();
	}
	
	public static function singleton($args = array()){
		if(!isset(self::$_singleton)) {
			self::$_singleton = new LL_Civi_Data($args);
		} else {
			self::$_singleton->set_vars($args);			
		}
		return self::$_singleton;
	}

 	/**
	 * Setzt die Abfragevariablen
	 * 
	 * @since
	 *
	 * @param array $args
	 *					action, Default get
	 *					options => limit = 100
	 *					return wird zu einem Array
	 *
	 * Basisdaten werden mit $args gemergt, damit würde beispielsweise limit überschrieben.
	 */

	private function set_vars($args) {
		$this->request['action'] = 'get'; 
		$this->request['params'] = array('options' => array('limit' => 100));
		if(isset($args['params']['return']) and (!is_array($args['params']['return'])))
			$args['params']['return'] = explode(",",$args['params']['return']);
		$this->request = array_merge($this->request,$args);	
	}
	
 	/**
	 * Setzt die Variablen für die SChnittstelle
	 * 
	 * @since
	 *
	 * @param 
	 *
	 * Zugriffsdaten werden aus crm_sets ausgelesen. Wenn nicht gesetzt
	 */

	protected function set_api_vars() {
		if(!isset($this->civi_api)) {
			$civi_params = get_option(LL_TOOLS_OPTION.'crm_sets');
			if(!isset($civi_params['sets'])) {
				$this->civi_api = array();
				return;
			}
			ll_crm_debug($civi_params['sets']);
			foreach($civi_params['sets'] as $key => $params) {
			     if($key == 0) {
					$params['key'] = "";						     
			     } else {
					$civi_api['home'] = $_SERVER['SERVER_NAME'];
					$civi_api['url'] = $params['api_server'];
					$civi_api['home'] = parse_url($civi_api['url'], PHP_URL_HOST);
					$civi_api['default'] = array(
					    CURLOPT_HEADER => false,
					    CURLOPT_RETURNTRANSFER => true,
					    CURLOPT_TIMEOUT => 15,
					    CURLOPT_HTTPGET => true
					  );
	//				$params['debug'] = 1;
					unset($params['api_server']);
			     }
				$civi_api['params'] = $params;
				//$civi_api['params']['version'] = 4; //vorläufig, kommt dann aus dem set.			
				$this->civi_api[$key] = $civi_api;		
			}
		}
	}

 	/**
	 * Holt die Zugriffsdaten für ein bestimmtes Set und eine Version
	 * 
	 * @since
	 *
	 * @param int data_source: Datenset
	 *			 int version: Version der CiviCRM_API die verwendet werden soll 
	 *							0 = nicht definiert
	 *                   1 = nicht aktiv
	 *							2 kein gültiger Wert, Versionen sind 3,4
	 *
	 * Es wird die kleinere Version gewählt zwischen abgefragt und definiert.
	 * Die Übergabe der Version dient hauptsächlich dazu, sie für bestimmte Abfragen zu reduzieren.
	 * 
	 */
	
	protected function get_api_vars($data_source,$version) {
		foreach($this->civi_api as $source => $data_set) {
			//wenn version = 0 wird die SChleife ein zweites mal durchlaufen, nachdem datasource gefunden wurde
			//dann ist source größer als datasource 
			//Dies bedeutet, dass statt Set=0 Set=1 verwendet wird, wenn das interne Civi nicht installiert ist.
			if(($source >= $data_source) and ($data_set['params']['version'] > 1)) {
			 	$data_set['params']['version'] = min($version,$data_set['params']['version']);
				return $data_set;			
			} elseif(($source >= $data_source) and ($data_set['params']['version'] == 1))
				return false;
		}		
		return false;	
	}

 	/**
	 * Führt eine Abfrage aus und gibt die Daten zurück.
	 * 
	 * @since
	 *
	 * @param array $args: Abfrageparameter
	 *			 int data_source: Datenset, Default = 0
	 *			 int version: Version der CiviCRM_API die verwendet werden soll, Default = 4
	 *
	 * Wenn die Abfrage einen Fehler ergibt, wird dieser nach $error übernommen.
	 * Wenn es keine Ergebnisdaten gibt, wird ein leeres Array zurückgegeben.
	 *
	 * ab 4.2.6 wird version auch aus $args ermittelt 
	 */
	
	public static function get_request($args, $data_source = 0, $version = 4) {
		if(isset($args['version'])) {
			$version = $args['version'];
			unset($args['version']); //unklar ob notwendig
		}
		if($version < 3) return array(); //nicht installiert oder nicht aktiv
		$civi_Data = self::singleton($args);

		ll_crm_debug($data_source);
		$civi_Data->ll_result = array(); //ausleeren für wiederholten Aufruf!
		$civi_Data->get_data($data_source, $version);

		if(isset($civi_Data->ll_result['result']['error'])) $this->error[] = $civi_Data->ll_result['result']['error'];
		if(!isset($civi_Data->ll_result['result']['values'])) return array();
		return $civi_Data->ll_result['result']['values'];
	}
	
 	/**
	 * Führt Abfragen für alle definierten DatenSets aus
	 * 
	 * @since
	 *
	 * @param array $args: Abfrageparameter (für alle DataSets ident)
	 *
	 * Gibt ein Array mit je einem Subarray für jedes Datenset zurück.
	 * Die Ergebnisse werden nicht gemergt.
	 */
	
	public static function all_request($args) {
		$civi_Data = self::singleton($args);
		$result = array();
		foreach($civi_Data->civi_api as $source => $data_set) {
			ll_crm_debug($civi_Data->civi_api);
			if($data_set['params']['version'] > 1)
				$result[$source] = $civi_Data->get_request($args,$source);
		}
		return $result;
	}
 

 	/**
	 * Prüft auf Fehler und gibt diese zurück, wenn es welche gab.
	 * 
	 * @since
	 *
	 * @param 
	 *
	 */

 	public static function error() {
		if(isset(self::$_singleton) and !empty(self::$_singleton->error))
			return self::$_singleton->error;
		return false; 	
 	}

 	/**
	 * Setzt Fehler und gibt diesen aus (Settings) und macht Protkolleintrag.
	 * 
	 * @since 4.2.11
	 *
	 * @param string $message, Fehlertext
	 *
	 */

 	private function make_error($message,$data = "") {
		$this->error = $message;
	  if(function_exists('add_settings_error'))
			add_settings_error('ll_tools',esc_attr('settings_updated'),"Datenbank: - ".$this->request['entity']." - ".$message);
		if(!empty($data)) $message = array($message,$data);
		ll_crm_debug($message,'print');			
 	}


 	/**
	 * Führt die eigentliche Abfrage aus
	 * 
	 * @since
	 *
	 * @param int $data_source
	 *			 int $version
	 *
	 * 
	 */
 
	protected function get_data($data_source = 0, $version) {
		ll_crm_debug($data_source);
		ll_crm_debug($this->civi_api);
		ll_crm_debug($this->request);
		
		$civi_api = $this->get_api_vars($data_source, $version);
 		if(!$civi_api) {
/*
			$this->ll_result['error'] = 'Set "'.$data_source.'" not defined';
			add_settings_error('ll_tools_civi',esc_attr('settings_updated'),
									$this->request['entity']." - ".$this->ll_result['error']);			
			return;
*/
			$this->make_error('Set "'.$data_source.'" not defined');
			return;
		} 

		$this->ll_result['request'] = $this->request;
		$this->ll_result['set'] = $data_source;
		$civi_api = apply_filters('ll_crm_Civi_api',$civi_api,$data_source);
		ll_crm_debug($civi_api);

		if(empty($civi_api['params']['key'])) {
			if($civi_api['params']['version'] == 4) 
				$this->get_data_db4();
			else 
				$this->get_data_db();
		} else {
			$civi_api['params']['entity'] = $this->request ['entity'];
			$civi_api['params']['action'] = $this->request ['action'];
			$this->get_data_json($civi_api);
		}
		return $this->ll_result;
	} 
	
	protected function get_data_db() {
		if(!function_exists('civicrm_api3')) civicrm_initialize();
		try{
		   $this->ll_result['result'] = civicrm_api3($this->request['entity'], $this->request['action'], $this->request['params']);
		   ll_crm_debug($this->ll_result['result']);
		   //hier kommt bei 'getcount' nur ein numerischer Wert, TAG[IN] funktioniert nicht!
		   //if(($this->request['action'] == 'getcount') and ($result['is_error'] == 0)) $this->result['result'] = $result['result'];			
		}
		catch (CiviCRM_API3_Exception $e) {
			$this->make_error($e->getMessage());
		}
	}		

	protected function get_data_db4() {
		if(!function_exists('civicrm_api4')) civicrm_initialize();
		try{
		   $this->ll_result['result'] = civicrm_api3($this->request['entity'], $this->request['action'], $this->request['params']);
		   ll_crm_debug($this->ll_result['result']);
		   //hier kommt bei 'getcount' nur ein numerischer Wert, TAG[IN] funktioniert nicht!
		   //if(($this->request['action'] == 'getcount') and ($result['is_error'] == 0)) $this->result['result'] = $result['result'];			
		}
		catch (CiviCRM_API4_Exception $e) {
			$this->make_error($e->getMessage());
		}		
		
	}
	
	protected function get_data_json($civi_api) {
		$civi['query'] = "?";
		foreach ($civi_api['params'] as $civi_key => $civi_value) {
			$civi['query'] .= $civi_key."=".$civi_value."&";				
		}
		$civi['query'] .= 'json='.json_encode($this->request['params']);
		$this->ll_json_error('encode',$civi,$this->request['params'],$civi_api['params']);
		if(!isset($civi['handle'])) $civi['handle'] = curl_init();
		$civi['options'] = array(
		    CURLOPT_URL => $civi_api['url'].$civi['query'],
		);
		$civi['options_sum'] = $civi['options'] + $civi_api['default'];
		curl_setopt_array($civi['handle'],$civi['options_sum']);
		  // send request and wait for responce
	 	$this->ll_result['result'] =  json_decode(curl_exec($civi['handle']),true);
	 	ll_crm_debug($this->ll_result['result']);
      if(!empty($this->ll_result['result']['is_error']))
		  $this->make_error("Civi-Error: ".$this->ll_result['result'][1],$civi);      
		if (curl_errno($civi['handle']) > 0) {
		  $info = curl_getinfo($civi['handle']);
		  //ll_crm_debug(curl_error($civi['handle']),true);
//		  ll_crm_debug('TRACE');
		  $this->make_error(curl_error($civi['handle'])." (".curl_errno($civi['handle']).")");
		} else
   		$this->ll_json_error('decode',$civi,$this->request['params'],$civi_api['params']);		
	}
	
	protected function ll_json_error($modus,$civi,$request,$civi_api) {
		if(json_last_error() == JSON_ERROR_NONE) return false;
		ll_crm_debug(curl_exec($civi['handle']),'print');
//		ll_crm_debug(ll_json_error(),true);
		ll_crm_debug(array($modus,json_last_error_msg()),'print',false,true); 

		$this->make_error("Json-".$modus.": ".json_last_error_msg()." -> ".serialize($request));
		ll_crm_debug(array($civi,$request,$civi_api),'print');
		return;
	}
	
}

?>
