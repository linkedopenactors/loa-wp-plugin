<?php
/* Kompatibilitätsfunktionen für den Umstieg von ll-crm 3.x.x auf ll-tools ab 4.0.7
 * mit Verwendung der CiviCRM Basisklasse für den Zugriff auf die CiviCRM-Daten direkt oder über Json.
 * Component Name: Kompatibilitätsfunktionen für 3.x.x
 * Components Group: Civi
 * Components Order: 12
 * Components Parent: Civi-Data
 *
 * Eingeführt mit 4.0.7 
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

//******* Hier können verschiedene Adressen nach Tags abgerufen werden. (Analog LinkList jedoch als Array)
// wird bei bleu zur Darstellung des Vorstandes und der Vereine auf der Homepage verwendet.
/*
function LL_crm_LinkArray($tag, $type = "Desc") {
	
	$ll_crm_contact = new ll_crm_Contact(array(),
										 array('tag' => $tag,'api.CustomValue.get' => array(), 'api.Website.get' => array('website_type_id' => 1))
										 );
	do_action('LL_crm_LinkArray_request',$ll_crm_contact);
	$ll_crm_contact->execute();
	if(!empty($ll_crm_contact->ll_result['error'])) ll_crm_debug($ll_crm_contact->ll_result['error'],true);

	$result = $ll_crm_contact->ll_result['result'];
	if(!is_array($result)) return $result;
	ll_crm_debug($result);
	
	$result_list = $result['values'];
	$LL_crm_out_all = array();
	foreach($result_list as $Contact) {
		$LL_crm_out = $Contact;
		if($Contact['image_URL'] == '') unset($LL_crm_out['image_URL']);
		if(isset($Contact['api.Website.get']['values'][0]['url'])) {
			$LL_crm_out['Website'] = $Contact['api.Website.get']['values'][0]['url'];
		}
		$Sub_Data = $ll_crm_contact->get_CustomFields($Contact['contact_id'],0,'Desc');
		$LL_crm_out['Descrizione'] = '';
		if(!empty($Sub_Data)) {
			$LL_crm_out = array_merge($LL_crm_out, reset($Sub_Data));
			if(isset($LL_crm_out['text'])) $LL_crm_out['Descrizione'] = $LL_crm_out['text'];			
		}
		
		if(isset($Contact['sort_name'])) {
			$LL_crm_out_all[$Contact['sort_name']] = $LL_crm_out;
		} else {
			$LL_crm_out_all[] = $LL_crm_out;			
		}
	}
	return $LL_crm_out_all;
}
*/
function LL_civi_comp_Contact($args = array()) {
	$contacts = LL_civi_Contact($args);
	
	if(isset($args['type'])) {
		foreach($contacts as $key => $contact) {
			$contacts[$key] = array_merge($contact,current($contact[$args['type']]));
			unset($contacts[$key][$args['type']]);
		}
	}
	return $contacts;
}

//******* Hier können verschiedene Adressen nach Tags abgerufen werden. (Nicht für Projekte!)
//wird derzeit noch für das Komittee und die Vereine verwendet -> zumindest für den Aufruf aus der Seite
// wird bei bleu zur Darstellung des Vorstandes und der Vereine auf der Homepage verwendet.

function LL_crm_LinkList($params = array()) {
	$args = array('type' => 'Desc',
						  'params' => array('tag' => $params['tag'], 'api.Website.get' => array('website_type_id' => 1)));
	$result_list = LL_civi_Contact($args);

	$LL_crm_out = "<table class='ll-table'>";
	foreach($result_list as $Contact) {
		$Contact['allegato'] = current($Contact['Desc']);
		ll_crm_debug($Contact['allegato']);
//		$Contact['allegato'] = LL_crm_SubData($Contact,'list','Desc');
//		LL_crm_SubData($Contact, 'list');
		$LL_crm_out .= "<tr><td class='ll-td-img'></td><td class='ll-td-name'></td><td class='ll-td-desc'></td>";
		$LL_crm_out .= "<tr>";
		if($Contact['image_URL'] != '') {
			$LL_crm_out .= "<td><img src='".$Contact['image_URL']."' alt='".$Contact['image_URL']."' /></td>";				
		} else {
			$LL_crm_out .= "<td></td>";
		}
	
		$LL_crm_out .= "<td id='ll-display-name'><img id='ll-img-display-name' src='".$Contact['image_URL']."' alt='".$Contact['image_URL']."' /><br>";							
		if(isset($Contact['link'])) {
			$LL_crm_out .= "<a target='_blank' href='".$Contact['link']."'> ".$Contact['display_name']."</a>";				
		} else {
			$LL_crm_out .= $Contact['display_name'];
		}
		//if($Contact['country']<>'Italia') $LL_crm_out .= "<br>".$Contact['country'];
		$LL_crm_out .= "</td>";
		//Da zur Zeit noch beide Verwendet werden, werden einfach beide verwendet!
		$LL_crm_out .= "<td id='ll-display-desc'>".$Contact['allegato']['html'].$Contact['allegato']['text'];
		$LL_crm_out .= "</td></tr>";												
	}
	$LL_crm_out .= "</table>";
	return $LL_crm_out;
}

//Wird verwendet von OO für Team und Unterstützer, müßte so funktionieren!
function LL_crm_flexList($params = array()) {
	if(!isset($params['text'])) $params['text'] = "Desc";
	if(!isset($params['design'])) $params['design'] = 1;
	ll_crm_debug($params);
	if(isset($params['tag'])) {
		$args = array('type' => $params['text'], 'sort' => 'sort_name',
						  'params' => array('tag' => $params['tag'], 'api.Website.get' => array('website_type_id' => 1)));
		$LL_crm_out = LL_civi_comp_Contact($args);
		ll_crm_debug($LL_crm_out);
		if(!is_array($LL_crm_out)) return $LL_crm_out; 
		return ll_crm_list_rendering($LL_crm_out, $params['design']);
	}
}

?>
