<?php
/*
# Hier können die Daten für die Zusatzfelder zur Darstellung von Informationen zu den Punkten und Linien erstellt und ausgewertet werden.
Component Name: Ergänzt die Felder für LL Anhänge zum Kontakt (nur mit eigenem 	CiviCRM)
Components Group: CIVI
Components Order: 90
Components Parent: Civi-Data
*/

function field_params() {
    return array("group" => array(
        					"title"=> "Anhaenge",
      					"extends"=> "Contact",
      					"style"=> "Tab with table",
      					"collapse_display"=> "0",
      					"weight"=> "3",
      					"is_active"=> "1",
      					"is_multiple"=> "1",
      					"collapse_adv_display"=> "0",
      					"is_reserved"=> "0",
      					"is_public"=> "1",
							"help_post" => '<!-- ll-tools Allegati -->'
							
						),
				 array(
			            "label"=> "Gruppe",
			            "data_type"=> "String",
			            "html_type"=> "Select",
			            "weight"=> "1",
			            "help_post"=> "Die Gruppe bestimmt die Vewendung der Einträge und damit auch die Verwendung der Felder. Vorläufig gibt es folgenden Gruppen:\n- Beschreibung/Foto: werden derzeit ident verwendet. Den Text bitte als formierten Text eintragen, obwohl noch an einigen Stellen auch der andere Text verwendet wird, wenn der formatierte Text leer ist. Sprache, wichtig zur Anzeige der korrekten Texte.\n- Ammappalitalia: Eintragung der Wanderwege. Wanderwege müssen einen eindeutigen Titel und eine Reihenfolge haben. Zwischenpunkte können ev. auch im Text eingetragen sein.".'<!-- ll-tools type,Key -->',
			            "is_active"=> "1",
			            "is_view"=> "0",
			            "text_length"=> "30",
			            "note_columns"=> "60",
			            "note_rows"=> "4",
			            "option_group_id"=> "groups",
			            "in_selector"=> "1"
					),
					array(
			            "label"=> "Sprache",
			            "data_type"=> "String",
			            "html_type"=> "Select",
			            "default_value"=> "*",
			            "weight"=> "7",
			            "help_pre"=> "Beschreibung/Foto",
			            "help_post"=> "Bei Beschreibungen werden hier nur jene Einträge mit \"Alle Sprachen\" oder der jeweils passenden Sprache gewählt. Andere Einträge werden ignoriert. \nBei anderen Gruppen ist die Sprache derzeit bedeutungslos.".'<!-- ll-tools language,Language -->',
			            "is_active"=> "1",
			            "is_view"=> "0",
			            "text_length"=> "5",
			            "note_columns"=> "60",
			            "note_rows"=> "4",
			            "option_group_id"=> "languages",
			            "in_selector"=> "1"										
					),
					array(
		            	"label"=> "Titel",
		            	"data_type"=> "String",
		            	"html_type"=> "Text",
		            	"weight"=> "13",
		            	"help_pre"=> "Title/Name der Linie",
		            	"help_post"=> "Bei Linien ist es wichtig, dass alle Eintragungen in den verschiedenen Kontakten (Punkten) zur selben Linie den exakt gleichen Titel haben. Nur dann kann das System die beiden Punkte miteinander verbinden.".'<!-- ll-tools title -->',
		            	"is_active"=> "1",
		            	"is_view"=> "0",
		            	"text_length"=> "255",
		            	"note_columns"=> "60",
		            	"note_rows"=> "4",
		            	"in_selector"=> "1"
		            ),
					array(
			            "label"=> "Text",
			            "data_type"=> "Memo",
			            "html_type"=> "TextArea",
			            "weight"=> "14",
			            "help_pre"=> "Text (alt), Zwischenpunkte",
			            "help_post"=> "Für die Eingabe von Beschreibungstexten soll in Hinkunft das Feld HTML verwendet werden, da dort auch Formatierungen, Links, Absätze udgl. möglich sind. Dieses Feld ist teilweise noch belegt und wird in Beschreibung/Foto verwendet, wenn HTML leer ist.\n\nFür Linien  können hier Zwischenpunkte im Format [42.652974,11.912299]. Mehrer Punkte durch Komma trennen.".'<!-- ll-tools text -->',
			            "attributes" => "rows=4, cols=60",
			            "is_active"=> "1",
			            "is_view"=> "0",
			            "note_columns"=> "60",
			            "note_rows"=> "4",
			            "in_selector"=> "1"
					),
					array(
			            "label"=> "Link",
			            "data_type"=> "Link",
			            "html_type"=> "Link",
			            "weight"=> "16",
			            "is_active"=> "1",
			            "is_view"=> "0",
			            "text_length"=> "255",
			            "note_columns"=> "60",
			            "note_rows"=> "4",
			            "in_selector"=> "1",
						"help_post" => '<!-- ll-tools link -->'
					),
					array(
			            "name"=> "Allegato",
			            "label"=> "Allegato",
			            "data_type"=> "File",
			            "html_type"=> "File",
			            "weight"=> "17",
			            "is_active"=> "1",
			            "is_view"=> "0",
			            "text_length"=> "255",
			            "note_columns"=> "60",
			            "note_rows"=> "4",
			            "in_selector"=> "1",
						"help_post" => '<!-- ll-tools file -->'
						
					),
					array(
			            "label"=> "HTML",
			            "data_type"=> "Memo",
			            "html_type"=> "RichTextEditor",
			            "weight"=> "15",
			            "is_active"=> "1",
			            "is_view"=> "0",
			            "note_columns"=> "60",
			            "note_rows"=> "4",
			            "in_selector"=> "1",
						"help_post" => '<!-- ll-tools html -->'
					),
					array(
			            "label"=> "Datum",
			            "data_type"=> "Date",
			            "html_type"=> "Select Date",
			            "weight"=> "11",
			            "help_pre"=> "ACHTUNG=> Einträge mit gleichem Datum werden nur dann angezeigt, wenn sie im Feld Reihenfolge unterschiedliche Nummern enthalten.",
						"help_post" => '<!-- ll-tools date -->',
			            "is_active"=> "1",
			            "is_view"=> "0",
			            "text_length"=> "255",
			            "date_format"=> "DD, d MM yy",
			            "note_columns"=> "60",
			            "note_rows"=> "4",
			            "in_selector"=> "1"
					),
					array(
			            "label"=> "Reihenfolge",
			            "data_type"=> "Int",
			            "html_type"=> "Text",
			            "weight"=> "12",
			            "help_pre"=> "Reihenfolge für Beiträge und Linien",
			            "help_post"=> "Wird derzeit nur für die Linien verwendet und ist maßgeblich dafür, in welcher Reihenfolge die einzelnen Punkte miteinander verknüpft werden sollen. Es empfiehlt sich die Nummern zu Beginn als 10, 20, 30 zu vergeben, damit genug Zwischenraum bleibt, falls später weitere Punkte eingefügt werden sollen.".'<!-- ll-tools order -->',
			            "is_active"=> "1",
			            "is_view"=> "0",
			            "text_length"=> "255",
			            "note_columns"=> "60",
			            "note_rows"=> "4",
			            "in_selector"=> "1"
					),
					array(
			            "label"=> "Image",
			            "data_type"=> "String",
			            "html_type"=> "Text",
			            "weight"=> "21",
			            "is_active"=> "1",
			            "is_view"=> "1",
			            "text_length"=> "255",
			            "in_selector"=> "1",
						"help_post" => '<!-- ll-tools image -->'				
					),
				);
} 

function field_options() {
	return array(
		"groups" => array("group" => array(
        								"title"=> "LL_Group",
            							"data_type"=> "String",
            							"is_active"=> "1",
										"is_reserved" => "0"			
									),
									array(
							            "label"=> "Beschreibung",
							            "value"=> "Desc",
							            "name"=> "description",
							            "is_default"=> "0",
							            "weight"=> "1",
							            "is_active"=> "1"										
									),
									array(
							            "label"=> "Foto",
							            "value"=> "Foto",
							            "name"=> "Foto",
							            "is_default"=> "0",
							            "weight"=> "2",
							            "is_active"=> "1"
										
									)
		        	),
		"languages" => array("group" => array(
						            	"title"=> "LL_Language",
						            	"data_type"=> "String",
						            	"is_active"=> "1",
										"is_reserved" => "0"
									),
									array(
							            "label"=> "Deutsch",
							            "value"=> "de",
							            "name"=> "Deutsch",
							            "weight"=> "4",
							            "is_active"=> "1"
										
									),
									array(
							            "label"=> "Alle",
							            "value"=> "*",
							            "name"=> "Alle",
							            "weight"=> "5",
							            "is_active"=> "1"										
									)
					)
	);
}

add_filter('ll_tools_settings_add_opt_value_civi',function($opt_values) {
	$opt_values['add_fieldgroup'] = array('title' => 'CiviCRM Add Fieldgroups',
					'description' => 'Es wirde eine Custom-Field-Gruppe Anhänge mit allen zugehören Feldern und Optionen erzeugt.'.
					'Die Funktion kann mehrfach aufgerufen werden und ergänzt dann fehlende Elemente.');
	return $opt_values;
});


add_filter('pre_update_option_'.LL_TOOLS_OPTION.'add_fieldgroup', function($value,$oldvalue) {
	if($value) {
		$field_params = field_params();
		$params = $field_params['group'];
		unset($field_params['group']);

		$result = LL_Civi_Data::get_request(array('entity' => 'CustomGroup', 'action' => 'get', 'params' => array("name" => $params['name'])));
		if(empty($result)) {
			LL_Civi_Data::get_request(array('entity' => 'CustomGroup', 'action' => 'create', 'params' => $params));
			LL_Civi_Data::get_request(array('entity' => 'CustomGroup', 'action' => 'get', 'params' => array("title" => $params['title'])));
		}
		ll_crm_debug($result);
		$custom_group_id = current($result)['id'];
		foreach($field_params as $params) {
			$params["custom_group_id"] = $custom_group_id;
			if(!empty($params["option_group_id"])) 
				$params["option_group_id"] = ll_tools_make_options($params["option_group_id"]);
			ll_crm_debug(array($custom_group_id,$field_params));
			$result = LL_Civi_Data::get_request(array('entity' => 'CustomField', 'action' => 'get', 
										  'params' => array("custom_group_id" => $custom_group_id, "label" => $params['label'])));
			if(empty($result)) {
				LL_Civi_Data::get_request(array('entity' => 'CustomField', 'action' => 'create', 'params' => $params));
			}
			ll_crm_debug($result);			
		}
	}
	return $oldvalue;
},10,2);


function ll_tools_make_options($group) {
	$option_params = field_options()[$group];
	ll_crm_debug(array($option_params,$group));
	$result = LL_Civi_Data::get_request(array('entity' => 'OptionGroup', 'action' => 'get', 'params' => array("title" => $option_params['group']['title'])));
	if(empty($result)) {
		LL_Civi_Data::get_request(array('entity' => 'OptionGroup', 'action' => 'create', 'params' => $option_params['group']));
		$result = LL_Civi_Data::get_request(array('entity' => 'OptionGroup', 'action' => 'get', 'params' => array("title" => $option_params['group']['title'])));
		$option_group_id = current($result)['id'];
		unset($option_params['group']);
		foreach($option_params as $params) {
			$params['option_group_id'] = $option_group_id;
			LL_Civi_Data::get_request(array('entity' => 'OptionValue', 'action' => 'create', 'params' => $params));
		}		
	} else {
		$option_group_id = current($result)['id'];		
	}
	return $option_group_id;
}




?>
