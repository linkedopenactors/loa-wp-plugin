<?php

// functions that create our options page
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*
* Spamfilter für CiviCRM Formulare als Ergänzung zu Livinglines-Tools
* Component Name: Spamfilter für Civi-Formulare
* Components Group: Civi
* Components Order: 20
* Components Parent: Civi-Data
* 
*/

add_action('load-ll-tools_page_ll_tools_civi','ll_tools_civiSpam_settings_init');
add_action('load-options.php','ll_tools_civiSpam_settings_init',10,0);

//Keine eigene Menuseite, daher nicht aktiviert.
//function menu_options(array('site_title' => 'Komponenten','group' => 'plug'));

function ll_tools_settings_civiSpam_cb() {
//	$spamFilters = get_option(LL_TOOLS_OPTION.'Spam-Conditions');
//	ll_crm_debug(explode(chr(10),$spamFilters['email-Primary']),true);
//	echo $prot_file;
	if(file_exists(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs/crm_forms_spam.txt"))
		echo "<a href='".wp_upload_dir()['baseurl']."/".LL_PLUGIN_NAME."/logs/crm_forms_spam.txt' target=_blank>Protokoll der abgewiesenen Nachrichten</a>";
}

function ll_tools_civiSpam_settings_init() {
	$default_args = array('group' => 'civiSpam', 'page' => 'civi', 'type' => 'checkbox','title' => 'Spam Filter');
	$opt_values = array();
	$opt_values = array('Spam-Conditions' => array('title' => "Filterbedingungen", 'options' => array(
						'name' => 'Vorname ident mit Nachname',
						'note' => 'http(s):// in den Bemerkungen',
						'email-Primary' => array('title' => 'Angeführte Maildomains, ein Eintrag je Zeile', 'type' => 'textarea')
	)));
	ll_tools_make_section($default_args,$opt_values);
}


################ Spam-Schutz für Formulare #########
add_filter( 'civicrm_validateForm', function($formName, &$fields, &$files, &$form, &$errors) {
	ll_crm_debug(array($formName, $fields, $files, $form, $errors));
	if($formName == "CRM_Profile_Form_Edit") {
		$spamFilters = get_option(LL_TOOLS_OPTION.'Spam-Conditions');
//		ll_crm_debug($spamFilters,true,true);
		if(is_array($spamFilters)) {
			if(isset($spamFilters['name']) and isset($fields['first_name']) and isset($fields['last_name']) and
			  ($fields['first_name'] == $fields['last_name']))
				$errors['email-Primary'] =ts('Dieser Eintrag scheint von einem Robot zu sein');
			if(isset($spamFilters['note']) and isset($fields['note']) and (preg_match(';http(s?)://;',$fields['note']) == 1)) {
				$errors['note'] =ts('Dieser Eintrag scheint von einem Robot zu sein');
			}
			if(isset($spamFilters['email-Primary']) and 
				ll_tools_civiSpam_CheckMailDomain($fields['email-Primary'],$spamFilters['email-Primary']))		
				$errors['email-Primary'] =ts('Dieser Eintrag scheint von einem Robot zu sein');
		}
	}
//	ll_crm_debug(array($fields,$errors,count($errors)),(count($errors) > 0));
	if(!empty($errors))
		ll_crm_debug(array($fields,$errors),'print','crm_forms_spam.txt');
	return $formName;
}, 10, 5 );

function ll_tools_civiSpam_CheckMailDomain($email,$MailFilter) {
	$MailDomain = substr($email, strpos($email,"@")+1);
	$MailFilter = explode(chr(10),str_replace(chr(13),"",$MailFilter));
//	ll_crm_debug(array($MailDomain,$MailFilter),true);
	if(in_array($MailDomain,$MailFilter)) return true;
	return false;	
}

?>
