<?php
/* Standardfunktionen in Verbindung mit CiviCRM
 * wird derzeit von ll-tools-Civi-Data geladen
 * 
 * since 4.2.6 unter diesem Namen
*/

########################### Map-Settings und Functions

	/**
	 * Filter zum Aufruf der Klasse zur Aufbereitung von Kartendaten aus Civi
	 * wird aktiviert in LL_Map_map::get_map
	 * 
	 * @since
	 *
	 * @param
	 */

add_action('ll_tools_get_map_data', array('LL_Civi_Map','make_map_data'),10,1);


	/**
	 * Filter zur von DataSets in den MapOptions für CiviCRM
	 * wird aktiviert in LL_Map_map::get_map
	 * Funktion wird auch in civi/ll_class_map verwendet, um die relevanten tags zu filtern.
	 * 
	 * @since
	 *
	 * @param array $types DataSet-Typen 
	 */


add_filter('ll_map_optionset_type','ll_civi_map_optionset_types',10,1);
function ll_civi_map_optionset_types($types = array()) {
	$types = $types + array('Tag' => 'set_tag','Kontakt' => 'set_contact_type');	
	return $types;
}

	/**
	 * Filter zur Erweiterung der New-Options in OptionOption
	 * 
	 * @since 4.2.11
	 *
	 * @param array $new Options für neues Elemente
	 * @param array $defaults Alle Default-Elemente
	 */


add_filter('ll_tools_newOptionSet_option_set',function($new) {
	$sets = ll_get_crm_sets();
	if(count($sets) > 1) {
		$types = ll_civi_map_optionset_types();
		foreach($types as $name => $type) {
			unset($new['options'][$name]);
			foreach($sets as $set => $dummy)
				$new['options'][$name." (".$set.")"] = $type."-".$set;
		}		
	}
	ll_crm_debug(array($new));
	return($new);
},10,1);


	/**
	 * Filter konvertiert die New-Options mit crm-set in korrekte Posts
	 * 
	 * @since 4.2.11
	 *
	 * @param array $defaults Werte des neuen Elementes
	 */


add_filter('ll_tools_new_option_set',function($defaults) {
	$sets = ll_get_crm_sets();
	$par = explode("-",$defaults['post']);
	if(in_array($par[0],ll_civi_map_optionset_types())) {
		if(isset($par[1])) $defaults['data_set'] = $par[1];
		else $defaults['data_set'] = key($sets);
		$defaults['post'] = $par[0];
	}
	return $defaults;
},10,1);


	/**
	 * Filter für die Nachbearbeitung von OptionSets nach der Änderung
	 * 
	 * @since 4.2.11 hierher übersiedelt
	 *
	 * @param array $set Werte des geänderten DataSsets
	 * @param array $defaults Vorgabewerte des DataSsets
	 * @param array $oldvalue alten Werte des DataSsets
	 
	 */


add_filter('ll_tools_modify_option_set',function($set,$defaults,$oldvalue) {
	if(!in_array($set['post'],ll_civi_map_optionset_types())) return $set;
	$tag = explode("_",$set['set_tag']);
	$set['data_set'] = $tag[0];
	$set['tag'] = $tag[1];
	if($set['post'] == 'set_tag') {
		$set['tagname'] = ll_map_make_tagOptions($set['data_set'],$set['set_tag']);		
	} else {
		$set['tagname'] = ll_map_make_ContactTypeOptions($set['data_set'],$set['set_tag']);				
	}
	ll_crm_debug($set);
	ll_tools_modify_option_set_substyles($set,'line');
	ll_tools_modify_option_set_substyles($set,'point');
	ll_crm_debug($set);
	return $set;
},10,3);







	/**
	 * Filter zur Anpassung von Werte im StandardSubSet für MapOptions
	 * DatSet set_contact_typ
	 * Filter: ll_tools_default_option_set_*
	 * 
	 * @since
	 * 4.2.11 Filter neu ll_tools_default_data_option_set_* + weiterer Parameter mit den Daten
	 *
	 * @param array $data_set Default-DataSet für den SubTyp
	 */


add_filter('ll_tools_default_data_option_set_set_contact_type',function($data_set,$data){
//	ll_crm_debug(array($data,$data_set),($data['data_set'] == NULL));
	$data_set['set_tag'][1] = "Den gewünschten Kontakttyp auswählen";
	$data_set['set_tag']['options'] = ll_map_make_ContactTypeOptions($data['data_set']);
	$subSets = array("-" => "") + ll_map_make_subSetTypes($data['data_set']); 
	$data_set['line']['options'] = $subSets;
	$data_set['point']['options'] = $subSets;
	return $data_set;
},10,2);

	/**
	 * Filter zur Anpassung von Werte im StandardSubSet für MapOptions
	 * DatSet set_tag
	 * Filter: ll_tools_default_option_set_*
	 * @since
	 * 4.2.11 Filter neu ll_tools_default_data_option_set_* + weiterer Parameter mit den Daten
	 *
	 * @param array $data_set Default-DataSet für den SubTyp
	 */


add_filter('ll_tools_default_data_option_set_set_tag',function($data_set,$data){
	$data_set['set_tag'][1] = "Den gewünschten Tag auswählen";
	$data_set['set_tag']['options'] = ll_map_make_tagOptions($data['data_set']);
	$subSets = array("-" => "") + ll_map_make_subSetTypes($data['data_set']); 
	$data_set['line']['options'] = $subSets;
	$data_set['point']['options'] = $subSets;
	return $data_set;
},10,2);


/**
 * Holt die crm_sets aus get_option und behält sie als static, damit sie nicht ständig neu gelesen werden müssen.
 * Gibt die $set['sets'] zurück, oder ein leeres Array.
 *
 * @since 4.2.11
 *
 */


function ll_get_crm_sets() {
	static $sets;
	if(!isset($sets)) {
		$sets = get_option(LL_TOOLS_OPTION."crm_sets",array());
		if(isset($sets['sets'])) $sets = $sets['sets'];
		else $sets = array();	
	}
	return $sets;
}


	/**
	 * Ermittelt die Liste der Tags aus allen DataSets für die SelectOptions in den MapOptions
	 *
	 * @since
	 *
	 * @param string $set_tag Falls nur der Name eines Tags gesucht wird
	 */

function ll_map_make_tagOptions($data_set,$set_tag=NULL) {
	static $mapTags;
	if(!isset($mapTags)) {
		$mapTags = get_transient(LL_TOOLS_OPTION."tags");
		if(empty($mapTags)) {
			$mapTags = array();		
			$allMapTags = LL_Civi_Data::all_request(array('entity' => 'Tag',
						 'params' => array('is_selectable' => 1, 'is_tagset' => 0, 'used_for' => 'civicrm_contact')));
			foreach($allMapTags as $data_source => $tags) {
				foreach($tags as $tag) {
					$mapTags[$data_source][$tag['name']." (".$tag['id'].")"] = $data_source."_".$tag['id'];
				}				
			}	
			set_transient(LL_TOOLS_OPTION."tags",$mapTags,60*60);
		}	
	}
	ll_crm_debug($mapTags);
	if(!empty($set_tag)) return array_search($set_tag,$mapTags[$data_set]);
	return $mapTags[$data_set];
}

	/**
	 * Ermittelt die Liste der Contact_Types aus allen DataSets für die SelectOptions in den MapOptions
	 *
	 * @since
	 * 4.2.11 ContactTypes werden in Abhängigkeit vom Set zurückgegeben
	 *
	 * @param string $set data_source, das gewünscht wird. Neu ab 4.2.11
	 * @param string $set_tag Falls nur der Name eines contact_types gesucht wird
	 */


function ll_map_make_ContactTypeOptions($data_set, $set_tag=NULL) {
//	if($data_set == NULL) $data_set = 0;
	static $mapContactTypes;
	if(!isset($mapContactTypes)) {
		$mapContactTypes = get_transient(LL_TOOLS_OPTION."contactTypes");
		if(empty($mapContactTypes)) {
			$mapContactTypes = array();		
			$allMapContactTypes = LL_Civi_Data::all_request(array('entity' => 'ContactType',
						 'params' => array('is_activ' => 1)));
			foreach($allMapContactTypes as $data_source => $ContactTypes) {
				if(!empty($ContactTypes)) {
					$mapContactTypes[$data_source]["All"] = $data_source;
					foreach($ContactTypes as $ContactType) {
						$mapContactTypes[$data_source][$ContactType['name']." (".$ContactType['id'].")"] 
							= $data_source."_".$ContactType['id'];
					}				
				}
			}	
			set_transient(LL_TOOLS_OPTION."contactTypes",$mapContactTypes,60*60);
		}	
	} 
//	ll_crm_debug($mapContactTypes,true);
//	ll_crm_debug('TRACE',($data_set == NULL));
	if(!empty($set_tag)) return array_search($set_tag,$mapContactTypes[$data_set]);
	return $mapContactTypes[$data_set];
}

	/**
	 * Ermittelt die Liste der möglichen SubTypen (Options) die für Linien und SubPoints in Frage kommen.
	 * Wenn $pset und $pvalue übergeben werden, wird der Name der Option in der korrekten Sprache zurückgegeben.
	 *
	 * @since
	 *
	 * @param string $pset = DataSource
	 * @param string $pvalue = OptionName
	 */


function ll_map_make_subSetTypes($pset, $pvalue = NULL) {
	static $subSets;
	if(!isset($subSets)) {
		$subSets = get_transient(LL_TOOLS_OPTION."subSets");
		if(empty($subSets) or empty($subSets[get_locale()])) {
			if(empty($subSets)) $subSets = array();
			$sets = ll_get_crm_sets();
//			if(isset($sets['sets'])) {
//				foreach($sets['sets'] as $set => $dummy) {
				foreach($sets as $set => $dummy) {
					$fields = get_option(LL_TOOLS_OPTION."field_def_".$set,array());
					if(!empty($fields) and isset($fields['keys_options']))
						foreach($fields['keys_options'] as $field => $dummy) {
							$subSet = LL_Civi_Data::get_request(array('entity' => 'OptionValue',
							 'params' => array('option_group_id' => $field)),$set);
							foreach($subSet as $params) {
//								$subSets['sets'][$set."-".$field][$params['name']] = $params['value'];						
								$subSets[$set][$params['name']] = $params['value'];						
								$subSets[get_locale()][$set][$params['value']] = $params['label'];						
							}						
							ll_crm_debug($subSet);
						}
				}
				set_transient(LL_TOOLS_OPTION."subSets",$subSets,7*24*60*60);		
//			}
		}	
	}
	ll_crm_debug($subSets);
	if(!isset($subSets[$pset])) return array();
	if($pvalue === NULL) return $subSets[$pset];
	return $subSets[get_locale()][$pset][$pvalue];
}


################ Funktionen für CiviCRM

/**
 * Merge von Initialisierungs/StandardRequest und übergebenem Request
 *
 * @since 4.0.7, überarbeitet 4.2.6, 4.2.11 übersiedelt von Contact-Klasse nach hier
 *
 * @param array $request = StandardRequest, Initialisierungsrequest
 *        array $args = übergebener Request, ev für ein Set
 */

function ll_civi_merge_request($request,$args) {
	if(!empty($args['request'])) 
		$request = array_merge($request,$args['request']);

	if(!empty($args['params'])) 
		$request['params'] = array_merge($request['params'],$args['params']);

	if(!empty($args['return'])) 
		$request['params']['return'] = array_merge($request['params']['return'],$args['return']);	
		
	if(!empty($args['version']))
		$request['version'] = $args['version'];	
		
	return $request;
}


/**
 * Gibt Daten aus dem Contact als Array zurück
 * Diese Funktion liest jedesmal neue Kontaktdaten aus!
 *
 * @since 4.2.6
 *
 * @param array $args
 *					'params', 'return', 'request' als Abfrageparemter
 *					'data_source' Datenquelle, default = 0
 *					'type' (string,array) FieldGroups die aus den indiv. Feldern geholt weden sollen.
 */


function LL_civi_Contact($args = array()) {
	if(!isset($args['data_source'])) $args['data_source'] = 0;
	if(isset($args['type']) and !is_array($args['type'])) $args['type'] = explode(",",$args['type']);
	if(!isset($args['sort'])) $args['sort'] = 'id';

	$contacts = LL_Civi_Contact::get_data($args,true);

	foreach($contacts as $contact) {
		$key = $contact[$args['sort']];
		$output[$key] = $contact;
	
		if(isset($args['type'])) {
			foreach($args['type'] as $type)
				$output[$key][$type] = LL_Civi_Contact::get_CustomFields($contact['id'],$args['data_source'],$type);
		}
		$output[$key]['link'] = "";
		if(isset($Contact['api.Website.get']['values'][0]['url']))
			$output[$key]['link'] = $contact['api.Website.get']['values'][0]['url'];
		unset($output[$key]['api.Website.get']);
	}
	return $output;
}

	/**
	 * Filter zur Anpassung von Abfragewerte für die Kartendaten bei verschiedenen Ausgabeformen.
	 * Da der Select nur einmal ausgeführt wird, wenn die Daten in einer Ansicht mehrfach verwendet werden (Projekt mit Karte)
	 * muß sich der output auf die Abfrage beziehen, die das Objekt erzeugt.
	 * 
	 * @since 4.2.6
	 *
	 * @param array $args Array mit den Abfrageparametern, sollte grundsätzlich leer sein?
	 *			 string $output 'map', 'list', 'project', Ausgabe mit der das Object initialisiert wird.
	 */


add_filter('ll_civi_map_request',function($args,$output) {
	ll_crm_debug($output);
	ll_crm_debug($args);
	if($output['element'] == 'project') {
		$args['params'] = array('api.Website.get' => array('website_type_id' => 1));
		//Dieser Filter könnte wohl ersetzt werden?
		$args['params'] = apply_filters('LL_crm_Project_request',$args['params']);
		$args['return'] = array("postal_code");

	}
	return $args;
},10,2);

#### Löscht die Zwischenspeicher, wird aus admin aber auch aus dem Widget aufgerufen.

function ll_del_trans($value = true,$oldvalue = false) {
	if($value) {
		foreach(array('tags','subSets','contactTypes') as $transient) {
			if(get_transient(LL_TOOLS_OPTION.$transient)) delete_transient(LL_TOOLS_OPTION.$transient);		
		}
	}
	return $oldvalue;
}

?>
