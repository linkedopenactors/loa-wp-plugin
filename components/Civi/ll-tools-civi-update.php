<?php
/* Führt ein Update für civicrm durch, ist Bestandteil des Plugins Livinglines-tools
*  Die Updateprüfung ist in LivingLines-Tools integriert, die Version um am Server livinglines.at hochgeladen sein
*  Im Rahmen des Updates wird eine Datenbankkopie angelegt, das Programm-Verzeichnis kopiert, Teile des Verzeichnisses im uploads kopiert,
*  die File im custom_php mit der Versionsnummer gekennzeichnet und als .bak gekennzeichnet 
*  und die aktuellen Files aus der neuen Installation zum Abgleich geladen (nach dem Update)
*  Damit die alte Version lauffähig ist, wenn sie aktiviert wird, wird ein modifiziertes civicrm.settings.php ins Programmverzeichnis kopiert
*  Beim Aktivieren des Plugins wird der Pluginname in Option geschrieben, damit LivingLines-Tools auf die richtige Version prüft.
*  Stand 21.02.2020
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
define('LL_TOOLS_CIVI_NAME','civicrm/civicrm.php');

//Erweiterte Löschfunktion für die Verzeichnisse in uploads
add_action('pre_uninstall_plugin',function($plugin){
	if(substr(dirname(plugin_basename($plugin)),0,7) == "civicrm") {
		define('LL_UNINSTALL_PLUGIN',dirname(plugin_basename($plugin)));
		require_once(dirname(LL_PLUGIN_FILE).'/uninstall.php');
		ll_uninstall_sites(LL_UNINSTALL_PLUGIN);		
	}
});


add_filter('pre_update_option_'.LL_TOOLS_OPTION.'civi_update_test',function($value,$oldvalue){
	if($value[0] != 'ND') {
		set_time_limit(30);
		ll_update_civicrm::ll_civi_PreDownload(false,array('test' => true, 'dbtest' => ($value[0] == 'NP')));
		ll_update_civicrm::ll_civi_PreUpdate(false);
		ll_update_civicrm::ll_civi_PostUpdate(false);					
	} 
	return array('ND');
},10,2);

class ll_update_civicrm {
	protected $old_version;
	protected $db;
	protected $db_org;
	protected $db_old;
	protected $log_file;
	protected $log_mod;
	protected $plugin;
	protected $plugin_dir;
	protected $settingsFile;
	protected $error;
	protected $update;
	protected $civi_db_copy;
	protected $skin;
	protected $go;
	
	private static $_singleton;
		
	function __construct($args = array()) {
		$this->log_mod = 'single';
		$this->test = false;
		$this->dbtest = false;
		$this->update = false;
		$this->go = true; 

		$this->settingsFile = "civicrm.settings.php";
		$this->plugin = LL_TOOLS_CIVI_NAME;
		$this->plugin_dir = dirname(LL_TOOLS_CIVI_NAME);
		$this->old_version = get_plugin_data(WP_PLUGIN_DIR."/".LL_TOOLS_CIVI_NAME)['Version'];
		$this->log_file = ABSPATH.'wp-content/uploads/civi_update_'.$this->old_version.'.txt';
		$this->civi_db_copy = get_option(LL_TOOLS_OPTION."civi_db_copy");

		$this->set_vars($args);
		$this->ll_update_log("Objekt initialisiert, Testmodus: ".$this->test.", ".$this->dbtest);
	}

	private function set_vars($args) {
		if(empty($args)) return;
		if(!empty($args['skin'])) $this->skin = $args['skin'];
		if(!empty($args['log_mod'])) $this->log_mod = $args['log_mod'];
		if(isset($args['test'])) {
			$this->test = $args['test'];
			$this->dbtest = $this->test; 
		} 
		if(isset($args['dbtest'])) $this->dbtest = $args['dbtest'];
		if(isset($args['update'])) $this->update = $args['update'];
		if(file_exists($this->log_file) and $this->update) unlink($this->log_file);
		$this->ll_update_log("Objekt aktualisiert, Testmodus: ".$this->test.", ".$this->dbtest);
//		ll_crm_debug($args,'print');
	}
	
	public static function singleton($args = array()){
		if(!isset(self::$_singleton)) {
			self::$_singleton = new ll_update_civicrm($args);
		} else {
			self::$_singleton->set_vars($args);			
		}
		return self::$_singleton;
	}

	public static function ll_fieldtype_conversion($html,$modify = false) {
		$civiUpgrader = self::singleton();
		
		if(!class_exists('CRM_Utils_Check_Component_Timestamps')) return $html;
		if(!method_exists('CRM_Utils_Check_Component_Timestamps','getConvertedTimestamps')) return $html;
		$fields = CRM_Utils_Check_Component_Timestamps::getConvertedTimestamps();
		ll_crm_debug($fields);
		$civiUpgrader->ll_credentials();
		if($modify) {
			if(!$civiUpgrader->ll_civi_duplicateDb()) return false;
		}
		$db_change = $civiUpgrader->ll_db_connect($civiUpgrader->db_org);
		if(empty($db_change)) return false;			
		foreach($fields as $field) {
			$table = $civiUpgrader->ll_db_conn_exec($db_change,"DESC ".$field['table'],PDO::FETCH_ASSOC);
			if(!isset($field['comment'])) $field['comment'] = '';
			//ll_crm_debug(array($table,$field));
			foreach($table as $column) {
				if(empty($field['comment']) and isset($column['comment'])) $field['comment'] = $column['comment'];
				if(($column['Field'] == $field['column']) and $column['Type'] != 'timestamp') {
					$html .= "<tr><td>".$field['table']."</td><td>".$column['Field']."</td><td>".$column['Type']."</td><td>".$field['comment']."</td></tr>";
					if($modify) {
						if(isset($field['default'])) {
							$mod = " TIMESTAMP NOT NULL DEFAULT ".$field['default']." COMMENT '";
						} else {
							$mod = " TIMESTAMP NULL DEFAULT NULL COMMENT '";
						}
						$civiUpgrader->ll_db_conn_exec($db_change,"ALTER TABLE ".$field['table']." MODIFY ".$column['Field'].$mod.$field['comment']."'");
					}
				}
			}
		}
		if(!empty($html)) {
			$html = "<table class = 'll_table'><tr><th>Tabelle</th><th>Feld</th><th>Datentyp</th><th>Kommentar</th></tr>".$html."</table>";
		}
		return $html;
	}

	public static function ll_civi_PreDownload($bool,$args) {
		$args['update'] = 'preD';
		$args['log_mod'] = 'sum';
		//	$args['log_mod'] = 'single'; //TEST
		$ll_civi_update = self::singleton($args);
		$bool = $ll_civi_update->ll_civi_sites('preD',$bool);
		//Hier wird ev. ein Error-Objekt zurückgeben!
		if(is_object($bool)) {
			$ll_civi_update->go = false;
			$ll_civi_update->ll_update_log("PreDownLoad wegen Fehler abgebrochen",true);		
		} else {
			$ll_civi_update->ll_update_log("PreDownLoad beendet",true);					
		}
		$ll_civi_update->ll_update_log(true);
		return $bool;
	}

	public static function ll_civi_PreUpdate($bool,$args = array()) {
		//	$args['log_mod'] = 'single'; //TEST
		$ll_civi_update = self::singleton();
		if(!$ll_civi_update->go) return;
		$ll_civi_update->ll_civi_sites('preU');
 		$ll_civi_update->ll_civi_move(); //TEST	

		$ll_civi_update->ll_update_log("PreUpdate beendet",true);		
		$ll_civi_update->ll_update_log(true);
		return $bool;
	}

	public static function ll_civi_PostUpdate($bool,$args = array()) {
		$ll_civi_update = self::singleton();
		if(!$ll_civi_update->go) return;

		$ll_civi_update->ll_civi_sites('post');
		//Kopiert civicrm.settings
		$ll_civi_update->ll_civi_settingsfile();
		$ll_civi_update->ll_update_log("PostUpdate beendet",true);		
		$ll_civi_update->ll_update_log(true);
		return $bool;
	}


	private function ll_civi_move() {
		if(!$this->test) {
			$pass = rename(WP_PLUGIN_DIR."/".$this->plugin_dir,WP_PLUGIN_DIR."/".$this->plugin_dir."-".$this->old_version);
		} else {
			$pass = true;
		}
		$this->ll_update_log("Plugin-Verzeichnis wird umbenannt in: ".$this->plugin_dir."-".$this->old_version);
		return $pass;
	}

	private function ll_civi_settingsfile() {
		if(($this->update == 'post') and ($this->db['org'] == 'done') and is_multisite()) { //Sicherheitsabfrage ...
			if(!$this->test) $pass = rename(WP_PLUGIN_DIR."/".$this->plugin_dir."-".$this->old_version."/civicrm.settings.php-org",
										  WP_PLUGIN_DIR."/".$this->plugin_dir."/civicrm.settings.php");
		    $this->ll_update_log(array("SettingsFile: ",
										WP_PLUGIN_DIR."/".$this->plugin_dir."-".$this->old_version."/civicrm.settings.php-org wird nach",
									    WP_PLUGIN_DIR."/".$this->plugin_dir."/civicrm.settings.php verschoben"));
		}
	}
	
	private function ll_civi_sites($update = '',$bool = false) {
		if(!empty($update)) $this->update = $update;
		$this->ll_update_log($this->update."-Update wird gestartet ".current_time('Y-m-d H:i:s'));
		if(function_exists('get_sites')) {
			$ll_tools_sites = get_sites();
			foreach($ll_tools_sites as $ll_tools_site) {
				switch_to_blog($ll_tools_site->blog_id);
				$this->ll_update_log($ll_tools_site->domain." wird geprüft");
				$this->ll_update_log(wp_upload_dir()['basedir']);
//				if(is_dir(wp_upload_dir()['basedir']."/".$this->plugin_dir)) {
				if($this->ll_civi_site_functions($ll_tools_site->blog_id) === false) {
					restore_current_blog();
					return new WP_Error('dbexists',"Datenbankduplikat für ".$ll_tools_site->domain." konnte nicht erstellt werden");
				}
//				}					
				restore_current_blog();
			}
		} else {
			if($this->ll_civi_site_functions() === false)
				return new WP_Error('dbexists',"Datenbankduplikat konnte nicht erstellt werden");
		}
		return $bool;		
	}
	
	private function ll_civi_site_functions($blog_id = "") {
		//upload-Verzeichnis kopieren
		if(substr($this->update,0,3) == 'pre') {
			$this->ll_update_log("Update Site ".$blog_id);
			if(is_plugin_active(LL_TOOLS_CIVI_NAME)) {
				//Plugin ist aktiv 
				$this->ll_update_log("Plugin ist aktiv ");
				if((!isset($this->db['org']) or ($this->db['org'] != 'done') and ($this->update == 'preD'))) {
					$this->ll_update_log("Datenbank soll bearbeitet werden");
					if($this->civi_db_copy !== false) {
						$this->ll_update_log($this->civi_db_copy);
						if(substr($this->civi_db_copy['checked'],0,2) != 'OK') return false;
						$this->ll_credentials();
						//Datenbank duplizieren
						if(!$this->ll_civi_duplicateDb()) return false;
						if($this->db['org'] == 'single') {
							$this->db['org'] = 'done';
							$blog_id = ""; //wennn nur eine DB, dann auch nur ein settings.php
						}
					}
					return true;
				} elseif ($this->update == "preU") {
					if(!$this->test) deactivate_plugins($this->plugin,false,false); //TEST
					$this->ll_update_log("Deaktivate ".$blog_id);						
				}
			}
		}
		$this->ll_civi_uploads(wp_upload_dir()['basedir']."/",$blog_id); //TEST
		return true;
	}
	
	private function ll_civi_uploads($dir,$blog_id) {
		$baseDir = $dir.$this->plugin_dir."/";
		if(!is_dir($baseDir)) return;
		$oldDir = $dir.$this->plugin_dir."-".$this->old_version."/";
		if($this->update == "post") {
			$this->ll_civi_custom_php($baseDir."custom_php/");
			return;
		}
		$this->ll_update_log(array("Uploadsverzeichnis wird bearbeitet:",$baseDir,$oldDir));
		if(!$this->test) {
			$dirlist = array();
			ll_civi_deldir($baseDir."/templates_c/",$dirlist);
			ll_civi_deldir($baseDir."/ConfigAndLog/",$dirlist);		
			$this->ll_update_log("Cache wurde gelöscht");						
			rename($baseDir,$oldDir);
			mkdir($baseDir);			
		}
		
		if(is_dir($oldDir)) {
			//Dieser Ast wird im Testfall nicht angefahren, weil es das Verzeichnis nicht gibt!
			$ll_uploads = scandir($oldDir);
			foreach($ll_uploads as $file) {
				$this->ll_update_log('Uploads: '.$file);
				if(substr($file,0,1) == ".") {
				} elseif($file == 'civicrm.settings.php') {
					//Modifizieren und Koperien
					$this->ll_update_log("Filecopy: ".$file);
					if(!$this->test) copy($oldDir.$file,$baseDir.$file);
					$settingsFile = WP_PLUGIN_DIR."/".$this->plugin_dir."/".$file;
					if(is_file($settingsFile)) {
						$this->ll_civi_settings(substr($baseDir,0,-1),$settingsFile);
						//Hier muß das Original weggesichert werden!
						if(!$this->test) copy($settingsFile,$settingsFile."-org"); 
					} else {
						$this->ll_civi_settings(substr($baseDir,0,-1),$oldDir.$file); 						
						if(!$this->test) copy($oldDir.$file,$settingsFile.$blog_id);
					}
				} elseif(in_array($file,array('custom','custom_php','ext','persist','upload'))) {
					$this->ll_update_log("Dircopy: ".$file);
					$this->ll_civi_dircopy(substr($baseDir,0,-1),$file);
				} elseif(is_dir($oldDir.$file)) {
					$this->ll_update_log("Makedir: ".$file);
				 	if(!$this->test) mkdir($baseDir.$file);
				}
			}
		}
		return true;
	}
	
	private function ll_civi_settings($baseDir,$settingsFile) {
		$code_array = file($settingsFile);
		foreach($code_array as $line => $code) {
			if(strpos($code,$this->db['credentials']) !== false) { 
				$code_array[$line] = str_replace($this->db_org,$this->db_old,$code);
				$this->ll_update_log("Credentials wurden modifiziert");
			} elseif(strpos($code,WP_PLUGIN_DIR."/".$this->plugin_dir."/".$this->plugin_dir) !== false) {
				$code_array[$line] = str_replace($this->plugin_dir."/".$this->plugin_dir,
												 $this->plugin_dir."-".$this->old_version."/".$this->plugin_dir,$code);
				$this->ll_update_log(array("Plugin-Dir",$code,$code_array[$line]));
			} elseif(strpos($code,$baseDir."/") !== false) {
				$code_array[$line] = str_replace($baseDir,$baseDir."-".$this->old_version,$code);
				$this->ll_update_log(array("Compile-Dir",$code,$code_array[$line]));
			} 
		}
		if(!$this->test) file_put_contents($settingsFile,$code_array);		
	}
	
	private function ll_civi_dircopy($baseDir,$file) {
		if($this->test)	return;
		llToolsZip($baseDir."-".$this->old_version."/".$file,$baseDir."-".$this->old_version."/".$file);
		$zip = new ZipArchive();
		if ($zip->open($baseDir."-".$this->old_version."/".$file.".zip") === TRUE) {
			$this->ll_update_log("Uploads-Kopie:".$file);
		    $zip->extractTo($baseDir);
		    $zip->close();
			unlink($baseDir."-".$this->old_version."/".$file.".zip"); //Löscht den ZipFile im alten uploads-DIR
		}
		
	}
	
	private function ll_civi_custom_php($dir) {
		static $baseDir;
		static $pluginDir;
		if(!isset($pluginDir)) {
			$baseDir = $dir; //..wc_content/uploads/sites .../civicrm/custom_php/
			$pluginDir = WP_PLUGIN_DIR."/".$this->plugin_dir."/".$this->plugin_dir."/";
			$this->ll_update_log(array("Custom_php: ",$baseDir,$pluginDir));
		}
		$this->ll_update_log("Custom_php: ".str_replace($baseDir,"",$dir));
		$files = scandir($dir);
		foreach($files as $file) {
			if(substr($file,0,1) != ".") {
				if(is_dir($dir.$file)) $this->ll_civi_custom_php($dir.$file."/");
				if(is_file($dir.$file)) {
					$this->ll_update_log("Wird geprüft:".$dir.$file);
					if(substr($file,-4) == ".php") {
						if(!$this->test) rename($dir.$file,$dir.str_replace(".php","-".$this->old_version.".bak",$file));
						$origFile = str_replace($baseDir,$pluginDir,$dir.$file);
						$this->ll_update_log("Gesichert, neues Original:".$origFile);
						if(!$this->test) copy($origFile,$dir.$file);						
					}
				}				
			}
		}
	}

	public function ll_civi_get_value($var) {
		return $this->$var;		
	}
	
	protected function ll_update_log($text, $skin = false) {
		if($this->log_mod == 'single') {
			ll_crm_debug($text,'print');
		} else {
			static $log;
			if($text === true) {
				$log[] = "Protokollierung abgeschlossen";
				file_put_contents($this->log_file, print_r($log,true).chr(10), FILE_APPEND | LOCK_EX);
				
				$log = array(); //sonst wird beim post-Durchlauf das alte Protokoll wiederholt!
			} else {
				$log[] = $text;
				if(isset($this->skin) and $skin) {
					$this->skin->feedback($text);
				}
			}
		}
	}

	protected function ll_credentials($civi_db_copy = NULL) {
		$settingsFile = WP_PLUGIN_DIR."/".$this->plugin_dir."/".$this->settingsFile;
		if(isset($this->db['org']) and ($this->db['org'] != 'multi')) {
			$this->ll_update_log("DB-Credentials ORG nicht geändert");		
		} else {
			$this->db['org'] = 'single';			
			if(!file_exists($settingsFile)) {
				$settingsFile = wp_upload_dir()['basedir']."/".$this->plugin_dir."/".$this->settingsFile;
				if(is_multisite()) $this->db['org'] = 'multi';
			}
			if(defined('CIVICRM_DSN')) {
				$credentials = substr(CIVICRM_DSN,strpos(CIVICRM_DSN,"//") + 2);
				//ll_crm_debug($credentials);			
			} else {
				$matches = preg_grep('#^\s*define\s*\( *[\D\W]CIVICRM_DSN[\D\W]\s*,\s*[\D\W]mysql:#',file($settingsFile));
				$credentials = substr(current($matches),strpos(current($matches),"//") + 2);			
			}

			$this->db_org['dbserver'] = substr($credentials,strpos($credentials,"@") + 1, 
										                   strpos($credentials,"/") - strpos($credentials,"@") - 1); //"localhost";
			$this->db_org['pw'] =   substr($credentials,strpos($credentials,":") + 1, 
				                                           strpos($credentials,"@") - strpos($credentials,":") - 1); //"root";
			$this->db_org['user'] =   substr($credentials,0,strpos($credentials,":")); //"root";
			$this->db_org['db'] =   substr($credentials,strpos($credentials,"/") + 1, 
				                                   strpos($credentials,"?") - strpos($credentials,"/") - 1); //"database";
			//ll_crm_debug($this->db_org);
			$this->db['credentials'] = $credentials; //wird benötigt für ll_civi_settings.
		}
	   if(isset($this->db_old) and ($this->db['org'] != 'multi') and !isset($civi_db_copy)){
			$this->ll_update_log("DB-Credentials OLD nicht geändert");		
	   } else {
			if(isset($civi_db_copy)) $this->civi_db_copy = $civi_db_copy;
			$this->db_old = $this->db_org;
			if(empty($this->civi_db_copy['db'])) {
				$this->db['auto'] = true;
				$this->db_old['db'] = $this->db_org['db']."_".strtr($this->old_version,".","_");
			} else {
				$this->db_old['db'] = $this->civi_db_copy['db'];
				if(!empty($this->civi_db_copy['user'])) {
					$this->db_old['user'] = $this->civi_db_copy['user'];
					$this->db_old['pw'] = $this->civi_db_copy['pw'];
					$this->db['diff'] = true;
				}
				if(!empty($this->civi_db_copy['dbserver'])) {
					$this->db_old['dbserver'] = $this->civi_db_copy['dbserver'];				
					$this->db['diff'] = true;
				}
			} 		
	   } 
		if($this->log_mod != 'single') {
			$prot = array("DB-Credentials: ",
							$this->log_mod,
							$this->db,
							array_merge($this->db_org,array('pw' => '***')),
							array_merge($this->db_old,array('pw' => '***'))
						);
			$this->ll_update_log($prot);
		}
	}

	public static function ll_civi_createtest($civi_db_copy = NULL,$args = array()) {
		$civiUpgrader = self::singleton($args);

		if($civi_db_copy !== NULL) $civiUpgrader->ll_credentials($civi_db_copy);
		if(isset($civiUpgrader->db['auto'])) {
			$db_org = $civiUpgrader->ll_db_connect($civiUpgrader->db_org);
			if(empty($db_org)) return "Verbindung zu ".$civiUpgrader->db_org['db']." kann nicht hergestellt werden";
			$test = $civiUpgrader->ll_db_conn_exec($db_org,"CREATE DATABASE ".$civiUpgrader->db_old['db']);
			ll_crm_debug($civiUpgrader->error);
			if($test !== true) {
				$test = $civiUpgrader->error[2];
				if(!empty($civiUpgrader->ll_db_connect($civiUpgrader->db_old))) $test = "OK ? =>".$test;
				return $test;
			}
			$civiUpgrader->ll_db_conn_exec($db_org,"DROP DATABASE ".$civiUpgrader->db_old['db']);		
		} else {
			$db_old = $civiUpgrader->ll_db_connect($civiUpgrader->db_old);
			if(empty($db_old)) return "Verbindung zu ".$civiUpgrader->db_old['db']." kann nicht hergestellt werden";			
		}
		return true;
	}

	public static function ll_civi_data_check() {
		$civiUpgrader = self::singleton();
		$civiUpgrader->ll_update_log("DataCheck");
		$civiUpgrader->ll_credentials();
		$db_old = $civiUpgrader->ll_db_connect($civiUpgrader->db_old);
		ll_crm_debug($db_old);
		if(empty($db_old)) {
			if(isset($civiUpgrader->db['auto'])) return true;
			$civiUpgrader->civi_db_copy['checked'] = "Datenbank fehlt";
			update_option(LL_TOOLS_OPTION."civi_db_copy",$civiUpgrader->civi_db_copy);
			return false;
		}
		if(!empty($civiUpgrader->ll_db_conn_exec($db_old,"SHOW FULL TABLES",PDO::FETCH_KEY_PAIR)))
			return "Datenbank ".$civiUpgrader->db_old['db']." enthält Tabellen.";
		return true;
	}
	
	protected function ll_civi_duplicateDb() {
		$definer_new = array($this->db_old['user'],$this->db_old['dbserver']);
		
		$this->ll_update_log("Database ".$this->db_org['db']." wird kopiert nach ".$this->db_old['db']);

		$dbTriggerList = array('FUNCTION','PROCEDURE');

		$db_org = $this->ll_db_connect($this->db_org);
		if(empty($db_org)) return false;
		if(isset($this->db['auto'])) $this->ll_db_conn_exec($db_org,"CREATE DATABASE ".$this->db_old['db']);
		$db_old = $this->ll_db_connect($this->db_old);
		if(empty($db_old)) return false;
		$this->ll_db_conn_exec($db_old,"SET SESSION foreign_key_checks = OFF");
	
		$allTables = $this->ll_db_conn_exec($db_old,"SHOW FULL TABLES",PDO::FETCH_KEY_PAIR); 
		if(!empty($allTables)) {
			$option = get_option(LL_TOOLS_OPTION.'civi_db_drop_tables');
			$this->ll_update_log("Database ".$this->db_old['db']." enthält Tabellen.");
			if($option[0] == 'Daten löschen') {
				$this->ll_update_log("Tabellen in Database ".$this->db_old['db']." werden gelöscht.");
				if($this->dbtest) return true;
				foreach($allTables as $table => $table_type) {
					if($table_type == "BASE TABLE") {
						$this->ll_db_conn_exec($db_old,"DROP TABLE ".$table);
					} else {
						$this->ll_db_conn_exec($db_old,"DROP VIEW ".$table);
					}
				}
				foreach($dbTriggerList as $dbTrigger) {
				    $dbTriggers[$dbTrigger] = $this->ll_db_conn_exec($db_old,"SHOW $dbTrigger STATUS WHERE db='".$this->db_old['db']."'",PDO::FETCH_ASSOC);
					foreach($dbTriggers[$dbTrigger] as $trigger)
				    	$dbTriggers[$dbTrigger] = $this->ll_db_conn_exec($db_old,"DROP ".$dbTrigger." ".$trigger['Name']);	
				    $dbTriggers = array();								
				}
				update_option(LL_TOOLS_OPTION.'civi_db_drop_tables',array("Wählen"));
			} elseif($option[0] == 'Daten sind gültig') {
				$this->ll_update_log("Tabellen in Database ".$this->db_old['db']." werden als Kopie verwendet");
				if($this->dbtest) return true;
				update_option(LL_TOOLS_OPTION.'civi_db_drop_tables',array("Wählen"));
				return true;
			} else {
				return false;
			}
		}
		if($this->dbtest) return true;
		
		$allTables = $this->ll_db_conn_exec($db_org,"SHOW FULL TABLES",PDO::FETCH_KEY_PAIR);
		
		foreach($dbTriggerList as $dbTrigger)
	    	$dbTriggers[$dbTrigger] = $this->ll_db_conn_exec($db_org,"SHOW $dbTrigger STATUS WHERE db='".$this->db_org['db']."'",PDO::FETCH_ASSOC);

		foreach($dbTriggerList as $dbTrigger) 
			foreach($dbTriggers[$dbTrigger] as $trigger) {
				$triggerData = $this->ll_db_conn_exec($db_org,"SHOW CREATE $dbTrigger ".$trigger['Name'],PDO::FETCH_ASSOC);
				$this->ll_update_log($trigger['Definer']);	
				$triggerData[0]['Create Function'] = preg_replace('#CREATE\s*DEFINER\s*\=.*\s*FUNCTION#','CREATE FUNCTION',$triggerData[0]['Create Function']);
				$this->ll_db_conn_exec($db_old,$triggerData[0]['Create Function']);		
			}
		
		foreach($allTables as $table => $table_type) {
			$this->ll_db_conn_exec($db_old,"SET SESSION foreign_key_checks = OFF");
			if($table_type == "BASE TABLE") {
				$create_table = $this->ll_db_conn_exec($db_org,"SHOW CREATE TABLE ".$table,PDO::FETCH_ASSOC);
				$this->ll_db_conn_exec($db_old,$create_table[0]['Create Table']);
				if(strpos($table,"cache") or strpos($table,"job_log")) {
					$this->ll_update_log($table." wird nicht kopiert");
				} else {
					if(isset($this->db['diff'])) {
						$this->ll_data_copy($table,$db_org,$db_old);						
					} else {
						$this->ll_db_conn_exec($db_old,"INSERT INTO $table SELECT * FROM ".$this->db_org['db'].".".$table);						
					}
				}
				$Triggers = $this->ll_db_conn_exec($db_org,"SHOW TRIGGERS LIKE '$table'",PDO::FETCH_ASSOC);
				foreach($Triggers as $trigger) {
					$statement = 'CREATE TRIGGER `'.$trigger['Trigger'].'` '.$trigger['Timing'].' '.$trigger['Event'].
					             ' ON `'.$trigger['Table'].'` FOR EACH ROW '. $trigger['Statement'];				
					$this->ll_db_conn_exec($db_old,$statement);		
				}
				
			} else {
				$CreateView = $this->ll_db_conn_exec($db_org,"SHOW CREATE VIEW ".$table,PDO::FETCH_ASSOC);
				$CreateView[0]['Create View'] = preg_replace('#\s*DEFINER\s*\=.*\s*SQL#',' SQL',$CreateView[0]['Create View']);
				$this->ll_db_conn_exec($db_old,$CreateView[0]['Create View']);
			} 
		}

		//für dbClose, obwohl nach verlassen der Funktion ...
		unset($db_org);
		unset($db_lod);
		return true;
	}

	private function ll_data_copy($table,$db_org,$db_old){
//		$retour=array();
//		$retour['status']='ok';
		$columns = $this->ll_db_conn_exec($db_org,"SHOW COLUMNS FROM ".$table,PDO::FETCH_ASSOC);
		foreach($columns as $column) {
			$colSetA[] = $column['Field'];
			if($column['Null'] == "YES") {
				$colRequestA[] = "if(ISNULL(".$column['Field']."),'NULL',".$column['Field'].") AS ".$column['Field'];
			} else {
				$colRequestA[] = $column['Field'];
			}
		}
		$colRequest = implode(",",$colRequestA);
		$result = $this->ll_db_conn_exec($db_org,"SELECT $colRequest FROM ".$table,PDO::FETCH_ASSOC); //LIMIT ".$from.','.NUMMBER_OF_ROW_TO_COPY
		if(empty($result)) {
			$this->ll_update_log("Tabelle $table enthält keine Daten");
			return true;
		}
		foreach($result as $row) {
			foreach ($row as $key => $value) {
        		$row[$key]=str_replace("'", "\'", $value);
				if($value != 'NULL') $row[$key] = "'".$row[$key]."'";
        	}
            $dataSet[]=implode(",", $row);
        }
		
		$colSet = implode("`, `", $colSetA);
        $data=" (".implode("),(", $dataSet).")";
		$this->ll_db_conn_exec($db_old,'INSERT INTO `'.$table.'` (`'.$colSet.'`) VALUES '.$data);
		$this->ll_update_log(count($result)." Datensätze von $table wurden kopiert");
//		$retour['nDone']=$from+NUMMBER_OF_ROW_TO_COPY;
		return true;
	}


	protected function ll_db_connect($db) {
		try {
		    $conn = new PDO("mysql:host=".$db['dbserver'].";dbname=".$db['db'], $db['user'], $db['pw']);
		    // set the PDO error mode to exception
		    $conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
			return $conn;
	    }
		catch(PDOException $e)
	    {
		    $this->ll_update_log("Connection failed: ". $db['db'] . " - " . $e->getMessage());
			$this->error = $e->errorInfo;
	    }
	}
	
	protected function ll_db_conn_exec($conn,$sql,$resultSet = NULL,$param = NULL) {
		try {
		    $stmt = $conn->prepare($sql);
		    $stmt->execute();
			
			if(!isset($resultSet)) return true;
			
		    // set the resulting array to associative
			if(isset($param)) {
				$stmt->setFetchMode($resultSet,$param);
			} else {
			    $stmt->setFetchMode($resultSet);			
			}
			$result = $stmt->fetchAll();
			return $result;
		}
		catch(PDOException $e) {
		    $this->ll_update_log(array(str_replace("),(",")".chr(10)."(",$sql),$e->getMessage()));
			$this->error = $e->errorInfo;
			//ll_crm_debug($e->errorInfo);
		}
	}	
}
?>
