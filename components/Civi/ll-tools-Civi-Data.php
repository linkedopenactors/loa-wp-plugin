<?php

// functions that create our options page
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*
* Adminseiten für CiviCRM als Ergänzung zu Livinglines-Tools
Component Name: Anbindung, Datenbanken, Indiv. Felder, Update
Components Group: Civi
Components Order: 10
* 
*/

ll_tools_make_menu(array('site_title' => 'CiviCRM', 'page' => 'civi'));

add_action('load-ll-tools_page_ll_tools_civi','ll_tools_civi_settings_init');
add_action('load-options.php','ll_tools_civi_settings_init');

//Das sollte wohl noch weg?
ll_tools_file_loader("components/Civi","ll-civi-functions");
if(is_admin() and LL_TOOLS_CIVICRM) ll_tools_file_loader("components/Civi","ll-tools-civi-update");

function ll_tools_settings_data_cb() {
//	settings_errors('ll_tools');
//	ll_crm_debug(get_option(LL_TOOLS_OPTION."comp_mode"),true);
//	delete_option(LL_TOOLS_OPTION.'crm_sets');
//	ll_crm_debug(get_option(LL_TOOLS_OPTION.'crm_sets',array()),true);
//	ll_crm_debug(get_option(LL_TOOLS_OPTION."field_def_1"),true);
//	ll_crm_debug(get_transient(LL_TOOLS_OPTION."crm_sets_bak"));
//	ll_crm_debug(get_option(LL_TOOLS_OPTION."crm_sets"),true);
}

function ll_tools_settings_civi_cb() {
	echo '<p><a href="https://docs.civicrm.org/dev/en/latest/api/v4/usage/">Doku Civi API 4</a>, ';
	echo '<a href="'.esc_url(get_site_url().'/wp-admin/admin.php?page=CiviCRM&q=civicrm/api4#explorer')
			.'">CiviCRM Explorer 4</a>, ';				
	echo '<a href="'.esc_url(get_site_url().'/wp-admin/admin.php?page=CiviCRM&q=civicrm/api3').'">CiviCRM Explorer 3</a></p>';				
	echo '<p>'.get_option(LL_TOOLS_OPTION."civirm").'</p>';
}

function ll_tools_settings_civi_db_cb() {
	echo '<p>Vor dem Update wird die bestehende civiCRM-Datenbank dupliziert.<br>
		  Für das Duplizieren muß der DB-User das Recht haben, eine Datenbank anzulegen.<br>
		  Wenn er dieses Recht nicht hat, muß die Datenbank zuvor angelegt und der Name der Datenbank hier eingetragen werden.<br></p>';

	echo '<p>Nach dem Update ist für jede aktive Instanz mit eigener Datenbank das Datenbankupate manuell zu starten.</br>
		  <a href="'.esc_url(get_site_url().'/wp-admin/admin.php?page=CiviCRM&q=civicrm/upgrade&reset=1').'">Hier ist der Link zum Datenbankupdate</a></p>';				
}


function ll_tools_civi_settings_init() {
	// Notwendig, für die Erstinstallation von CiviCRM, weil es da noch kein CIVICRM_DSN gibt, dann tritt ein Fehler auf!
	// Das funktionierte aber nicht bei OO, weil hier civicrm nicht initiiert war.
	// Wieso hier civi_crm nicht initialisiert ist, ist unklar, bei nicht multisite ist es, andere multisite habe ich derzeit nicht!
	if(LL_TOOLS_CIVICRM and !defined('CIVICRM_DSN')) {
		civi_wp()->initialize();
		if(!defined('CIVICRM_DSN')) return;
	}
	#### Datensetzs
	$default_args = array('group' => 'data', 'page' => 'civi', 'type' => 'text','title' => 'Data Sets');
	$version_options = array("Nicht installiert"=>0,"Nicht aktiv"=>1,"Version 3" => 3,"Version 4" => 4);
	$opt_values = array('crm_sets' => array('title' => 'Datenbanksets',"type" => 'text'));
	if(LL_TOOLS_CIVICRM)
		$opt_values['crm_sets']['options'] = array('default' => array('title' => 'Eigenes CiviCRM verwenden?', 'type' => 'select',
																'options' => $version_options));
	$crm_sets = get_option(LL_TOOLS_OPTION.'crm_sets');
	ll_crm_debug($crm_sets);
	if(isset($crm_sets['sets'])) {
		$opt_values['crm_sets']['post_text'] = 'Beispiel für URL: http://mydomain/wp-content/plugins/civicrm/civicrm/extern/rest.php';
		foreach($crm_sets['sets'] as $set => $values) {
			if($set > 0) {
				if(!isset($crm_sets['api_text'.$set])) $crm_sets['api_text'.$set] = "";
				$opt_values['crm_sets']['options']['api_server'.$set] = array('title' => 'URL', 'size' => 50);
				$opt_values['crm_sets']['options']['key'.$set] = array('title' => 'Key', 'size' => 50, 'type' => 'password');
				$opt_values['crm_sets']['options']['api_key'.$set] = array('title' => 'Api Key', 'size' => 50, 'type' => 'password');
				$opt_values['crm_sets']['options']['version'.$set] = array('title' => 'Api Version', 'size' => 1, 'type' => 'select',
											'options' => array_slice($version_options,1));
				$opt_values['crm_sets']['options']['api_text'.$set] = array('title' => $crm_sets['api_text'.$set],'type' => 'hidden');										
			}
		}
	}
	if(empty($set) or !empty($crm_sets['api_server'.$set]))
		$opt_values['crm_sets']['options']['next'] = array('title' => 'Weiteres/Anderes CiviCRM?', 'type' => 'checkbox');

		$opt_values['clear_cache'] = array('title' => 'Cache Variablen löschen', 
					 			"description" => 'Löscht die Zwischenspeicher der Tags, Subtypes, ContactTypes ..', "default" => false, 'type' => 'checkbox');

	ll_tools_make_section($default_args,$opt_values);		

	if(LL_TOOLS_CIVICRM) {
		#### CiviCRM Addon
		$default_args = array('group' => 'civi','title' => "CiviCRM - Addon");
		$opt_values = array('clear_civi_cache' => array('title' => 'CiviCRM Cache leeren',
								"description" => 'CiviCRM Cache Verzeichnisse (ConfigAndLog, templates_c) leeren.', "default" => false));
		ll_tools_make_section($default_args,$opt_values);

		#### CiviCRM Update
		$opt_values = array();
		if(isset(get_option(LL_TOOLS_OPTION.'updates')['civicrm/civicrm.php'])) {
			$default_args = array('group' => 'civi_db', 'page' => 'civi','title' => "CiviCRM - UpdateHandling");
			$opt_values['civi_db_copy'] = array('title' => 'Datenbank für Kopie',"type" => 'text', 'options' => array(
									'checked' => array('type' => 'hidden'),
									'db' => 'Name der Datenbank',
									'user' => 'Benutzername, leer wenn ident mit Original',
									'pw' => array('title' => 'Passwort, leer wenn gleicher Benutzer', 'type' => 'password'),
									'dbserver' => 'Datenbankserver, nur notwendig wenn abweichend'),
									'description' => 'Diese Datenbank wird für die Erstellung einer Kopie vor dem Update verwendet,'.'</br>'.
																 'wenn alle Angaben leer bleiben, versucht das System eine Datenbank zu erstellen!');
			$opt_values = ll_tools_settings_add_drop_tables($opt_values);
			$opt_values['civi_update_test'] = 
				array('title' => 'Civi Update Test','description' => "Testfunktion für Civi-Update", 'type' => 'radio', 
						'options' => array('ND' => 'Nicht durchführen','NP' =>'Nur Protokoll','DB' => 'Protokoll und Datenbankkopie'));
			$opt_values = ll_tools_settings_add_field_test($opt_values);								
			ll_tools_make_section($default_args,$opt_values);		
		}
	
	}

}


################## DataSets
add_filter('pre_update_option_'.LL_TOOLS_OPTION.'crm_sets',function($value,$oldvalue) {
	if(isset($value['sets'])) return $value;
	$value['sets'] = $oldvalue['sets'];
	ll_crm_debug($value);
	foreach(array('default','next','sets') as $option) {
		if($option == 'default') {
			$value['sets'][0] = array('version' => $value[$option]);
		} elseif(($option == 'next') and isset($value[$option])) {
			$value['sets'][] = make_crm_set(array("","","",0));
		} else {
			foreach($value['sets'] as $i => $values) {
				if($i > 0) {
					if(empty($value['api_server'.$i]) and !isset($value['next'])) {
						unset($value['sets'][$i]);
					}
					if(!empty($value['api_server'.$i]) and 
					   (($value['api_server'.$i] != $oldvalue['api_server'.$i]) or
					   ($value['api_key'.$i] != $oldvalue['api_key'.$i]) or
	  				   ($value['key'.$i] != $oldvalue['key'.$i]) or 
	  				   ($value['version'.$i] != $oldvalue['version'.$i]))) {
						$value['sets'][$i] = make_crm_set(array($value['api_server'.$i], $value['key'.$i], 
														$value['api_key'.$i],$value['version'.$i]));
						$value['check'] = true; 						
					}					
				}
			}
		}
	}
	unset($value['next']);
	ll_crm_debug($value);
	return $value;
},8,2);

function make_crm_set($values) {
	$default = array('api_server','key','api_key','version');
	return array_combine($default,$values);
}

function ll_civi_updated_option($option,$old_value,$value) {
	if(($option == LL_TOOLS_OPTION.'crm_sets') and isset($value['check'])) {
		ll_crm_debug(array($option,$old_value,$value)); 
		if(isset($value['sets'])) {
			//if(isset($value['sets'][0]) and empty($value['sets'][0])) unset($value['sets'][0]);
			//$i = 1;
			
			foreach($value['sets'] as $key => $set) {
				$result = LL_Civi_Data::get_request(array('entity' => 'Country', 
													 'params' => array('iso_code' => 'AT',
													 'return' => "name,region_id")),$key,$set['version']);
				ll_crm_debug(array($result,$set['version']));
				if(LL_Civi_Data::error()) {
					$value['api_text'.$key] = LL_Civi_Data::error();
					$value['version'.$key] = '1';
					$value['sets'][$key]['version'] =  $value['version'.$key];					
				} else {
					$value['api_text'.$key] = "Geprüft und OK";				
				}
				//$i++;
			}
			unset($value['check']);
			ll_crm_debug($value);
			update_option(LL_TOOLS_OPTION.'crm_sets',$value);
		}
	}
}

add_action('updated_option','ll_civi_updated_option',10,3);

############### FieldTest

function ll_tools_settings_add_field_test($opt_values){
	if(get_option(LL_TOOLS_OPTION.'civi_db_field_test') == 'OK') return $opt_values;
	$post_text = ll_update_civicrm::ll_fieldtype_conversion("");
	if(!empty($post_text)) {
		$opt_values['civi_db_field_test'] = array('title' => 'Datentypen', 'default' => false, 'post_text' => $post_text,
												  'description' => 'Datentypen nach TIMESTAMP konvertieren?');
	} 
	update_option(LL_TOOLS_OPTION.'civi_db_field_test',"OK");
	return $opt_values;
}

add_filter('pre_update_option_'.LL_TOOLS_OPTION.'civi_db_field_test', function($value,$oldvalue) {
	if($value == 'OK') return $value;
	if($value) {
		ll_update_civicrm::ll_fieldtype_conversion("",$value);
		return $oldvalue;	
	}
},10,2);

############### FieldDef
function ll_tools_settings_add_field_def_all($opt_values) {
	$crm_sets = get_option(LL_TOOLS_OPTION.'crm_sets',array());
	if(isset($crm_sets['sets']))
		foreach($crm_sets['sets'] as $key => $value) {
			ll_crm_debug($value);
			if(($value['version'] > '1')) {
				$opt_values['field_def_'.$key] = array('title' => 'CustomFields Set '.$key, 'type' => 'checkbox',
									 			'function' => array('LL_classes_option','default_field_cb'));
				if(!LL_SETTINGS_SAVE) {
					$options = ll_tools_settings_add_field_def($key);
					if(is_array($options)) {
						$description = "Gewünschte Gruppe zum Ändern anhacken";
						if(isset($options['check'])) $description = "Bitte die Änderungen Eintragen";
						$opt_values['field_def_'.$key]['description'] = $description;
						$opt_values['field_def_'.$key]['options'] = $options;	
					}					
					if(!is_array($options)) unset($opt_values['field_def_'.$key]);
				}
			}
		}
	return $opt_values;	
}

	
function ll_tools_settings_add_field_def($crm_set) {
	$field_def = get_option(LL_TOOLS_OPTION."field_def_".$crm_set,array());
	unset($field_def['keys_options']);
//	unset($field_def['raw']);
	$field_def_org = $field_def;

	if(isset($field_def['check'])) {
		ll_crm_debug($field_def['check']);
		$options = array();
		$check = false;
		ksort($field_def['raw']);
		foreach($field_def['raw'] as $field_id => $def) {
			$field = explode("-",$field_id);
			if(substr($field_id,2,1) == "A") {
				$check = isset($field_def['check'][$field_id]);
				if($check) $options["*Gruppe = ".$field[2]] = "";
			} 
			if($check) {
				$title = (substr($field[0],2) == "A") ? 'Gruppe: ' : 'Feld: ';
				$title .= $field[1]." - ".$field[2];
				$options[$field_id] = array('default' => $def, 'title' => $title, 'type' => 'text');
			}
		}
		$options['check'] = array('default' => 'updated', 'type' => 'hidden'); 
		unset($field_def['check']);
		update_option(LL_TOOLS_OPTION."field_def_".$crm_set,$field_def);
		return $options;

	} else {
		$custom_groups = LL_Civi_Data::get_request(array('entity' => 'CustomGroup', 
											'params' => array('return' => 'extends,name,help_post')),$crm_set);

		ll_crm_debug($crm_set);
		ll_crm_debug($custom_groups);
		if(LL_Civi_Data::error()) {
			return false;
		}
		if(empty($custom_groups)) return false;
		ll_crm_debug(array($custom_groups,$crm_set));
		$group_options = ll_tools_field_def_data_check($custom_groups,"","Gruppe",$field_def,$crm_set);
		ll_crm_debug($group_options);
		if(!isset($field_def_org['raw']) or ($field_def_org != $field_def)) {
			unset($field_def['check']);
			update_option(LL_TOOLS_OPTION."field_def_".$crm_set,$field_def); 
					
		} 
		return $group_options;
	}
	return false;
}

add_filter('ll_tools_settings_add_opt_value_data','ll_tools_settings_add_field_def_all',10,5);

add_filter('pre_update_option',function($value,$option,$oldvalue){
	if(strpos($option,LL_TOOLS_OPTION."field_def_") === 0) {
		ll_crm_debug($option);
		$value = pre_update_option_field_def($value, $oldvalue);
	}
	return $value;
},15,3);


function pre_update_option_field_def($value, $oldvalue) {
	if(isset($value['raw'])) {
		return $value;
	}
	if(isset($value['check']) and ($value['check'] == 'updated')) {
		unset($value['check']);
		$text = "";
		foreach($value as $id => $def_string) {
			$def = explode(",",$def_string);
			if(isset($def[1]) and !in_array($def[1],array("Key","Language","Image")))
				$text = "Key ".$def[1]." ist nicht gültig in ".$id."<br>";
			if(($def[1] == "Image") and !isset($def[2]))
				$text = "Key ".$def[1]." verlangt einen Feldnamen in ".$id."<br>";
		}
		if(empty($text))
			$oldvalue['raw'] = array_merge($oldvalue['raw'],$value);
		else 
			add_settings_error('ll_tools',esc_attr('settings_updated'),$text."Werte sind nicht gespeichert!");
		
		return $oldvalue;
	} 
	if(($value != $oldvalue)) {
		$oldvalue['check'] = $value;
	}
	return $oldvalue;
}

function ll_tools_field_def_data_check($elements,$group,$text,&$field_def,$crm_set = "") {
	$group_def = "";
	$type = (empty($group)) ? 'group' : 'group/field';
	foreach($elements as $element) {
		$element_id = (empty($group)) ? $element['id']."A" : $element['custom_group_id']."B";
		$element_id = str_pad($element_id,3," ",STR_PAD_LEFT)."-".str_pad($element['id'],2," ",STR_PAD_LEFT)."-".$element['name'];
		$url = admin_url('admin.php?page=CiviCRM&q=civicrm/admin/custom/'.$type.'&reset=1&action=update&id='.$element['id']);
		$definition=array();
		if(isset($field_def['raw'][$element_id]))
			$definition = str_replace(" ","",$field_def['raw'][$element_id]);
		if(empty($definition) and isset($element['help_post'])) {
			preg_match('#<!--(\s*?)ll-tools(.*?)-->#', $element['help_post'], $definition);		
			$definition = str_replace(" ","",end($definition));				
		}
		if(empty($definition)) {
			$group_def .= '<a href="'.$url.'">'.$element['id'].":".$element['name'].'</a>=nicht definiert,';
			$field_def['raw'][$element_id] = "";
		} elseif($type=='group') {
			$group_def .= '<a href="'.$url.'">'.$definition.'</a>: ';
			$field_def['raw'][$element_id] = $definition; 
			$custom_fields = LL_Civi_Data::get_request(array('entity' => 'CustomField', 
						'params' => array('custom_group_id' => $element['id'],
									   'return' => "custom_group_id,name,help_post,option_group_id")),$crm_set);
			$group_def .= ll_tools_field_def_data_check($custom_fields,$definition,"Feld",$field_def);
		} else {
			$group_def .= '<a href="'.$url.'">'.$element['id'].":".$element['name']."</a>=".$definition.", ";
			$field_def['raw'][$element_id] = $definition; 
			$definition = explode(",",$definition);
			$field_def['fields'][$element['id']] = array('group' => $group,'field' => $definition[0]);
			if(isset($definition[1])) {
				if($definition[1] == 'Key') {
					$field_def['keys'][$group] = $definition[0];
					//Diese Angabe wird für die Definition der DataSets verwendet!
					$field_def['keys_options'][$element['option_group_id']] = "";
				} 
				if($definition[1] == 'Language') $field_def['languages'][$group] = $definition[0];
				if($definition[1] == 'Image') $field_def['images'][$group] = $definition[0].",".$definition[2].",custom_".$element['id'];
			}
		}
		if($type=='group') {
			$group_options[$element_id] = $group_def;
			$group_def = "";
		}

	}
	return ($type=='group') ? $group_options : $group_def;
}




########################## Update

add_filter('ll_tools_field_pre_text_civi_db_copy', function($text,$args) {
	$civi_db = $args['setting'];
	if(!isset($civi_db['checked'])) {
		$civi_db = ll_civi_db_copy($civi_db,$civi_db);
	}
	if(substr($civi_db['checked'],0,3) != "OK ") {
		$text .= '<p style="color:red;">Die Datenbankangaben für das Updating von CiviCRM sind nicht korrekt!<br>'.
			 $civi_db['checked'].'</p>';
	} else {
		$text .= '<p style="color:green;">'.$civi_db['checked'].'</p>';		
	}	
	return $text;
},10,2);


function ll_tools_settings_add_drop_tables($opt_values){
	$civi_db = ll_update_civicrm::ll_civi_data_check();
	if(($civi_db !== true) or LL_SETTINGS_SAVE) {
		$pre_text = '<p style="color:red;">'.$civi_db.'</p>';
		$opt_values['civi_db_drop_tables'] = array('title' => 'Sicherungsdatenbank',
													'pre_text' => $pre_text, 'type' => 'radio',
													 'options' => array('Wählen','Daten sind gültig','Daten löschen'));
	}
	return $opt_values;
}


add_filter('pre_update_option_'.LL_TOOLS_OPTION.'civi_db_copy','ll_civi_db_copy',10,2);

function ll_civi_db_copy($value,$oldvalue) {
	if(empty($oldvalue['checked']) or !empty(array_diff_assoc($value,$oldvalue))) {
		$test = ll_update_civicrm::ll_civi_createtest($value,array('plugin' => "civicrm/civicrm.php"));
		if($test === true) $test = "OK Datenbankzugriff erfolgreich geprüft am ".wp_date('j. F Y - G:i');
		$value['checked'] = $test;
		update_option(LL_TOOLS_OPTION.'civi_db_drop_tables',"Wählen");
	}
	return $value;
}

add_filter('pre_update_option_'.LL_TOOLS_OPTION.'clear_civi_cache',function($value,$oldvalue) {
	if($value) {
		$dirlist = array();
		ll_civi_deldir(dirname(CIVICRM_TEMPLATE_COMPILEDIR)."/templates_c/",$dirlist);
		ll_civi_deldir(dirname(CIVICRM_TEMPLATE_COMPILEDIR)."/ConfigAndLog/",$dirlist);		
	}
	return $oldvalue;
},10,2);


add_filter('pre_update_option_'.LL_TOOLS_OPTION.'clear_cache', 'll_del_trans',10,2);
		
?>
