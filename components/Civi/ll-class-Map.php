<?php
/* Enthält die Klasse für die Karte
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

//ll_tools_file_loader("crm","ll-map-default-settings");


	/**
	 * Basisklasse zur Aufbereitung der Kontaktdaten für Karten
	 * Singleton 
	 * 
	 * @since 
	 * ab 4.2.6 werden Abfrageparameter nur noch über get_civi_select() übernommen.
	 * ab 4.2.11 wird das MultiSet hier und nicht im Kontakt gebildet.
	 */

class LL_Civi_Map { 
	
	/* Zwischenspeicher für Civi-Sub-Kartendaten ab 4.2.6 */
	protected $civi_map_data;
	
	/* Array der Kontakte aus der Abfrage (bzw. Array der Subarrays) */
	protected $Contact;

	/* Übergebene StandardKartenObjekt */
	protected $view_map;
	
	/* Attribute der MapFlags aus dem Submit */
	protected $map_flags;
	
	/* Liste der Sets aus dem Submit */
	protected $map_sets;

	private static $_singleton;
	
	function __construct($map) {
		$this->view_map = $map;
		$this->map_flags = $this->view_map->get_submitted()['attr'];
		
//		$this->map_sets = $this->view_map->get_submitted()['sets'];
		foreach($this->view_map->map_sets as $set => $setdata)
			if(in_array($setdata['post'],ll_civi_map_optionset_types(array()))) $this->map_sets[$set] = $setdata;
	}

	private static function singleton($map){
		if(!isset(self::$_singleton))
			self::$_singleton = new LL_Civi_Map($map);
		return self::$_singleton;
	}


	/**
	 * Löst eine Datenabfrage und die Aufbereitung der Daten für die Karte aus.
	 *  
	 * @since 
	 *
	 * @param object $map Standard MapObject
	 *			 array $args Weitere Parameter
	 *
	 * Ab Version 4.2.6 entsprechen die Krtierien den Request-Paramtern.
	 */


	public static function make_map_data($map) {
		$civi_map = self::singleton($map);
	
		$kriterienSet = $civi_map->get_civi_select();
		if(empty($kriterienSet['sets'])) return array();
	
		//wird nur einmal zum Aufbau von $Contact aufgerufen
		$civi_map->make_civi_map_contact($kriterienSet);

		//wird nur einmal erzeugt und nur wenn output = 'map'
		$civi_map->make_CustomFlag_Groups();

		return $civi_map;
	}
	

	/**
	 * Bildet das RequestSet (KriterienSet) für die Civi-Abfrage aus den Submits und den DataSets
	 *  
	 * @since überarbeitet 4.2.6
	 *
	 * @param 
	 *
	 * Ab Version 4.2.6 entsprechen die Krtierien den Request-Paramtern.
	 */


	private function get_civi_select($args = array()) {
		$args = apply_filters('ll_civi_map_request',$args,$this->view_map->output);
		static $kriterienSet;
		if(isset($kriterienSet)) return $kriterienSet;
		$kriterienSet['params']['geo_code_1'] = array('<>' => "");
		$kriterienSet['params']['geo_code_2'] = array('<>' => "");
//		$kriterienSet['params']['api.CustomValue.get'] = array();
		if(!empty($args['params'])) 
			$kriterienSet['params'] = array_merge($kriterienSet['params'],$args['params']);

		$kriterienSet['return'] = array("geo_code_1","geo_code_2","city","tag");
		if(!empty($args['return'])) 
			$kriterienSet['return'] = array_merge($kriterienSet['return'],$args['return']);	

		$kriterienSet['params']['options']['limit'] = 400; //wichtig, sonst nur 25!
//		$kriterienSet = array();
		$submit = $this->view_map->get_submit();
		//Wenn $submit leer ist, sollte keine CiviCRM Abfrage enthalten sein!
		ll_crm_debug($submit);
		ll_crm_debug($this->map_sets);
		ll_crm_debug($this->map_flags);		
		ll_crm_debug($this->view_map->output);
		//Hier muß allerdings die set_uid ebenfalls ausgewertet werden und die steht nicht im optionset und auch nicht im set.
		//daher können die map_sets vorläufig nicht verwendet werden!
		
		############## Indiv. Auwahl
		foreach($submit as $var => $values) {
			//zweite Bedingung, damit nur Civi-Abfragen gefunden werden.
			if(!is_int($var) and strpos($var,"set_") === 0) {
				//set & tag
				$par = explode("_",$var);
				foreach($values as $value) {
					//0 & 10
					$field = explode("_",$value);
					ll_crm_debug(array($var,$values,$field));
					if(isset($field[1]))
						$kriterienSet['sets'][$field[0]]['params'][$par[1]]['IN'][] = $field[1];					
				}
			}
		}
		ll_crm_debug($kriterienSet);
		return $kriterienSet;
	}

	public function get_contact($id,$set = 0) {
//		ll_crm_debug(array($id,$set),true);
//		ll_crm_debug($this->Contact,true);
		return $this->Contact[$set."-".$id];
	}
	
	/**
	 * Ermittelt einen oder alle Sets, denen der Kontakt angehört
	 *  
	 * @since überarbeitet 4.2.6
	 *
	 * @param array $contact Datensatz aus CiviCRM
	 * 		 int $set DataSet dem dieser Datensatz angehört
	 *	       string $return 'single', Default gibt den ersten gefunden Wert zurück, sonst alle gültigen Sets mit line oder point
	 *
	 * Ab Version 4.2.6 entsprechen die Krtierien den Request-Paramtern.
	 */

	
	private function get_contact_type($contact,$return = 'first') {
//		ll_crm_debug(array($contact,$this->map_sets),!isset($contact['multi_set']));
		$sets = array();
		
		foreach($this->map_sets as $key => $map_set) {
			ll_crm_debug($map_set);
			if($map_set['data_set'] == $contact['multi_set']) {
				if($map_set['post'] == 'set_tag') {
					if(in_array(substr($map_set['tagname'],0,strpos($map_set['tagname']," (")),explode(",",$contact['tags'])))
						if($return == 'first') return $key;
				}elseif($map_set['post'] == 'set_contact_typ') {
					if($contact['contact_type'] == substr($map_set['tagname'],0,strpos($map_set['tagname']," (")))
						if($return == 'first') return $key;
				}
			} elseif($return == 'first') continue;
			
			//Geht von einem $map_sets aus, das bereits auf civi reduziert ist.
			foreach(array('line','point') as $type) {
				if(!empty($map_set[$type])) {
					$sets[$type][$key] = $map_set[$type];
					unset($map_set); //Damit ein Map_set Line nicht nochmal in den points auftaucht!
				} 			
			}
		}
		return $sets;
	}

	/**
	 * Bereitet die Kontaktdaten für die Karte auf und merged alle Abfragen.
	 *  
	 * @since überarbeitet 4.2.6,
	 * 4.2.11 die Kontakte werden hier je KriterienSet abgerufen und nicht mehr über Contact-MultiValues.
	 *
	 * @param array $kriterienSet
	 *
	 */
	
	private function make_civi_map_contact($kriterienSet) {
		//Daten bereits aufgebaut!
		if(isset($this->Contact)) return;
		$this->Contact = array();

		foreach($kriterienSet['sets'] as $set => $data_set) {
			$data_set = ll_civi_merge_request($kriterienSet,$data_set);
			$data_set['data_set'] = $set;
			$Contact = LL_Civi_Contact::get_data($data_set);
				
			if(!is_numeric($this->view_map->response['response'])) $this->view_map->response['response'] = 0;
			
			//Hier kommt die neue Felddefiniton!

			foreach($Contact as $entity_id => $data) {
				$this->view_map->response['response'] ++;

//				$data['multi_set'] = $ValueSet;
				$data['multi_set'] = $data_set['data_set'];
				$data['entity_id'] = $entity_id; //Kompatibilitätswert weil ident mit contact_id
				$data['id'] = $data['multi_set']."-".$data['entity_id'];
				$data['point_data'][] =array($data['geo_code_1'],$data['geo_code_2']);
//				$data['pos'] =array($data['geo_code_1'],$data['geo_code_2']);
//				$data['image'] = $data['image_URL']; //Kompatibilitätswert weil ident.
				$data['group'] = 'main';
				$data['type'] = $this->get_contact_type($data);

				// => Mainmarkers, hinterher, damit die childs bekannt sind!
				$this->Contact[$data['id']] = $data; //zur internen Speicherung.
				
				if($this->view_map->output['element'] == 'project') {
					$data['data'] = $this->make_civi_map_contact_subdata($data);
				}
			   $this->view_map->list_groupValues[$data['type']][$data['id']] = $data;
			}
		}
	}
	
	/**
	 * Baut die Sub-Kartendaten für CiviCRM auf.
	 *  
	 * @since komplett überarbeitet 4.2.6
	 *
	 * @param 
	 *
	 * Ab Version 4.2.6 entsprechen die Krtierien den Request-Paramtern.
	 * Daten für Linien und Punkte werden aufbereitet
	 * Mehrfacher Aufruf ist möglich, wobei nur fehlende Datenbereiche neu aufgebaut werden.
	 * 
	 */


	public function make_CustomFlag_Groups() {
		//Daten sind bereits vorhanden oder keine Kartenausgabe.
		if(isset($this->civi_map_data) or (!in_array($this->view_map->output['element'],array('map','print')))) return;
		
		$this->civi_map_data['line'] = array();
		foreach($this->Contact as $id => $data) {
			ll_crm_debug($id);
			$field_groups = $this->get_contact_type($data,'all');
			ll_crm_debug($field_groups);
			$childs = array();
			foreach(array('line','point') as $flagtyp) {
				if(isset($field_groups[$flagtyp])) 	
					foreach($field_groups[$flagtyp] as $field_group) {
						$method = 'get_CustomObjects_'.$flagtyp;
						$objectSet = LL_Civi_Contact::get_CustomFields($data['entity_id'],$data['multi_set'],$field_group);
						ll_crm_debug($objectSet);
						if(!empty($objectSet))
							$childs[$field_group] = $this->$method($objectSet,$field_group);				
					}				
			}
			ll_crm_debug($childs);
			if(in_array(1,$childs)) $data['childs'] = $data['id'];
			$this->view_map->makePointObjects(array($data),'main');
		}
		if(!empty($this->civi_map_data['line'])) $this->makeLineObjects();
//			ll_crm_debug($this->civi_map_data['point'],true); 		
	}

	/**
	 * Datensammlung für den Aufbau der Linien.
	 * Diese können nur mit vollständigen DataSets aufgebaut werden.
	 *  
	 * @since überarbeitet 4.2.6
	 *
	 * @param array $lineSet Daten aus den CustomFields
	 *			 array $contact_data Kontaktdaten zu Punktdefinition
	 *			 string $group fieldgroup
	 *        string $typ 'line'  
	 *
	 * Jeder Kontakt könnte mehrer Titel haben, daher Array!
	 * Hier wird je Set  aus Contact ein Set mit dem Key Title/Order/Elementnummer gebildet 
	 * wobei immer die Order des ersten Satzes je Titel aus dem Set verwendet wird, um nur eine Order je Hauptmarker zu haben.
	 * 
	 */


	private function get_CustomObjects_line($lineSet,$group) {
		ll_crm_debug(array($lineSet));
		foreach($lineSet as $lineKey => $lineValue) {
			$lineValue['id'] = $lineValue['multi_set']."-".$lineValue['entity_id'];
			ll_crm_debug($lineValue);
			$this->civi_map_data['line'][$group][$lineValue['title']][$lineValue['order']][$lineKey] = $lineValue;						
		}
		return false;
	}
	
	private function get_CustomObjects_point($pointSets,$group) {
//	   ll_crm_debug(array($pointSets,$contact,$group),($group=='Lines'));
		foreach($pointSets as $key => $pointSet) {
			ll_crm_debug($pointSet);
			if(!empty($pointSet['text'])) {
				ll_crm_debug($pointSet);
				$Set[$key]['group'] = $pointSet['multi_set']."-".$pointSet['entity_id'];
				$Set[$key]['type'] = $group;				
				$Set[$key]['point_data'] = $this->ll_pointsToArray($pointSet['text']);
				$Set[$key]['display_name'] = ll_map_make_subSetTypes($pointSet['multi_set'],$group); 
				$Set[$key]['image_URL'] = "";
				if(!empty($pointSet['image'])) {
					$Set[$key]['image_URL'] = content_url()."/uploads/civicrm/custom/".$pointSet['image'];
					ll_crm_debug($Set);	
				}
			}		
		}
		if(isset($Set)) {
			$this->civi_map_data['point'][$pointSet['multi_set']."-".$pointSet['entity_id']][$group] = $Set; //zur internen Speicherung.
			$this->view_map->makePointObjects($Set,'point');
			return true;
		}
		return false;
	}
	
	private function makeLineObjects() {
		$lineGroups = $this->civi_map_data['line'];
		ll_crm_debug($this->Contact);
		ll_crm_debug($lineGroups);
		// Lines ...
		foreach($lineGroups as $lineGroup => $lineTitles) {
			$lines = array();			

			ll_crm_debug(array($this->map_flags,$lineGroups,$lineTitles));

			$attr = $this->map_flags['line'][$lineGroup];
//			$line_groups[$lineGroup]['attr']['style'] = $attr;
			ll_crm_debug($attr);
			//A, B, C ...
			foreach($lineTitles as $lineTitle => $lineSet) {
				ksort($lineSet);
				//10, 20, 30, 40 ..
				foreach($lineSet as $order => $pointset) {
					// Es müssen immer alle Datensätze mit demselben Titel, und demselben Kontakt (main-marker) zusammengeführt werden.
					// Diese haben auf Grund der Aufbereitung je Contact immer die gleiche Order!
					ll_crm_debug($pointset);
					$data = current($pointset);
					$contact = $this->Contact[$data['id']];

					ll_crm_debug($data);
					$marker_point = array($contact['geo_code_1'], $contact['geo_code_2']);

					if(!empty($line)) {
						//Der letzte Punkt kommt vom nächsten Marker!
						$line['data'][] = $marker_point;
						$line['infowindow']['contact'][] = $contact;
						$lines[] = $this->make_line_window_values($line,$attr);												
						//Punkte vom letzten Durchlauf erzeugen
						if(!empty($line_point)) $line_points[] = $line_point;
						
						if(!empty($line_points)) {
						ll_crm_debug($line_points);
							$this->get_CustomObjects_point($line_points,$lineGroup);						
						}
					}
					$line = array();
					$line_points = array();
					$line_point = array();
									
					foreach($pointset as $data) {
						ll_crm_debug($data);
						if(!empty($line_point)) $line_points[] = $line_point;
						$line_point = array();
						$points = $this->ll_pointsToArray($data['text']);
						if(empty($line)) {
							$line['line_id'] = str_replace(" ","",$data['id']."-".$data['title']);
							$line['data'][] = $marker_point;
							$line['infowindow']['childs'] = '';
							$line['infowindow']['data'] = $data;
							$line['infowindow']['group'] = $lineGroup;							
							$line['infowindow']['contact'][] = $contact;
						} 
						if(!empty($points) and isset($this->map_flags['point'][$lineGroup])) {
						   ll_crm_debug($data);
							$line_point = $data;
						   $line_point['entity_id'] .= "-".str_replace(" ","",$data['title']);
						//	$line_point['title'] = $lineTitle." ".key($lineSet); //order
							ll_crm_debug($line_point['title']);
							$line_point['point_data'] = $points;
							$line['infowindow']['childs'] = $line['line_id'];
						}									
						if(!empty($data['text'])) $line['data'] = array_merge($line['data'],$points);
						ll_crm_debug($line['data']);
					}


				} //Set
				//Linie mit Endpunkt ohne Marker (es muß aber mindestens einen zusätzlichen Punkt geben!)
				//Der Endpunkt selbst wird nicht als Linie eingetragen!
				if(count($line['data']) > 1) {
					$lines[] = $this->make_line_window_values($line,$attr);
					// Wenn der letzte Punkt kein Marker ist, muß er hier entfernt werden, da er sonst doppelt vorkommt!
					if(!empty($line_point)) {
						ll_crm_debug($line_point);
						$line_point['point_data'] = array_slice($line_point['point_data'],-1);
						$line_points[] = $line_point;						
					}
				}
				if(!empty($line_points)) {
					$this->get_CustomObjects_point($line_points,$lineGroup);						
				} 
				ll_crm_debug($line);
				$line = array();
			} //Titles
			if(!empty($lines)) $line_groups[$lineGroup]['data'] = $lines;
		} //Groups
		ll_crm_debug($line_groups);
		$this->view_map->add_object_groupValues('line',$line_groups);

	} 

	private function make_line_window_values($line,$attr) {
		$data = $line['infowindow'];
		ll_crm_debug(array($data));
		if($attr['line_box'] == 'amappa') {
//			$values['display_name'] = $this->llGetOptions("Gruppo",$data['data']['type'],$data['data']['multi_set']);
			$values['display_name'] = ll_map_make_subSetTypes($data['data']['multi_set'],$data['data']['type']);
			if(isset($data['contact'][0])) $values['display_name'] .= ": <br>".$data['contact'][0]['city'];
			if(isset($data['contact'][1])) $values['display_name'] .= " - <br>".$data['contact'][1]['city'];
			$values['link'] = $data['data']['link'];
			$values['pos'] = $data['group'];
			$values['title'] = $data['data']['title'];
		} elseif($attr['line_box'] == 'tubo') {
			$values = array('display_name' => $data['data']['title'],'pos' => $data['group'],'childs' => $data['childs']);
		} else {
			unset($line['infowindow']);
			return $line;
		}
//		$line['infowindow'] = str_replace(array("'",'"'),"@@",getInfoContent($values));
		$line['infowindow'] = getInfoContent($values);
		return $line;		
	}
		
	/******* Hilfsfunktionien für make_markers
	*/
	private function ll_pointsToArray($points,$array = true) {
		//für den Fall, dass es zwischen den Elementen space, tab, lf, cr gibt ...
		$points = str_replace(array(chr(32),chr(9),chr(10),chr(13)),"",$points);
		if($points != "") {
			$trans = array("],[" => "];[");
			$apoints = explode(";",strtr($points,$trans));
			if(!empty($apoints)) {
				for($i = 0; $i < count($apoints); $i++) {
					$chars = count_chars($apoints[$i]);
					ll_crm_debug($chars);
					//Ohne diese Prüfung gibt es einen Fehler im Script und es wird nichts angezeigt!
					if(($chars[ord(',')] != 1) or ($chars[ord('.')] != 2)) {
						ll_crm_debug(array("Fehler bei Punktdefinition",$apoints[$i],"Punkt wird entfernt"),'print');
						unset($apoints[$i]);
					}
					if($array) $apoints[$i] = explode(",",substr($apoints[$i],1,-1));
				}			
			}
		} else {
			$apoints = array();
		}
		ll_crm_debug($apoints);
		return $apoints;
	}		

	/**
	 * Bereitet die CustomFields für die Ausgabe auf der Projektseite auf.
	 *  
	 * @since überarbeitet 4.2.6
	 *
	 * @param array $data ausgelesene und ergänzte Kontaktdaten
	 *
	 * 
	 */

	private function make_civi_map_contact_subdata($data) {
		$SubValue = LL_Civi_Contact::get_CustomFields($data['entity_id'],$data['multi_set']);
		ll_crm_debug($SubValue);
		$set = key($this->map_sets);
		ll_crm_debug(array($this->map_sets,$set));
		
		$show_set = array();
		if(LL_TOOLS_CIVICRM and current_user_can('edit_pages')) 
			$show_set["9999-01-15"] = array('type' => 'text','title' => '',
									'html' => '<div><a href="'.admin_url('admin.php?page=CiviCRM&q=civicrm/contact/view&reset=1&cid='.$data['entity_id']).'">Edit Data in civicrm</a></div>');

		$show_set["9999-01-10"] = array('type' => 'text', 'title' => 'Indirizzo',
						'html' => "<a title='evidenza sulla mappa' href='".
							$this->view_map->get_LinkUrl(array('id' => $data['id'], 'type' => $set ))."'>".
							$data['postal_code']." ".$data['city']."</a>");
						
		if(isset($data['api.Website.get']['values'][0]['url'])) {
			$website = $data['api.Website.get']['values'][0]['url']; 
			$show_set["9999-01-01"] = array('type' => 'html', 'title' => 'Homepage', 
													'text' => "<a target='_blank'	href='".$website."'>".$website."</a>");
		}

		foreach($SubValue as $type => $DataSet) {
			if(in_array($type,array('Foto','Desc','Lines','Camp'))) {
				foreach($DataSet as $key => $show) {
					$show['project_type'] = ll_map_make_subSetTypes($data['multi_set'],$type);

					if(!empty($show['image'])) $show['img'] = content_url()."/uploads/civicrm/custom/".$show['image'];
					if(in_array($type,array('Foto','Desc')) and empty($show['html'])) $show['html'] = $show['text'];
					if(!empty($show['date'])) {
						$date = $show['date'];
						$show['date'] =	date("d.m.Y",strtotime(substr($show['date'],0,10)));
					} else {
						$date = '2018-01-31';						
					}
					if($show['title'] == '') $show['title'] = $show['date'];
					//HaZa, 16.09.2018, Ergänzung notwendig, damit zwei Einträge mit demselben Datum angezeigt werden können.
					//25.08.2019 verschoben.
					if(!empty($show['html']) or !empty($show['img'])) {
						if(isset($show_set[$date.$show['order']])) {
							$show_set[$date.$show['order']]['img2'][] = $show;
						} else {
							$show_set[$date.$show['order']] = $show;							
						}											
					}
				}
			}
			if($type == 'News') {
				foreach($DataSet as $key => $show) 
				$show_set = LL_crm_SubData_News($show_set,
							array( 'p' => $show['order'],
								   'post_type' => 'post',
								   'posts_per_page' => 1
							   ),
							array('type' => $type, 'link' => $show['link']));					
			}

		} //Ende SubValues

		$show_set = $this->make_civi_map_contact_news($show_set,
									array( 'meta_key' => 'project',
					   						'meta_value' => $data['entity_id'],
					   						'meta_compare' => 'LIKE'
					   				));

		krsort($show_set);
		return $show_set;
	}	


	private function make_civi_map_contact_news($show_set,$args = array(),$DataSet = array('type' => 'News', 'link' => "")) {
		$news = new WP_Query( $args );	
		if($news->have_posts()) 
		while ($news->have_posts()) {
			$news->the_post();
			//if($DataSet[$field_def['date']] == "") $DataSet[$field_def['date']] = get_the_time();
			$show['type'] = $DataSet['type'];
			$show['html'] = "<h3><a href='".get_permalink()."'>".get_the_title()."</a></h3><br/>";
			$show['title'] = get_the_time("d.m.Y");
			$show['link'] = $DataSet['link'];
			$rm_text = "Weiterlesen";
			$show['html'] .= "<p>".get_the_excerpt()."</p>";
			if(function_exists("pll__")) $rm_text = pll__('Leggi di più');
			$show['html'] .= "<p style='text-align:right;'><a href='".get_permalink()."' >".$rm_text."</a></p>";

			$show['img_src'] = get_the_post_thumbnail();
			//$show['text'] .= get_the_content("Weiter lesen ...",true);;;
			$show_set[get_the_time("Y-m-d").$news->ID] = $show;
		}
		return $show_set;
	}
}

?>
