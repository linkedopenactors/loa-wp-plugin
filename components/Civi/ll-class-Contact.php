<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


	/**
	 * Basisklasse für den Zugriff auf Kontaktdaten vom CivCRM
	 * Singleton 
	 * 
	 * @since 4.0.7, überarbeitet 4.2.6
	 * 4.2.11 Multi-Data-Funktionaliät aufgelöst und nach civi-Map übertragen. Dafür kann es mehrere Instanzen geben.
	 *
	 * Notwendige TESTs:
	 * Individueller Punkt
	 * Wird neues Bild auch wirklich als filename gespeichert?
	 * Kann ohne DataSource/Set gearbeitet werden?
	 */


class LL_Civi_Contact {
	/*prefix für die Felder */
	protected $field_key;

	/* Speichert die Initialisierungs/Standardabfrage */
	private $request;

	/* SetNummer der Datenquelle (ev. ident mit dem Key der singleton-Instanz) */
	private $data_source;
	
	/* Speichert die Daten je DataSet */
	private $data;
	
	/* Werte aus field_def */
	protected $field_def;
	
	/* FeldGruppen aus den CustomValues */
	protected $customGroupValues;
	
	/* Gewünschte Sprache für die indiv. Felder */
	protected $language;
		
	private static $_singleton;

	/**
	 * 
	 * @since 4.0.7, überarbeitet 4.2.6
	 *
	 * @param array $args
	 *			 array 'params' (wenn "return" enthalten, werden die Werte überschrieben)
	 *			 array 'return' (ergänzt Basiswerte oder solche die in $params bzw. $request enthalten sind) 
	 *			 array 'request' (wenn 'params' enthalten, werden die Werte überschrieben)
	 *        array 'sets' enthält die Abfragekriterien für jedes Sets, wenn es mehrere gibt, 
	 *							  Element 'params', 'return', 'request', 'version' 
	 *        int 'data_set' default = 0, Instanz von Contact. Damit können mehrere Instanzen gehalten und angesprochen werden.
	 *        int 'data_source', bei sets die SetNummer
	 *			 int 'version', Version der Api-Schnittstelle
	 */
	
	function __construct($args) {
		$this->request['entity'] = 'Contact';
		$this->request['action'] = 'get';
		$this->request['params'] = array('do_not_trade' => 0);
		$this->request['params']['return'] = array("contact_type","display_name","last_name","first_name","sort_name",
																"image_URL","country");			
			
		$this->request = ll_civi_merge_request($this->request,$args);

		$this->field_key = 'llcivi';

		$this->data_source = (isset($args['data_source'])) ? $args['data_source'] : $args['data_set'];

		$this->field_def = get_option(LL_TOOLS_OPTION.'field_def_'.$this->data_source);

		//Wird für die Auswahl der richtigen Custom-Fields benötigt
		if(LL_TOOLS_POLYLANG) 
			$this->language = pll_current_language( 'slug' );

	}
			
		
	private static function singleton($args = array(),$new = false){
		if(!isset($args['data_set'])) $args['data_set'] = 0;
		if(!isset(self::$_singleton[$args['data_set']]) or $new)
			self::$_singleton[$args['data_set']] = new LL_Civi_Contact($args);
		return self::$_singleton[$args['data_set']];
	}

	/**
	 * Liest die Felddefinition aus, bzw. erzeugt sie.
	 *  
	 * @since 4.2.8
	 *
	 * @param 
	 *
	 * 
	 */

	private function rendering_def($element) {
		$header = LL_classes_rendering::check_field_def($this->field_key);
		if(($header === false) or isset($_GET['rendering_def'])) {
			$fields = array_keys($element);
			LL_classes_rendering::make_row_def($this->field_key,$fields,array());		
		}
	}
	

	/**
	 * Abruf der Daten
	 * 
	 * @since 4.0.7, überarbeitet 4.2.6
	 * 4.2.11 multi-data aufgelöst, es gibt nur noch einfache Abfragen.
	 *
	 * @param 
	 *
	 * Gibt ein Array mit den gefunden Kontakten und Werte zurückgegeben
	 */


	public static function get_data($args,$new = false) {
		$Contact = self::singleton($args,$new);
		if(empty($Contact->data)) {
			$Contact->data = 
				apply_filters('LL_civi_Data_result',
					LL_Civi_Data::get_request($Contact->request,$Contact->data_source),$args,$Contact->data_source);
			//lHier sollte noch der Fehler ausgegeben werden?
			if(!LL_Civi_Data::error() and !empty($Contact->data))
				$Contact->rendering_def(current($Contact->data));
		}

		return $Contact->data; 
	}
	
	/**
	 * Abruf der Individuellen Felder
	 * 
	 * @since 4.0.7, überarbeitet 4.2.6
	 *
	 * @param int $contact_id
	 * 		 int $set = dataSource
	 *		    string $field_group 
	 *
	 * Gibt ein Array mit den strukturierten Daten der gewünschten FeldGruppe zurück
	 * 
	 */
	
		
	public static function get_CustomFields($contact_id,$set = 0,$field_group = '') {
		if(!isset(self::$_singleton[$set])) return array();
		$Contact = self::$_singleton[$set];

		//wird immer einmal für alle erstellt ...
		$customValues = $Contact->make_customGroupValues($contact_id,$set);
		if(!empty($field_group)) {
			if(isset($customValues[$field_group]))
				return $customValues[$field_group];
			else
				return array();		
		} 
		
		return $customValues;
	}

	/**
	 * Baut aus den ausgelesenen indiv. Feldern Datenstruktur auf Basis FieldDef
	 * 
	 * @since 4.0.7, überarbeitet 4.2.6
	 * 4.2.11 Multi_data-Funktionalität herausgenommen. Param $data_source entfällt.
	 *
	 * @param 
	 *
	 */

	private function make_customGroupValues($contact_id) {
		if(!isset($this->customGroupValues[$contact_id])) {
			$this->customGroupValues[$contact_id] = array();
			if(!empty($this->field_def['fields'])) {
				$this->customGroupValues[$contact_id] = $this->custom_fields_value($contact_id,$this->field_def);
			}
		}	
		return $this->customGroupValues[$contact_id];	
	}

	/**
	 * Aufbau der FieldDef-Struktu für einen Contact
	 * 
	 * @since 4.0.7, überarbeitet 4.2.6
	 *
	 * @param array $value api.CustomValue.get für einen Contact
	 *			 array $field_def DefinitionsSet
	 *
	 * ab 4.2.6 wird hier auch die Feldgruppe aus dem key augelesen, wenn er definiert ist.
	 * ab 4.2.6 werde die CustomValues hier abgefragt statt über api.CustomValue.get
	 * ab 4.2.11 entfällt der Parameter $data_source.
	 * ab Api4 könnte hier direkt der Table abgefragt werden, dann braucht es die Konvertierung in der Form nur noch eingeschränkt.
	 */
			
	private function custom_fields_value($contact_id,$field_def) {				
		$values = LL_Civi_Data::get_request(array('entity' => 'CustomValue',
															  'params' => array('entity_id' => $contact_id)),$this->data_source);

		if(empty($values)) return array();		

		$CustomValue = array();
		ll_crm_debug($values);
		foreach ($values as $valuekey => $valuevalue) {
			if(!isset($field_def['fields'][$valuevalue['id']])) continue;
			$field = $field_def['fields'][$valuevalue['id']];
			foreach ($valuevalue as $subkey => $subvalue) {
				ll_crm_debug($valuevalue);
				if(is_integer($subkey)) { 
					//zweite Bedingung, weil es nicht definierte Felder geben könnte.
					$CustomValue[$field['group']][$subkey][$field['field']] = $subvalue;
					//Versuch den späteren Zugriff zu vereinfachen.
					$CustomValue[$field['group']][$subkey]['entity_id'] = $contact_id;
					$CustomValue[$field['group']][$subkey]['multi_set'] = $this->data_source;
				}
			}
		}
		ll_crm_debug($CustomValue);
		ll_crm_debug($field_def);
		
		if(!empty($this->language) and !empty($field_def['languages'])) {
			foreach($field_def['languages'] as $group => $key) {
				if(!empty($CustomValue[$group]))
					foreach($CustomValue[$group] as $CustomValueGroupKey => $CustomValueGroup) { 
						//ll_crm_debug($CustomValueGroup[$key],true);
						if(isset($CustomValueGroup[$key]) and !in_array($CustomValueGroup[$key],array('*',$this->language)))
							unset($CustomValue[$group][$CustomValueGroupKey]);
						}
			}			
		}

		if(!empty($field_def['images']) ) {
			foreach($field_def['images'] as $group => $key) {
				if(!empty($CustomValue[$group]))
					foreach($CustomValue[$group] as $CustomValueGroupKey => $CustomValueGroup) { 
						$keys = explode(",",$key);
						ll_crm_debug($keys);
						ll_crm_debug($CustomValueGroup);
						if(empty($CustomValueGroup[$keys[0]]) and !empty($CustomValueGroup[$keys[1]])) 
							$CustomValue[$group][$CustomValueGroupKey][$keys[0]] = $this->custom_fields_images($CustomValueGroup['file'],
							 			$value['values'][0]['entity_id'],$keys[2].":".$CustomValueGroupKey);
					}
			}			
		}
		
		//Dies muß am Ende stehen, da hier die Gruppen geändert werden!
		if(!empty($field_def['keys']))
			foreach($field_def['keys'] as $group => $key) {
				if(!empty($CustomValue[$group])) {
					foreach($CustomValue[$group] as $CustomValueGroupKey => $CustomValueGroup) { 
						$CustomValue[$CustomValueGroup[$key]][$CustomValueGroupKey] = $CustomValueGroup;
					}
					unset($CustomValue[$group]);				
				}
			}			
		ll_crm_debug($CustomValue);
	 	return $CustomValue;
	}
	
	private function custom_fields_images($file_id,$project_id,$set_id) {
		if(empty($file_id)) return;
		$files = LL_Civi_Data::get_request(array('entity' => 'File','params' => array('id' => $file_id)),$this->data_source);
		$file_path = $files[$file_id]['uri'];
		ll_crm_debug($file_path);	

		ll_crm_debug(array($file_id,$project_id,$set_id,$file_path));

		LL_Civi_Data::get_request(array('entity' => 'CustomValue', 'action' => 'create',
							'params' => array('entity_id' => $project_id,$set_id => $file_path)),$this->data_source);

		return $file_path;

	}
}

?>
