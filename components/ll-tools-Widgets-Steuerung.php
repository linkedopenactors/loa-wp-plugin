<?php
/* Enthält eine Sammlung von Steuerungsfunktionen für Widgets ...
Component Name: Übersteuerungsmöglichkeit für die Inhalte diverser Widgets
Component Multisite: Both

*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

ll_tools_make_menu(array('site_title' => 'Widget', 'page' => 'widget', 'network' => false));

function ll_tools_settings_widget_cb() {
	echo 'Wird eine der untenstehenden Funktionen aktiviert gibt es bei den Widgets das Feld "Field Selection".<br>';
	echo 'Dort ist das Feld einzutragen, in dem das System die Steurung für das Widget auf der jeweiligen Seite auslesen soll.<br>';
	echo '<br>';
	echo '0 = Menu (und/oder Kategorie wenn Element 1 LEER oder nicht definiert ist).<br>';
	echo '1 = Kategorie (wenn leer = Kategorie = Menu) Mehrere Kategorien sind durch „,“ zu trennen für „oder“, mit „+“ wenn beide eingetragen sein müssen.<br>';
	echo '2 = Sidebar (wenn leer, keine Einschränkung), Überschrift aus dem Widget-Admin-Bereich, Eingetragener Titel für post_widget!<br>';
	echo '<br>';
	echo '<pre>ll-tools</pre>';
	echo 'Abhängig vom Widget wird nach einem Menu=ll-tools oder nach einer Kategorie=ll-tools gesucht';
	echo '<pre>ll-tools;ll-tools+allgemein</pre>';
	echo 'Es wird nach einem Menu "ll-tools" gesucht, dargestellte Posts müssen in der Kategorie "ll-tools" UND "allgemein" sein.';
	echo '<pre>ll-tools;;Main Sidebar</pre>';
	echo 'Es wird nach einem Menu, bzw. einer Kategorie "ll-tools" gesucht, aber nur, wenn sich das Widget in der Sidebar "Main-Sidebar" befindet.';
}

function ll_tools_widget_settings_init() {
	#### Widget - Settings
		
	$default_args = array('group' => 'widget','title' => "Widget Options");
	$opt_values = array('widget_menu' => array('title' => 'Menu-Widget','description' => 'Sucht das in der Seite eingetragene Menu oder beim Beitrag, ein Menu, dass einer Kategorie des Beitrages entspricht.'),
				  		'widget_page' => array('title' => 'Page-Widget','description' => 'Es werden alle Subseiten angezeigt oder die der gleichen Ebene, wenn es sich um eine Subseite handelt, bei Posts wird die Basisseite über die Kategorie im Feld Widget gesucht.'),
						'widget_post' => array('title' => 'Post-Widget','description' => 'Die angezeigten Posts werden auf die Kategorie eingeschränkt, die in der Seite oder einer Elternseite im Feld "Widget eingetragen ist."'),
						'widget_cat' => array('title' => 'Kategorie-Widget','description' => 'Es werden alle Subkategorien der in der Seite oder einer Elternseite im Feld "Widget" eingetragenen Kategorie angezeigt'),
						'widget_pages' => array('title' => 'Seitenauswahl','description' => 'Beim Widget können jene Seiten-IDs eingetragen werden, auf denen das Widget dargestellt werden soll'));
	ll_tools_make_section($default_args,$opt_values);
}

add_action('load-ll-tools_page_ll_tools_widget','ll_tools_widget_settings_init');
add_action('load-options.php','ll_tools_widget_settings_init');


//Fügt ein neues Feld in die definierten Widgets ein, beispielsweise um das Feld zur Widgetsteuerung zu definieren!
function ll_tools_in_widget_form($widget, $return, $instance)  {
	ll_crm_debug(array($widget,$return,$instance));
	$menus = array('recent-posts');
	if(get_option(LL_TOOLS_OPTION."widget_menu") == 'on') $menus[] = 'nav_menu';
	ll_crm_debug(array($menus,$widget->id_base));
	if(in_array($widget->id_base,$menus)) {
		$ll_field = esc_attr( $widget->id . '_field_choice' );
		$ll_out = '<p><label for='.$ll_field.'>'.esc_html__( 'Fieldname for Restrictions');
		$ll_out .= '<input type="text" class="widefat" id="'.$ll_field.'" name="'.$ll_field.'" value="'.esc_attr($instance['field_choice']).'"/>';
		$ll_out .= '</label></p>';
		echo $ll_out;
	}
//	if(get_option(LL_TOOLS_OPTION."widget_pages") == 'on') { //muß das wirklich sein?
		if(in_array($widget->id_base,array('nav_menu','pages'))) {
			$ll_field = esc_attr( $widget->id . '_page_choice' );
			$ll_out = '<p><label for='.$ll_field.'>'.esc_html__( 'Page-IDs (include)');
			$ll_out .= '<input type="text" class="widefat" id="'.$ll_field.'" name="'.$ll_field.'" value="'.esc_attr($instance['page_choice']).'"/>';
			$ll_out .= '</label></p>';
			echo $ll_out;
		}
//	}
}
if((get_option(LL_TOOLS_OPTION."widget_menu") == 'on') or 
	(get_option(LL_TOOLS_OPTION."widget_page") == 'on') or
	(get_option(LL_TOOLS_OPTION."widget_post") == 'on') or
	(get_option(LL_TOOLS_OPTION."widget_pages") == 'on') or
	(get_option(LL_TOOLS_OPTION."widget_cat") == 'on')) 
		add_action('in_widget_form','ll_tools_in_widget_form',10,3);

// Mit dieser Funktion wird der Wert auch tatsächlich abgespeichert.
function ll_tools_widget_update_callback($instance, $new_instance, $old_instance, $widget ) {
	$fields = array('_field_choice','_page_choice');
	foreach($fields as $field) {
		$key = $widget->id . $field;
		if ( ! empty( $_POST[ $key ] )) { 
			$instance[substr($field,1)] = $_POST[$key];
		} else {
			unset( $instance[substr($field,1)] );
		}		
	}
	return $instance;	
}

add_filter( 'widget_update_callback', 'll_tools_widget_update_callback', 10, 4 );


function ll_tools_page_choice($page_choice) {
	if(empty($page_choice)) return true;
	
	$page_list = explode(",",$page_choice);
	ll_crm_debug(array(get_the_ID(),$page_list));
	if(in_array(get_the_ID(),$page_list)) return true; 

	foreach($page_list as $page) {
		ll_crm_debug($page);
		if(substr($page,-1,1) == '*') {
			ll_crm_debug(substr($page,-1,1));
			if(ll_tools_get_root_parent(NULL,substr($page,0,-1))) return true;
		}
	}
}

/******** Steuert die Auswahl eines Menus im MenuWidget durch Feldeintrag "Widget" */
function ll_tools_menu_widget($args,$menuObj,$widget, $instance) {
	//Mit Hilfe von $widget könnte auf eine bestimmte Sidebar abgestellt weren, mit Hilfe von $instance auch auf ein bestimmtest Widget derselben Klasse!
	ll_crm_debug($widget);
	ll_crm_debug($instance);
	if(!ll_tools_page_choice($instance['page_choice'])) echo '<style>#'.$widget['widget_id'].'{display:none;}</style>';

	//Standardabwicklung, wenn keine Widgetfeld gesetzt wurde!
	if(!isset($instance['field_choice'])) return $args; 
	$menus = explode(";",get_post_meta(get_the_ID(), 'Widgets',true));
	ll_crm_debug($menus,true);
	//Element 2 = Name der Sidebar!
	if(!empty($menus[2]) and ($menus[2] != $widget['name'])) return $args;
	
	$type = get_post_type(); //vielleicht nicht die richtige Bedingung, weil sie sich nicht auf den Query bezieht.
	ll_crm_debug($type);
	if($type == 'page') {
		//Hier könnte direkt noch das MenuObjekt erzeugt und geändert werden ...
		ll_crm_debug(array('page',$menus));
		$menu = wp_get_nav_menu_object( $menus[0] );		
	} else {
		$menus = get_the_category();
		//Funktioniert zwar, zeigt aber nicht die eigene Seite, wenn Sie nicht im Menu ist!
		foreach($menus as $menu) {
			ll_crm_debug(array('post',$menu->to_array()['slug']));			
			$menu = wp_get_nav_menu_object( $menu->to_array()['slug'] );
			if(is_object($menu)) break;		
		}
	}

	//Es ist auf diese Art nicht möglich, kein Menu auszugeben, weil das nachfolgende Objekt jedenfalls versucht eines zu finden!
	//Siehe include/nav_menu_template.php => Daher dieser Weg!
	if(!is_object($menu)) echo '<style>.widget_nav_menu{display:none;}</style>';
	
	$args['menu'] = $menu;
	ll_crm_debug($args);
	return $args;
}
if((get_option(LL_TOOLS_OPTION."widget_menu") == 'on') or 
   (get_option(LL_TOOLS_OPTION."widget_pages") == 'on'))
		add_filter('widget_nav_menu_args', 'll_tools_menu_widget', 10,4);

function ll_tools_get_metakeys($meta_key) {
	static $meta_key_list = array();
	if(isset($meta_key_list[$meta_key])) return $meta_key_list[$meta_key];
	
	$ll_pages = get_pages(array('meta_key' => $meta_key));
	foreach($ll_pages as $ll_page) {
		ll_crm_debug($ll_page->post_title);
		$meta_key_list[$meta_key][$ll_page->ID]['text'] = get_post_meta($ll_page->ID, $meta_key,true);
		$meta_key_list[$meta_key][$ll_page->ID]['page'] = $ll_page;
	}
	return $meta_key_list[$meta_key];
}

function ll_tools_get_root_parent($post = NULL,$parent = NULL) {
	if(empty($post)) $post = get_post();
     while ($post->post_parent != 0) {
     	$post = get_post($post->post_parent);
     	if(!empty($parent) and $parent == $post->ID) break;
     }
	ll_crm_debug($post);
	return (empty($parent)) ? $post->ID : ($post->ID == $parent);
}


function ll_tools_search_parent_page($field_choice = NULL, $ll_id = NULL) {
	if(empty($field_choice)) $field_choice = "Widgets";
	if(empty($ll_id)) $ll_pages['post_parent'] = get_the_ID();
	if(!empty($ll_id)) $ll_pages['post_parent'] = $ll_id;
	do {
		$ll_pages = get_post($ll_pages['post_parent'],ARRAY_A);
		ll_crm_debug($ll_pages);
		$cats = explode(";",get_post_meta($ll_pages['ID'], $field_choice,true));
		ll_crm_debug($cats);
	} while (($ll_pages['post_parent'] <> 0) and empty($cats[0]));
	return $cats;
}

function ll_get_page_by_slug($slug,$params = array()) {
	$params = array_merge(array('post_type' => array('post','page')),$params,array('name' => $slug));
	$query = new WP_Query( $params );
	ll_crm_debug($query);
	//Liefert ein Warning, wenn nicht exisitiert!
	return $query->posts[0];
}

function ll_tools_search_post_page() {
	static $ll_page_ids;
	ll_crm_debug($ll_page_ids);
	if(isset($ll_page_ids)) return $ll_page_ids;
	$ll_page_ids = array(-1); //Diese ID soll nicht gefunden werden, und dient nur dazu, keine Daten anzuzeigen, wenn die Liste leer bleibt.
	$cat_search=array();
	if(is_category()) {
		ll_crm_debug(get_query_var('category_name'));
		$menus[] = get_query_var('category_name');
	} else {
		$menus = get_the_category(); //Categoryobjekte
	}
	ll_crm_debug($menus);
	foreach($menus as $menu) {
		$cat_search = array();
		if(is_object($menu)) $menu = $menu->to_array()['slug'];
		$ll_page = ll_get_page_by_slug($menu);	
		if(is_object($ll_page)) {
			$ll_page_ids[] = $ll_page->ID;
		} else {
			$cat_search[] = $menu;
		}
	}
	ll_crm_debug($ll_page_ids);
	foreach($cat_search as $cat_slug) {
		$ll_pages = get_pages(array('meta_key' => 'Widgets', 'meta_value' => $cat_slug));
		ll_crm_debug($cat_slug);
		if(empty($ll_pages)) {
			// Diese Suche ist notwendig, weil get_pages keine Suche über Teilstrings in meta_values zuläßt!
			$page_list = ll_tools_get_metakeys('Widgets');
			foreach($page_list as $ll_page => $text) {
				ll_crm_debug($text['text']);
				if((strpos($text['text'],$cat_slug) !== false) and 
					(substr($text['text'],strpos($text['text'],$cat_slug)-1,1) != "+")) {
						ll_crm_debug($text);
						$ll_page_ids[] = $text['page']->ID;
				}
			}
		} else {
			$ll_page_ids[] = current($ll_pages)->ID;
			ll_crm_debug($ll_page_ids);
		}
	}
	return $ll_page_ids;
}


/******** Steuert die Auswahl von Seiten für das PageWidget durch Feldeintrag "Widget" */
function ll_tools_page_widget($args,$instance) {
	ll_crm_debug(get_post_type());
	$found = false;
	$page_name = array();
	if(isset($instance['page_choice']) and !ll_tools_page_choice($instance['page_choice'])) {
		$args['post_status'] = 'future'; //damit nichts gefunden wird!
	} 

	if(get_post_type() == 'page') {
		$ll_pages = get_post(0,ARRAY_A);
		ll_crm_debug($ll_pages);
		$ll_parent = $ll_pages['post_parent'];
		$ll_page = $ll_pages['ID'];
		ll_crm_debug($ll_pages['post_parent']);
		if($ll_parent <> 0) {
			$ll_page = $ll_parent;
		}
		$found = true;
		ll_crm_debug(get_post());
		$args['child_of'] = $ll_page;		
	} else {
		$ll_page_ids = ll_tools_search_post_page();
		ll_crm_debug($ll_page_ids);
		$ll_parent = 0;
		$args['include'] = implode(",",$ll_page_ids);
		ll_crm_debug($args);	
	}
	//Fügt ein übergeordnetes Element in die Liste!
	if($ll_parent <> 0) {
		$args['title_li'] = "<a href='".esc_url(get_permalink($ll_page))."'>".get_the_title($ll_page)."</a>";
	}

	ll_crm_debug($args);
	return $args;
}

if(get_option(LL_TOOLS_OPTION."widget_page") == 'on') add_filter('widget_pages_args', 'll_tools_page_widget',10,2);


/******** Steuert die Auswahl von Posts für das PostsWidget durch Feldeintrag "Widget" */
function ll_tools_posts_widget($args,$instance) {
	ll_crm_debug($instance);
//	ll_crm_debug(get_post()->post_name);
	$cats = array();
	$ll_page = 0;
	$field_choice = "Widgets"; 
	if(!empty($instance['field_choice'])) $field_choice = $instance['field_choice'];
	if(is_category() or (get_post_type() != 'page')) {
		$ll_page_ids = ll_tools_search_post_page();
		ll_crm_debug($ll_page_ids);
		if(count($ll_page_ids) == 2) {
			$ll_page = $ll_page_ids[1];
		} else {
			ll_crm_debug(array(is_category(),$field_choice));
			if(is_category() and ($field_choice == "Widgets") and (count($ll_page_ids) == 1)) {
				$args['category_name'] = get_query_var('category_name');
			} else {
				//Auf einer Beitragsseite ohne gefundene Seite wird nichts angezeigt!
				$args['category_name'] = "xxx";				
			}
			return $args;		
		}
	} 
	ll_crm_debug($field_choice);
	$cats = ll_tools_search_parent_page($field_choice,$ll_page);
	if(!empty($cats[2]) and ($cats[2] != $instance['title'])) return $args;
	if(empty($cats[1])) $cats[1] = $cats[0];
	if(empty($cats[1]) and ($field_choice != "Widgets")) $cats[1] = "xxxx";
	if(strpos($cats[1],":") > 0) {
		$cats = explode(":",$cats[1]);
		$args['category_name'] = strtolower($cats[0]);					
		$args['cat'] = array_slice($cats,1);
	} else {
		$args['category_name'] = strtolower($cats[1]);			
	}
	if(empty($args['category_name'])) $args['category_name'] = get_post($ll_page)->post_name;

	ll_crm_debug($args);
	return $args;
}

if(get_option(LL_TOOLS_OPTION."widget_post") == 'on') add_filter('widget_posts_args', 'll_tools_posts_widget',10,2);

/******** Steuert die Auswahl von Kategorien im CategoriesWidget durch Feldeintrag "Widget" */
function ll_tools_cat_widget($args) {
	if(get_post_type() == 'page') {
		$cats = ll_tools_search_parent_page();
		ll_crm_debug($cats);
		if(!empty($cats[0])) {
			if(empty($cats[1])) $cats[1] = $cats[0];

			$cat = explode("+",$cats[0]);
			$args['child_of'] = get_category_by_slug($cat[0])->term_id;
			ll_crm_debug(get_category_by_slug($cat[0]));
			ll_crm_debug($args); 			
		}
	}
	ll_crm_debug(array(get_post_type(),$cats));
	if((get_post_type() == 'post') or empty($cats[0])) 
		echo '<style>.widget_categories{display:none;}</style>';
	//$args wird von wp_list_categories ausgewertet.
	return $args;
}

if(get_option(LL_TOOLS_OPTION."widget_cat") == 'on') add_filter('widget_categories_args','ll_tools_cat_widget',10);

?>
