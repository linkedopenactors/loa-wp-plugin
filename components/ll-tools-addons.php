<?php
/* Enthält eine Sammlung von Steuerungsfunktionen für Mailings und SociaMedia ...
Component Name: Standardkomponenten die nicht geschaltet werden können.
Component Multisite: Both; //Macht die Komponente unsichtbar.
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

// Setzt die Metadaten für das Auslesen des richtigen Fotos zur Seite, z.B. beim Posten in Facebook. Das erste Foto wäre sonst das Logo!
function ll_tools_social_meta() {
	echo '<meta property="og:image" content="'.get_the_post_thumbnail_url().'">'; 
}
add_action('wp_head', 'll_tools_social_meta');


// ermöglicht das Verwenden von &nbsp; im Text
function ll_tools_allow_nbsp_in_tinymce( $init ) {
    $init['entities'] = '160,nbsp,38,amp,60,lt,62,gt';   
    $init['entity_encoding'] = 'named';
    return $init;
}
add_filter( 'tiny_mce_before_init', 'll_tools_allow_nbsp_in_tinymce' );


############ Diese Erweiterung bewirkt, dass die Sprachwahl von Polylang auch im CiviCRM wirksam wird.
add_filter('pll_the_language_link',function($url, $slug, $locale) {
	ll_crm_debug(array($url, $slug, $locale));
	//Dies ist notwendig, sonst werden immer alle Sprachen angezeigt.
	if(!empty($url)) $url = $url.'?lcMessages='.$locale;
	return $url;
},10,3);

if(LL_TOOLS_POLYLANG) {
	add_filter('ll_tools_languages',function($languages = array()) {
		return array_merge($languages,pll_languages_list(array('fields' => 'locale')));
//		ll_crm_debug($languages,true,true);
	},10,1);
}

if(LL_TOOLS_MAILPOET) {	
	//################ Mailpoet Sprachwahl übernehmen ###############
	//Diese Änderung wird nur wirksam mit einer Änderung direkt im Mailpoet

	if(isset($_REQUEST['wysijaplugin']) and ($_REQUEST['wysijaplugin'] == "wysija-newsletters") 
		and ($_REQUEST['controller'] == "campaigns") 
		and ($_REQUEST['action'] == "wysija_ajax")) {
		define('PLL_ADMIN',false);	
	}

	function ll_wysija_lang($id) {
		if(function_exists('pll_the_languages')) {
			$languages = pll_the_languages(array('hide_if_no_translation' => 1, 'raw'=> 1, 'post_id' => $id));
			$slug = pll_get_post_language($id);
			$out = "";
			foreach($languages as $language) {
				if(!($language['slug'] == $slug)) {
					
					$out .= '<a lang="'.$language["id"].'" hreflang="'.$language["id"].'" href="'.$language["url"].'"><img src="'.$language["flag"].'" title="'.$language["name"].'" alt="'.$language["name"].'" /><span style="margin-left:0.3em;"></span></a>';
					
					//in der aktuellen Version von Wordpress enthält das Element "Flag" bereits den kompletten HTML-String.
					//$out .= '<a lang="'.$language["id"].'" hreflang="'.$language["id"].'" href="'.$language["url"].'">'.$language["flag"].
					//		'<span style="margin-left:0.3em;"></span></a>';
					
				}
			}
			return $out;
		}
	}	
}


?>
