<?php

// functions that create our options page
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
* 
* Development: Enthält Optionen für die Entwicklung
Component Name: Entwicklungstools (sollte hier eigentlich nicht aufscheinen ..)
Component Multisite: Both //wird nicht angezeigt, weil hier kein gültiger Eintrag steht (zumindest auf Multisite)
* 
*/

ll_tools_make_menu(array('site_title' => 'Development','page' => 'dev'));

add_action('load-ll-tools_page_ll_tools_dev','ll_tools_dev_settings_init');
add_action('load-options.php','ll_tools_dev_settings_init');

function ll_tools_settings_dev_cb() {
	echo "<p>Version: ".get_plugin_data(LL_PLUGIN_FILE)['Version']."</p>";
	echo "<p>Name: ".plugin_basename(LL_PLUGIN_FILE)."</p>";
	echo "<p>Errors: ".ini_get('display_errors')."/".error_reporting()."</p>";
	
//	ll_crm_debug(get_option(LL_TOOLS_OPTION."function_test_variable"),true);
//	ll_crm_debug(ll_get_options_set(LL_TOOLS_OPTION),true);
}

function ll_tools_settings_test_cb() {
//	ll_crm_debug(basename(__FILE__),true);
//	ll_crm_debug(dirname("/Users/Hannes/Daten/MAMP_Web/bolsenalagodeuropa/wp-content/uploads/civicrm/templates_c/"),true);
//	ll_crm_debug(dirname(LL_PLUGIN_FILE),true);
//	ll_crm_debug(WP_DEBUG,true);
//	ll_crm_debug(get_transient(LL_TOOLS_OPTION.'taglist'),true);

//	ll_crm_debug(get_option(LL_TOOLS_OPTION.'field_def'),true);
	ll_get_cron_jobs();
	$mailsend = get_option(LL_TOOLS_OPTION."function_test_mail");
	if(is_array($mailsend)) {
		$style = $mailsend[0] ? "green" : "red";
		echo "<p style='color:".$style."'>Testmail: (".(($mailsend[0]) ? 'OK' : 'Failure' )." - ".$mailsend[1]." -> ".$mailsend[2].")</p>";
	} 
	// So kann diese nicht verwendet werden ...
	$old_functions = array('LL_crm_custom_fields_def' => 'Felddefinition',
					   'll_crm_options' => 'Filter zum Einlesen individueller Optionen für die Karte',
					   'LL_crm_Data_result' => 'Ersetzt durch LL_civi_Data_result, Werte werden anders übergeben.',
					   );
	echo "<table>";
	echo "<tr><tr cospan=3><b>Funktionen und Filter die nicht mehr benötigt/unterstützt werden</b></td></tr>";
	foreach($old_functions as $function => $comment) {
		if(function_exists($function))
			echo "<tr><td>Funktion: </td><td>".$function."</td><td>".$comment."</td></tr>";
		if(has_filter($function) !== false)
			echo "<tr><td>Filter: </td><td>".$function."</td><td>".$comment."</td></tr>";
	}
	echo "</table>";
	
//	phpinfo();
}

			
function ll_tools_dev_settings_init() {
	#### Development
	$default_args = array('group' => 'dev','title' => "Development");
	$opt_values = array();
	$options = array();
	$log_files = array();
	if(is_dir(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs"))
		$log_files = scandir(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs");
	foreach($log_files as $file) 
		if(is_file(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs/".$file))
			$options[wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs/".$file] = 
			"<a href='".wp_upload_dir()['baseurl']."/".LL_PLUGIN_NAME."/logs/".$file."' target=_blank>".$file."</a>"; 
	if(file_exists(WP_CONTENT_DIR."/debug.log")) $options[WP_CONTENT_DIR."/debug.log"] = 'Löscht WP debug.log';
	ll_crm_debug($options);
	if(!empty($options)) $opt_values['clear_log'] = array('title' => 'Log Files löschen', 'options' => $options);
	ll_tools_make_section($default_args,$opt_values);
	#### Test
	$opt_values = array();
	$default_args = array('group' => 'test','page' => 'dev', 'title' => "Test Functions");
	if(!is_network_admin()) {
		/* Derzeit keine Update-Funktionen, wären zu überarbeiten
		$opt_values['function_test_version'] = array('title' => 'Update-Funktionen', "default" => false,
																		'options' => ll_list_dir(dirname(__DIR__).'/admin/updates','ll-tools-update-'));
		*/
		$opt_values['function_test_variable'] = array('title' => 'Variablen Check', "default" => false, "description" => "Prüft auf veraltete Variablen");
		$opt_values['function_test_variable']['options']['check'] = array('title' => "Neuerlich Prüfen");
		$option_list = get_option(LL_TOOLS_OPTION."function_test_variable"); 
		if(is_array($option_list)) {
			foreach($option_list as $option => $value)
			$opt_values['function_test_variable']['options'][$option] = array('title' => $option." (".$value.")");
		}	
	}
	if(!is_multisite() or is_network_admin()) {
		if(!empty(ll_list_dir(WP_PLUGIN_DIR,'civicrm-')))
			$opt_values['function_test_civi_uninstall'] = array('title' => 'Civi-Uninstall', "default" => false,
																		'options' => ll_list_dir(WP_PLUGIN_DIR,'civicrm-'));
	}
	$opt_values['function_test_mail'] = array('title' => 'Mail() Test', 'description' => "Testet die Standard-Mail-Funktion des Servers","default" => false);	
	ll_tools_make_section($default_args,$opt_values);

}

add_filter('ll_tools_field_post_text_uploaded_version',function($out,$args){
	ll_crm_debug($args);
	$version = ll_tools_get_options(LL_TOOLS_OPTION."uploaded_version");
	$error = get_transient(LL_TOOLS_OPTION.'upload_fail');
	if($error == false) {
		$out = '<p style="color:green"> Letzte hochgeladene Version: '.$version.'</p>';			
	} else {
		$out = '<p style="color:red"> '.$error.', letzte hochgeladene Version: '.$version.'</p>';					
		delete_transient(LL_TOOLS_OPTION.'upload_fail');
	}
	return $out;	
},10,2);

add_filter('pre_update_option_'.LL_TOOLS_OPTION.'function_test_variable',function($value,$oldvalue) {
	ll_crm_debug($value);
	if(empty($value)) return $oldvalue;	
	if(isset($value['check'])) {
		return ll_get_options_set(LL_TOOLS_OPTION,true);
	} else {
		foreach($value as $option) {
			delete_option($option);
		}
		return ll_get_options_set(LL_TOOLS_OPTION,true);
	}
	 
	return $oldvalue;
},10,2);


add_filter('pre_update_option_'.LL_TOOLS_OPTION.'upload_server',function($value,$oldvalue) {
//	ll_crm_debug(array($value,$oldvalue),'print');
	if(($value == $oldvalue) or empty($value)) return $oldvalue;
	$conn_id = ftp_connect($value['server'], 21); 
	if($conn_id === false) {
		add_settings_error('ll_tools',esc_attr('settings'),"Server nicht gefunden");
	} else { 
		ftp_pasv($conn_id, true);
		if(!ftp_login($conn_id, $value['user'], $value['pw'])) {
			add_settings_error('ll_tools',esc_attr('settings'),"User und/oder Passwort ungültig");
		} 
		ftp_close($conn_id);		
	}
	return $value;
},10,2);


add_filter('pre_update_option_'.LL_TOOLS_OPTION.'clear_log',function($value,$oldvalue) {
	ll_crm_debug($value);
	if(is_array($value)) {
		foreach($value as $key => $dummy) {
			if(is_dir($key))  {
				$del_list = scandir($key);
				foreach($del_list as $file) 
					if(is_file($key."/".$file)) unlink($key."/".$file); //ll_crm_debug($key."/".$file,'print'); //
			} else {
				unlink($key); //ll_crm_debug($key,'print'); //
			}
		}	
	}
	return $oldvalue;
},10,2);

function ll_tools_test_mail() {
   $from = "webadmin@webcoach.at";
   $to = "hannes.zagar@coachit.at";
   $subject = "Livinglines PHP Mail Test";
   $message = "This is a test to check the PHP Mail functionality";
   $headers = "From:" . $from;
   $erg = mail($to,$subject,$message, $headers);
   ll_crm_debug(array("Testmail wurde verschickt -> ".$erg,$from,$to),'print');
   return array($erg,$from,$to);
}
	
function ll_get_cron_jobs() {
	$crons = wp_get_ready_cron_jobs();
	$text = "Keine offenen CronJobs gefunden";
	$style = $style ="style='color:green;'";
	if(count($crons) > 0) {
		$nextcron = array_keys($crons)[0];
		if($nextcron < time()) $style ="style='color:red;'";
		$text = "Nächster CronJob: ".date("Y-m-d H:i:s",$nextcron)." (".date("Y-m-d H:i:s").")";
	}
	echo "<p ".$style.">".$text."</p>";
}

add_filter('pre_update_option_'.LL_TOOLS_OPTION.'function_test_mail',function($value,$oldvalue){
	if($value) return ll_tools_test_mail();
	return $oldvalue;
},10,2);
	
function ll_field_function_test_version($value,$oldvalue) {
	if(!empty($value)) {
		$versions = ll_list_dir(LL_PLUGIN_DIR.'admin/updates','ll-tools-update-');
		ll_crm_debug($versions);
		foreach($versions as $version) {
			if(isset($value[$version])) {
				ll_crm_debug("Geladen:".$version,'print');	
				include(LL_PLUGIN_DIR.'admin/updates/'.$version); //ll_crm_debug('updates/'.$version,'print');//
			}
		}
	}
	return $oldvalue;
}

add_filter('pre_update_option_'.LL_TOOLS_OPTION.'function_test_version','ll_field_function_test_version',10,2);

function ll_field_test_function_civi_uninstall($value,$oldvalue) {
	if(!empty($value)) {
		$versions = ll_list_dir(WP_PLUGIN_DIR,'/civicrm-');
		foreach($versions as $version) {
			if(isset($value[$version])) {
				$ll_uninstall_plugin = $version;
				include_once(WP_PLUGIN_DIR."/".basename(dirname(__FILE__)).'/uninstall.php');
				ll_uninstall_sites($ll_uninstall_plugin);
			}
		}
		ll_crm_debug(array("Ausgeführt",$value),'print');		
	}
	return $oldvalue;
}

add_filter('pre_update_option_'.LL_TOOLS_OPTION.'function_civi_uninstall','ll_field_function_civi_uninstall',10,2);

?>
