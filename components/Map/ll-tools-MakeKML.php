<?php

// functions that create our options page
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*
* Adminseiten für CiviCRM als Ergänzung zu Livinglines-Tools
Component Name: Erzeugt aus Civi-Daten KML-Files (in Entwicklung)
Component Multisite: wird nicht angezeigt.
* 
*/

//ACHTUNG, hier muß es noch einen Ausstieg geben, wenn CIVI nicht aktiv ist!
if(!LL_TOOLS_CIVICRM) return;

ll_tools_make_menu(array('site_title' => 'Civi KML', 'page' => 'kml'));

add_action('load-ll-tools_page_ll_tools_kml','ll_tools_kml_settings_init');
add_action('load-options.php','ll_tools_kml_settings_init');

//Das sollte wohl noch weg? => sollte in ll-tools-Civi_Data geladen werden
//ll_tools_file_loader("crm","ll-crm-functions");

function ll_tools_settings_kml_cb() {
	$kml = new ll_crm_MakeKML(array('set_tag' => array('0_10')));
	$XML = $kml->make_kml();
	$XML = str_replace('&', '&amp;', $XML);
	$XML = str_replace('<', '<br>&lt;', $XML);
	ll_crm_debug($XML,true);
}


function ll_tools_kml_settings_init() {
	#### Test
	$default_args = array('group' => 'kml', 'type' => 'checkbox','title' => 'Civi KML');
	$opt_values = array('crm_kml' => array('title' => 'Make KML'));
	ll_tools_make_section($default_args,$opt_values);		
}



add_filter('pre_update_option_'.LL_TOOLS_OPTION.'crm_kml',function($value,$oldvalue) {
	if($value) {
		$kml = new ll_crm_MakeKML(array('set_tag' => array('0_10')));
		ll_crm_debug($kml->make_kml(),true);
	}
	return $oldvalue;
},10,2);



		
?>
