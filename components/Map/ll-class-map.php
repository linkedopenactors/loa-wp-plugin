<?php
/* Enthält die Klasse für die Karte
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

	/**
	 * Basisklasse für die Darstellung von Karten
	 * Singleton 
	 * 
	 * @since
	 *
	 * @param array $args 
	 *
	 */

class LL_Map_map { 
	private static $_singleton;

	/* Enthält die Styles für die anzuzeigenden DataSets */
	protected $attribs;
	
	/* Enthält Basisoptionen für die Karte */
	protected $options;

	/* Enthält die Sets, die in attribs aufbereitet werden. */
	public $map_sets;	
	
	/* Enthält die aufbereiteten Daten für die Übergabe an die Karte nach Gruppen */
	protected $object_groupValues;

	/* Enthält die aufbereiteten Daten für Ausgabe als List */
	public $list_groupValues;
	
	/* Gibt an, wie das Ergebnis dargestellt werden soll */
	//array page = Basiselement, element = Element auf der Seite, Rendering = Aussehen des Elementes
	// 'map' (Default), 'list'
	public $output;


	/* Nummer der Karte, Default 0, wenn nicht als 'map' übergeben */
	public $map;
	
		/**
	 * Konstruktur
	 * 
	 * @since 
	 *
	 * @param array $args 
	 *
	 * ab 4.2.6 können die Request-Parameter nicht mehr in $args übergeben werden.
	 */
	
	function __construct($args) {
		$this->map = (empty($args['map'])) ? 0 : $args['map']; 
		$this->response = $this->get_response();
		$this->response['sidebar'] = "mapselect_".$this->map;
		$this->response['response'] = 'START';

		//Übersteuerung der Karteneinstellungen für öroject
		$this->get_submit_keys($args); //Dieser Teil hier, damit output bekannt ist.
		//Hier können $args in Abhängigkeit von der Page übersteuert werden.
		if($this->output['page'] == 'print') $args = array_merge($args,$this->get_map_args_print());
		if(isset($args[$this->output['page']."_options"])) {
			$args = array_merge($args,$args[$this->output['page']."_options"]);										  
			unset($args[$this->output['page']."_options"]);
		}
		$args = array_merge($args,apply_filters('ll_tools_get_map_args_'.$this->output['page'],array()));
		/*				
		
		if($this->output['page'] == 'project') {

			$args = array_merge($args,array('zoom' => 15, 
													  'maxzoom' => 16, 
													  'reset' => 'NoCookie', 
													  'mapCenter' => 'Auto',
													  'map_init' => "<div id='map' class='map-project'></div>", 
													  'sidebar' => ""));
			if(isset($args['project'])) {
				$args = array_merge($args,$args['project']);										  
				unset($args['project']);
			}
		}elseif($this->output['page'] == 'print') {
			$args = array_merge($args,$this->make_print_options());
			if(isset($args['print'])) {
				$args = array_merge($args,$args['print']);										  
				unset($args['print']);
			}			
		}
		*/
		
		$this->set_options($args);
		$this->attribs = array();
			}
	
	public static function singleton($args = array()){
		if(!isset(self::$_singleton))
			self::$_singleton = new LL_Map_map($args);
		return self::$_singleton;
	}


	/**
	 * Bereitet die Antworttexte für die Sidebar auf der Karte auf.
	 * 
	 * @since
	 *
	 *
	 * Die Texte können je Karte und/oder Sprache unterschiedlich sein.
	 * Reihenfolge map+language, language, map, all/all
	 */

	public function get_response() {	
		$text_sets = get_option(LL_TOOLS_OPTION."map_response");
		if(empty($text_sets['sets'])) {
			$default = ll_map_standard_response(array());
				foreach($default as $key => $value) $text[$key] = $value[0];
			return $text;
		}
		$text[4] = current($text_sets['sets']);
		foreach($text_sets['sets'] as $set) {
			if(($set['mapNr'] == "map".$this->map) and $set['language'] == get_locale())
				return $set;
			elseif(($set['mapNr'] == 'all') and $set['language'] == get_locale())
				$text[1] = $set;
			elseif(($set['mapNr'] == "map".$this->map) and empty($set['language']))
				$text[2] = $set;
			elseif(($set['mapNr'] == 'all') and empty($set['language']))
				$text[3] = $set;
		}
		ksort($text);
		ll_crm_debug($text);
		ll_crm_debug($text_sets);
		return current($text);
	}


	/**
	 * Bereitet die Parameter für die Karte auf.
	 * 
	 * 
	 * @since
	 *
	 * @param array $args
	 *
	 */
	
	protected function set_options($args = array()) {
		$maps = get_option(LL_TOOLS_OPTION."map_sets");
		if(($maps === false) or empty($maps['sets'])) {
			$maps_default = ll_map_standard_options();
			foreach($maps_default as $map => $param ) $maps[$map] = $param[0];
		} else {
			if(isset($maps['sets'][$this->map])) {
				$maps = $maps['sets'][$this->map];
			} else {
				$maps = current($maps['sets']);
			}
		}
		ll_crm_debug($maps);
		$this->options = $maps;
		$this->options['linkToMap'] = "";
		$this->options['class'] = 'map map'.$this->map;
		//Notwendig weil in den Parameter keine Leerzeichen übergeben werden können.
		foreach($this->options as $option => $dummy) {
			if(isset($args[strtolower($option)])) $this->options[$option] = str_replace(","," ",$args[strtolower($option)]);
			unset($args[strtolower($option)]);		
		}
		$this->options = array_merge($this->options,$args);
		$this->response['select'] = false;
		$this->response['print'] = false;
		if(!empty($this->options['sidebar']))
			$widgets = wp_get_sidebars_widgets();
			if(isset($widgets['mapselect_'.$this->map])) 
				foreach($widgets['mapselect_'.$this->map] as $widget) {
					if(strpos($widget,'ll_map_widgetselect') === 0)
						$this->response['select'] = true;
					if(strpos($widget,'ll_map_widgetprint') === 0)
						$this->response['print'] = true;
				}
		if($this->output['page'] == 'print') $this->response['select'] = true;
		if(!$this->response['select']) $this->options['reset'] = 'NoCookie';
		
		ll_crm_debug($this->options);
//		if (preg_match ("/^([0-9]+)$/", $input_number))
//		if(substr($this->options['height'],-2) != 'px') $this->options['height'] .= "px";
		if(is_numeric($this->options['height'])) $this->options['height'] .= "px";
		$this->options['style'] = "height: ".$this->options['height'].";";
		if(!empty($this->options['width'])) {
			if(is_numeric($this->options['width'])) $this->options['width'] .= "px";
			$this->options['style'] .= "width:".$this->options['width'].";";		
		}
		if(!isset($this->options['Legend'])) $this->options['Legend'] = "";
		if(($this->options['lat'] == 0) or ($this->options['lon'] == 0))  {
			$this->options['mapCenter'] = 'Auto';
			$this->response['mapCenter'] = 'Auto';
		}
		ll_crm_debug($this->options);
		$this->response = apply_filters('ll_tools_map_response',$this->response,$this);
	}


	/**
	 * Gibt ein oder Paremeter für die Kartendarstellung zurück.
	 * 
	 * 
	 * @since
	 *
	 * @param string $option gewünschtes DataSet.
	 *
	 */

	public function get_options($option = NULL) {
		if(isset($option)) return $this->options[$option];
		return $this->options;
	}

	/**
	 * Holt die DatenSets der Karte aus der Option
	 * Selektiert die Felder für die aktuelle Sprache
	 * Entfernt nicht aktive Elemente
	 * Behält das Set in einer static und gibt es zurück.
	 * 
	 * @since
	 *
	 */


	public function get_optionset() {
		static $optionset;
		if(empty($optionset)) {		
			$optionset = get_option(LL_TOOLS_OPTION.'option_set'.$this->map);
			$optionset = (isset($optionset['sets'])) ? $optionset['sets'] : array(); 
			if(empty($optionset)) return array();
			foreach($optionset as $key => $set) {
				if(empty($set['aktiv'])) unset($optionset[$key]);
				else
					$optionset[$key] = ll_convert_optionOptions_lang($set,'option_set');
			ll_crm_debug($optionset);
			}
		}
		return $optionset;
	}


	/**
	 * Ermittelt die Abfragewerte aus GET, POST oder options
	 *
	 * @since
	 *
	 */

	private function get_submit_keys($args) {
		static $submit;
		if(!empty($submit)) return $submit; 
		
		//indiv. Punkt(e) in der URL
		$pos = $this->get_submit_values('pos',$args);
			if(isset($pos)) $this->make_CustomObjects_point(array('pos' => $pos),array(),'main','indiv');

		$set_id = $this->get_submit_values('set_id',$args);
			if(isset($set_id)) $submit['set_id'][] = $set_id;

		$submit[0] = 'mapSelect'.$this->map;
		//durch den Merge wird sichergestellt, dass output einen Default bekommt.
		$this->output['page'] = $this->get_submit_values($submit[0],array_merge(array($submit[0] => 'map'),$args));
		$this->output['element'] = $this->output['page'];
		ll_crm_debug($this->output);
	}

	/**
	 * Prüft ob die gezeigten Daten durch ein submit, GET oder ein Cookie definiert sind, oder ob alle ausgegeben werden.
	 *
	 * @since
	 *
	 */

	public function get_submit() {
		static $submit;
		if(!empty($submit)) return $submit; 
		$submit = $this->get_submit_keys($this->options); 

		ll_crm_debug(array($submit));
		ll_crm_debug($_COOKIE);
		$select_options = $this->get_optionset();
		ll_crm_debug($select_options);
		foreach($select_options as $key => $set) {
			if(in_array($set['post'],array('widget_group')))
				unset($select_options[$key]);
		   $value = $this->get_submit_values($set['post'],$this->options);
		   if(isset($value))
				$submit[$set['post']] = $this->make_submitted($set['post'],$value);
		}
		$submit = $this->comp_submit($submit);

		ll_crm_debug(array($this->response['select'],$submit));
		ll_crm_debug(count($submit));
		
		if(($this->output['element'] == 'project') and (count($submit) == 1)) {
		   //MapProject funktioniert nicht ohne Parameter, daher Umleitung zur Seite, wenn kein Request
			//Redirect funktioniert nicht, weil bereits Ausgabe erfolgt ist.
			//Dies ist möglicherweise nicht mehr notwendig oder kann durch Umstellung von output geregelt werden!
			echo '<meta http-equiv="refresh" content="0; url='.$this->get_LinkUrl().'" />';
			exit;
		}elseif(empty($this->response['select']) and (count($submit) == 1)) {
			########### Alles Tags werden sofort angezeigt, wenn keine Sidebar
			foreach($select_options as $set) {
				if(!isset($submit[$set['post']])) $submit[$set['post']] = array();
				$submit[$set['post']] = 
					array_merge($submit[$set['post']],$this->make_submitted($set['post'],$set['set_tag']));
			}		
		} elseif($this->options['reset'] != 'NoCookie') //denn wenn keine schreiben, dann vor allem keine lesen!
			if((count($submit) == 1) and (!isset($_POST[$submit[0]]) or ($_POST[$submit[0]] == 'print')) ) {
				foreach($select_options as $set) 
	 				if(isset($_COOKIE[$submit[0]][$set['post']]))
						$submit[$set['post']] = $this->make_submitted($set['post'],$_COOKIE[$submit[0]][$set['post']]);
			}
	 	ll_crm_debug($submit);
		return $submit;
	}


	/**
	 * Sucht Abfragewerte und gibt den gefundenen Wert zurück.
	 *
	 * @since 4.2.6 vorher in get_submit();
	 *
	 * @param string $post Wert der Abgefragt wird.
	 *        array $args Array der Übergabe- oder Defaultparameter
	 *
	 */

	private function get_submit_values($post,$args) {
			if(isset($_POST[$post])) {
				unset($_GET[$post]);
				return $_POST[$post];
			}elseif(isset($_GET[$post])) {			
				return $_GET[$post];
			//Es könnten Kriterien in den args stehen!
			}elseif(isset($args[$post])) {
				return $args[$post];
			}
			return NULL;
	}

	/**
	 * Prüft ob GET-Variablen nach altem Muster gesetzt sind und konvertiert sie.
	 *
	 * @since
	 *
	 * PRüfen, ob dies Kompatibilitätsfunktion nicht bereits entfernt werden könnte?	
	 * 'uid' wird beim Projekt übergeben.
	 */


	private function comp_submit($submit) {
		$set = (isset($_GET['set'])) ? $_GET['set']-1 : 0;
		if(isset($_GET['uid'])) 
			$submit['set_id'][] = $set."_".$_GET['uid'];
		//Neue Überage für Kommagetrennte Werte ..
		if(isset($_GET['tag']) and ($set == 0)) {
			ll_crm_debug($_GET['tag']);
			foreach($_GET['tag'] as $tag)
				$submit['set_tag'] = $this->make_submitted('set_tag',$set."_".$tag);		
//				$submit['set_tag'][] = $set."_".$tag;
		}
//		if(isset($_GET['pos'])) 
//			$submit[] = $_GET['pos'];
		return $submit;
	}	
	

	/**
	 * Bereitet das array $attribs für die DataSets vor, die angezeigt werden sollen.
	 *
	 * @since
	 *
	 * @param $var DataSet-Gruppe ($post)
	 * @param array/string $value set_tag, bzw. Array aus set_tag Werten
	 */

	private function make_submitted($var,$value) {
		if(!is_array($value)) $value = explode(",",$value);
			foreach($value as $set_tag) $this->attribs[$var][$set_tag] = "";
		return $value;
	}

	/**
	 * Befüllt das array $attribs für die DataSets, die angezeigt werden sollen.
	 * Sortierung nach Prioritäten, wobei der Ausschluß negativer Werte nicht mehr unterstützt wird.
	 * sets: Array aus Basiswerten: post, set_tag, data_set, name, tag, tagname
	 * attr: Attribute zur Verwendung in dieser Klasse
	 * jattr: Attribute wie sie per Json übergeben werden.
	 *
	 * @since
	 *
			//jattr sind für json, die anderen attr sind für alle Codes hierher
			//Für die Ermittlung der Subdata-Sets wird "sub" benötigt.
	 * jattr und attr sollten zusammengeführt werden.
	 */
	
	public function get_submitted() {
		$this->get_submit();
		static $attribs;
		if(isset($attribs)) return $attribs;
//		$submit = $this->get_submit();
		$optionset = $this->get_optionset();
		$submitted = array();
		$attribs = array();
		ll_crm_debug($this->attribs);
		foreach($optionset as $key => $set) {
			if(isset($this->attribs[$set['post']][$set['set_tag']])) {
				ll_crm_debug($set);
				$submitted[$key] = $set['prio'];
			}		
		}
		arsort($submitted);
		ll_crm_debug($submitted);
		foreach($submitted as $key => $dummy) {
			$set = $optionset[$key];
			ll_crm_debug($set);
			if(isset($this->options['attribs'])) $set = array_merge($set,$this->options['attribs']); //für den Druck!
			$values = array('post','set_tag','data_set','name','tag','tagname','line','point','project');
			foreach($values as $value)
				$this->map_sets[$key][$value] = $set[$value];

			$values = array('iconClass','icon','iconColor','markerColor');
			foreach($values as $value) {
				$attribs['attr']['main'][$key][$value] = $set[$value];
			}
			if(!empty($set['point'])) {
				$attribs['attr']['point'][$set['point']] = array_merge($attribs['attr']['main'][$key],$set['point_style']);			
			}
			if(!empty($set['line'])) {
				//Gehört nicht hierher, sondern ins set, doch wird für lineinfowindow gebraucht
				$attribs['attr']['line'][$set['line']]['style'] = $set['line_style'];
				//w.o. stört aber nicht
				$attribs['attr']['line'][$set['line']]['line_box'] = $set['line_box'];			
			}				
		}
		ll_crm_debug($attribs);
		return $attribs;
	}

	/**
	 * Gibt die URL für einen Link innerhalb der Karte zurück
	 *
	 * @since
	 *
	 * @param array $args 'type' = Set, 'id' = Element-Id, 'par' = array mit vorbereiteten Elementen, 'pos' mit Positionen
	 */

	public function get_linkUrl($args = array()) {
		if(isset($args['par']))
			$par = $args['par'];
		if(isset($args['type'])) {
			$set = $this->map_sets[$args['type']];
			$par[] = $set['post']."=".$set['set_tag'];
			if(empty($set['project'])) return "";
		}
		if(isset($args['id'])) {
			//Konvertierung für id ist notwendig, weil an Hand _ aufgelöst wird.
			$par[] = 'set_id='.str_replace("-","_",$args['id']);
		}
		if(isset($args['pos']))
			$par[] = 'pos = '.$args['pos'];
		if(($this->output['page'] != 'project') and !in_array('linkToMap',$args,true)) {
			$par['page'] = "mapSelect".$this->map."=".'project';				
		}

		$base = $this->options['map_page'];
		if($base != get_the_ID()) {}
			if(LL_TOOLS_POLYLANG) {
				$base = pll_get_post($base, pll_current_language());
			}	

		return get_permalink($base)."?".implode("&",$par);		
	}

	
	/**
	 * Holt die Kartenparameter und bereitet sie für die Übergabe an leaflet auf.
	 * es werden nur jene Werte übergeben, die für leaflet von Bedeutung sind.
	 * Wenn notwendig, wird der Link zur Kartenseite mit allen Paremetern aufgebaut.
	 * Der IconPath wird vollständig gesetzt.
	 *
	 * @since
	 *
	 */

	protected function get_options_json() {
		ll_crm_debug($this->options);
		$options = array('mapCenter','zoom','reset','lat','lon','zoomControl','minzoom','maxzoom','linkToMap','iconPath','map');
		foreach($options as $option)
			if(isset($this->options[$option])) 
				$json_options[$option] = $this->options[$option];
		if(isset($json_options['linkToMap']) and ($json_options['linkToMap'] == 1)) {
			$get_vars = array();
			ll_crm_debug($this->map_sets);
			foreach($this->map_sets as $select_var) {
				if(!isset($get_vars[$select_var['post']])) $get_vars[$select_var['post']] = $select_var['post']."=".$select_var['set_tag'];
				else $get_vars[$select_var['post']] .= ",".$select_var['set_tag'];
			}
			$json_options['linkToMap'] = $this->get_linkURL(array('par' => $get_vars,'linkToMap'));
			ll_crm_debug($json_options['linkToMap']);
		}
		$json_options['iconPath'] = wp_upload_dir()['baseurl']."/".LL_PLUGIN_NAME."/".$json_options['iconPath']."/";
		return $json_options;
	}


	/* Gehört nicht hierher 
	
	protected function get_kml_json() {
		$kml_files = array();
		//Achtung Optionset ist anders gebaut!
		foreach($this->get_optionset() as $optionset) {
			ll_crm_debug($optionset['post']);
			if($optionset['post'] == 'kml_file') {
				foreach($optionset as $key => $value) {
					if(is_array($value))
						foreach($value as $valuekey => $valuevalue)
							if(empty($valuevalue)) unset($optionset[$key][$valuekey]);
					if(empty($optionset[$key]))
						unset($optionset[$key]);
				}
				$kml_files[] = $optionset;			
			}
		}
		return $kml_files;		
	}

	*/
	/* Gehört nicht hierher */

	public function get_map_json($args = array()) {
		//für MakeKML
		$submit = array_merge($this->get_submit(),$args);
		ll_crm_debug($submit);
		return $this->get_civi_data();
	}


	/**
	 * Übergibt die abgefragten DataSets mittels Json an leaflet
	 *
	 * @since
	 *
	 * @param string $data: DataSet (main, line, point, sparql, kml, attr, options ...)
	 *
	 * Diese Funktion muß generischer werden und einen Filter bekommen, damit eine Einbindung anderer Daten möglich wird.
	 * Dazu braucht es eine generische Abfrage von leaflet
	 */


	public function get_data_json($data) {
		try{
			$json_options = array();
			if(in_array($data,array('main','line','point'))) {
//				$object_groupValues = $this->get_civi_data();		
				if(empty($this->object_groupValues[$data])) $json_options = array();
				else {
					$json_options = $this->object_groupValues[$data];
					if($this->output['page'] == 'print') $json_options = $this->move_print_markers($json_options);
				}
//				ll_crm_debug($json_options,($data == 'main'));
			}elseif($data == 'kml') {
				/* Wäre in Leaflet noch auf generische Form umzustellen, damit es hier weg kann. */
//				$json_options = $this->get_kml_json();
				if(function_exists('ll_tools_make_kml_json'))
					$json_options = ll_tools_make_kml_json($this);
			}elseif($data == 'attr') {
				$json_options = (empty($this->get_submitted()['attr'])) ? array() : $this->get_submitted()['attr'];
			}elseif($data == 'options') {
				$json_options = $this->get_options_json();
			}

			$json_string = json_encode($json_options);
			if(json_last_error() != JSON_ERROR_NONE)
				$this->response['error'] = new WP_Error( 'll_tools_map', json_last_error_msg());
		}catch(Exception $e) {
			$this->response['error'] = new WP_Error( 'll_tools_map', $e->getMessage());
		}
		return $json_string;
	}


	/**
	 * Hilfsfunktion für das Einbinden der Karte über Singleton Aufruf
	 *
	 * @since
	 * 4.2.11 Filter ergänzt.
	 *
	 * @param array $args
	 *
	 */
	
	public static function get_map1($args) {
		$args = apply_filters('ll_tools_get_map_args',$args);
		$map = self::singleton($args);
		return $map->get_map();
	}


	/**
	 * Generiert die Karte und Liste (zu Kartendaten)
	 *
	 * @since
	 *
	 * @param string $output => übersteuert den output für die Karte auf der Projektseite.
	 *
	 * Kontakte sind nur einmal in der ersten gefundenen Kategorie enthalten ..
	 * Linien und Points werden von den dahinterliegenden Funktionen vollständig aufgebaut.
	 *
	 * Rückgabe Array DataSet.Id - $data-source-$id - DatenArray
	 */

	public function get_map($output = NULL) {
		//für Karte auf Projektseite.
		if(isset($output)) $this->output['element'] = $output;
		//Nur für Debugzecke!
		//$this->get_civi_data();
//		$this->get_sparql_data();
		ll_crm_debug($this->options);
//		ll_crm_debug($this->get_submitted(),true);
//		ll_crm_debug($this->response['title']);

		//Wenn es keine gewählten DataSets gibt, brauchen auch keine Daten aufgebaut werden!
		if(!empty($this->get_submitted()))
			do_action('ll_tools_get_map_data',$this);

//		ll_crm_debug($this->object_groupValues,true);
//		ll_crm_debug($this->list_groupValues,true);
		ll_crm_debug($this->response);


		$content = '';
		ob_start();

		if($this->output['element'] == 'list') {
			echo "<div id='ll_list".$this->map."' class='list_area'>";	
			ll_crm_debug($this->list_groupValues);
			echo ll_map_list_rendering($this);

		}elseif($this->output['element'] == 'project') {
			if(!function_exists('leaflet_head')) {
				require_once(WP_PLUGIN_DIR."/". LL_PLUGIN_NAME . '/leaflet/leaflet-head.php');	
				leaflet_head();
			}
			echo "<div id='ll_project".$this->map."' class='project_area'>";	
//			ll_crm_debug($this->list_groupValues);
			echo ll_map_project_rendering($this);		
		}elseif($this->output['element'] == 'print'){
			if(!function_exists('leaflet_head')) {
				require_once(WP_PLUGIN_DIR."/". LL_PLUGIN_NAME . '/leaflet/leaflet-head.php');	
				leaflet_head();
			}
			$style = "style='height: ".$this->options['height'].";"."width:".$this->options['width_total']."';";
			echo "<div id='map".$this->map."' class='map_area' ".$style.">";	
			if(empty($this->options['map_init'])) 
				echo '<div id="map" class="'.$this->options['class'].'" style="'.$this->options['style'].'"></div>';
			else
				echo $this->options['map_init'];
			include(WP_PLUGIN_DIR."/". LL_PLUGIN_NAME ."/leaflet/leaf-script.php");					
		}else{
			if(!function_exists('leaflet_head')) {
				require_once(WP_PLUGIN_DIR."/". LL_PLUGIN_NAME . '/leaflet/leaflet-head.php');	
				leaflet_head();
			}

			echo "<div id='map".$this->map."' class='map_area'>";	
			if(empty($this->options['map_init'])) 
				echo '<div id="map" class="'.$this->options['class'].'" style="'.$this->options['style'].'"></div>';
			else
				echo $this->options['map_init'];
			include(WP_PLUGIN_DIR."/". LL_PLUGIN_NAME ."/leaflet/leaf-script.php");					
		}


		if(!empty($this->options['sidebar'])) {
			ll_show_map_sidebar($this->response);
		}
		echo "</div>";
		$content .=  ob_get_contents();
	 	ob_clean();
	 	return $content;
	}
	
	/**
	 * Bereitet die Options für den Druck vor
	 *
	 * @since 4.2.11
	 *
	 */




	/**
	 * Bereitet die Daten für Punkte vor, vorerst nur für "indiv"
	 * Vielleicht später generell für alle?
	 *
	 * @since 4.2.6 übernommen von civi/ll-class-Map.php für 'inidv'
	 *
	 * @param array $pointSet enhält die Daten für Element
	 * @param array $contact_data enthält 
	 * @param string $group FieldGroup (beispielsweise line_id)
	 * @param string $typ Type des Datenobjektes ('indiv')
	 *
	 */

	private function make_CustomObjects_point($pointSet,$contact_data,$group,$typ) {
		if($typ == 'indiv') {
			$Set[0]['type'] = $typ;
			$Set[0]['group'] = $group; //OK, damit nicht noch ein Set ausgelesen werden muß
			if($this->output['element'] == 'map_page') {
				$Set[0]['link'] = $this->get_linkUrl(array('pos' => "[".implode(",",$marker['point_data'][0])."]"));
				$Set[0]['display_name'] = 'Enlarge the map';			
			}

			$temp = explode("][",substr($pointSet['pos'],1,-1));
			foreach($temp as $points) $Set[0]['point_data'][] = explode(",",$points);
 		}
//		ll_crm_debug(array($Set,$typ));
		if(!empty($Set)) {
//			$this->makePointObjects($Set,$typ);
			$this->makePointObjects($Set,$typ);			
			return true;
		}
		return false;
	}

	/**
	 * Baut die Datenelemente für die Karte auf 
	 * Diese Methode wird von den Funktionen zur Datengenerierung verwendet.
	 *
	 * @since 4.2.5.5 hierher übernommen vorher in ll_classes_sparqlMap
	 *
	 * @param array $marker_set enhält die Daten für die Elemente (bei Main immer nur eines?)
	 * @param string $typ Type des Datenobjektes ('main','line','point')
	 *
	 */


	public function makePointObjects($marker_set,$typ) { 
		//Dies sollte hier weg und direkt in Attr. korrekt eingetragen werden.
		$default = ll_tools_default_map_fields();				
		ll_crm_debug($marker_set);
		$ii = 1;
		foreach($marker_set as $marker) {
			$values = array_merge($default,$marker);
			ll_crm_debug($values);

			if(!in_array($this->output['element'],array('map','print'))) {
				if(!isset($this->list_groupValues[$values['type']][$values['id']])) 
					$this->list_groupValues[$values['type']][$values['id']] = $values;	
				return; //ein wenig buggy, da dies nur funktioniert wenn bei "main" nur ein Element übergeben wird.
			}

			if(empty($values['id'])) {
				ll_crm_debug($values);
				$values['id'] = $values['group'];
			}

			if($typ == 'indiv') $typ = 'main'; //funktioniert daher nur einmal für ein Set!
			if($typ === 'main') {
				if($this->output['page'] == 'project') {
					$values['display_name'] = $this->map_sets[$values['type']]['name'];
					$values['image_URL'] = "";
					$values['childs'] = "";	
					$values['description'] = "";
					$values['link'] = "";			
				}
				
			/* Bei sparql wird der link in den Daten übergeben */
			$values['more'] = $this->get_linkUrl($values);
			if(empty($values['link']))
				$values['link'] = $this->get_linkUrl($values);
			else										
				$values['link'] .= "' target='_blank";
			}
		
			//Ein Datensatz
			foreach($values['point_data'] as $pos) {
				//Subpunkte im Datensatz
				$point['pos'] = $pos;
				$point['text'] = str_pad($ii,2," ",STR_PAD_LEFT); //ist zu klären
				if($typ == 'point') {
					$values['display_name'] = $marker['display_name']." ".$ii;
					$point['group'] = $values['group'];
				}
//				$point['infowindow'] = str_replace("'","@@",getInfoContent($values));
				$point['infowindow'] = getInfoContent(apply_filters('preInfoContent',array_merge($values,$point),$this));
				if(isset($values['image_URL'])) unset($values['image_URL']);
				ll_crm_debug($point);
				$points[] = $point;
				$ii++;
			}
			ll_crm_debug($points);
		}
		ll_crm_debug($points);
		//Eintragen der Points ins Sammelarray!
		if(!isset($this->object_groupValues[$typ][$marker['type']]['data'])) $this->object_groupValues[$typ][$marker['type']]['data'] = array();
		$this->object_groupValues[$typ][$marker['type']]['data'] = array_merge($this->object_groupValues[$typ][$marker['type']]['data'],$points);		
	}

	/**
	 * Ermöglicht das Ergänzen eines Datenelementes eines bestimmten Typs
	 * Diese Methode wird von den ll-crm-class-Civi-Map zum Eintragen der Linien verwendet.
	 *
	 * @since 4.2.5.6 
	 *
	 * @param string $typ 'line', 'main', 'point', ...
	 * @param array $data Datenarray für Linien.
	 */

	public function add_object_groupValues($typ,$data) {
		if(isset($this->object_groupValues[$typ]))
			$this->object_groupValues[$typ] = array_merge($this->object_groupValues[$typ],$data);
		else
			$this->object_groupValues[$typ] = $data;		
	}
	
		/**
	 * Bereitet die Options für den Druck vor
	 *
	 * @since 4.2.11
	 *
	 */

	private function get_map_args_print() {
		$args = array();

		$print_options = array();
		foreach(ll_tools_print_option_set() as $option => $values) {
			$print_options[$option] = $this->get_submit_values($option,$this->options);
			ll_crm_debug($values);
			if($option == 'Format') $format = $values['data'][$print_options[$option]]['format'];
		}
		ll_crm_debug($print_options);
		
		if(!isset($format[3])) $args['legend'] = 75/100;
		if($print_options['Legend'] == 'maponly') $args['legend'] = 1;
		
		$args['width_total'] = $format[0]."mm";
		$args['width'] = $format[0] * $args['legend']."mm";
		$args['height'] = $format[1]."mm";
		$args['zoom'] = $format[2];
		
		$args['zoomControl'] = false;
		$args['attribs']['iconClass'] = 'Circle'; 
//		$args['radius'] = 10; //notwendig?

		$args['images'] = isset($print_options['Options']['pict']);
		$args['Columns'] = $print_options['Columns'];
		$args['reset'] = 'Print'; //Bewirkt die Übernahme desselben Ausschnittes bei anderem Format aus dem Cookie.
		$args['sidebar'] = ""; //Damit keine Sidebar angezeigt wird.

		return $args;
	}

		/**
	 * Verschiebt Punkte an den Rand der Karte, wenn sie zu weit draußen liegen 
	 * verändert den Abstand wenn sie zu Nahe sind.
	 *
	 * @since 4.2.11
	 *
	 * @param array $markers Liste der anzuzeigenden Elemente.
	 */

	
	private function move_print_markers($markers) {
		$dist['mm-13'] = 46500;
		$dist['mm-korr'] = 0.73;
		$dist['mm-dist'] = 10;
//		$dist['kartenanteil'] = 75;

		$dist['mm-zoom'] = $dist['mm-13'] * 2 ** (13 - $this->options['zoom']);
		$dist['min'] = $dist['mm-zoom']/100000000 * $dist['mm-dist'];

		ll_crm_debug($markers);
		foreach($markers as $group => $elements) {
			foreach($elements['data'] as $key => $element) { 
				//Abändern der Koordinaten, damit Einträge außerhalb der Karte an den Rand gestellt werden.
		//		$data[$key]['lon'] = min(max($element['lon'],$borders['A']['lon']),$borders['B']['lon']);
		//		$data[$key]['lat'] = min(max($element['lat'],$borders['C']['lat']),$borders['A']['lat']);
				$point['lon'] = $element['pos'][0];
				$point['lat'] = $element['pos'][1];
				$projekt_lon[] = strval($element['pos'][0]);
				$projekt_lat[] = strval($element['pos'][1]);
				$point['key'] = $key;
				$point['group'] = $group;
				$points[] = $point;
			}		
		}
		array_multisort($projekt_lat,SORT_DESC, SORT_NUMERIC,$projekt_lon,SORT_NUMERIC,$points);
		ll_crm_debug($points);

	//	$i = 1;
		foreach($points as $i => $element) { 
			$ii = 1;
			while (($i > 0) and ($ii < $i) and (floatval($points[$i]['lat']) - floatval($points[$i-$ii]['lat']) < $dist['min'])) {
				$dist['lon'] = (floatval($points[$i]['lon']) - floatval($points[$i-$ii]['lon'])) * $dist['mm-korr'];
				$dist['lat'] = floatval($points[$i]['lat']) - floatval($points[$i-$ii]['lat']);
				ll_crm_debug(array("A",$i,$ii));

				if(($dist['lon'] ** 2 + $dist['lat'] ** 2) < ($dist['min'] ** 2)) {
					if(($dist['lon'] + $dist['lat']) <> 0) { 
						$dist['korr'] = sqrt($dist['min'] ** 2 / ($dist['lon'] ** 2 + $dist['lat'] ** 2));					
						$points[$i]['lon'] = strval(floatval($points[$i-$ii]['lon']) + $dist['lon'] * $dist['korr'] / $dist['mm-korr']);
						$points[$i]['lat'] = strval(floatval($points[$i-$ii]['lat']) + $dist['lat'] * $dist['korr']);
					} else {
						$points[$i]['lon'] = strval(floatval($points[$i-$ii]['lon']) + $dist['min']);				
					}
					
	//				ll_crm_debug(array($i,$ii));					
				}	
				$ii++;				
			}

			$result[$points[$i]['group']]['data'][] =  array('pos' => array($points[$i]['lon'],$points[$i]['lat']), 'text' => strval($i+1));
//			$data[$points[$i]['key']] = $points[$i];
//			$data[$points[$i]['key']]['pos'] = array($points[$i]['lon'],$points[$i]['lat']);
//			$data[$points[$i]['key']]['text'] = $i+1;
//			unset($data[$points[$i]['key']]['infowindow']);
	//		$i++;			
		}
		ll_crm_debug($result);
		return $result;
	}

}
?>
