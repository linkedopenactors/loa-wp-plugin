<?php
/*  Widget für die Auswahl- und Druckfunktionen der Karte.
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class LL_Map_WidgetSelect extends WP_Widget {
	var $buttonset;
	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array( 
			'classname' => 'LL_Map_WidgetSelect',
			'description' => 'Kriterienauswahl für Karten',
		);

		parent::__construct( 'LL_Map_WidgetSelect', 'LL-Tools:Map1 Select', $widget_ops );
	}
	
	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */

	public function widget( $args, $instance ) {
		$class_map = LL_Map_map::singleton(array());		
		$ll_crm_optionset = $class_map->get_optionset();
		$response = $class_map->get_response();
		$submit = $class_map->get_submit();
		ll_crm_debug($submit);
//		$iconPath = plugins_url().'/livinglines-tools/leaflet/images/';
		$iconPath = wp_upload_dir()['baseurl']."/".LL_PLUGIN_NAME."/".$class_map->get_options()['iconPath']."/";
		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		// outputs the content of the widget			
		
		ll_crm_debug($ll_crm_optionset);
		ll_crm_debug($instance);		

		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		echo "<form action = '".get_permalink()."' method='post' id='Abfragen' style='margin:0px;'>";
//		echo "<form method='post' id='Abfragen' style='margin:0px;'>";
		echo "<table class='ll-widget-table'><tr>";		

		$count = 0;
		$spalten = 1;
		$group_out = "<tr>";
		$colspan = 'colspan = 2';
		foreach($ll_crm_optionset as $values) {
			$name = (is_array($values['name'])) ? $values['name'][get_locale()] : $values['name'] ;
			if($values['post'] == 'widget_group') {
				if($count > 0) {
					if($count % $spalten == 1) $group_out .= "<td></td>"; 
					echo $group_out."</tr><tr>";
				}
				$group_out = "<th colspan=2 class='ll-widget-table-group-header'>".$name."</th></tr><tr>";
				$spalten = max($values['spalten'],1);
				$colspan = ($spalten == 1) ? 'colspan = 2' : "";

				$count = 0;
//			} elseif(in_array($values['post'],array('set_tag','set_contact_type','kml_file','sparql_file'))) {
			} else {
				$field = $values['post'];
				$value = 'set_tag';
// 				ll_crm_debug($values,($field=='kml_file'));
				$onInput =  'oninput = "saveCookie(this)"';
				if(($field=='kml_file')) $onInput .= ' oninput ="showLayer(this)"'; 
				$checked = '';
				$count +=1;	
				$texticonColor = (empty($values['TexticonColor'])) ? $values['iconColor'] :$values['TexticonColor'];
				if(isset($submit[$field]) and in_array($values[$value],$submit[$field])) $checked='checked="checked"';
				$group_out .= '<td '.$colspan.' class="ll-widget-table-cell">';

				$group_out .= '<input type="checkbox" name="'.$field.'[]" value="'.$values[$value].'" '.
							$checked.' class="ll-widget-input" '.$onInput.'/> ';
				if($values["iconClass"] == 'Icon')	
					$group_out .=  "<img src='".$iconPath.$values['icon'].".png' width='20' />&nbsp;&nbsp;";	
				elseif($values["iconClass"] == 'fa')
					$group_out .= '<i style="color:'.$texticonColor.';" class=" fa fa-'.$values['icon'].' "></i>';
				$group_out .= ' <span class="ll-widget-inputname">'.$name.'</span></td>';
				if($count % $spalten == 0) $group_out .= "</tr><tr>";						
			}
		}
		if($count > 0) {
			if($count % $spalten == 1) $group_out = "<td></td>"; 
			echo $group_out."</tr>";
		}

		if(!empty($response['button'])) {
			$buttons = explode(",",$response['button']);
			ll_crm_debug($buttons);
			$button_values = array('search','map','list','reset');

			if($class_map->output['page'] == 'list') unset($buttons[2]); //Auswahl Liste nicht auf LIste
			else unset($buttons[1]); //Auswahl Karte nicht auf Karte)

			echo "<tr><td colspan=2 class='ll-widget-button'>";
			foreach($buttons as $key => $button_text) {
				$button_value = $button_values[$key];
				//Damit das System auf der Liste bleibt!
				
				if($button_value == 'search')
					$button_value = ($class_map->output['page'] == 'list') ? 'list' : 'map';
				$button = "<button type='submit' name='mapSelect".$class_map->map."' Value='".$button_value.
							    "' class='ll-submit-button'>".$button_text."</button>";
				echo $button;
			} 
			echo "</td></tr>";
		}					

		echo "</table>";
		echo "</form>";

		echo $args['after_widget'];
		
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin

	}
	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		return $new_instance;
	}
}


?>
