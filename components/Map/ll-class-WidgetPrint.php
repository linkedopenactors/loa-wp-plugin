<?php
/*  Widget für die Druckfunktionen der Karte.
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

						
class LL_Map_WidgetPrint extends WP_Widget {
	/**
	 * Sets up the widgets name etc
	 */
	public function __construct() {
		$widget_ops = array( 
			'classname' => 'LL_Map_WidgetPrint',
			'description' => 'Ermöglicht die Ausgabe der aktuellen Karte im Druckformat',
		);

		parent::__construct( 'LL_Map_WidgetPrint', 'LL-Tools:Map Print', $widget_ops );
	}
	
	/**
	 * Outputs the content of the widget
	 *
	 * @param array $args
	 * @param array $instance
	 */

	public function widget( $args, $instance ) {
		$class_map = LL_Map_map::singleton(array());		
		/** This filter is documented in wp-includes/default-widgets.php */
		$title = apply_filters( 'widget_title', empty( $instance['title'] ) ? '' : $instance['title'], $instance, $this->id_base );
		// outputs the content of the widget			
		
		echo $args['before_widget'];
		if ( ! empty( $title ) ) {
			echo $args['before_title'] . $title . $args['after_title'];
		}

		echo "<form method='post' id='Map-Print' target='_blank' style='margin:0px;'>";
		echo "<table class='ll-widget-table'>";		

		foreach(ll_tools_print_option_set() as $key => $group) {
			if(!isset($group['title'])) $group['title'] = $key;
			$group_title = "";
			$group_out = "";
			$group_title = "<tr><th class='ll-widget-table-group-header'>".$group['title']."</th></tr>";
			if($group['type'] == 'radio') {
				$default = key($group['data']);
				if(isset($_POST[$key])) $default = $_POST[$key];
				foreach($group['data'] as $option => $optionValue) {
					$checked = "";
					if($default == $option) $checked = 'checked';
					$group_out .= "<tr><td class='ll-widget-table-cell'>";
					$group_out .= "<input type='radio' id='".$option."' name='".$key."' value='".$option."' ".$checked." class='ll-widget-input'>";
					$group_out .= "<label for='".$option."'>".$optionValue['title']."</label></td></tr>";
				}
			} else {
			    foreach ($group["data"] as $option => $optionValue) {			
					if(isset($ll_crm_submit[$option]) and in_array($option,$ll_crm_submit[$option])) $checked='checked="checked"';
					$group_out .= '<td class="ll-widget-table-cell">';
					$group_out .= '<input type='.$group['type'].' name="'.$key.'['.$option.']" value="'.$option.'" '.$checked.' class="ll-widget-input"/> ';
					$group_out .= ' <span class="ll-widget-inputname">'.$optionValue['title'].'</span></td>';												
				} 				
			}


			if($group_out != "") {
				echo $group_title;
				echo $group_out;
			}
		}
		echo "<input type='hidden' name='ll-tools-map-print-template' value='".__DIR__."/ll-map-print.php'>";
		$button = "<button type='submit' name='mapSelect".$class_map->map."' Value='print' class='ll-submit-button'>Print</button>";
		
		echo "<tr><td id='ll-widget-suche' class='ll-widget-button'>".$button."</td></tr>";
					

		echo "</table>";
		echo "</form>";

		echo $args['after_widget'];
		
	}

	/**
	 * Outputs the options form on admin
	 *
	 * @param array $instance The widget options
	 */
	public function form( $instance ) {
		// outputs the options form on admin


		$instance = wp_parse_args( (array) $instance, $options);

		ll_crm_debug($instance,false,true);
	}

	/**
	 * Processing widget options on save
	 *
	 * @param array $new_instance The new options
	 * @param array $old_instance The previous options
	 */
	public function update( $new_instance, $old_instance ) {
		// processes widget options to be saved
		
		return $new_instance;
	}
}


?>
