<?php
/*  Template für die Printausgabe von Karten
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if ( have_posts() ) {
   while ( have_posts() ) : the_post();
   	echo the_content();
   endwhile;
}

?>
