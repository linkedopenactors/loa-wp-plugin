<?php
/* Klasse zur Erstellung KML-Files. Erstellt in Version 4.2.2
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

class LL_Map_MakeKML {
	protected $map;
	protected $map_data;
	protected $path;
	protected $dom;
	protected $pm;

	public function __construct($args){
		$this->map = new LL_Map_map($args);
		$this->map_data = $this->map->get_map_json($args);
		$this->path = LL_PLUGIN_DIR.'leaflet/images/';
	}
	
	private function def_structure() {
		$stru['Placemark'][0] = $this->pm;
		$stru['Placemark']['IconStyle']['Icon']['color'] = array();
		$stru['Placemark']['Point']['coordinates'] = array();
		return $stru;
					
	}
		
	private function make_nodes($stru,$nodeName,$data = NULL){
		$values = array("");
		if(isset($stru[0])) {
			if(is_array($stru[0])) {
				$values = $stru[0];
			} else
				$values = array($stru[0]);
			unset($stru[0]);	
		}
		if(isset($data))
			$values = array($data);
		$node = array();
		ll_crm_debug($values);
		foreach($values as $valueKey => $value)
			$node[$valueKey] = $this->make_node($stru,$nodeName,$value);
		ll_crm_debug($values);
		return $node;
	}
	
	private function make_node($stru,$nodeName,$data) {
		ll_crm_debug(array($nodeName));
		$node = $this->dom->createElement($nodeName);
		if(isset($data[0])) {
			if(is_array($data[0])) {
				foreach($data[0] as $attrName => $attrValue) 
					$node->setAttribute($attrName,$attrValue);										
			} else
				$node->nodeValue = $data[0];
		}				
		foreach($stru as $key => $element) {
			$subData = (isset($data[$key])) ? $data[$key] : NULL;
			$childs = $this->make_nodes($element,$key,$subData);
			foreach($childs as $child)
				$node->appendChild($child);									
		}
		return $node;	
	}
	
	private function make_data_sets() {
		$pm = array();
		foreach($this->map_data['main']['Acqua']['data'] as $id => $data) {
			$pm[$id][0]['id'] = 'placemark' . $id;
			$pm[$id]['IconStyle'][0]['iconClass'] = "fa";
			$pm[$id]['IconStyle']['Icon'][0] = "tint";
			$pm[$id]['IconStyle']['Icon']['color'] = "blue";
			$pm[$id]['IconStyle'][0]['makerColor'] = "lightblue";
			$pm[$id]['Point']['coordinates'][0] = $data['pos'][1].",".$data['pos'][0];			
		}
		ll_crm_debug($pm);
		$this->pm = $pm;
	
	}

	public function make_kml() {
		$this->make_data_sets();
		$this->dom = new DOMDocument('1.0', 'UTF-8');

		// Creates the root KML element and appends it to the root document.
		$node = $this->dom->createElementNS('http://www.opengis.net/kml/2.2', 'kml');
		$parNode = $this->dom->appendChild($node);
		$childs = $this->make_nodes($this->def_structure(),'Document');
		foreach($childs as $child)
			$parNode->appendChild($child);
		

		$kmlOutput = $this->dom->saveXML();
		file_put_contents($this->path."test.kml", $kmlOutput);
		//header('Content-type: application/vnd.google-earth.kml+xml')
		return $kmlOutput;


	}

/*
// Creates the Document.
$dom = new DOMDocument('1.0', 'UTF-8');

// Creates the root KML element and appends it to the root document.
$node = $dom->createElementNS('http://earth.google.com/kml/2.1', 'kml');
$parNode = $dom->appendChild($node);

// Creates a KML Document element and append it to the KML element.
$dnode = $dom->createElement('Document');
$docNode = $parNode->appendChild($dnode);

// Creates the two Style elements, one for restaurant and one for bar, and append the elements to the Document element.
$restStyleNode = $dom->createElement('Style');
$restStyleNode->setAttribute('id', 'restaurantStyle');

$restIconstyleNode = $dom->createElement('IconStyle');
$restIconstyleNode->setAttribute('id', 'restaurantIcon');

$restIconNode = $dom->createElement('Icon');

$restHref = $dom->createElement('href', 'http://maps.google.com/mapfiles/kml/pal2/icon63.png');

$restIconNode->appendChild($restHref);
$restIconstyleNode->appendChild($restIconNode);
$restStyleNode->appendChild($restIconstyleNode);
$docNode->appendChild($restStyleNode);

$barStyleNode = $dom->createElement('Style');
$barStyleNode->setAttribute('id', 'barStyle');
$barIconstyleNode = $dom->createElement('IconStyle');
$barIconstyleNode->setAttribute('id', 'barIcon');
$barIconNode = $dom->createElement('Icon');
$barHref = $dom->createElement('href', 'http://maps.google.com/mapfiles/kml/pal2/icon27.png');
$barIconNode->appendChild($barHref);
$barIconstyleNode->appendChild($barIconNode);
$barStyleNode->appendChild($barIconstyleNode);
$docNode->appendChild($barStyleNode);

// Iterates through the MySQL results, creating one Placemark for each row.
while ($row = @mysql_fetch_assoc($result))
{
  // Creates a Placemark and append it to the Document.

  $node = $dom->createElement('Placemark');
  $placeNode = $docNode->appendChild($node);

  // Creates an id attribute and assign it the value of id column.
  $placeNode->setAttribute('id', 'placemark' . $row['id']);

  // Create name, and description elements and assigns them the values of the name and address columns from the results.
  $nameNode = $dom->createElement('name',htmlentities($row['name']));
  $placeNode->appendChild($nameNode);
  $descNode = $dom->createElement('description', $row['address']);
  $placeNode->appendChild($descNode);
  $styleUrl = $dom->createElement('styleUrl', '#' . $row['type'] . 'Style');
  $placeNode->appendChild($styleUrl);

  // Creates a Point element.
  $pointNode = $dom->createElement('Point');
  $placeNode->appendChild($pointNode);

  // Creates a coordinates element and gives it the value of the lng and lat columns from the results.
  $coorStr = $row['lng'] . ','  . $row['lat'];
  $coorNode = $dom->createElement('coordinates', $coorStr);
  $pointNode->appendChild($coorNode);
}

$kmlOutput = $dom->saveXML();
header('Content-type: application/vnd.google-earth.kml+xml');
echo $kmlOutput;
*/


}
?>

