<?php

// functions that create our options page
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

/*
* Adminseiten für CiviCRM - MAP als Ergänzung zu Livinglines-Tools
Component Name: OpenStreetMap für Civi, SparQl;
Components Group: MAP
Components Order: 15

* 
*/


ll_tools_make_menu(array('site_title' => 'Map Options', 'page' => 'map'));

add_action('load-ll-tools_page_ll_tools_map','ll_tools_map_settings_init');
add_action('load-options.php','ll_tools_map_settings_init');


//Das sollte wohl noch weg? => Sollte in ll-tools-Civi-Data geladen werden!
//ll_tools_file_loader("crm","ll-crm-functions"); 
add_action( 'widgets_init', 'll_tools_map_init_sidebar');


function ll_tools_settings_map_cb() {
//	settings_errors('ll_tools_civi');

//	ll_crm_debug(get_transient(LL_TOOLS_OPTION."subSets"),true);
//	ll_crm_debug(LL_classes_optionOptions::get_default(array("map_sets")),true);
//	ll_crm_debug(LL_classes_optionOptions::get_default(array("option_set0","option_set")),true);

//	delete_option(LL_TOOLS_OPTION."map_sets");
//	delete_option(LL_TOOLS_OPTION."option_set0");
//	ll_crm_debug(get_option(LL_TOOLS_OPTION."map_response"),true);
//	$maps = get_option(LL_TOOLS_OPTION."map_sets");
//	$sets = get_transient(LL_TOOLS_OPTION."option_set0_bak");
//	unset($sets['sets']);
//	ll_crm_debug($sets,true);
//	ll_crm_debug(get_option(LL_TOOLS_OPTION."option_set0"),true);	
//	ll_crm_debug($maps,true);
//	ll_crm_debug(ll_map_standard_options());
//	ll_crm_debug(get_transient(LL_TOOLS_OPTION."marker_options"),true);
//	ll_crm_debug(LL_Civi_Data::all_request(array('entity' => 'Tag')),true);
}



function ll_tools_map_settings_init() {
	#### Map - Settings
	$default_args = array('group' => 'map','type' => 'number','title' => 'Map Options');
	$maps = get_option(LL_TOOLS_OPTION."map_sets",array("Basiskarte",'check' => array(0)));
	$opt_values['map_sets'] = array('title' => "Kartendaten");
	$opt_values['map_sets']['options'] = LL_classes_optionOptions::get_suboption_set(array("map_sets"));
	if(!empty($maps['sets']))
	foreach($maps['sets'] as $map => $set) {
		$option = 'option_set'.$map; 
		$setName = (empty($set['name'])) ? 'Karte '.$map : $set['name'];
		$opt_values[$option] = array('title' => 'Optionset für '.$setName, 'type' => 'text');
		$opt_values[$option]['options'] = LL_classes_optionOptions::get_suboption_set(array($option,'option_set'));
	}
	$opt_values['map_response'] = array('title' => 'Kartentexte','type' => 'text');
	$opt_values['map_response']['options'] = LL_classes_optionOptions::get_suboption_set(array("map_response"));
	ll_tools_make_section($default_args,$opt_values);
}





add_filter('ll_tools_delete_map_sets',function($return,$option,$setNr){
//	ll_crm_debug(get_option(LL_TOOLS_OPTION.'option_set0'),true);
//	ll_crm_debug(array($return,$option,$setNr),true,true);
	if(!empty(get_option(LL_TOOLS_OPTION.'option_set'.$setNr)['sets'])) {
		add_settings_error('ll_tools',esc_attr('settings_updated'),
						"Karte wurde nicht gelöscht, es existiert ein Optionset für diese Karte");
		return false;	
	} else {
		delete_option(LL_TOOLS_OPTION.'option_set'.$setNr);
	}
	return true;
},10,3);

function ll_tools_map_sets($typ = "map") {
	$sets = get_option(LL_TOOLS_OPTION.$typ."_sets",array());
	$maps = array();
	if(is_array($sets) and isset($sets['sets']))
		foreach($sets['sets'] as $mapNr => $set) {
			$maps[$set['name']." (".$mapNr.")"] = $typ.$mapNr;
		
		}
	return $maps;
}

	/**
	 * Definiert die OptionOptions für die Antworttexte für Karten
	 * Filter aus LL_classes_optionOptions
	 * 
	 * @since
	 *
	 * @param string $typ Gewünschtes Rückgabearray
	 *
	 * Anmerkung: Hier sollte an erster Stelle das Gesamtarray übergeben werden,
	 * damit die Filter hintereinander geschaltet werden können (4.2.5.6)
	 */


add_filter('ll_tools_get_standardSet_map_response','ll_map_standard_response',10,2);
function ll_map_standard_response($options = array(), $typ = "default") {
	$options = array('default' => array(
		'mapNr' => array('',"Karte, für die das Set gilt",'type' => 'select', 
					  'options' => array_merge(array('all' => "all"),ll_tools_map_sets())),
		'language' => array('',"Sprache",'type' => 'select', 
						'options' => apply_filters('ll_tools_languages',array("all" => ""))),
		'title' => array('Karte','Titel der Karte'),
		'start' => array('Hier werden die Ergebnisse ihrer Auswahl dargestellt werden.'.
					  ' Bitte klicken Sie auf "Karte" um eine Auswahl zu treffen.',
					   "Starttext für leere Karte",'type' => 'textarea',
					   'style' => "resize:both;",'cols' => 40, 'rows' =>5),
		'empty' => array('Für ihre Auswahl wurden keine Ergebnisse gefunden!',"Nachricht, wenn keine Ergebnisse gefunden wurden"),
		'result' => array('Es wurde(n) %1 Teilnehmer gefunden.',"Darstellung der Anzahl der Ergebnisse"),
		'map' => array('Karte auf',"Karte auf ... (für den Ausschnitt)"),
		'mapfit' => array('Ergebnisse anpassen',"Ergebnisse anpassen"),
		'mapdefault' => array('Standard setzen.',"Voreinstellungen"),
		'button' => array("Suchen,Karte,Liste","Button für das Widget in dieser Reihenfolge",'type' => 'text')
	), 'definition' => array(
		'detail_header' => "Texte für die Karte ",
		'element_name' => "Textset",
		'element_new' => "Neue Textset hinzufügen",
 	));
	if($typ == 'all') return $options;
	return($options[$typ]);
}

	/**
	 * Definiert die OptionOptions für Karten
	 * Filter aus LL_classes_optionOptions
	 * 
	 * @since
	 *
	 * @param string $typ Gewünschtes Rückgabearray
	 *
	 * Anmerkung: Hier sollte an erster Stelle das Gesamtarray übergeben werden,
	 * damit die Filter hintereinander geschaltet werden können (4.2.5.6)
	 * 
	 * Die Funktion wird aus ll-class-map direkt aufgerufen.
	 */


add_filter('ll_tools_get_standardSet_map_sets','ll_map_standard_options',10,2);
function ll_map_standard_options($options = array(),$typ = "default") {
	//Array: default, title, validate, min, max ...
	$options = array('default' => array(
		'name' => array('',"Bezeichnung der Karte",'type' => 'text'),
	 	'width' => array(''),
		'lon' => array(0,"geogr. Länge (11,936) für das Zentrum der Karte",'step' => 0.0001, 'max' => 90),
		'lat' => array(0,"geogr. Breite (42,593) für das Zentrum der Karte",'step' => 0.0001, 'max' => 180),
		'height' => array("100px","Höhe der Karte in px, (700)",'type' => 'text'),
		'zoom' => array(0,"Zoomfaktor der Karte bei Aufruf (11)",'min' => 0,'max' => 15),
		'minzoom' => array(0,"Zoom min. begrenzt auf (max 15)",'min' => 0,'max' => 20),
		'maxzoom' => array(15,"Zoom max. begrenzt auf (max 0)",'min' => 0,'max' => 20),
		'zoomControl' => array(true),
//		'desc' => array('Keine Projekte in dieser Auswahl'),
		'map' => array(true),
		'reset' => array(''),
		'limit' => array(250),
		'mapCenter' => array(''),
		'map_page' => array("","ID der Kartenseite für wechselweise Links"),
//		'map_project' => array("","ID der Projektseite für wechselweise Links"),
		'sidebar' => array(false,'Sidebar zur Auswahl der Kriterien', 'type' => 'checkbox'),
//		'button' => array("Suchen,Karte,Liste","Button für das Widget in dieser Reihenfolge",'type' => 'text','language' => true),
		'iconPath' => array("images","Verzeichnis für Icon-Images",'type' => 'text'),
		'layout' => array('map')
	), 'definition' => array(
		'detail_header' => "Daten für Karte ",
		'element_name' => "Karte",
		'new' => array('',"Neue Kartendaten hinzufügen",'type' => 'checkbox')
 	));
	if($typ == 'all') return $options;
	return($options[$typ]);
}

	/**
	 * Definiert die OptionOptions für die OptionSets für Karten
	 * Filter aus LL_classes_optionOptions
	 * 
	 * @since
	 *
	 * @param string $typ Gewünschtes Rückgabearray
	 *
	 * Anmerkung: Hier sollte an erster Stelle das Gesamtarray übergeben werden,
	 * damit die Filter hintereinander geschaltet werden können (4.2.5.6)
	 */


add_filter('ll_tools_get_standardSet_option_set','ll_map_standard_optionset',10,2);
function ll_map_standard_optionset($options = array(), $typ = "default") {
	$colors = array('-' => "", 'red', 'darkred', 'lightred', 'orange', 'beige', 'green', 'darkgreen', 'lightgreen', 
						'blue', 'darkblue', 'lightblue', 'purple', 'darkpurple', 'pink', 'cadetblue', 'white', 'gray', 'lightgray', 'black');
	$base_type = array("-","Gruppe" => 'widget_group');
	$type = apply_filters('ll_map_optionset_type',$base_type);
	$iconClass = array("-" => "", "FontAwesome" => "fa", "Image" => "Icon", "Kreis" => "Circle");

	$subSets = array("-" => ""); //nur für den Fall, dass ein Select aktiv ist, ohne Werte

	$options = array('default' => array(
		'post' => array('','Typ - Ändern des Types kann Verlust von Werten bedeuten','type' => 'select',
						'options' => $type),
		'set_tag' => array('',"Den gewünschten Tag auswählen",$base_type,'type' => 'select', 'options' => ""),
		'data_set'  => array(0),
		'name' => array('',"Name",'language' => true),
		'group' => array('','Gruppe','type' => 'number'),
		'group_elem' => array('','Reihenfolge in der Gruppe',$base_type,'type' => 'number'),
		'prio' => array('','Marker mit höherer Priorität rücken weiter in den Vordergrund','type' => 'number'),
		'markerColor' => array('',"Farbe des Markers",$base_type, 'type' => 'select', 'options' => $colors),
		'iconClass' => array('',"Klasse für Icons (fa=FontAwesome,icon=Icon:iconType,Circle)",$base_type, 
									'type' => 'select', 'options' => $iconClass), 
		'iconColor' => array('',"Farbe des Icons (nur fa)",$base_type,'type' => 'select', 'options' => $colors), 
		'icon' => array('',"Icon-Name für FontAwesome oder <Name>.png für Icon",$base_type), 
//		'icontype' => array(""), //veraltet => darf erst weg, wenn aus make_attr_set entfernt!
		'line' => array('','Gruppenname für eine Linie (wenn zugeordnet)',$base_type,'type' => 'select', 'options' => $subSets),
		'line_style+color' => array('','Farbe der Linie',$base_type, 'type' => 'select', 'options' => $colors),
		'point' => array('','Gruppenname für SubPunkte (wenn zugeordnet)',$base_type,'type' => 'select', 'options' => $subSets),
		'point_style+icon' => array('','Icon-Name für den Punkte',$base_type),
		'point_style+iconClass' => array('','Klasse für den Punkt, wird wenn leer übernommen',$base_type,
											'type' => 'select', 'options' => $iconClass),
		'point_style+iconColor' => array('','Farbe des Icons, wie Klasse',$base_type,'type' => 'select', 'options' => $colors),
		'line_box' => array('','Box die als PopUp für die Linie verwendet wird',$base_type),
		'spalten' => array('','Anzahl der Spalten',array_slice($type,2),'type' => 'number'),
		'tag' => array(0),
		'tagname' => array(''),
		'TexticonColor' => array('','texticonColor',$base_type),
		'project' => array('',"Projektseite aktiv",$base_type,'type' => 'checkbox'),
		'aktiv' =>  array("",'Aktiv','type' => 'checkbox')
	), 'definition' => array(
		'detail_header' => "Tags und Flags für Karte ",
		'element_name' => "Flag ",
		'element_group' => "post",
		'new' => array('-',"Neues Element hinzufügen","type" => "select",'options' => $type)
	));
	if($typ == 'all') return $options;
	return($options[$typ]);

}

	/**
	 * Definiert die Optionns für die Auswahl der Gruppe
	 * ACHTUNG der Filter lautet 'll_tools_default_'...
	 * 
	 * @since
	 *
	 * @param array $data_set Das aktuelle Set aus der Defintion
	 * @param string $post Das aktuelle Auswahlfeld im DataSet
	 * @param array $data_sets Alle Sets aus der Definition
	 * @param string $option OptionOption Element
	 *
	 */

//
add_filter('ll_tools_default_option_set','ll_tools_default_option_set',10,4);

function ll_tools_default_option_set($data_set,$post,$data_sets,$option) {
	static $group_options;
	if(!isset($group_options[$option])) {
		$group_options[$option] = array("-" => "");
		foreach($data_sets as $element) {
			if($element['post'] == 'widget_group') {
				$name = (is_array($element['name'])) ? $element['name'][get_locale()] : $element['name'];
				$group_options[$option][$name." (".$element['group'].")"] = $element['group'];				
			}
		}	
	}
	if($post != 'widget_group') {
		ll_crm_debug($data_sets);
		//Hier werden die Options für die Auswahl der Gruppe erstellt
		$data_set['group']['type'] = 'select';
		$data_set['group']['options'] = $group_options[$option];	
	}
	return $data_set;
}

	/**
	 * Übersteuert die Options für die Projektseite
	 *
	 * @since 4.2.11
	 *
	 */


add_filter('ll_tools_get_map_args_project',function($args) {
	return array('zoom' => 15, 
				  'maxzoom' => 16, 
				  'reset' => 'NoCookie', 
				  'mapCenter' => 'Auto',
				  'map_init' => "<div id='map' class='map-project'></div>",
				  'sidebar' => "");
},10,1);


	/**
	 * Definiert die Felder, die es für die Karte geben muß, mit Defaults
	 *
	 * @since 4.2.6, ab 4.2.11 von der ll-class-map nach hierher übersiedelt.
	 *
	 */


	function ll_tools_default_map_fields() {
	return array('id' => "",
//					 'pos' => array(),
					 'link' => "",
					 'display_name' => "",
					 'image_URL' => "",
					 'city' => "",
					 'description' => "",
					 'childs' => false,
					 'point_data' => array());
	}

	/**
	 * Definiert die Felder, die es für den Druck der Karte geben muß, mit Defaults
	 *
	 * @since 4.2.6, ab 4.2.11 von ll-class-map.php hier übersiedelt.
	 *
	 */

 
  function ll_tools_print_option_set() {
	$optionset = array("Format" => array("data"=> 
					array("A4" => array('title' => "A4 landscape (294 x 210 mm)",'format' => array(841, 594, 13)),
						  "A3" => array('title' => "A3 landscape (420 x 297 mm)",'format' => array(594, 420, 12)),
						  "A2" => array('title' => "A2 landscape (594 x 420 mm)",'format' => array(420, 297, 12, 70)),
						  "A1" => array('title' => "A1 landscape (841 x 594 mm)",'format' => array(294, 210, 11, 70))
					), 'type'=>'radio'),
					"Legend" => array("data" =>
					array("maponly" => array('title' => "Map only, without listing"),
						  "list"    => array('title' => "Listing without reference"),
						  "listno"	=> array('title' => "Numbered icons with reference")
					), 'type' => 'radio', 'title' => 'Listing'),
					"Columns" => array("data" =>
					array("one" => array('title' => 'one column'),
						  "two" => array('title' => 'two columns')
					), 'type' => 'radio', 'title' => 'Listing Columns'),
					"Options" => array("data" =>
					array("pict" => array('title' => 'show pictures')
					), 'type' => 'checkbox', 'title' => 'Listing Options')
				);
				
	return apply_filters('ll_print_options',$optionset);
}

	/**
	 * Übersteuert das aktuelle Template für den Druck. 
	 *
	 * @since 4.2.11
	 *
	 */


if(isset($_POST['ll-tools-map-print-template'])) {
	ll_crm_debug($_POST['ll-tools-map-print-template']);
	add_filter('template_include',function($template) {
		//var/www/html/bolsenalagodeuropa/wp-content/themes/hathor/page-full_width.php
		ll_crm_debug($template);
		return $_POST['ll-tools-map-print-template'];	
},10,1);

}

	/**
	 * Kopiert die Substyles
	 * 
	 * @since
	 *
	 * @param array $set Stylearray
	 * @param string $subtype SubType der erstellt werden soll.
	 *
	 */

function ll_tools_modify_option_set_substyles(&$set,$subtype) {
	if(!empty($set[$subtype])) 
		foreach($set[$subtype.'_style'] as $style => $dummy)
			if(empty($set[$subtype.'_style'][$style])) $set[$subtype.'_style'][$style] = $set[$style];

}

	/**
	 * Kopiert ein Optionset, wenn map_set kopiert wird.
	 * 
	 * @since
	 *
	 * @param array $set MapSet, das kopiert wird.
	 * @param array $value beim Abspeichern des MapSets
	 *
	 */

add_filter('ll_tools_modify_all_map_sets',function($set,$value) {
	ll_crm_debug(array($set,$value));
	if(isset($set['sets']))
		foreach($set['sets'] as $setNr => $map_set ) {
			if(isset($value['set_'.$setNr]) and ($value['set_'.$setNr] == 'kopieren')) {
				$option_set = get_option(LL_TOOLS_OPTION.'option_set'.$setNr);
				update_option(LL_TOOLS_OPTION.'option_set'.$set['check'],$option_set);
				break;
			}
		}
	return $set;
},10,2);
	


	/**
	 * Sortiert das Optionset wenn es verändert wurde.
	 * Sortiert wird nach Reihenfolge der Gruppe und innerhalb der Gruppe
	 * 
	 * @since
	 *
	 * @param array $set Optionset, das geändert wird.
	 * @param array $value beim Abspeichern des Optionsets
	 *
	 */

add_filter('ll_tools_modify_all_option_set',function($set,$value) {
//	ll_crm_debug(array($value,(is_string($value['save']) and empty($value['save'])),gettype($value['save'])));
	if(isset($value['save']) and is_string($value['save']) and empty($value['save'])) return $set; // Ausstieg für Update
	if(!isset($set['sets'])) return $set;
	$new = array();
	$debug = false;
//	$debug = ($setNr == 'option_set0');
	ll_crm_debug($set,$debug);
	foreach($set['sets'] as $element) {
		ll_crm_debug($element);
		if(empty($element['group_elem'])) {
			$element['group_elem'] = 0;
			if($element['post'] != 'widget_group') $element['group_elem'] = 10;
		}
		while(isset($new[$element['group']][$element['group_elem']])) {
			$element['group_elem'] += 10;
		}
		$new[$element['group']][$element['group_elem']] = $element;
	}
	ll_crm_debug($new);
	$set['sets'] = array();
	ksort($new);
	foreach($new as $key => $elements) {
		ksort($elements);
		foreach($elements as $element)
			$set['sets'][] = $element;
	}
	ll_crm_debug($set,$debug,$debug);
	return $set;
},10,2);


	/**
	 * Initialisiert die Sidebar für die Karte und die zugehörigen Widgets
	 * 
	 * @since
	 *
	 */

function ll_tools_map_init_sidebar() {
	$maps = get_option(LL_TOOLS_OPTION.'map_sets');
	$before_widget = '<div id="%1$s" class="widget %2$s"><div class="widget_wrap">';
	$after_widget = '</div></div>';
	$before_title = '<h3 class="widgettitle">';
	$after_title = '</h3>';
	$sidebar = false;

	if(isset($maps['sets']))
		foreach($maps['sets'] as $mapNr => $map) {
			ll_crm_debug($map);
			if(!empty($map['sidebar'])) {
				$sidebar = true;
				register_sidebar( array(
					'name' => __( 'Sidebar für Karte '.$map['name'], 'll_map_'.$mapNr ),
					'id' => 'mapselect_'.$mapNr,
					'description' => __( 'Wird von den Karten/Listenansicht '.$map['name'].' verwendet' ),
					'before_widget' => $before_widget,
					'after_widget' => $after_widget,
					'before_title' => $before_title,
					'after_title' => $after_title
					));
			
			}
	}
	if($sidebar) {
		register_widget( 'LL_Map_WidgetSelect' );
		register_widget( 'LL_Map_WidgetPrint' );	
	}
}

################ Map Settings für KML_Files
//add_action('ll_tools_get_map_data', 'll_tools_make_kml_json',10,1);

function ll_tools_make_kml_json($map) {
	$kml_files = array();
	//Achtung Optionset ist anders gebaut!
	foreach($map->get_optionset() as $optionset) {
		ll_crm_debug($optionset['post']);
		if($optionset['post'] == 'kml_file') {
			foreach($optionset as $key => $value) {
				if(is_array($value))
					foreach($value as $valuekey => $valuevalue)
						if(empty($valuevalue)) unset($optionset[$key][$valuekey]);
				if(empty($optionset[$key]))
					unset($optionset[$key]);
			}
			$kml_files[] = $optionset;			
		}
	}
	return $kml_files;		
}

add_filter('ll_map_optionset_type',function($types) {
	$types = $types + array("KML-File" => 'kml_file');
	return $types;
},5,1);

add_filter('ll_tools_default_option_set_kml_file',function($data_set){
	$data_set['set_tag'][1] = "Name des KML-Files ohne Extension";
	$data_set['set_tag']['options'] = "";
	unset($data_set['set_tag']['type']);
	$styles = array('line','line_style+color','point','point_style+icon','point_style+iconClass','point_style+iconColor','line_box'); 
	foreach($styles as $style) unset($data_set[$style]);
	return $data_set;
},10,1);

################ Initialisiert die Funktionen für CiviCRM
add_action('init_ll_functions','init_ll_functions');
//wird extern aufgerufen bei bleu, daher als externe Funktion ausgebildet!
function init_ll_functions($params = array()){
	if(!empty($params['func'])) {
		if(in_array(strtolower($params['func']),array('map','project','flexlist'))) {
			//Diese Einbidnungen sind notwendig!
			require_once('ll-map-functions.php');
			require_once('ll-map-templates.php');
//			require_once('ll-map-default-settings.php');
			
			//Anführen der Funktionen, die ohne Karte nicht funktionieren!
			//oder andere Lösung? 
			//oder gar keine?
		}
	}	
}
	
?>
