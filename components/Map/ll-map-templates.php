<?php
/* Enthält die Hilfsfunktionen für die Karte/Liste
*/

defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

if(!function_exists('ll_show_map_sidebar')){
	function ll_show_map_sidebar($respText) {
		$title = (empty($respText['title'])) ? get_the_title() : $respText['title'];
		$map_reset="setMapView('Reset')";
		$map_view =	'<p id="map-text">'.$respText['map'].
						' <span id="map-section" onClick="fitMapView()" >'.$respText["mapfit"].'</span>';
		if(!isset($respText['mapCenter']) or ($respText['mapCenter'] != 'Auto'))				
			$map_view .= ' / <span id="map-standard" onClick="'.$map_reset.'">'.$respText["mapdefault"].'</span>';
		$map_view .= '</p>';

		echo "<div id='ll-map-widget' class='".$respText['sidebar']."'>";
//		ll_crm_debug($respText['error'],true);
		echo "<div id='ll-map-widget-header' onClick = 'zeigeWidget()'>";		
		echo "<h3 id = 'll-map-title' >".$title."</h3>";
		if(isset($respText['error'])) {
			echo "</div><div id='mapInfo' class = 'map-over'>";
			echo '<p id="map-result">'.$respText['error']->get_error_message().'</p>';
			echo '</div></div>';
			return;
		}
		if(current_user_can('administrator') and $respText['print']) 
			echo "<span id='ll-map-print-icon' onClick = 'startPrint()'></span>";
		if($respText['select']) {
			echo "<span id='ll-map-select-icon' ></span>";
			//Antwortkästen
			echo "</div>
				<div id='mapInfo' class = 'map-over'>
				<div class='post_info_wrap'>";
			if(is_numeric($respText['response']) and ($respText['response'] > 0)) {
				echo '<p id="map-result">'.str_replace("%1",$respText['response'],$respText["result"]).'</p>'.$map_view;
			} elseif(is_numeric($respText['response'])) {
				echo '<p>'.$respText["empty"].'</p>';						
			} elseif($respText['response'] == 'START') {
				echo '<p>'.$respText["start"].'</p>';
			}

			echo "</div>
				</div>";		
		} else {
			echo $map_view;
			echo "</div>";
		}
		echo "<div id='sidebar'>
			    <div class='widgets'>";          
			       if ( is_active_sidebar($respText['sidebar']) || !dynamic_sidebar($respText['sidebar']) ) dynamic_sidebar($respText['sidebar']);
		echo "</div>
			</div>
		</div>";
	}	
}


/**
 * Aufbau der InfoBox auf der Karte. Filter wird nun beim Aufruf in Map/ll-class-map.php aufgerufen
 *
 * @since 4.2.7
 *
 * @param array $row vorbereitete Daten aus dem Kontakt
 */

/******* Info Box für Kartendarstellung
*/
if(!function_exists('getInfoContent')) {
	function getInfoContent($row) {
//		$row = apply_filters('preInfoContent',$row);
	    $infocontent='';
		$bild = "";
		if(isset($row['image_URL']) and ($row['image_URL'] != "") ) 
			$infocontent .= "<img src='".$row['image_URL']."' width='80'/>";			

	   $infocontent .= "<div class='projektinfos'>";
		
		if(isset($row['link']) and ($row['link'] != '')) {
			$infocontent.="<a href='".$row['link']."'><strong>".$row['display_name']."</strong></a><br>";		
		} else {
			if(isset($row['display_name'])) $infocontent.= "<strong>".$row['display_name']."</strong><br>";
		}

	//    if($row["postal_code"]) $infocontent.='<i>'.$row["postal_code"].'</i>';
	   if(isset($row["city"])) $infocontent.= ' <i>'.$row["city"].'</i><br>';

		if(!empty($row['description'])) {
		   $infocontent .= "<div class='projektinfos-desc'>";
			$text = str_replace(array(chr(13),chr(10),"<br />"),array("","<br>","<br/>"),$row['description']);
			ll_crm_debug($row);
			$more = "Details hier";						
			if(strlen($text) > 100) {
				$text	= substr($text,0,strpos($text," ",100))."... ";
				$more = "weitere Infos hier";			
			}
			if(!empty($row['more']))
				$text .= "<br><a href='".$row['more']."'>".$more."</a>";					

		   $infocontent .= $text."</div>";
		}
		
		if(isset($row["pos"])) {
			if(is_array($row["pos"])) $row['pos'] = implode("/",$row['pos']);
			$infocontent.= "<div class='projektinfos-pos'>".$row['pos']."</div>";				
		} else {
			$infocontent.= "<div class='projektinfos-pos'>".$row['geo_code_1'].'/'.$row['geo_code_2'].'</div>';		
		}

		if(isset($row["childs"]) and ($row['childs'] != "")) {
			$subpoints = "subpoints";
			$onclick = "showsubs('".$row['childs']."')";
			$infocontent.="<div onclick=".$onclick." class='ll-map-infobox-subpoints'>";
			$infocontent.="<small >".$subpoints."</small>";
			$infocontent.="</div>";		
		}

		$infocontent .= '</div>'; //Ende ProjektInfos

		ll_crm_debug($infocontent);
		$infocontent = str_replace(array("\n",chr(9),"\r",chr(92)),array("<br>","","<br>",""),$infocontent);			
      return str_replace(array("'",'"'),"@@",$infocontent);
	}
}

function ll_map_list_rendering($map) {
//	$tag_list = $map->get_submitted()['sets'];
	$options = $map->get_optionset();

	$projects = $map->list_groupValues; 
	ll_crm_debug($options);
//	ll_crm_debug($tag_list);
	
/*
	if(isset($Submit['Columns'])) $columns = $Submit['Columns']; else $columns = 2;
	if(isset($Submit['Print'])) $print = $Submit['Print']; else $print = false;
*/
	$columns = 2;
	$print = false;
	$group = "";
	
	$LL_crm_out = '<div class="ll_list_box">';
	foreach($options as $set => $option) {
		if($option['post'] == 'widget_group') {
			 $group = $option['name'];
			 continue;		
		}
		if(!isset($projects[$set])) continue;
		
		$LL_crm_out .= '<div class="ll_list_header_box">';
		$LL_crm_out .= '<div class="ll_list_header" id="ll_list_header">';
		$LL_crm_out .= '<span class = "ll_list_header_group">'.$group.'</span>';
		$LL_crm_out .= '&nbsp;<i style="color:'.$option['TexticonColor'].';" class=" fa fa-'.$option['icon'].' "></i>&nbsp;';
		$LL_crm_out .= '<span class = "ll_list_header_item">'.$option["name"].'</span>';
		$LL_crm_out .= "</div>";			
		$LL_crm_out .= '<div class="ll_list_line_box">';
		foreach($projects[$set] as $element) { 
			ll_crm_debug($element);
			$LL_crm_out .= '<div class="ll_list_line ll_list_'.$columns.'_column" id="element_'.$element["id"].'">';
			$LL_crm_out .= '<div class="ll_list_line_content">';
//			$LL_crm_out .= '<div class="ll_list_listno" id="listno_'.$element["id"].'"></div>';
			if(!$print) ll_display_image($element,$LL_crm_out);
			$LL_crm_out .= '<div class="ll_list_title">';
			$link = $map->get_linkUrl($element);
			$link = (empty($link)) ? $element['link'] : $link;
			if(empty($link))
				$LL_crm_out .= $element['display_name'];
			else 
				$LL_crm_out .= "<a href='".$link."'>".$element['display_name'] . "</a>";
//			$LL_crm_out.="<a href='".$map->get_linkUrl(). "?uid=".$element['id'] . "&set=".$element['set']."'>" .
//										 $element['display_name'] . "</a>";
			$LL_crm_out .= '</div>'; 
			if(isset($element['notes'])) $LL_crm_out .= '<div class="ll_list_text">'.$element['notes'].'</div>';
			$LL_crm_out .= '</div></div>'; 
		}
		$LL_crm_out .= '</div></div>';
	} 
	$LL_crm_out .= '</div>';
	return $LL_crm_out;
}

function ll_map_project_rendering($map) {
	$projekt_out = current(current($map->list_groupValues)); 
	ll_crm_debug($projekt_out);
//ll_crm_debug($map->get_options(),true);
//ll_crm_debug($map->get_map('map'),true);
	$LL_crm_out = "<div id='ll-project-header'>";
	$LL_crm_out .= "<img src='".$projekt_out['image_URL']."' title='caption'>"; 
	$LL_crm_out .= $map->get_map('map');
	$LL_crm_out .= "<div id='ll-caption' class='nivo-caption'>";
	$LL_crm_out .= "<h3>".$projekt_out['display_name']."</h3>" ;
	$LL_crm_out .= "</div></div>";  			
	//dürfte nur bei Set = 0 gemacht werden!

	$LL_crm_out .= '<div class="project_box">';
	
//		$LL_crm_out .= '<div class="project_line" id="project_header">';
//		$LL_crm_out .= "</div>";
	if(isset($projekt_out['data']))
		foreach($projekt_out['data'] as $element) {
			ll_crm_debug($projekt_out);
			ll_crm_debug($element);
			$LL_crm_out .= '<div class="project_line" id="'.$element['type'].'">';
			$LL_crm_out .= '<div class="project_line_content">';
			$LL_crm_out .= '<div class="project_title">';
			$LL_crm_out .= $element['title'];
			if(!empty($element['project_type']))
				$LL_crm_out .= '<p class = "project_type">('.$element['project_type'].')</p>';
			$LL_crm_out .= "</div>";
			
			if(isset($element['img2'])) {
				$LL_crm_out .= '<div class="project_text_2">';
				$LL_crm_out .= $element['html'];
				$LL_crm_out .= "</div>";

				ll_display_image($element,$LL_crm_out,"_2");
	//				ll_display_image($element['img2'],$LL_crm_out,"_2");
				
			} else {
				$LL_crm_out .= '<div class="project_text">';
				$LL_crm_out .= $element['html'];
				$LL_crm_out .= "</div>";

				ll_display_image($element,$LL_crm_out);
			}
			
			$LL_crm_out .= "</div></div>";			
		}
	$LL_crm_out .= '</div>';
	return $LL_crm_out;	
}



/*
<video class="wp-video-shortcode" id="video-1679-2" width="630" height="354" preload="metadata" controls="controls">
<source type="video/mp4" src="http://www.bolsenalagodeuropa.local/wp-content/uploads/civicrm/custom/cantinone_18022018_trim_833309009cbb61c45692b420a2037fb7.mp4?_=2" />
<a href="http://www.bolsenalagodeuropa.local/wp-content/uploads/civicrm/custom/cantinone_18022018_trim_833309009cbb61c45692b420a2037fb7.mp4">http://www.bolsenalagodeuropa.local/wp-content/uploads/civicrm/custom/cantinone_18022018_trim_833309009cbb61c45692b420a2037fb7.mp4</a>
</video>
*/

function ll_display_image($element, &$LL_crm_out, $attr = "") {
//	ll_crm_debug($element,!empty($element['image_URL']));
	if(isset($element['image_URL'])) $element['img'] = $element['image_URL'];
//	if((isset($element['img']) and ($element['img'] !='')) or (isset($element['img_src']) and ($element['img_src'] != ''))) {
	if(isset($element['img']) or isset($element['img_src'])) {
		if(!isset($element['img-title'])) $element['img-title'] = '';
		if(array_key_exists('img-type',$element) and ($element['img-type'] == "video/mp4")) {
			$LL_crm_out .= '<div class="ll_list_video">';
//			$LL_crm_out .= wp_video_shortcode( array('src' => $element['img-uri']));
//			$LL_crm_out .= "[video src = '".$element['img']."']";
			$LL_crm_out .= "<video controls='controls' preload='metadata'>";
			$LL_crm_out .= "<source type='video/mp4' src='".$element['img-uri']."'>";
			$LL_crm_out .= 	"<a href='".$element['img']."'>Video</a></video>";
			$LL_crm_out .= "</div>";				
		} elseif(isset($element['img_src']) ) {
			$LL_crm_out .= '<div class="ll_list_foto'.$attr.'">';
			$LL_crm_out .= $element['img_src'];
			$LL_crm_out .= "</div>";
		} else {
			$LL_crm_out .= '<div class="ll_list_foto'.$attr.'">';
			if($element['img'] == '') $LL_crm_out .= '&nbsp';
			$LL_crm_out .= "<img src='".$element['img']."' title='".$element['img-title']."'>";
			if(isset($element['img2']))
			foreach($element['img2'] as $image) {
				if(!isset($image['img-title'])) $image['img-title'] = '';
				$LL_crm_out .= "<img src='".$image['img']."' title='".$image['img-title']."'>";				
			}
			$LL_crm_out .= "</div>";
		}
	}
	return $LL_crm_out;					
}

?>
