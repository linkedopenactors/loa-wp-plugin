<?php
/* Übernahme einer modifizierten Version des alten Plugins Domain-Mapping.
*/
/*
Component Name: Ermöglicht eine Zuordnung von Domains zu Blogs
Component Multisite: only
components Group: NET

Plugin Name: WordPress MU Domain Mapping
Plugin URI: http://ocaoimh.ie/wordpress-mu-domain-mapping/
Description: Map any blog on a WordPress website to another domain.
Version: 0.5.5.1
Author: Donncha O Caoimh
Author URI: http://ocaoimh.ie/
*/
/*  Copyright Donncha O Caoimh (http://ocaoimh.ie/)
    With contributions by Ron Rennick(http://wpmututorials.com/), Greg Sidberry and others.

    This program is free software; you can redistribute it and/or modify
    it under the terms of the GNU General Public License as published by
    the Free Software Foundation; either version 2 of the License, or
    (at your option) any later version.

    This program is distributed in the hope that it will be useful,
    but WITHOUT ANY WARRANTY; without even the implied warranty of
    MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
    GNU General Public License for more details.

    You should have received a copy of the GNU General Public License
    along with this program; if not, write to the Free Software
    Foundation, Inc., 59 Temple Place, Suite 330, Boston, MA  02111-1307  USA
*/

//Für den Fall, dass Sunrise nicht beim Starten geladen wurde!
if(!function_exists("ll_get_domains")) {
	include_once(__DIR__."/sunrise.php");
} else {
	if(defined('LL_DOMAIN_MAPPING')) {
		//Hier wird die Location gemacht, im Grunde nur bei der ersten manuellen Eingabe der URL, alle Aufrufe sollten automatisch richtig sein
		ll_crm_debug($_SERVER);
		$url = home_url();
		if(strpos($_SERVER[ 'REQUEST_URI' ],'/wp-admin') !== false or strpos($_SERVER[ 'REQUEST_URI' ],'/wp-login') !== false)
			$url = site_url();
		if($_SERVER['HTTP_HOST'] != wp_parse_url($url,PHP_URL_HOST)) {
			ll_crm_debug("Kontrollfehler: ".$url,true);
			header( "Location: {$url}{$_SERVER[ 'REQUEST_URI' ]}", true, $redirect );
			exit;	   
		}	
	}
}
define('LL_CROSS_DOMAIN',(home_url() != site_url()));

add_action('load-toplevel_page_ll_tools_plug','ll_tools_domain_settings_init');
//add_action('load-ll-tools_page_ll_tools_plug','ll_tools_domain_settings_init',10,0);
//Dies ist ein Hack, weil die Werte sonst nicht abgespeichert werden!
//Vielleicht kein Hack, sondern notwendig?
//Kann man das auch anders lösen?
add_action('load-options.php','ll_tools_domain_settings_init',10,0);

//Keine eigene Menuseite, daher nicht aktiviert.
//function menu_options(array('site_title' => 'Komponenten','group' => 'plug'));

function ll_tools_settings_dom_cb() {
	ll_dm_domain_listing(is_network_admin());
}

function ll_tools_domain_settings_init() {
	//erste Bedinung bewirkt beim Speichern keine Prüfung an dieser Stelle!
	if(is_network_admin() and !ll_dm_check()) return;
	###### Domain-Mapping
	$opt_values = array();
	$default_args = array('group' => 'dom', 'page' => 'plug', 'title' => "Domain Mapping");
//	ll_get_domains(); //Damit $wpdb->dm_table initialisiert wird.
	if(is_network_admin()) {
		$opt_values['dm_domains'] = ll_edit_domain();
	} else {
		//hier nur ein Register, weil die Felder schon in settings_dom_db aufgebaut werden
		register_setting(LL_TOOLS_OPTION.$default_args['page'],LL_TOOLS_OPTION.'dm_active');
		$opt_values['dm_domains'] = ll_edit_domain();
	}
	$opt_values['dm_options'] = ll_edit_dm_options();	
	ll_tools_make_section($default_args,$opt_values);
}

function ll_edit_dm_options() {
	$values['title'] = "Domain Mapping Options";
	$values['network'] = true;
	$values['options']['301_redirect'] = array('title' => "Permanent redirect (better for your blogger's pagerank)");
	$values['options']['user_settings'] = array('title' => 'User domain mapping page');
	return $values;
}

//Löscht eine ausgewählte Domain direkt nach dem ersten Aufbau der Seite nach der Auswahl aus der Interimsvariable
add_filter('ll_tools_settings_field_dm_domains',function($text,$args){
	if(!empty($args['setting']['id'])) update_option($args['field'],array());
	return $text;
},10,2);

//Damit wird eine aus der Liste gewählte ID in die Option geschrieben
//Damit werden die Optionen geprüft und gespeichert.
add_filter('pre_update_option_'.LL_TOOLS_OPTION.'dm_domains',function($value,$oldvalue) {
	if(empty($value)) return;
	if(isset($value['delete'])) {
		ll_dm_delete_table_row($value);
		return array();
	}
	if(isset($value['edit'])) {
		$value = ll_get_domains($value['edit']);
		if($value['url'] == get_home_url($value['blog_id'])) $value['active'] = 'active';
		return $value;
	} 
	//oldvalue ist hier immer leer, da er sofort nach dem Anzeigen wieder gelöscht wird!
	if(isset($value['url'])) {
		$value['domain'] = wp_parse_url($value['url'],PHP_URL_HOST);
	}
	if(empty($value['blog_id']) and empty($value['domain'])) return array(); //Keine Eingabe
	if(!empty($value['blog_id_hidden'])) $value['blog_id'] = $value['blog_id_hidden'];
	$value['active'] = empty($value['active']) ? "0" : "1";
	if(!empty($value['id'])) {
		$oldvalue = ll_get_domains($value['id']);
	 	if($oldvalue === $value) return array();//Keine Änderung
	 	if(($oldvalue['blog_id'] == $value['blog_id']) and ($oldvalue['domain'] == $value['domain'])) {
	 		ll_dm_modify_tables($value); //Nur Primaryänderung daher keine Prüfungen!
			ll_dm_modify_primary($value);
	 		return array();
	 	}
	}
	$text = ll_domains_validate($value);	//Prüfung der Änderungen.
	if(empty($text)) {
		ll_dm_modify_tables($value); //Änderungen nach Prüfung.
		ll_dm_modify_primary($value);
	} else {
		add_settings_error('ll_tools',esc_attr('settings_updated'),$text);
	}
	return array();
},10,2);

function ll_dm_modify_primary($value) {
	switch_to_blog($value['blog_id']);
	if($value['active'] == "1")
		update_option('home',$value['url']);
	else
		update_option('home',ll_dm_get_org_url($value['blog_id']));	
	restore_current_blog();				
}

function ll_domains_validate($value) {
	$text = "";
	//gültige Blog_ID
	if(empty($value['blog_id'])) {
		return "Blog ID darf nicht leer sein.<br>";
	}
	//gültiger Domain-Name
	if(empty($value['domain'])) return "Domain darf nicht leer sein.<br>";
	//doppelter domain-Name (incl. site Urls)
	$domains = ll_get_domains(true);
	ll_crm_debug($domains);
	foreach($domains as $domain) 
		if($domain['domain'] == $value['domain']) return "Domain wird bereits verwendet.<br>";

	$sites = get_sites();
	$blog_id = false;
	ll_crm_debug(array($sites,$value['blog_id']));
	foreach($sites as $site) {
		$blog_id = (($site->blog_id == $value['blog_id']) or $blog_id); 
		if($site->domain == $value['domain']) return "Domain wird bereits verwendet.<br>";
	}
	if(!$blog_id) return "Blog-ID existiert nicht.<br>";		

	return "";
}

function ll_dm_modify_tables($values) {
	global $wpdb;
	ll_crm_debug($values);
	if(empty($values['id'])) { //neu
		$wpdb->query( $wpdb->prepare( "INSERT INTO {$wpdb->dmtable} ( `blog_id`, `domain` ) VALUES ( %d, %s )", 
												$values[ 'blog_id' ], $values['url'] ) );	
	} else { //modify
		$wpdb->query( $wpdb->prepare( "UPDATE {$wpdb->dmtable} SET blog_id = %d, domain = %s WHERE id = %d", 
													$values[ 'blog_id' ], $values['url'], $values[ 'id' ] ) );
	}
}

function ll_dm_delete_table_row($values) {
	global $wpdb;
	$wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->dmtable} WHERE id = %d", $values['delete'] ) );
}


add_filter('pre_update_option_'.LL_TOOLS_OPTION.'dm_active',function($value,$oldvalue) {
	if(empty($value)) return;
	//Hier erfolgt der Einstieg nur aus einem lokalen blog
   if($value[0] != home_url())
		update_option('home',$value[0]);
	ll_crm_debug($value);
},10,2);


function ll_dm_domain_listing($network) {
	$ebutton = LL_TOOLS_OPTION.'dm_domains[edit]';
	$dbutton = LL_TOOLS_OPTION.'dm_domains[delete]';
	if($network) 
		$sites = get_sites();
	else {
		$sites = array(get_blog_details());
		if(!isset(get_site_option(LL_TOOLS_OPTION."dm_options")['user_settings'])) return;	
	}
		
	//Dieses Submit an erster Stelle bewirkt, dass ein Enter in der Eingabe eines Feldes immer ein Speichern auslöst und nicht EDit oder Delete aus der ersten Zeile!
	$out = "<input type='submit' name='submit' style='display:none;'>";
	$out .= "<table style='width:100%;text-align:left'><tr>";
	$out .= "<th>ID</th>";
	$out .= "<th>Site ID</th>";
	$out .= "<th>Domain</th>";
	$out .= "<th>Home/Site</th>";
	if(!$network) $out .= "<th>Primary</th>";
	$out .= "<th></th><th></th></tr>";
	ll_crm_debug($sites);
	foreach($sites as $site) {
		$out .= "<tr><td></td><th>{$site->blog_id}</th><th>".$site->domain."</th><td>";
		$out .= get_home_url($site->blog_id)."<br>".get_site_url($site->blog_id)."</td>";
		if(!$network) {
			$checked = ($site->domain == wp_parse_url(home_url(),PHP_URL_HOST)) ? "checked = 'checked'" : "";
			$out .= "<td><input type='radio' name='".LL_TOOLS_OPTION."dm_active[]' value='".ll_dm_get_org_url()."' ".$checked."></td>"; 
		}
		$out .= "<td></td><td></td></tr>";
		$rows = ll_get_domains(false,$site->blog_id);
		foreach($rows as $row) {
			$out .= "<tr>";
			$out .= "<td>".$row['id']."</td>";
			$out .= "<td>".$row['blog_id']."</td>";
			$out .= "<td>".$row['domain']."</td>";
			$out .= "<td>".$row['url']."</td>";
			if(!$network) {
				$checked = ($row['url'] == home_url()) ? "checked = 'checked'" : "";
				$out .= "<td><input type='radio' name='".LL_TOOLS_OPTION."dm_active[]' value='".$row['url']."' ".$checked."></td>";
			}
			$out .= "<td><button type='submit' name='".$ebutton."' class='button-secondary' value='".$row['id']."'>Edit</button></td>" ;
			if($row['url'] == get_home_url($row['blog_id'])) {
				$out .= "<td></td>";
			} else {
				$out .= "<td><button type='submit' name='".$dbutton."' class='button-secondary' value='".$row['id']."'>Delete</button></td>";		
			}
			$out .= "</tr>";				
		}
	}
	$out .= "</table>";
	echo $out;
}

function ll_edit_domain() {
	$options = array();
	if(!isset(get_site_option(LL_TOOLS_OPTION."dm_options")['user_settings']) and !is_network_admin()) $options['network'] = true;
	$options['title'] = (!empty( $values['domain'] )) ? "Edit Domain" : "New Domain";
	$options['options']['id'] = array('type' => 'hidden');
	if(is_network_admin()) {
		$options['options']['blog_id'] = array('title' => 'Blog ID', 'type' => 'number');	
	} else {
		$options['options']['blog_id_hidden'] = array('type' => 'hidden', 'default' => get_current_blog_id(	));	
	}
	$options['options']['url'] = array('title' => 'URL (http(s)://example.com)',  'type' => 'url');
	$options['options']['active'] = array('title' => 'Primary');
	return $options;
}

function ll_dm_get_org_url($blog_id = NULL) {
	$home = get_home_url($blog_id);
   $site = get_site_url($blog_id);
	$domain = get_blog_details($blog_id)->domain;
	if($domain == wp_parse_url($home,PHP_URL_HOST)) {
		return $home;
	} elseif($domain == wp_parse_url($site,PHP_URL_HOST)) {
		return $site;
	} 
	$scheme = wp_parse_url($site,PHP_URL_SCHEME);
	return $scheme.$domain;
}

// delete mapping if blog is deleted
function delete_blog_domain_mapping( $blog_id, $drop ) {
	global $wpdb;
//	$wpdb->dmtable = $wpdb->base_prefix . 'domain_mapping'; => müßte eigentlich schon lange definiert sein???
	if ( $blog_id && $drop ) {		
		$wpdb->query( $wpdb->prepare( "DELETE FROM {$wpdb->dmtable} WHERE blog_id  = %d", $blog_id ) );
	}
}
add_action( 'delete_blog', 'delete_blog_domain_mapping', 1, 2 );

// show mapping on site admin blogs screen
function ra_domain_mapping_columns( $columns ) {
	$columns[ 'dom' ] = 'Domains';
	return $columns;
}
add_filter( 'wpmu_blogs_columns', 'ra_domain_mapping_columns' );

function ra_domain_mapping_field( $column, $blog_id ) {
	$domains = ll_get_domains(false,$blog_id);
	if (( $column == 'dom' ) and !empty($domains))
		foreach($domains as $domain) echo $domain['domain']."<br />";
}
add_action( 'manage_sites_custom_column', 'ra_domain_mapping_field', 1, 3 );


function ll_dm_check() {
	$text = "";
	if(!file_exists(WP_CONTENT_DIR."/sunrise.php")) {
		if(file_exists(__DIR__."/sunrise.php")) {
			copy(__DIR__."/sunrise.php",WP_CONTENT_DIR."/sunrise.php");
			if(!file_exists(WP_CONTENT_DIR."/sunrise.php")) $text .= "sunrise.php in wp-content fehlt<br>";
			if(!ll_dm_maybe_create_db()) $text .= "Fehler bei der Erstellung der Tabelle";		//erzeugt die Datenbanke		
		} else {
			$text .= __DIR__."/sunrise.php wurde nicht gefunden!";
		}
	} else {
		If(!defined('SUNRISE')) $text .= "Konstante SUNRISE nicht definiert, bitte folgendes in wp-config einfügen:<br>".
													"if(file_exists(__DIR__.'/wp-content/sunrise.php')) define( 'SUNRISE', 'on' );";
	}
	if(!empty($text)) add_settings_error('ll_tools',esc_attr('settings'),$text);
	return (empty($text));
}

function ll_dm_maybe_create_db() {
	global $wpdb;
	$wpdb->dmtable = $wpdb->base_prefix . 'domain_mapping';
   $exists =	($wpdb->get_var("SHOW TABLES LIKE '{$wpdb->dmtable}'") == $wpdb->dmtable );
	if (!$exists)  {
		$wpdb->query( "CREATE TABLE IF NOT EXISTS `{$wpdb->dmtable}` (
			`id` bigint(20) NOT NULL auto_increment,
			`blog_id` bigint(20) NOT NULL,
			`domain` varchar(255) NOT NULL,
			`active` tinyint(4) default '1',
			PRIMARY KEY  (`id`),
			KEY `blog_id` (`blog_id`,`domain`,`active`)
		);" );
	   $exists =	($wpdb->get_var("SHOW TABLES LIKE '{$wpdb->dmtable}'") == $wpdb->dmtable );
	}
	return $exists;
}

add_filter( 'template_directory_uri', function($template_dir, $template, $theme_root){
	if(defined('LL_DOMAIN_MAPPING') and LL_CROSS_DOMAIN) {
		if(!is_admin() and (strpos($template_dir,site_url()) !== false )) {
			return(str_replace(site_url(),home_url(),$template_dir));
		}	
	}
	return $template_dir;
},10,3);

add_filter( 'preview_post_link', function($preview_link, $post) {
	if(LL_CROSS_DOMAIN) {
		if(strpos($preview_link,home_url()) !== false ) {
			return(str_replace(home_url(),site_url(),$preview_link));
		}	
	}
	return $preview_link;
},10,2 );

//Notwendig für die Anzeige im Customizer (Fehler: Non-existent UUID ...)
//Weil auf home_url kein Benutzer angemeldet ist, daher keine Adminrechte hat.	
add_filter('home_url', function($url, $path, $orig_scheme, $blog_id) {
	if(defined('IFRAME_REQUEST') and IFRAME_REQUEST and LL_CROSS_DOMAIN) {
		return site_url();
	}
	return $url;
},10,4);

//nur sinnvoll, wenn die Networkdomain keine Zertifkat hat, ev autom. in der Option speichern?
add_filter( 'network_site_url', function($url, $path, $scheme) {
	ll_crm_debug(array($url, str_replace("https:","http:",$url), $path, $scheme));
	return str_replace("https:","http:",$url);
}, 10, 3);

?>
