<?php
if ( !defined( 'SUNRISE_LOADED' ) )
	define( 'SUNRISE_LOADED', 1 );

//Zweite Bedingungen bewirkt, dass das Laden beim Einschalten aus update_option-HOOK erkannt wird!
if ( defined( 'COOKIE_DOMAIN' ) and !defined('LL_PLUGIN_NAME') ) {
	die( 'The constant "COOKIE_DOMAIN" is defined (probably in wp-config.php). Please remove or comment out that define() line.' );
}

function ll_get_domains($id = true, $blog_id = NULL) {
	if(!isset($blog_id)) $blog_id = get_current_blog_id();
   static $rows;
   $result = array();
	if(empty($rows)) {
	   global $wpdb;
	   $wpdb->dmtable = $wpdb->base_prefix . 'domain_mapping';
		$rows = $wpdb->get_results( "SELECT * FROM {$wpdb->dmtable} ORDER BY blog_id" );
	   //Array aus stdclss-Objekten mit id, blog_id, domain, active (=1 für primary)
	   foreach($rows as $key => $row) {
	   	$rows[$key]->url = $row->domain;	   
	   	$rows[$key]->domain = str_replace(array("http://",'https://'),"",$row->domain);
	   }
	}
	foreach($rows as $row) {
 		if(is_numeric($id) and ($row->id == $id)) return (array) $row;
 		if(($id === true) or ($row->blog_id == $blog_id)) {
 			if(is_string($blog_id) and ($row->domain == $blog_id)) return $row->blog_id;
 			$result[] = (array) $row;
 		}	
	}
	return $result;
}

add_filter( 'pre_get_site_by_path', function($pre, $domain, $path, $segments, $paths){
	$dm_domain = $_SERVER[ 'HTTP_HOST' ];


	//Ist notwendig, weil sonst www als Subdomain interpretiert und neu angelegt werden will.
	if( ( $nowww = preg_replace( '|^www\.|', '', $dm_domain ) ) != $dm_domain )
		$domain = $nowww;

	$domain_mapping_id = ll_get_domains(true,$domain);
	if(is_numeric($domain_mapping_id)) {
		define( 'COOKIE_DOMAIN', $domain );
		define('LL_DOMAIN_MAPPING',true);
		return WP_Site::get_instance($domain_mapping_id);
	}

	return $pre;
},10,5);

?>
