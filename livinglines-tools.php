<?php
/**
* Plugin Name: LivingLines Tools
* Description: Sammelt die verschiedenen Funktionen, die im Laufe der Zeit zur Ergänzung von WP-Funktionalitäten entstandenen sind. Es stellt außerdem eine Plattform dar, mit der indiv. Wünsche und Anpassungen innerhalb einer bestimmten Installation realisiert werden können. Eine zentrale Funktionalität ist die Integration von CiviCRM zur Ausgabe von Daten auf Karten und Listen.
* Version: 4.2.12
* Author: Hannes Zagar
* License:
* Plugin URI: http://livinglines.at/livinglines-tools/
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' ); 
//Dieser Hook muß hier oder zumindest in diesem File angesiedelt sein, damit er funktioniert.
register_activation_hook(__FILE__,function($network){
	require_once('admin/ll-tools-admin-functions.php');
	llActivate($network);
});
############ Defines
define('LL_PLUGIN_FILE',__FILE__);
define('LL_PLUGIN_NAME',basename(__FILE__,".php"));
define('LL_PLUGIN_DIR',__DIR__."/"); //neu mit 10.01.2021
if(!defined('LL_TOOLS_OPTION')) define('LL_TOOLS_OPTION', 'll_tools_');

include_once( ABSPATH . 'wp-admin/includes/plugin.php' );
define('LL_TOOLS_POLYLANG',is_plugin_active("polylang/polylang.php"));
define('LL_TOOLS_MAILPOET',is_plugin_active("wysija-newsletters/index.php"));
define('LL_TOOLS_CIVICRM',is_plugin_active(get_option(LL_TOOLS_OPTION."civirm","civicrm/civicrm.php")));
define('LL_TOOLS',is_plugin_active(str_replace(WP_PLUGIN_DIR."/","",__FILE__)));

define('LL_SETTINGS_SAVE',(basename($_SERVER['REQUEST_URI']) == 'options.php'));

############ Filter die sofort angefahren werden müssen.


############ Start der Anwendungen
add_action('plugins_loaded','ll_start_components');

############ Laden der immer unbedingt notwendigen Funktionen
require_once('admin/ll-tools-core-functions.php');
require_once('admin/ll-tools-components.php'); 
//Die indiv. Erweiterungen sind hier hier gewandert, weil sie immer geladen werden müssen
//und möglicherweise vor dem Laden der Plugins bereits geladen sein sollen.
if(!is_network_admin() and LL_TOOLS) 
	ll_tools_file_loader(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME,"ll_","");
elseif(is_network_admin() or LL_SETTINGS_SAVE) 	
	ll_tools_file_loader(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME,"ll_tools-version","");


############ Hilfsfunktionen
function ll_tools_style_version() {
	$ret = get_plugin_data(LL_PLUGIN_FILE)['Version'];
	if(defined('LL_TOOLS_DEV') and LL_TOOLS_DEV) $ret .= microtime();
	return $ret;
}

function ll_tools_styles() {
	wp_enqueue_style( 'll_tools_awesome', plugins_url().'/'.LL_PLUGIN_NAME.'/leaflet/fontawesome/font-awesome.min.css');
	wp_enqueue_style( 'll_tools_styles', plugins_url().'/'.LL_PLUGIN_NAME.'/css/ll-tools-styles.css',array(),ll_tools_style_version());
	wp_enqueue_script( 'll_tools_scripts', plugins_url().'/'.LL_PLUGIN_NAME.'/css/ll-tools-scripts.js',array('jquery'));
}

function ll_tools_styles_indiv(){
	wp_enqueue_style( 'll_tools_styles_indiv', 
	wp_upload_dir()['baseurl']."/".LL_PLUGIN_NAME."/ll_indiv_styles.css",array(),ll_tools_style_version());
}


?>
