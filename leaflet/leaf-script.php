<?php
/**
 * OSM-Karte
 *
 * PHP Plugin
 *
 * @author          Hannes Zagar
 * @copyright       Copyright 2005-2013, RRZE
 * @license         CC-A 2.0 (http://creativecommons.org/licenses/by/2.0/)
 * @version         1.2
 * @lastmodified    01/2020 komplett umgebaut.
 * http://astronautweb.co/snippet/font-awesome/
 */
?>
<script type="text/javascript">

var options = JSON.parse(<?php echo "'".$this->get_data_json('options')."'"; ?>);	
var attrib = JSON.parse(<?php echo "'".$this->get_data_json('attr')."'"; ?>);
// set up the map		
var legend = options['reset'];
var listno = 1;

var map;
var ajaxRequest;
var plotlist;
var homeurl = '<?php echo home_url() ?>/';
var plotlayers = [];

//console.log((options['zoomControl']!=1));
map = new L.Map('map', {zoomControl:(options['zoomControl']==1)});		
	
// create the tile layer with correct attribution
var osmUrl='http://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png';
var osmAttrib='Map data © <a href="http://openstreetmap.org">OpenStreetMap</a> contributors';
var osm = new L.TileLayer(osmUrl, {minZoom: options['minzoom'], maxZoom: options['maxzoom'], attribution: osmAttrib});		

setMapView(options['reset']);


map.addLayer(osm);
if(options['reset'] != 'Print') map.on('moveend',function(){ save_map() });
map.on('zoomend',zoom_map);	
//console.log(options.linkToMap);
if(options.linkToMap.length > 2) {
	map.on('click',goToMap);
	map.on('dblclick',goToMap);
}

//Hier werden die Kml-Files geladen und zu Anzeige vorbereitet.
var kmlfiles = JSON.parse(<?php echo "'".$this->get_data_json('kml')."'"; ?>);
var kmlLayers = [];
makeKmlLayers();

var group_list = [];
var all_groups_list = [];
var all_points = [];
var allsubpoints = L.layerGroup();
var GroupsOfPoints = {};
var printMapBounds = map.getBounds();
//var printMapMarkers = [];
//var addmarker = true; //für Print wo nicht alle hinzugefügt werden.
var attrib_typ;
var lines = [];

//ein if hilft hier nicht, weil der php-Code immer ausgeführt wird!
makeLineMarkerGroups(JSON.parse(<?php echo "'".$this->get_data_json('line')."'"; ?>),'line');	
makepointMarkerGroups(JSON.parse(<?php echo "'".$this->get_data_json('main')."'"; ?>),'main');	
//makepointMarkerGroups(JSON.parse(<?php echo "'".$this->get_data_json('sparql')."'"; ?>),'main');	
makepointMarkerGroups(JSON.parse(<?php echo "'".$this->get_data_json('point')."'"; ?>),'point');

if(options['mapCenter'] == 'Auto') { 
	fitMapView(options['zoom']); 
}
zoom_map();
</script>   

