/* ll_crm Funktionsbibliothek
Version 1.0.0, 03.09.2019
*/


function goToMap() {
//	link='http://bolsenalagodeuropa.net/mappa?tag[]=31';
    window.open(homeurl+options.linkToMap, "_blank");
//	console.log(homeurl+options.linkToMap.replace(/:/gi,"="));
}

function makeLineMarkerGroups(line_object,attr_typ) {
	attrib_typ = attr_typ;
	line_groups = Object.entries(line_object);
//	console.dir(line_groups);
	line_groups.forEach(makeLineMarker);
}


function makeLineMarker(line_group) {
//	line_attr = line_group.attr;
	line_attr = attrib[attrib_typ][line_group[0]];
	//Es gibt die Situation, dass nur die attr. eingetragen sind!
	if(typeof(line_group[1].data) == 'object') {
		line_array = line_group[1].data;	
		line_array.forEach(lineMarkers);			
	}
}

function lineMarkers(value,index,array) {
//	console.log(line_attr);
	lines[value.line_id] = L.polyline(value.data,line_attr.style).addTo(map);
	if(typeof(value.infowindow) == 'string') {
		lines[value.line_id].bindPopup(value.infowindow.replace(/@@/gi,"'"));
		lines[value.line_id].on({mouseover:function(event){this.openPopup();}});	
	}
}

function makepointMarkerGroups(point_object,attr_typ) {
	attrib_typ = attr_typ;
//	console.log(point_object);
	point_groups = Object.entries(point_object);
//	console.dir(point_groups);
	point_groups.forEach(makepointMarker);
}

function makepointMarker(point_group) {
//	point_attr = point_group[1].attr;
	console.log(attrib);
	point_attr = attrib[attrib_typ][point_group[0]];
	if(typeof(point_attr) == 'undefined') {
		console.log(attrib_typ);
		console.log(point_group);
	}
	console.log(point_group[1].data);
	point_array = point_group[1].data;
	if(point_attr.iconClass == 'fa') {
		point_array.forEach(make_fa_marker);			
	} else if(point_attr.iconClass == 'Circle') {
		point_array.forEach(make_circle);			
	} else if(point_attr.iconClass == 'Icon') {
		point_array.forEach(make_icon_marker);
	} else {		
		point_array.forEach(make_marker)		
	}
}

function make_icon_marker(value) {
	var Icon = L.icon({iconUrl: options['iconPath']+point_attr.icon+'.png', iconSize: [20, 35], iconAnchor: [10,35], popupAnchor: [0, -25]});
	if(value.infowindow.match('SAIPEM')) {
		Icon = L.icon({iconUrl: options['iconPath']+point_attr.icon+'.png', iconSize: [30, 55], iconAnchor: [10,35], popupAnchor: [0, -25]});		
	}
//	console.log(Icon);
	var marker = L.marker(value.pos, {icon:Icon});
//	console.log(value);
	if(typeof(value.infowindow) == 'string') {
		infoWindow(marker,value.infowindow.replace(/@@/gi,"'"));
	}			
	marker.addTo(map);	
//	marker.addTo(allsubpoints);
}

function make_marker(value) {
	var marker = L.marker(value.pos);
	if(typeof(value.infowindow) == 'string') {
		infoWindow(marker,value.infowindow.replace(/@@/gi,"'"));
	}			
	marker.addTo(map);	
//	marker.addTo(allsubpoints);
}

function make_fa_marker(value) {
//	console.log(point_attr);
//	console.log(value);
	all_points.push(new L.latLng(value.pos));
	var myIcon = L.AwesomeMarkers.icon({icon: point_attr.icon, prefix: point_attr.iconClass, 
										markerColor: point_attr.markerColor, iconColor: point_attr.iconColor});
	//, spin: big									
//	addmarker = true;
	/*
	if(options['reset'] == "Print") {
		if(printMapBounds.contains(L.latLng(value.pos))) {
			printMapMarkers.push([listno,""]);
			if(legend == 'listno') {
				marker = L.layerGroup();
				marker_part = L.marker( value.pos, {icon: myIcon});
				marker_part.addTo(marker);
				myIcon = L.divIcon({className: 'll-print-icon', html: listno});
				marker_part = L.marker( value.pos, {icon: myIcon,bgPos:[400,200]});
				marker_part.addTo(marker);
				listno++;
			} else {
				marker = L.marker( value.pos, {icon: myIcon});				
			}
		} else {
			addmarker = false;			
		}
	} else {
	*/
		marker = L.marker( value.pos, {icon: myIcon});
//		console.log(value.infowindow);
		if(typeof(value.infowindow) == 'string') {
			infoWindow(marker,value.infowindow.replace(/@@/gi,"'"));
		}		
//	}
//	if(addmarker) marker.addTo(map);	
	marker.addTo(map);
}

function make_circle(value) {
	radius = 10;
   if(options['reset'] == "Print") radius = 20;
	marker = L.layerGroup();
	var myIcon = L.divIcon({className: 'll-div-icon', html: '<span style="color:'+point_attr.iconColor+';">'+value.text.replace(" ","&nbsp;")+'</span>' });
	var marker_part = L.circleMarker( value.pos, {color:point_attr.iconColor,radius:radius});
	marker_part.addTo(marker);
	var marker_part = L.marker(value.pos, {icon:myIcon,bgPos:[100,200]});
	if(typeof(value.infowindow) == 'string') {
		infoWindow(marker_part,value.infowindow.replace(/@@/gi,"'"));
	}		
	marker_part.addTo(marker);	
   if(options['reset'] == "Print") {
	   marker.addTo(map);   
   }else	if(typeof(value.group) == 'string') {
		addToGroup(value.group,marker)
	}
	marker.addTo(allsubpoints);
}

function addToGroup(group,marker) {
	if(typeof(GroupsOfPoints[group]) != 'object') {
		GroupsOfPoints[group] = L.layerGroup();
	}
	marker.addTo(GroupsOfPoints[group]);
}

function showsubs(cname) {
	group = GroupsOfPoints[cname]; 
	if(map.hasLayer(group)) {
		console.log(group);
		group.remove();
		setMapView(map_pos);
	} else if(map.hasLayer(allsubpoints) && !group_list.includes(group)) {
		// Liste wird hier im umgekehrten Sinn verwendet!
		console.log(group);
		group_list.push(group)
		group.addTo(map);
		group.remove();			
	} else {
		console.log(group); 
		map_pos = [map.getCenter(),map.getZoom()];
		group.addTo(map);
		if(typeof(lines[cname]) == 'object') {
			bounds = lines[cname].getBounds();
		} else {
			bounds = defineBounds(group);
		}
			
	//	alert(bounds.isValid());
	//	alert(bounds.getCenter());
		map.fitBounds(bounds);
		save_map(bounds,cname);
	}
}

function defineBounds(group) {
	points = [];
	group.eachLayer(function (layer) {
		if(typeof(layer.getLayers) != 'undefined') {
			layer.eachLayer(function (layer) {
			    points.push(layer.getLatLng());						
			}) 
		} else {
		    points.push(layer.getLatLng());					
		}
	})
	return L.latLngBounds(points);					
}


function infoWindow(fmarker,textinfowindow) {
	fmarker.bindPopup(textinfowindow);
	fmarker.on({mouseover:function(event){this.openPopup();}});		
}

function setMapView(par) {
	if(typeof(par) == 'string') {
		if (par == 'Reset') {
			document.cookie = "ll_lat =";
			document.cookie = "ll_lng =";
			document.cookie = "ll_zoom =";
			mapCenter = '';
//		} else if (par == 'Auto') {
			 
		} else if ((par != 'NoCookie') && (getCookie('ll_lat') != 0)) {
			mapCenter = [getCookie('ll_lat'),getCookie('ll_lng'),getCookie('ll_zoom')];
		}
		if(typeof(mapCenter) != 'object') {
			mapCenter = [options['lat'],options['lon'],options['zoom']];
		}
		var latlng = L.latLng(mapCenter[0],mapCenter[1]);
		if(par == 'Print') {
			mapCenter[2] = map.getBoundsZoom([[getCookie('ll_south'),getCookie('ll_west')],[getCookie('ll_north'),getCookie('ll_east')]]);
			map.setMinZoom(mapCenter[2]);
			map.setMaxZoom(mapCenter[2]);
		}
		map.setView(latlng,mapCenter[2]);
//			save_map();
	} else {
		map.setView(par[0],par[1]);		
	}
}

function makeKmlLayers() {
//Options:	var attributes = {color: true, width: true, Icon: true, href: true, hotSpot: true};

	kmlfiles.forEach(function(value,index,array){
		fetch(options['iconPath']+value.set_tag+'.kml')
			.then(res => res.text())
			.then(kmltext => {
			const parser = new DOMParser();
			const kml = parser.parseFromString(kmltext, 'text/xml');

//			console.log(value);
			if(value.set_tag == 'query') { //Dies muß noch ausgefeilt werden!
				kmlLayers[value.set_tag] = new L.KML(kml,{'Style' : {'color':value.iconColor}});
				x = kml.getElementsByTagName('name');
					if(typeof(x) != 'undefined') {
						kmlLayers[value.set_tag].bindPopup('<b>'+x[0].innerHTML.replace(/_/g," ")+'</b><br>'+x[1].innerHTML);			
					}			
			} else {
				var myIcon = L.AwesomeMarkers.icon({icon: 'tint', prefix: 'fa', 
											markerColor: 'lightblue', iconColor: 'blue'});
				
				kmlLayers[value.set_tag] = new L.KML(kml,{'iconOptions' : myIcon});
			}
			if(options['reset'] != 'NoCookie') {
				par = Object.values(getCookie("mapSelect"+options['map']+"[kml_file]"));
	//			console.log(Object.values(par));
				par.forEach(function(value) {
	//				console.log(value);
					kmlLayers[value].addTo(map);				
				});			
			}
		});
	});
}

function showLayer(layer) {
//	console.log(kmlLayers);
//	console.log(typeof(layer.value));
	if(layer.checked) {
		kmlLayers[layer.value].addTo(map);	
	} else {
		kmlLayers[layer.value].remove();	
	}
}

function fitMapView(par) {
	var bounds = L.latLngBounds(all_points);
	map.fitBounds(bounds);
	if(typeof(par) == 'number') {
		map.setZoom(par);
	}
	zoom_map();
	save_map(bounds);			
}

function saveCookie(par) {
	//Event im Widget zur Anzeige löst saveCookie aus!
	if(par.name.slice(0,-2) == 'kml_file') {
		showLayer(par);
	}
//	console.log(par.value);
//	console.log(par.name);
//	console.log(par.checked);
//	console.log(options['map']);
	if(par.checked) {
		setCookie("mapSelect"+options['map']+"["+par.name.slice(0,-2)+"]["+par.value+"]",par.value,1);	
	} else {
		setCookie("mapSelect"+options['map']+"["+par.name.slice(0,-2)+"]["+par.value+"]","",-1);		
	}
}
/*
function saveSelectionCookie() {
	cookievars = Object.getOwnPropertyNames(submit);
	cookievars.forEach(function(value,index,array){
		field = value;
		if(value.length > 1) {
			setCookie(submit[0]+"["+field+"]",submit[field],1);
		}
	});
}
*/

function setCookie(cname, cvalue, exdays) {
	if(cvalue.length == 0) {
		exdays = -1;
	}
	var d = new Date();
	d.setTime(d.getTime() + (exdays*24*60*60*1000));
	var expires = "expires="+ d.toUTCString();
	document.cookie = cname + "=" + cvalue + ";" + expires + ";path=/";
}

function save_map(par,par2) {
//	alert(par2);
	if(options['reset'] == 'NoCookie') {
	 return;
	}
	
	var x = map.getCenter();
	var y = map.getBounds();
	if(typeof(par) == 'object') {
		var y = par;
	}
	document.cookie = "ll_lat =" + x.lat;
	document.cookie = "ll_lng =" + x.lng;
	document.cookie = "ll_west =" + y.getWest();
	document.cookie = "ll_south =" + y.getSouth();
	document.cookie = "ll_east =" + y.getEast();
	document.cookie = "ll_north =" + y.getNorth();
	document.cookie = "ll_zoom =" + map.getZoom();	
}

function zoom_map() {
//	save_map("zoom"); wird durch moveend ausgefüht
	if(map.getZoom() >= 16) {
		allsubpoints.addTo(map);
	} else if(map.getZoom() <= 15 ) {
		allsubpoints.remove();
	}		
}

function getCookie(cname) {
  var name = cname + "=";
  var arr = cname + "[";
  var array = [];
  var decodedCookie = decodeURIComponent(document.cookie);
 // console.log(arr);
  var ca = decodedCookie.split(';');
  for(var i = 0; i <ca.length; i++) {
    var c = ca[i];
    while (c.charAt(0) == ' ') {
      c = c.substring(1);
    }
    if (c.indexOf(name) == 0) {
      return c.substring(name.length, c.length);
    }
    if (c.indexOf(arr) == 0) {
    	par = c.substring(arr.length,c.length).split("]=");
		array[par[0]] = par[1];
    }
  }
  return array;
}

function zeigeWidget() {
   console.log("select");
  	var wobj = document.getElementsByClassName('LL_Map_WidgetSelect');
	var tobj = document.getElementById('mapInfo');
	var pobj = document.getElementsByClassName('LL_Map_WidgetPrint');
	if(!checkDisplayStatus(pobj[0],'block')) {		
		if ((wobj[0].style.display == 'block')) {
			wobj[0].style.display = 'none';
			tobj.style.display = 'block';
		} else {
			wobj[0].style.display = 'block';
			tobj.style.display = 'none';
		}	
	}
}

function startPrint() {
   console.log("print");
	var wobj = document.getElementsByClassName('LL_Map_WidgetSelect');
	var tobj = document.getElementById('mapInfo');
	var pobj = document.getElementsByClassName('LL_Map_WidgetPrint');
	if(checkDisplayStatus(pobj[0],'block')) {
		tobj.style.display = 'block';
		pobj[0].style.display = 'none';			
	} else {
		wobj[0].style.display = 'none';
		tobj.style.display = 'none';
		pobj[0].style.display = 'block';			
	}
}

function checkDisplayStatus(obj,status) {
	if(typeof(obj) == 'object') {
		return (obj.style.display == status);
	} else return (status == 'none');
}

/*
function showMapListElements() {
	for(var i = 0; i < printMapMarkers.length; i++) {
		var lobj = document.getElementById('element_'+ printMapMarkers[i][1]);
		if(lobj == null) {
//			alert('element_'+printMapMarkers[i]);
		} else {
			lobj.style.display = 'unset';
			if(legend == 'listno') document.getElementById('listno_'+printMapMarkers[i][1]).innerHTML = printMapMarkers[i][0];					
		}
	}
}
*/
