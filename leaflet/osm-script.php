<?php
/**
 * OSM-Karte
 *
 * PHP Plugin
 *
 * @author          Hannes Zagar
 * @copyright       Copyright 2005-2013, RRZE
 * @license         CC-A 2.0 (http://creativecommons.org/licenses/by/2.0/)
 * @version         1.0
 * @lastmodified    04/2017
 */

?>
<div id="map" style='width: <?php echo $options["width"]; ?>; height: <?php echo $options["height"]; ?>;'></div>
<script>
	mapboxgl.accessToken = 'pk.eyJ1IjoiaGFubmVzemFnYXIiLCJhIjoiY2oxMmYzZGJ6MDY2dDJxcWRyd2txcnRkcyJ9.fl4JiGBoYjcX_cXk_XFcgw';
	var map = new mapboxgl.Map({
		container: 'map',
		style: 'mapbox://styles/mapbox/streets-v9'
	});
</script>
