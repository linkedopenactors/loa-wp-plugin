<?php
function leaflet_head() {
  echo 	'<link rel="stylesheet" href="https://unpkg.com/leaflet@1.5.1/dist/leaflet.css" />';
  echo 	'<script src="https://unpkg.com/leaflet@1.5.1/dist/leaflet.js"></script>';
  echo  '<script defer src="'.plugins_url()."/". LL_PLUGIN_NAME.'/leaflet/fontawesome/fontawesome-all.js"></script>';

  echo  '<link rel="stylesheet" href="'.plugins_url()."/". LL_PLUGIN_NAME.'/leaflet/dist/leaflet.awesome-markers.css">';
  echo  '<script src="'.plugins_url()."/". LL_PLUGIN_NAME.'/leaflet/dist/leaflet.awesome-markers.js"></script>';
	//ll_Bibliothek
  echo  '<script src="'.plugins_url()."/". LL_PLUGIN_NAME.'/leaflet/ll_crm.js"></script>';
	//Hier können noch CSS-Modifikation installiert werden!
  echo  '<script src="'.plugins_url()."/". LL_PLUGIN_NAME.'/leaflet-kml-master/L.KML.js"></script>';
	if(isset($_POST['Legend']) and ($_POST['Legend'] == 'listno')) {
		echo  '<style>.awesome-marker {font-size: 12px;} .leaflet-pane .svg-inline--fa {vertical-align: -3em;margin-top:0em;} </style>';
	} else {
		echo  '<style>.awesome-marker {font-size: 12px;} .leaflet-pane .svg-inline--fa {vertical-align: 0em;margin-top:1em;} </style>';		
	}

	do_action('leaflet_head');
}	
?>
