/* Steuer die Multiple Felder */

var namespaces = [];

function addRow(option,set,field,index, element) {
 if(element=="") {
 	index = namespaces[option][field][set].length;
 	namespaces[option][field][set].push("");
 }

 var elementName = "multiple_"+option+"_"+field;

 var elementRow = jQuery("#"+elementName).clone();
 elementRow.attr("id", "#" + elementName + "-" + index);
 elementRow.show();

 var inputFieldPrefix = jQuery("#id_" + elementName, elementRow);
 inputFieldPrefix.attr("name", "ll_tools_"+option+"["+field+set+"]["+index+"]");
 inputFieldPrefix.attr("id", "ll_tools_"+option+"-"+ field +"-" + set + "-" + index);

 var labelFieldPrefix = jQuery("#label_" + elementName, elementRow);
 labelFieldPrefix.attr("id", "#label_" + elementName + "-" + index);
 labelFieldPrefix.append(index + ". " + field);

 inputFieldPrefix.val(element);

 jQuery("#base_"+elementName+"_"+set).append(elementRow);
}


jQuery(document).ready(function() {
  initSettings();

  function initSettings() {
    var supported_namespaces = jQuery("input[name=ll_tools_multiple]").val() || '[]';
    namespaces = JSON.parse(supported_namespaces);
    for (var option in namespaces) {
		 for (var field in namespaces[option]) {
		 	for(var set in namespaces[option][field]) {
				 for (var index in namespaces[option][field][set]) {
					addRow(option,set,field,index,namespaces[option][field][set][index]);
				 }    	
		 	}    	
		 }
    }
	
  }

});

/* Öffnet und schließt die Gruppendetails für OptionOption */

function showGroupDetails(tobj) {
	var icon = tobj.attributes['id'].nodeValue;
//			icon = icon.replace('optionsgroup','ll_opt_id');
	icon = 'set_'+icon;
	if((tobj.style.display == 'none')) {
		tobj.style.display = 'inherit';
		jQuery( "#"+icon ).removeClass('ll-option-icon');
		jQuery( "#"+icon ).addClass('ll-option-icon-open');
	} else {
		jQuery( "#"+icon ).removeClass('ll-option-icon-open');
		jQuery( "#"+icon ).addClass('ll-option-icon');
		tobj.style.display = 'none';
	}
}


