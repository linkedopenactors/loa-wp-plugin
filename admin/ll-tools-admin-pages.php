<?php
/**
* Funktionen zur Darstellung der Adminseiten
*/

################### Funktionen für die Adminseiten ####################

function ll_tools_get_options($option,$get = false) {
	static $site_options;
	if(!isset($site_options)) $site_options = array();
	if($option === true) return set_transient(LL_TOOLS_OPTION."site_options",$site_options);
	if(is_multisite() and (is_network_admin() or $get)) {
		$site_options[] = $option;
		$value = get_site_option($option);
	} else {
		$value = get_option($option);	
	}
	return $value;
}

function ll_tools_make_section($default_args,$opt_values) {
	$group = $default_args['group'];

	$opt_values = apply_filters('ll_tools_settings_add_opt_value_'.$group,$opt_values);

	$default = array('type' => 'checkbox', 
				  'page' => $default_args['group'], 
				  //'function' => 'll_tools_default_field_cb', 
					'function' => array('LL_classes_option','default_field_cb'),
				  'description' => "", 
				  'format' => array(), 
				  'pre_text' => '', 
				  'post_text' => '' , 
				  'validate' => '');
	$default_args = array_merge($default,$default_args);
	add_settings_section('ll_tools_settings_'.$group,
					 $default_args['title'],
					 'll_tools_settings_'.$group.'_cb',
					 LL_TOOLS_OPTION.$default_args['page']);
					 
	foreach($opt_values as $opt_value => $opt_args) {
		$opt_args = array_merge($default_args,$opt_args,array('field' => LL_TOOLS_OPTION.$opt_value));

		register_setting(LL_TOOLS_OPTION.$default_args['page'],$opt_args['field'],$opt_args['validate']);

		if((empty($opt_args['network']) or is_network_admin() or !is_multisite()) and !LL_SETTINGS_SAVE ) 
			add_settings_field($opt_args['field'],
						    $opt_args['title'],
						    $opt_args['function'],
						    LL_TOOLS_OPTION.$opt_args['page'],
						    'll_tools_settings_'.$group,
						    $opt_args);
	}
	//Was kann dieser Filter?
	apply_filters('ll_tools_settings_init_section_'.$group,'ll_tools_settings_'.$group,$group);	
}

function ll_tools_options_page_html() {
	global $plugin_page;
	// $plugin_page: z.b.: ll_tools_dev ...
	// add error/update messages
	// check if the user have submitted the settings
	// wordpress will add the "settings-updated" $_GET parameter to the url
	ll_crm_debug($plugin_page);
	if ( isset( $_GET['settings-updated'] ) ) {
		// add settings saved message with the class of "updated"
		add_settings_error( 'll_tools', 'll_tools_message', __( 'Settings Saved', $plugin_page ), 'updated' );
	} 
	?>
	<div class="wrap ll-tools">
		<h1><?php echo esc_html( get_admin_page_title() ); ?></h1>
		<?php settings_errors('ll_tools'); ?>
		<form action="<?php echo site_url(); ?>/wp-admin/options.php" method="post">
			<?php
			// output security fields for the registered setting "wporg"
			settings_fields( $plugin_page );
			// output setting sections and their fields
			// (sections are registered for "wporg", each field is registered to a specific section)
			do_settings_sections( $plugin_page );
			ll_tools_get_options(true); //Speichert den Transient für NetzwerkOptions

			LL_classes_option::set_multiple(); //Erzeugt das hidden field für die Multiple Felder
			// output save settings button
			submit_button( 'Save Settings' );
			?>
		</form>
	</div>
	<?php
}

####### Hier werden die Siteoptions geschrieben!
/**
 * Liest die NetzwerkOptionen aus, wechselt die Filter und prüft, ob Netzwerkoption.
 * 
 * @since 4.2.10
 *
 * @param string $option Option die upgedatet wird.
 *
 */


function ll_tools_change_update_filters($option = false) {
	static $site_options;
	if($site_options === false) return false;
	if(!isset($site_options)){
		if($option !== false) add_settings_error( 'll_tools', 'll_tools_message', __( 'Site-Optionkonvertierung fehlgeschlagen' ));
		$site_options = get_transient(LL_TOOLS_OPTION."site_options");
		if($site_options === false) return;
		global $wp_filter;
		foreach($site_options as $option) {
			if(isset($wp_filter['pre_update_option_'.$option])) {
				$debug = false;
				ll_crm_debug($option,$debug);
				$wp_filter['pre_update_site_option_'.$option] = $wp_filter['pre_update_option_'.$option];
				unset($wp_filter['pre_update_option_'.$option]);
				ll_crm_debug($wp_filter['pre_update_site_option_'.$option],$debug);
			}		
		}
	} elseif(in_array($option,$site_options)) {
		unset($site_options[array_search($option,$site_options)]); //Wichtig, sonst entsteht eine Schleife!
		return true;
	} 
	return false;
}	

/**
 * Bereitet den Filterwechsel für das Abspeichern vor.
 * 
 * @since 4.2.10
 *
 * Nur notwendig zum Speichern der Werte in einer MultiSiteumgebung!
 */

if(LL_SETTINGS_SAVE and is_multisite()) {
	ll_tools_change_update_filters();

	/**
	 * Nur notwendig zum Abspeichern in MultiSite.
	 * options.php macht immer update_option, daher müssen Site_options hier gesondert behandelt werden.
	 * 
	 * @since überarbeitet 4.2.10 => wesentliche Funktionen wurden nach ll_tools_change_update_filters wegen des Filterwechsels.
	 *
	 * @param Standardfilterparameter
	 *
	 */

	add_filter('pre_update_option',function($value,$option,$oldvalue) {
		if(ll_tools_change_update_filters($option)) {
			update_site_option($option,$value);
			return $oldvalue;	
		}
		return $value;
	},5,3);
}

################## Ende Adminseiten


?>
