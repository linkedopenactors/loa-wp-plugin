<?php
/* ll-Tools, hier werden die Funktionen für Updates gesammelt.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

########### Version 4.1.7 #############
class update_4_1_8 {

	function __construct() {
		$this->protFile = wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs/Update-4.1.8.txt";
		$this->prod = true;
		$this->text = ($this->prod) ? "": "(Test) ";

		$this->ll_update_prot("Update 4.1.8 wird gestartet - ".current_time('Y-m-d H:i:s'));
		$this->ll_update_sunrise();
		$this->ll_update_prot("Update 4.1.8 wurde abgeschlossen - ".current_time('Y-m-d H:i:s'));
	}
	
	
	private function ll_update_sunrise() {
		if(!file_exists(WP_CONTENT_DIR."/sunrise.php")) {
			$this->ll_update_prot("Domainmanger nicht aktiv");
			return;
		}
		unlink(WP_CONTENT_DIR."/sunrise.php");
		$this->ll_update_prot(WP_CONTENT_DIR."/sunrise.php wurde gelöscht");
		$this->ll_update_prot(dirname(__DIR__)."/sunrise.php wird kopiert");
		if(file_exists(dirname(__DIR__)."/sunrise.php")) {
			copy(dirname(__DIR__)."/sunrise.php",WP_CONTENT_DIR."/sunrise.php");
			$this->ll_update_prot(dirname(__DIR__)."/sunrise.php wurde kopiert");
		}		
	}
	
	private function ll_update_prot($text) {
		file_put_contents($this->protFile, print_r($text,true).chr(10), FILE_APPEND | LOCK_EX);
	} 
}

new update_4_1_8();
ll_crm_debug("Updatefunktionen 4.1.8 wurden ausgeführt","print");


?> 
