<?php
/* ll-Tools, hier werden die Funktionen für Updates gesammelt.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

########### Version 4.1.6 #############
class update_4_1_6 {
	function __construct() {
		$this->ll_update_vars();
	}
	
	private function ll_update_vars() {
		if(!is_array(get_option(LL_TOOLS_OPTION.'civi_db_copy'))) {
			$options = array('civi_db_copy','civi_db_copy_db','civi_db_copy_user','civi_db_copy_pw','civi_db_copy_dbserver');
			ll_crm_debug($options,'print');
			foreach($options as $option) {
				$key = str_replace(array('civi_db_copy','_'),array("",""),$option);
				if($key == "") $key = 'checked';		
				$new[$key] = get_option(LL_TOOLS_OPTION.$option);
				ll_crm_debug($option,'print');
			}
			if(!add_option(LL_TOOLS_OPTION.'civi_db_copy',$new)) update_option(LL_TOOLS_OPTION.'civi_db_copy',$new);			
		}
		$options = array('map');
		foreach($options as $option) {
			if(!is_array(get_option(LL_TOOLS_OPTION.$option))) {
				$new[0] = get_option(LL_TOOLS_OPTION.$option);
				ll_crm_debug($option,'print');
				if(!add_option(LL_TOOLS_OPTION.$option,$new)) update_option(LL_TOOLS_OPTION.$option,$new);				
			}
		}
		if(LL_TOOLS_CIVICRM and (get_option(LL_TOOLS_OPTION.'map')[0] == 'OpenStreetMap')) {
			global $ll_crm_sets;
			
			if(isset($ll_crm_sets)) {
				foreach($ll_crm_sets as $key => $set) {
					if(empty($set['api_server'])) {
						$crm_sets['default'] = 'default';
					} else {
						$crm_sets['api_server'.($key-1)] = $set['api_server'];
						$crm_sets['api_key'.($key-1)] = $set['api_key'];
						$crm_sets['key'.($key-1)] = $set['key'];
						$crm_sets['next'] = true;						
					}
				}
				ll_crm_debug($crm_sets,'print');
				update_option(LL_TOOLS_OPTION.'crm_sets',$crm_sets);
			}
		}
	}
}

new update_4_1_6();
ll_crm_debug("Updatefunktionen 4.1.6 wurden ausgeführt","print");


?>
