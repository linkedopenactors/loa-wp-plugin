<?php
/* ll-Tools, hier werden die Funktionen für Updates gesammelt.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

########### Version 4.0.10 #############
class update_4_0_10 {
	function __construct() {
		$this->ll_field_def_write();
	}
	
	function ll_field_def_write() {
		$field_def = get_option(LL_TOOLS_OPTION.'field_def');
	//	ll_crm_debug($field_def,'print');
		if(empty($field_def)) return true;
		foreach($field_def['fields'] as $field_num => $field) {
			if($field['field'] == 'image') return true;
			if($field['field'] == 'file') $field_id = $field_num;
		}
		if(!isset($field_id)) return true;

		$CustomDef = new ll_crm_CiviCRM_api();
		$CustomDef->set_request(array('entity' => 'CustomField','action' => 'get', 'debug' => 1),array('id' => $field_id));
		$CustomValues = current($CustomDef->execute());
		ll_crm_debug($CustomValues,'print');
		$CustomDef = new ll_crm_CiviCRM_api();
		$add_help_post = '<!-- ll-tools image,Image,file-->';
		$params = array("custom_group_id"=> $CustomValues['custom_group_id'], "label"=> "Image", "data_type"=> "String", "html_type"=> "Text", 
		             	"help_post"=> $add_help_post, "weight" => 20, "is_view"=> true, "text_length"=> 255); 
		$CustomDef->set_request(array('entity' => 'CustomField','action' => 'create', 'debug' => 1),$params);
		$CustomDef->execute();
		ll_crm_debug("Feld Image angefügt","print");	
	}

	function ll_field_def_update_function() {
		//Mit function_exists() prüfen, ob die Funktion überhaupt aktiv ist!
		//Sie könnte deaktiviert sein oder CiviCRM ist nicht installiert!
		//Beides nicht notwendig, wenn nur ein Kommentar gesetzt wird!
		$ll_update_note = array('/*LL_UPD: Dieser Funktion wird ab Version 4.0.7 nicht mehr verwendet und kann daher entfernt werden!'.chr(10),
							  ' *ll_field_def wird aus den Tabellen über die Adminseite nach option = ll_tools_field_def gespeichert'.chr(10),
							  ' *ACHTUNG das Format des Arrays ist geändert'.chr(10),
							  ' */'.chr(10));
		if(file_exists(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/ll_crm_settings.php")) {
			$code_array = file(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/ll_crm_settings.php");
		} else {
			$code_array = file(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/ll_tools_settings.php");		
		}
		$count = 0;
		foreach($code_array as $line => $code) {
			if((strpos($code,'LL_crm_custom_fields_def(') !== false) or $count > 0) {
				if($count == 0) {
					array_splice($code_array,$line,0,$ll_update_note);
				}
	//			echo '<br>'.$line.'-'.$code;
	//			unset($code_array[$line]);
				$count += substr_count($code,'{') - substr_count($code,'}'); 
				if($count == 0) {
					//Element mit Ende der Auskommentierung einfügen.
					break;
				}
			} 
		}
		ll_crm_debug($code_array);
		file_put_contents(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/ll_tools_settings.php",$code_array);
	}
	
}

if(LL_TOOLS_CIVICRM) {
	new update_4_0_10();
	ll_crm_debug("Updatefunktionen 4.0.10 wurden ausgeführt","print");
}


?>