<?php
/* ll-Tools, hier werden die Funktionen für Updates gesammelt.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

########### Version 4.0.7 #############
function ll_field_def_update($field_def,$field) {
	// UpdateVersuch
	ll_crm_debug($field);
	static $custom_fields_old;
	$output = "";
	$key = "";
	if(!isset($custom_fields_old)) {
		if(function_exists('LL_crm_custom_fields_def')) {
	  		$custom_fields_old = LL_crm_custom_fields_def();
		} else {
			return $output;
		}		
	}
	ll_crm_debug($custom_fields_old);
	if(!empty($custom_fields_old))
		foreach($custom_fields_old as $group_name_old => $group_old) {
			ll_crm_debug($group_old);
			if(!empty($group_old['Data'][$field['id']])) {
//				echo "<p>Alte Definition gefunden: Feldname: ".$group_old['Data'][$field['id']].", Gruppe: ".$group_name_old." - Werte werden eingetragen!</p>";
				if(empty($field_def['fields'][$field['id']]['group'])) {
					$field_def['fields'][$field['id']]['group'] = $group_name_old;
				}
				if(empty($field_def['fields'][$field['id']]['field'])) {
					$field_def['fields'][$field['id']]['field'] = $group_old['Data'][$field['id']];
				}
				if($group_old['Data'][$field['id']] == 'language') $key = ',Language';
				ll_crm_debug($field_def['fields'][$field['id']]['group']);
				if(!empty($group_old['Key']) and empty($field_def['keys']['fields'][$field['id']]['group'])) {
					$field_def['keys'][$field_def['fields'][$field['id']]['group']] = $group_old['Key'];
					if($group_old['Key'] == $group_old['Data'][$field['id']]) $key = ',Key';
				}
				ll_field_def_update_def($field,$group_name_old,$group_old['Data'][$field['id']],$key);
			}
		}
	return $field_def;
}
add_filter('ll_field_def_update_field','ll_field_def_update',10,2);

function ll_field_def_update_def($field,$group_name,$field_name,$key_field) {
	static $group_list;
	static $CustomDef;
	if(!isset($CustomDef)) $CustomDef = new ll_crm_CiviCRM_api();																		
	if(empty($group_list[$group_name])) {
		$CustomDef->set_request(array('entity' => 'CustomGroup', 'action' => 'get', 
									  'params' =>array('id' => $field['custom_group_id'],
									  'extends' => $field['api.CustomGroup.get']['values'][0]['extends'],
									  'is_multiple' => $field['api.CustomGroup.get']['values'][0]['is_multiple'])));  
		$add_help_post = '<!-- ll-tools '.$group_name.' -->';
		ll_field_def_update_def_write($CustomDef,$add_help_post);
	}
	$CustomDef->set_request(array('entity' => 'CustomField', 'action' => 'get', 
								  'params' =>array('id' => $field['id'])));
	$add_help_post = '<!-- ll-tools '.$field_name.$key_field.' -->';
	ll_field_def_update_def_write($CustomDef,$add_help_post);
}


function ll_field_def_update_def_write($CustomDef,$add_help_post) {
	$CustomDef->execute();
	ll_crm_debug($CustomDef->ll_result['result']);
	$help_post = "";
	if(!empty($CustomDef->ll_result['result']['values'][$CustomDef->ll_result['result']['id']]['help_post']))
		$help_post = $CustomDef->ll_result['result']['values'][$CustomDef->ll_result['result']['id']]['help_post'];
	if(strpos($help_post,$add_help_post) === false) {
		$CustomDef->set_request(array('action' => 'create'),array('help_post' => $help_post.$add_help_post));
		$CustomDef->execute();
		ll_crm_debug(substr($add_help_post,1,-1));
		ll_crm_debug($CustomDef->ll_result);			
	}	
}

//Löscht die Funktion aus dem Code ohne Rückfrage.
function ll_field_def_update_function() {
	//Mit function_exists() prüfen, ob die Funktion überhaupt aktiv ist!
	//Sie könnte deaktiviert sein oder CiviCRM ist nicht installiert!
	//Beides nicht notwendig, wenn nur ein Kommentar gesetzt wird!
	$ll_update_note = array('//LL_UPD: Dieser Funktion wird ab Version 4.0.7 nicht mehr verwendet und kann daher entfernt werden!'.chr(10),
						  '//ll_field_def wird aus den Tabellen über die Adminseite nach option = ll_tools_field_def gespeichert'.chr(10),
						  '//ACHTUNG das Format des Arrays ist geändert'.chr(10),
						  '//'.chr(10));
	if(file_exists(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/ll_crm_settings.php")) {
		$code_array = file(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/ll_crm_settings.php");
	} else {
		$code_array = file(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/ll_tools_settings.php");		
	}
	$count = 0;
	foreach($code_array as $line => $code) {
		if((strpos($code,'LL_crm_custom_fields_def(') !== false) or $count > 0) {
			if($count == 0) {
				array_splice($code_array,$line,0,$ll_update_note);
			}
//			echo '<br>'.$line.'-'.$code;
//			unset($code_array[$line]);
			$count += substr_count($code,'{') - substr_count($code,'}'); 
			if($count == 0) {
				//Element mit Ende der Auskommentierung einfügen.
				break;
			}
		} 
	}
	ll_crm_debug($code_array);
	file_put_contents(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/ll_tools_settings.php",$code_array);
}

add_action('ll_field_def_update','ll_field_def_update_function');

if(LL_TOOLS_CIVICRM) {
	ll_tools_field_def_cb(false);
	ll_crm_debug("Updatefunktionen wurden ausgeführt","print");
}


?>