<?php
/* ll-Tools, hier werden die Funktionen für Updates gesammelt.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


########### Version 4.1.4 #############
class update_4_1_4 {
	function __construct() {
		$this->ll_update_vars();
	}
	
	private function ll_update_vars() {
		$options = array();
		$options['upd_server'] = 'http://livinglines.at/livinglines-tools/';
		$options['updates'] = apply_filters('ll_tools_UpdateCheck',array(plugin_basename(LL_PLUGIN_FILE),'civicrm/civicrm.php'));
		$options['plug_bak'] = apply_filters('ll_tools_pre_update',array(plugin_basename(LL_PLUGIN_FILE)));
		ll_crm_debug($options,'print');
		foreach($options as $option => $value) {
			if(!add_option(LL_TOOLS_OPTION.$option,$value))
				update_option(LL_TOOLS_OPTION.$option,$value);
			ll_crm_debug($option,'print');
		}
	}
}

//if(LL_TOOLS_CIVICRM) {
	new update_4_1_4();
	ll_crm_debug("Updatefunktionen 4.1.4 wurden ausgeführt","print");
//}


?>