<?php
/* ll-Tools, hier werden die Funktionen für Updates gesammelt.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

########### Version 4.1.5 #############
class update_4_1_5 {
	function __construct() {
		$this->ll_update_vars();
		if(is_multisite()) $this->ll_install_mu();
	}

	private function ll_install_mu() {
		$mu_file = "livinglines-tools-mu.php";
		if(!is_dir(WP_CONTENT_DIR.'/mu-plugins')) mkdir(WP_CONTENT_DIR.'/mu-plugins');		
		if(is_file(dirname(LL_PLUGIN_FILE)."/admin/".$mu_file) and !is_file(WP_CONTENT_DIR."/mu-plugins/".$mu_file))
			copy(dirname(LL_PLUGIN_FILE)."/admin/".$mu_file, WP_CONTENT_DIR."/mu-plugins/".$mu_file);	
	}
	
	private function ll_update_vars() {
		$options = array('updates','plug_bak','theme_bak','civi_update_test','civi_db_drop_tables');
		ll_crm_debug($options,'print');
		foreach($options as $option) {
			$temp = get_option(LL_TOOLS_OPTION.$option);
			foreach($temp as $value) $new[$value] = $value;
			update_option(LL_TOOLS_OPTION.$option,$new);
			ll_crm_debug($option,'print');
		}
	}
}

new update_4_1_5();
ll_crm_debug("Updatefunktionen 4.1.5 wurden ausgeführt","print");


?>