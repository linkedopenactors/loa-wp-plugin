<?php
/* ll-Tools, hier werden die Funktionen für Updates gesammelt.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

########### Version 4.1.7 #############
class update_4_1_7 {

	function __construct() {
		$this->prod = true;
		$this->text = ($this->prod) ? "": "(Test) ";
		$this->result = array("Update 4.1.7 wird gestartet".$this->text);
		$this->ll_update_vars();
		ll_crm_debug($this->result,'print');
	}
	
	private function ll_update_vars() {
	   $option = get_site_option(LL_TOOLS_OPTION.'upd_server');
	   if(!is_array($option)) {
	   	update_site_option(LL_TOOLS_OPTION.'upd_server',array('server' => $option));
	   }
		//Alte Variablen löschen 
		$options = array('civi_db_copy_db','civi_db_copy_user','civi_db_copy_pw','civi_db_copy_dbserver');
		foreach($options as $option) $this->delete_option(LL_TOOLS_OPTION.$option);
		//Updatevariablen löschen (nicht im MainBlog)
		
		$options = array('upd_server','updates','plug_bak','theme_bak','upload_server','uploaded_version');
		foreach($options as $option) {
		   if(get_option(LL_TOOLS_OPTION.$option)) {
				if(!get_site_option(LL_TOOLS_OPTION.$option) and is_multisite()){
					$this->result[] = "Siteoption: ".$option;
					update_site_option(LL_TOOLS_OPTION.$option,get_option(LL_TOOLS_OPTION.$option));
				}
				if(is_multisite()) $this->delete_option(LL_TOOLS_OPTION.$option);					   
		   }
		}
		$options = array('key*','api_key*','api_server*','datasets','clear_clear_cache','field_modify','function_test');
		foreach($options as $optionmask) {
			$set = array(LL_TOOLS_OPTION.$optionmask => get_option(LL_TOOLS_OPTION.$optionmask));
			if(substr($optionmask,-1) == "*") $set = ll_get_options_set(LL_TOOLS_OPTION.substr($optionmask,0,-1));
				foreach($set as $option => $value) $this->delete_option($option);			
		}
	}
	
	private function delete_option($option) {
		$this->result[] = "Delete Option: ".$this->text.$option;
		if($this->prod) delete_option($option);
	}
	
}

new update_4_1_7();
ll_crm_debug("Updatefunktionen 4.1.7 wurden ausgeführt","print");


?>
