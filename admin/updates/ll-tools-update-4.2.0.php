<?php
/* ll-Tools, hier werden die Funktionen für Updates gesammelt.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

########### Version 4.1.7 #############
class update_4_2_0 {

	function __construct() {
		$this->protFile = wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs/Update-4.2.0.txt";
		$this->prod = true;
		$this->text = ($this->prod) ? "": "(Test) ";

		$this->ll_update_prot("Update 4.2.0 wird gestartet - ".current_time('Y-m-d H:i:s'));
		$this->ll_update_tools_MU();
		$this->ll_update_prot("Update 4.2.0 wurde abgeschlossen - ".current_time('Y-m-d H:i:s'));
	}
	
	
	private function ll_update_tools_mu() {
		$mu_file = "livinglines-tools-mu.php";
		if(is_file(dirname(LL_PLUGIN_FILE)."/admin/".$mu_file) and is_file(WP_CONTENT_DIR."/mu-plugins/".$mu_file)) { 
			copy(dirname(LL_PLUGIN_FILE)."/admin/".$mu_file, WP_CONTENT_DIR."/mu-plugins/".$mu_file);
			$this->ll_update_prot(WP_CONTENT_DIR."/mu-plugins/".$mu_file." wurde kopiert");
		}		
	}
	
	private function ll_update_prot($text) {
		file_put_contents($this->protFile, print_r($text,true).chr(10), FILE_APPEND | LOCK_EX);
	} 
}

new update_4_2_0();
ll_crm_debug("Updatefunktionen 4.2.0 wurden ausgeführt","print");


?>
