<?php
/* ll-Tools, hier werden die Funktionen für Updates gesammelt.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

########### Version 4.1.7 #############
class update_4_2_3 {

	function __construct() {
		$this->protFile = wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs/Update-4.2.3.txt";
		$this->prod = true;
		$this->text = ($this->prod) ? "": "(Test) ";
		$plugins = get_option(LL_TOOLS_OPTION."plugins",array());
		
		$this->ll_update_prot("Update 4.2.3 wird gestartet - ".current_time('Y-m-d H:i:s'));
		if(!empty($plugins) and isset($plugins['Civi Data']) and isset($plugins['Civi Map']))
			$this->ll_update_civi_map();
		$this->ll_update_prot("Update 4.2.3 wurde abgeschlossen - ".current_time('Y-m-d H:i:s'));
	}
		
	private function ll_update_civi_map() {
		$this->text_sets = get_option(LL_TOOLS_OPTION."map_response");
		$map_sets = get_option(LL_TOOLS_OPTION."map_sets");
		$this->ll_update_prot("MapSets und MapResponse gelesen");

		if(!isset($map_sets['sets'])) return;
		foreach($map_sets['sets'] as $map_nr => $map_set) {
			if(!empty($map_set['button'])) {
				if(is_array($map_set['button']))
					foreach($map_set['button'] as $lang => $text)
						$this->ll_update_response($map_nr,$lang,$text);
				else
					$this->ll_update_response($map_nr,'all',$map_set['button']);	
			}
		}
		$this->text_sets['full'] = true;
		update_option(LL_TOOLS_OPTION."map_response",$this->text_sets);
	}
	
	private function ll_update_response($map,$lang,$button) {
		foreach($this->text_sets['sets'] as $text_nr => $set) {
			if(empty($set['button']) or empty(trim($set['button']))) {
				if(($set['mapNr'] == "map".$map) and $set['language'] == $lang)
					$this->text_sets['sets'][$text_nr]['button'] = $button;
				elseif(($set['mapNr'] == 'all') and $set['language'] == $lang)
					$this->text_sets['sets'][$text_nr]['button'] = $button;
				elseif(($set['mapNr'] == "map".$map) and $set['language'] == 'all')
					$this->text_sets['sets'][$text_nr]['button'] = $button;
				elseif(($set['mapNr'] == 'all') and $set['language'] == 'all')
					$this->text_sets['sets'][$text_nr]['button'] = $button;			
			}
		}
	
	}
	
	private function ll_update_prot($text) {
		file_put_contents($this->protFile, print_r($text,true).chr(10), FILE_APPEND | LOCK_EX);
	} 
}

new update_4_2_3();
ll_crm_debug("Updatefunktionen 4.2.1 wurden ausgeführt","print");


?>
