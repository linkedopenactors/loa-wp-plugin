<?php
/* ll-Tools, hier werden die Funktionen für Updates gesammelt.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );

########### Version 4.1.7 #############
class update_4_2_1 {

	function __construct() {
		$this->protFile = wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs/Update-4.2.1.txt";
		$this->prod = true;
		$this->text = ($this->prod) ? "": "(Test) ";
		$plugins = get_option(LL_TOOLS_OPTION."plugins",array());
		
		$this->ll_update_prot("Update 4.2.1 wird gestartet - ".current_time('Y-m-d H:i:s'));
		if(LL_TOOLS_CIVICRM) $this->ll_update_civi_api();
		if(!empty($plugins) and isset($plugins['Civi Data']) and isset($plugins['Civi Map 1']))
			$this->ll_update_civi_map();
		$this->ll_update_prot("Update 4.2.1 wurde abgeschlossen - ".current_time('Y-m-d H:i:s'));
	}
	
	
	private function ll_update_civi_api() {
		$field_def = get_option(LL_TOOLS_OPTION.'field_def');
		set_transient(LL_TOOLS_OPTION.'field_def_bak',$field_def,3600*24);
		if($field_def) {
			$this->ll_update_prot("field_def wurde nach field_def_0 kopiert");
			add_option(LL_TOOLS_OPTION.'field_def_0',$field_def);
		}
		$crm_sets = get_option(LL_TOOLS_OPTION.'crm_sets');
		set_transient(LL_TOOLS_OPTION.'crm_sets_bak',$crm_sets,3600*24);
		if($crm_sets) {
			$this->ll_update_prot("crm_sets wurde überarbeitet");			
			if(!empty($crm_sets['sets'][0])) $crm_sets['sets'][0] = array('version' => 4);
 			if(empty($crm_sets['sets'][0])) $crm_sets['sets'][0] = array('version' => 0);
			$crm_sets['default'] = $crm_sets['sets'][0]['version'];
			if(!empty($crm_sets['sets'][1])) {
				$this->ll_update_prot("crm_sets[set1] wurde überarbeitet");			
				$crm_sets['sets'][1]['version'] = 3;
				$crm_sets['version1'] = $crm_sets['sets'][1]['version'];
			}
			update_option(LL_TOOLS_OPTION.'crm_sets',$crm_sets);
		}
	}
	
	private function ll_update_civi_map() {	
		$ll_crm_map = new ll_crm_Map();
		$options = $ll_crm_map->get_options();
		if(is_array($options)) {
			$old_options = get_option(LL_TOOLS_OPTION."map_sets",array());
			if(!is_array($old_options)) $old_options = array(); //Sicherheit gegen falsche WErte!
			$options['save'] = "";
			$options['set_'] = "-";
			$this->ll_update_prot(array('map_sets' => array($options)));
			$classOptions = LL_classes_optionOptions::singleton(array('map_sets'));
			$old_options = $classOptions->update_data_sets($options,$old_options);
//			$old_options = pre_update_option_ll_tools_data_sets($options,$old_options,ll_map_standard_options());
			if(isset($old_options['check'])) unset($old_options['check']);
			$old_options['sets'][0] = $old_options['sets'][''];
			unset($old_options['sets']['']);
			foreach(array('map_page','map_project') as $option) {
				$temp = get_option(LL_TOOLS_OPTION.$option);
				if(!empty($temp)) $old_options['sets'][0][$option] = $temp;
			}
			$old_options['sets'][0]['sidebar'] = 'sidebar0';
			$old_options['full'] = true;
			ll_crm_debug($old_options);
			$this->ll_update_prot($old_options);

			update_option(LL_TOOLS_OPTION."map_sets",$old_options);
			$this->ll_update_prot("map_sets wurde überarbeitet");							
		}
		$groups = $ll_crm_map->get_optionset('','widget_group');
		ll_crm_debug($groups);
		$options = $ll_crm_map->get_optionset();
		if(is_array($options)) {
//			ll_crm_debug($options,true,true);
			$old_options = array();
			$old_options['sets'] = array();
			foreach($options as $tag => $tag_set) {
				$tag_set['save'] = "";
				$tag_set['set_'] = "-";

				if(isset($tag_set['line_style']))
					foreach($tag_set['line_style'] as $element => $value) $tag_set['line_style+'.$element] = $value;
				unset($tag_set['line_style']);
				if(isset($tag_set['point_style']))
					foreach($tag_set['point_style'] as $element => $value) $tag_set['point_style+'.$element] = $value;
				unset($tag_set['point_style']);

				$classOptions = LL_classes_optionOptions::singleton(array('option_set0','option_set'));
				$old_options = $classOptions->update_data_sets($tag_set,$old_options);
//				$old_options = pre_update_option_ll_tools_data_sets($tag_set,$old_options,ll_map_standard_optionset());
//				ll_crm_debug($old_options,true,true);
				if(is_array($old_options['sets'][""]['name'])) {
					$basic_tag_set = get_transient(LL_TOOLS_OPTION."marker_options");
					ll_crm_debug($basic_tag_set);
					foreach($old_options['sets'][""]['name'] as $language => $dummy) {
						ll_crm_debug(substr($language,0,2));
						$old_options['sets'][""]['name'][$language] =
							$basic_tag_set[substr($language,0,2)]['tags'][$tag]['tagname'];
					}
				} else
					$old_options['sets'][""]['name'] = $tag_set['tagname'];

				$old_options['sets'][""]['tagname'] = $tag_set[1];
				$old_options['sets'][""]['post'] = 'set_tag';
				$old_options['sets'][""]['set_tag'] = "0_".$tag_set[0];
				if(isset($old_options['sets'][""]['TexticonColor']) and
					($old_options['sets'][""]['TexticonColor'] == $old_options['sets'][""]['iconColor']))
					$old_options['sets'][""]['TexticonColor'] = "";
				$old_options['sets'][] = $old_options['sets'][""];
				unset($old_options['sets']['']);			
			}
			
			foreach($groups as $tag => $tag_set) {
				$tag_set['save'] = "";
				$tag_set['set_'] = "-";
				$tag_set['post'] = "widget_group";
				$classOptions = LL_classes_optionOptions::singleton(array('option_set0','option_set'));
				$old_options = $classOptions->update_data_sets($tag_set,$old_options);
				if(is_array($old_options['sets'][""]['name'])) {
					foreach($old_options['sets'][""]['name'] as $language => $dummy) {
						$old_options['sets'][""]['name'][$language] =
							$basic_tag_set[substr($language,0,2)]['widget_group'][$tag]['title'];
					}
				} else
					$old_options['sets'][""]['name'] = $tag_set['title'];
				$old_options['sets'][""]['group'] = $tag;
				$old_options['sets'][] = $old_options['sets'][""];
				unset($old_options['sets']['']);			
			}
						
			if(isset($old_options['check'])) unset($old_options['check']);
			$old_options['full'] = true;

			ll_crm_debug($old_options);
			$this->ll_update_prot($old_options);
			update_option(LL_TOOLS_OPTION."option_set0",$old_options);
			$this->ll_update_prot("option_set wurde überarbeitet");	
		}
		$page = get_option(LL_TOOLS_OPTION.'map_page');
		ll_crm_debug($page);	
		if($page !== false) {
			$old_options = array();
			$old_options['sets'] = array();
			$languages = apply_filters('ll_tools_languages',array());
			foreach($languages as $ll_lang) {
				$page = pll_get_post($page,$ll_lang);
				$text = apply_filters('ll_response',array(), get_post($page));
				ll_crm_debug($text);
				$text['save'] = "";
				$text['mapNr'] = 0;
				$text['language'] = $ll_lang;
				$classOptions = LL_classes_optionOptions::singleton(array('map_response'));
				$old_options = $classOptions->update_data_sets($text,$old_options);
				$old_options['sets'][] = $old_options['sets'][""];
				unset($old_options['sets']['']);			
			}	
			if(isset($old_options['check'])) unset($old_options['check']);
			$old_options['full'] = true;

			ll_crm_debug($old_options);
			$this->ll_update_prot($old_options);
			update_option(LL_TOOLS_OPTION."map_response",$old_options);
			$this->ll_update_prot("response-Texte wurde überarbeitet");	
		} 
		ll_crm_debug("");
	}
	
	private function ll_update_prot($text) {
		file_put_contents($this->protFile, print_r($text,true).chr(10), FILE_APPEND | LOCK_EX);
	} 
}

new update_4_2_1();
ll_crm_debug("Updatefunktionen 4.2.1 wurden ausgeführt","print");


?>
