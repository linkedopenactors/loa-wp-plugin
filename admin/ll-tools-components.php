<?php
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
* Tools-Livinglines - Componenten
* HIer werden die einzelnen Komponenten (de)aktiviert, und die zugehörigen Bibliotheken geladen.
* Dieser muß ganz am Anfang geladen werden und ist auch im MulitSite-Kernel notwendig, 
* da es Komponeten geben kann, die für das ganze Netzwerk aktiv werden.
*/



/* Beim Laden dieser Datei sollte es vorerst um das Laden der Komponenten gehen ...
*/
add_action('load-toplevel_page_ll_tools_plug','ll_tools_plug_settings_init');
//add_action('load-ll-tools_page_ll_tools_plug','ll_tools_plug_settings_init');
add_action('load-options.php','ll_tools_save_settings_init',5);

function ll_tools_save_settings_init() {
	//Wird beim Speichern der Variablen nicht geladen, weil is_network_admin() nicht TRUE ist.
	//Das Nachladen hier, wirkt aber für alle Settings-Seiten.
	//Ist aber trotzdem kein schöner Platz, oder könnte ev. immer und in allen Settings hier sein, da es nur hier gebraucht wird.
	//Das ist außerdem nur notwendig für Multisite, daher funktioniert auch die Konstante nur für MultiSite!
	ll_tools_file_loader("admin","ll-tools-admin-pages");
	//Dies ist nach livinglines-tools gewandert.
//	define('LL_SETTINGS_SAVE',true);
	ll_tools_plug_settings_init();
}

function ll_start_components() {
	//neu eingeführt, weil hier der User noch nicht zur Verfügung steht, sondern erst, wenn die Plugins geladen sind.
	if(!defined('LL_TOOLS_DEV'))
		define('LL_TOOLS_DEV',(strtolower(wp_get_current_user()->user_login) == apply_filters('ll_tools_test_user','hannes')));

	//Basiskomponenten
	if(LL_TOOLS or is_network_admin() or LL_SETTINGS_SAVE) {
		$ll_tools_plugins = array("Updates","addons");
		if(LL_TOOLS_DEV) $ll_tools_plugins[] = 'Development';
		ll_tools_load_components($ll_tools_plugins);	
	}

	//Multisitekomponeten
	if(is_multisite()) {
		$ll_tools_plugins = get_site_option(LL_TOOLS_OPTION."plugins");
		ll_tools_load_components($ll_tools_plugins);
	}
	if(is_network_admin()) {
		add_action('network_admin_menu', 'll_tools_make_menu');
	} elseif(LL_TOOLS) {
	//Die Options könnten existieren, wenn die Instanz nur vorübergehend deaktiviert ist?
		$ll_tools_plugins = get_option(LL_TOOLS_OPTION."plugins");
		ll_tools_load_components($ll_tools_plugins);
		if(is_admin()) {
			add_action('admin_menu', 'll_tools_make_menu');	
		} else {
			//Hier ist entschieden, dass es sich um das FrontEnd handelt.
			############ Styles
			add_action( 'wp_enqueue_scripts', 'll_tools_styles' ,15);
			add_action( 'wp_enqueue_scripts', 'll_tools_styles_indiv', 20 );
		}
		//muß ev. auch für den Admin geladen werden.
		//Die indiv. Files werden nun sofort geladen, damit auch Funktionen vor den Plugins aktiv werden können.
		//ll_tools_file_loader(wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME,"ll_","");
	}
	//Muß davor nicht eigentlich CiviCRM aktiviert sein?	
	if(get_option('ll_tools_update_list')) {
		LL_classes_upgrader::ll_update_site(get_option('ll_tools_update_list'));
	}
}


function ll_tools_make_menu($sub_menu = false) {
	ll_crm_debug($sub_menu);
	static $menu_options;
	if(empty($menu_options)) {
		$menu_options = array(array(
					 	"site_pre" => "Livinglines - ",
					 	"site_title" => "Komponenten",
					 	"rights" => "manage_options",
					 	"main_menu" => LL_TOOLS_OPTION."plug",
					 	"page" => "plug",
					 	"function" => "ll_tools_options_page_html",
					 	"network" => 'both'
					 ));	
	}
	if(is_array($sub_menu)) {
		$menu_options[] = array_merge($menu_options[0],$sub_menu);	
	}
	if(is_string($sub_menu)) { //der zweite Parameter der Action ist $context = STring
		//Dieser Bereich wird immer für Admin-Seiten aufgerufen.
		//admin-pages.php nur notwendig, wenn wirklich Adminpages angezeigt werden können.
		//d.h. hier eigentlich noch zu oft. Sollte direkt in die ll_tools_settings_init.
		ll_tools_file_loader("admin","ll-tools-admin");
		add_action( 'admin_enqueue_scripts', 'll_tools_styles' ,15);
		extract($menu_options[0],EXTR_OVERWRITE);	
		add_menu_page($site_pre.$site_title,'LL-Tools',$rights,$main_menu,$function,'',100);
		foreach($menu_options as $sub_menue) {
//			ll_crm_debug(array($sub_menue,is_network_admin()),$sub_menue['page'] == 'upd');
			if((!is_multisite() or $sub_menue['network'] === 'both') or ($sub_menue['network'] === is_network_admin())) {
				extract($sub_menue,EXTR_OVERWRITE);
				add_submenu_page($main_menu, $site_pre.$site_title,$site_title,$rights, LL_TOOLS_OPTION.$page, $function);						
			}
		}
	}
}



function ll_tools_settings_plug_cb() {
//	ll_crm_debug(ll_tools_register_components(true),true);

//	ll_crm_debug(has_filter('xmlrpc_enabled'),true);
//	ll_crm_debug(get_transient(LL_TOOLS_OPTION."site_options"),true);
// ll_crm_debug(get_site_option(LL_TOOLS_OPTION."plugins"),true);
// ll_crm_debug(get_option(LL_TOOLS_OPTION."plugins"),true);
//	global $new_allowed_options;
//	ll_crm_debug($new_allowed_options,true);

}

function ll_tools_plug_settings_init() {
	#### Integrierte Plugins
	$default_args = array('group' => 'plug', 'title' => "Komponeten");
		
	$opt_values['plugins'] = array('title' => 'Komponeten');
	########################NEUE VERSION
	##################Umstellung und notwendig für is_child!
	if(is_network_admin())
		$plugins = get_site_option(LL_TOOLS_OPTION.'plugins',array()); 
	else 
		$plugins = get_option(LL_TOOLS_OPTION.'plugins',array()); 
	#####ENDE
	ll_crm_debug($plugins);
	$components = ll_tools_register_components(isset($_GET['ll_buffer']));
	ll_crm_debug($components);
	$updated = false;
	foreach($components as $component) {
		##################Umstellung
		if(strpos($component['name'],"-") and !isset($plugins[$component['name']])
													 and isset($plugins[str_replace("-"," ",$component['name'])])) {
			$plugins[$component['name']] = $component['slug'];
			$updated = true;
		}
		if(isset($plugins[$component['name']]) and ($plugins[$component['name']] != $component['slug'])) {
			$plugins[$component['name']] = $component['slug'];
			$updated = true;		
		}
		#####ENDE
		if(!empty($component['is_child']) and (!isset($plugins[$component['is_child']])))
			continue;
		if(!empty($component['multisite'])) {
			ll_crm_debug($component['multisite']=='both');
			if((($component['multisite'] == 'only') and is_network_admin()) or ($component['multisite'] == 'both'))
				$opt_values = ll_tools_set_plugin_option($opt_values,$component);
		}elseif(!is_network_admin())
			$opt_values = ll_tools_set_plugin_option($opt_values,$component);

	} 	
	if($updated) {
		if(is_network_admin())
			update_site_option(LL_TOOLS_OPTION.'plugins',$plugins);														 
		else 				
			update_option(LL_TOOLS_OPTION.'plugins',$plugins);														 
	}

	ll_tools_make_section($default_args,$opt_values);	
}

	/**
	 * Baut das array $option_values['options'] für die Komponenten auf.
	 * 
	 * @since
	 *
	 * @param array $opt_values 
	 * @param array $component Definitionswerte der Komponente
	 *
	 * Komponenten, die bereits im Netzwerkadmin aktiviert sind, werden hier nicht nochmals angezeigt.
	 */


function ll_tools_set_plugin_option($opt_values,$component) {
	static $plugins;
	static $option_group;
	
	if(!isset($plugins))
		$plugins = get_site_option(LL_TOOLS_OPTION.'plugins',array());

	if((substr($component['slug'],0,1) == "*" )) {
		$option_group = $component;
	}else {
		//Prüft ob die Komponente im Netzwerkadmin aktiviert ist, wenn ja, keine Anzeig in der Instanz
		if(!isset($plugins[$component['slug']]) or !is_multisite() or is_network_admin()) {
			ll_crm_debug($option_group);
			if(!empty($option_group)) {
				$opt_values['plugins']['options'][$option_group['slug']] = $option_group['description'];
				$option_group = array();	
			}
			$opt_values['plugins']['options'][$component['name']] = array('title' => $component['description'],
																								'bvalue' => $component['slug']);
		}	
	}

	return $opt_values;			
}

add_filter('pre_update_site_option_'.LL_TOOLS_OPTION.'plugins',function($value,$oldvalue) {
	$option = 'Domain Manager';
	if(!isset($value[$option]) and isset($oldvalue[$option])) {
		//DeAktivierung
		unlink(WP_CONTENT_DIR."/sunrise.php");
		add_settings_error('ll_tools',esc_attr('settings'),"Plugin wurde deaktiviert!",'success');
	} 
	if(isset($value[$option]) and !isset($oldvalue[$option])) { //Aktivierung
	   if(!function_exists('ll_dm_check')) require_once(LL_PLUGIN_DIR."components/ll-tools-".str_replace(" ","-",$option).".php");
		if(!ll_dm_check()) {
			unset($value[$option]);
			add_settings_error('ll_tools',esc_attr('settings'),"Plugin wurde nicht aktiviert!");
		}
	} 	
	return $value;
},10,2);

	/**
	 * Hier werden aus Files aus dem Verzeichnis Komponentes die Component Data ausgelesen.
	 * Daten werden eine Stunden gespeichert.
	 * @since
	 *
	 * @param bool $new = Neuaufbau erzwingen.
	 *
	 * 4.2.5.7 is_child 
	 */

function ll_tools_register_components($new = false) {
	$comp_options = get_site_transient(LL_TOOLS_OPTION."register_components");
	if(empty($comp_options) or $new) {
		$comp_options = array();
		$dir = LL_PLUGIN_DIR."components/";
		$file_list = scandir($dir); 
		$data = array('description' => "Component Name", 'group' => 'Components Group', 'order' => 'Components Order',
							'multisite' => "Component Multisite", 'is_child' => "Components Parent");
		foreach($file_list as $dir_file) {
			if(substr($dir_file,0,1) != ".") {
				$dir_files = array();
				if(is_dir($dir.$dir_file)) {
					$files = ll_list_dir($dir.$dir_file,"ll-tools-".$dir_file);
					foreach($files as $file) $dir_files[] = $dir_file."/".$file;
//					$dir_file = $dir_file."/ll-tools-".$dir_file.".php"; 				
				} else $dir_files[] = $dir_file;
				//sunrise.php sollte in ein anderes Verzeichnis!
				foreach($dir_files as $dir_file)
					if($dir_file != "sunrise.php")
						$components[$dir_file] = get_file_data($dir.$dir_file,$data);
			} 
		}
		$group_descriptions = array('ALLG' => array(1,"<b>Allgemeine Komponenten</b>"),
											 'CIVI' => array(3,"<b>CIVICRM-Komponenten</b>"),
											 'MAP' => array(2,"<b>Map-Komponenten</b>"),
											 'SPARQL' => array(4,"<b>SPARQL-Anbindung</b>"),
											 'NET'=> array(0,"<b>MultiSite - Komponenten</b>"));
		foreach($components as $slug => $component) {
			if(empty($component['description'])) $component['description'] = $slug;

			if(empty($component['group'])) $component['group'] = 'ALLG';
			else $component['group'] = strtoupper(trim($component['group']));
			
			$component['multisite'] = strtolower(trim($component['multisite']));
			
			$comp_group = $group_descriptions[$component['group']][0]*1000;

			if(!isset($comp_options[$comp_group])) {
				$comp_options[$comp_group]['slug'] = "*".$component['group'];
				$comp_options[$comp_group]['description'] = $group_descriptions[$component['group']][1];
				$comp_options[$comp_group]['multisite'] = 'both';
				$comp_options[$comp_group]['name'] = "*".$component['group'];
			}
			$key = $group_descriptions[$component['group']][0]*1000+intval($component['order']);
			while(isset($comp_options[$key])) {
				$key += 10;
			}
			$comp_options[$key] = $component;
			$comp_options[$key]['slug'] = $slug;
			$start = (strpos($slug,"/") === false) ? 0 : strpos($slug,"/")+1;
 			$comp_options[$key]['name'] =	substr($slug,$start+9,-4);		
		}
		ksort($comp_options);
		ll_crm_debug($comp_options);
		set_site_transient(LL_TOOLS_OPTION."register_components",$comp_options,60*60);
	}
	return $comp_options;
}


?>
