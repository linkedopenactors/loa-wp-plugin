<?php

// functions that create our options page
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
/*
* Adminfunktione für Livinglines-Tools
* Werden nur für die Adminseiten geladen.
*/

add_filter('upgrader_pre_download', array('LL_classes_upgrader','llpre_download'),10,3);
add_filter( 'upgrader_pre_install', array('LL_classes_upgrader','llPreUpdate'), 10,3 );
add_filter( 'upgrader_post_install', array('LL_classes_upgrader','llPostUpdate'), 10,3 );
add_action( 'upgrader_process_complete', array('LL_classes_upgrader','llUpgrade_complete'),10,2);

############ Updateprüfung - hier wirklich richtig?????
add_action( 'load-plugins.php', 'll_tools_UpdateCheck',15,0 );
add_action( 'load-update.php', 'll_tools_UpdateCheck',15,0 );
add_action( 'load-update-core.php', 'll_tools_UpdateCheck',15,0);
add_action( 'admin_init', 'll_tools_UpdateCheck',15,0 );
add_action( 'wp_update_plugins', 'll_tools_UpdateCheck',15,0 );

		
function ll_get_options_set($mask,$check = false) {
	$llAllOptions = wp_load_alloptions();
	$option_list = array();
	if(is_array($llAllOptions)) {
		foreach($llAllOptions as $option => $value) {
			if(substr($option,0,strlen($mask)) == $mask) {
				$option_list[$option] = $value;
			}
		}								
	}
	if($check) {
		global $wp_registered_settings;
		ll_crm_debug($wp_registered_settings);
		$deprecated = array();
		foreach($option_list as $option => $key) {
			if(!isset($wp_registered_settings[$option])) {
				$deprecated[$option] = get_option($option);
				ll_crm_debug($deprecated[$option]);
//				if(is_array($deprecated[$option])) ll_crm_debug(serialize($deprecated[$option]),true);
				if(is_array($deprecated[$option])) $deprecated[$option] = serialize($deprecated[$option]);
			}
		}
		//if(!empty($deprecated)) return "Veraltete Optionen: ".substr($deprecated,0,-2);
		return $deprecated;
	}
	return $option_list;
}


function ll_tools_UpdateCheck() {
	$ll_plugins = ll_tools_get_options(LL_TOOLS_OPTION.'updates',true);
	if(empty($ll_plugins)) return;
	$ll_downloadServer = ll_tools_get_options(LL_TOOLS_OPTION.'upd_server',true);
	if(empty($ll_downloadServer['server'])) return;
	$beta = (isset($ll_downloadServer['beta'])) ? '&all' : '';
	
	
/*  Diese includes waren in der alten Funktion enthalten ...
	include_once(ABSPATH.'wp-includes/http.php'); 
	include_once(ABSPATH.'wp-includes/class-wp-error.php'); */ 
	$current = get_site_transient( 'update_plugins' );
	foreach($ll_plugins as $file) {
		//eingebaut für wpsparql, weil auch bei WP, allerdings mit Version 0.9.0
		if(isset($current->no_update[$file]) and !strstr($ll_downloadServer['server'],$current->no_update[$file]->package))
			unset($current->no_update[$file]);
		if(!isset($current->no_update[$file]) and !isset($current->response[$file])) {
			ll_crm_debug("Versionskontrolle ".$file,'print');
			$pluginInfo = get_plugin_data(WP_PLUGIN_DIR."/".$file);

			$llUpdateArray['slug'] = substr($file,0,strpos($file,"/"));

			$llUpdateFile = wp_remote_get($ll_downloadServer['server'].'update-helper.php?plugin='.$llUpdateArray['slug'].$beta);
			if(is_object($llUpdateFile)) { //Für den Fall, dass wp_remote_get einen error produziert!
				ll_crm_debug($llUpdateFile,'print',false,true);
				break;
			}
			$llUpdateFile = $llUpdateFile['body'];
			if(!empty($llUpdateFile)) {
				$llUpdateArray['id'] = 'w.org/plugins/'.$llUpdateArray['slug'];
				$llUpdateArray['plugin'] = $file;
				$llNewVersion = substr($llUpdateFile,strlen($llUpdateArray['slug'])+1,-4);
				$llUpdateArray['new_version'] =  $llNewVersion;
					$llUpdateArray['package'] = $ll_downloadServer['server'].$llUpdateFile;
				$llUpdateArray['icons'] = array();
				$llUpdateArray['banners'] = array();
				$llUpdateArray['banners_rtl'] = array();
				if(version_compare($pluginInfo['Version'],$llNewVersion) < 0) {
					ll_crm_debug($file);
					ll_crm_debug($llNewVersion);
					$current->response[$file] = (object) $llUpdateArray;
					ll_crm_debug($current->response[$file]);
				} else {
					$current->no_update[$file] = (object) $llUpdateArray;
				}
				$current->checked[$file] = $pluginInfo['Version'];
			}			
		} 
	}
	set_site_transient('update_plugins',$current);
	return;
}


function llActivate($network) {
	if($network) die("Bitte für jede Instanz aktivieren!");
	$llIndivDir = ll_make_dirs();	
 	$llBaseDir = wp_get_upload_dir()['basedir']."/ll_crm";
	if(is_dir(dirname(LL_PLUGIN_FILE)."/default/")) {
		$llBaseFiles = scandir(dirname(LL_PLUGIN_FILE)."/default/");
		foreach($llBaseFiles as $llBaseFile) {
			//Diese Bedingung ist notwendig, damit . und .. nicht zu kopieren versucht werden!
			// 2. Bedingung, damit die Files bei neuerlicher Aktivierung nicht überschrieben werden!
			if(is_file(dirname(LL_PLUGIN_FILE)."/default/".$llBaseFile) and !is_file($llIndivDir."/".$llBaseFile))
				copy(dirname(LL_PLUGIN_FILE)."/default/".$llBaseFile,$llIndivDir."/".$llBaseFile);
		}			
	} 		
	if(is_multisite()) {
		$mu_file = "livinglines-tools-mu.php";
		if(!is_dir(WP_CONTENT_DIR.'/mu-plugins')) mkdir(WP_CONTENT_DIR.'/mu-plugins');		
		if(is_file(dirname(LL_PLUGIN_FILE)."/admin/".$mu_file) and !is_file(WP_CONTENT_DIR."/mu-plugins/".$mu_file))
			copy(dirname(LL_PLUGIN_FILE)."/admin/".$mu_file, WP_CONTENT_DIR."/mu-plugins/".$mu_file);
		switch_to_blog(get_main_site_id());
		ll_make_dirs();
		restore_current_blog();
	}
}

function ll_make_dirs() {
	$llIndivDir = wp_get_upload_dir()['basedir']."/".LL_PLUGIN_NAME;
	if(!is_dir($llIndivDir)) mkdir($llIndivDir);
	if(!is_dir($llIndivDir.'/logs')) mkdir($llIndivDir.'/logs');
	ll_crm_debug($llIndivDir,'print');
	return $llIndivDir;
}


function llAfterActivate($plugin) {
//	ll_crm_debug($plugin,true,true);
	if(basename($plugin) == plugin_basename(LL_PLUGIN_FILE)) {
		
	}
	if(basename($plugin) == 'civicrm.php') {
		if(!add_option(LL_TOOLS_OPTION."civirm",$plugin))
			update_option(LL_TOOLS_OPTION."civirm",$plugin);
	}	
}

add_action('activated_plugin','llAfterActivate');

function llToolsZip($dirname, $filename = "wcZIPfile") {
	ll_crm_debug(array("ZIP-START",$dirname,$filename),'print',false,true);
	$dirlist = new RecursiveDirectoryIterator($dirname);
	$filelist = new RecursiveIteratorIterator($dirlist);

	$filelimit = 250;
	$emptydir = array();
	$useddir = array();
	$alldir = array();
	$allfile = array();

	
	$zip = new ZipArchive();
	$i = 1;
	$basename = $filename;
	while (file_exists($filename.".zip")) {
		$filename = $basename."-".strval($i);
		$i++;
	}
	$filename = "{$filename}.zip";
	if(file_exists($filename)) {
		ll_crm_debug("File existiert bereits, Vorgang abgebrochen!",'print',false,true);
		return false;
	}
	if ($zip->open($filename, ZipArchive::CREATE)!==TRUE) {
		ll_crm_debug("cannot open <$filename>",'print',false,true);
		return false;
	}
	foreach ($filelist as $key => $value) {
		$patharr = pathinfo($key);
		$pathtest = $patharr['dirname'];
		if(!in_array($pathtest,$alldir)) {
			$alldir[] = $pathtest;			
		}
		if(substr($key,-1) == ".") {
			if(!array_key_exists($pathtest,$useddir)) {
				if (!array_key_exists($pathtest,$emptydir)) {
					$emptydir[$pathtest] = "";				
			 	} 
			}
		} elseif(strpos($key,".~") === false) {
			ll_crm_debug($key);
			if(!array_key_exists($pathtest,$useddir)) $useddir[$pathtest] = "" ;
			if(array_key_exists($pathtest,$emptydir)) unset($emptydir[$pathtest]);
			$path = str_replace(dirname($dirname)."/", "", $key); 
			
		    if ($zip->numFiles == $filelimit) {$zip->close(); $zip->open($filename) or die ("Error: Could not reopen Zip");}
		    if (!file_exists($key)) { 
				$out.= $key.' does not exist. Please contact your administrator or try again later.'; 
			} elseif (!is_readable($key)) { 
				$out.= $key.' not readable. Please contact your administrator or try again later.'; 
			} else {
				$zip->addFile($key, $path) or die ("ERROR: Could not add file: $key </br> numFile:".$zip->numFiles);
				$allfile[] = $key;
			}
			
		}
	}
	foreach ($emptydir as $key => $value) {
		$path = str_replace(dirname($dirname)."/", "", $key); 		
		$zip->addEmptyDir( $path );
	}
	return $zip->close();
}


function ll_tools_version_upload($value,$pluginfile = LL_PLUGIN_FILE) {
	$pluginname = basename($pluginfile,".php");
	$type = ($value[0] == 'beta') ? '-Beta--' : '-Built-';
	$new_version = get_plugin_data($pluginfile)['Version'].$type.date('y.m.d-H.i');
	$filename = $pluginname.'-'.$new_version;
	$ll_downloadServer = get_site_option(LL_TOOLS_OPTION.'upd_server')['server'];
	if(empty($ll_downloadServer)) return false;


	$dirname = wp_upload_dir()['basedir'].'/'.LL_PLUGIN_NAME.'/uploads/';
	if(!is_dir($dirname))
	 	mkdir($dirname);
	
	if(llToolsZip(dirname($pluginfile),$dirname.$filename)) {
		if($value[0] == 'bak') return "Gesichert: ".$new_version;

		$lastversion = wp_remote_get($ll_downloadServer.'update-helper.php?plugin='.$pluginname.'&all');
		if(is_object($lastversion)) {
			add_settings_error('ll_tools',esc_attr('settings'), "Fehler in der Datenverbindung, Version nicht hochgeladen");
			set_transient(LL_TOOLS_OPTION.'upload_fail',"Fehler in der Datenverbindung",5*60);
			return;
		}
		ll_crm_debug(array($new_version,$lastversion['body'],substr($lastversion['body'],strlen($pluginname)+1,-4)));
		if(version_compare($new_version,substr($lastversion['body'],strlen($pluginname)+1,-4)) < 0) {
			set_transient(LL_TOOLS_OPTION.'upload_fail',"Bereits höhere Version vorhanden",5*60);
			return substr($lastversion['body'],strlen($pluginname)+1,-4);		
		}	
		
		ll_crm_debug("Start Upload",'print');
		$host = get_site_option(LL_TOOLS_OPTION."upload_server");
		if(empty($host)) return false;
				
		$local_file = $dirname.$filename.'.zip';
		$ftp_path = $filename.'.zip';
		$conn_id = ftp_connect($host['server'], 21) or die ("Cannot connect to host");     
		ftp_pasv($conn_id, true);
		ftp_login($conn_id, $host['user'], $host['pw']) or die("Cannot login");
		// perform file upload
		ll_crm_debug(array($conn_id,$ftp_path,$local_file),'print');
//		ftp_chdir($conn_id, '/web/'.$pluginname.'/') or ll_crm_debug("Falsches Verzeichnis",'print');
		ftp_chdir($conn_id, '/') or ll_crm_debug("Falsches Verzeichnis",'print');
		$upload = ftp_put($conn_id, $ftp_path, $local_file, FTP_BINARY);
		if($upload) { $ftpsucc=1; } else { $ftpsucc=0; }
		// check upload status:
		// close the FTP stream
		ftp_close($conn_id);		
	} else {
		add_settings_error('ll_tools',esc_attr('settings'), "Zip-File konnte nicht erstellt werden.");
		set_transient(LL_TOOLS_OPTION.'upload_fail',"Zip-File konnte nicht erstellt werden",5*60);
		return;	
	}
	if($upload) {
		ll_crm_debug('Upload complete','print');
		return $new_version;
	} else {
		set_transient(LL_TOOLS_OPTION.'upload_fail',"Fehler beim upload",5*60);
		ll_crm_debug(error_get_last(),'print');		
		ll_crm_debug('Cannot upload','print');		
		return false;
	}
}

		
?>
