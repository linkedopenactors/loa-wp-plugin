<?php
/**
* Kernfunktionen für den Betrieb des Plugins
* Ursprünglich im Startfile, für die MultiSite Version hierher übersiedelt.
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );


########## Initialisierung für Autoloader für Klassen
spl_autoload_register(function ($class) {
	if(substr($class,0,7) == 'll_crm_') {
		$class = str_replace(array('ll_crm_','_'),array('ll-crm-class-','-'),$class);
	    include LL_PLUGIN_DIR."crm/".$class . '.php';			
	} 
	//Dies System darüber sind aus Kompatibilitätsgründen noch hier!
	//Dies ist das neue System eingeführt mit Version 4.2.1
	//LL_ = Kenner, dann folgt der Pfad und hinter dem ldetzten "_" die indiv. Bezeichnung der Klasse
	if(substr($class,0,3) == 'LL_') {
		$path = substr($class,3,strrpos($class,"_")-3);
	//	ll_crm_debug("ll-class-".substr($class,strrpos($class,"_")+1) . '.php',true,true);
		if($path == 'classes') {
			if(is_dir(LL_PLUGIN_DIR.str_replace("_","/",$path)))
				 include LL_PLUGIN_DIR.str_replace("_","/",$path)."/ll-class-".substr($class,strrpos($class,"_")+1) . '.php';					
		}elseif(is_dir(LL_PLUGIN_DIR."components/".str_replace("_","-",$path)))
				 include LL_PLUGIN_DIR."components/".str_replace("_","-",$path).
				 							"/ll-class-".substr($class,strrpos($class,"_")+1) . '.php';					
	}
});



/*
##################### Debugging
Grundsätzlich erfolgt eine Ausgabe nur für den in LL_CRM_TEST_USER definierten User, es sei denn, die Funktion wird in der URL übergeben!
Die Ausgabe kann für eine Funktion auch über die URL definiert werden: ll_func=<Name der Funktion> 
Die Ausgabe der Werte wird über einen Schalter direkt im Aufruf aktiviert (Par2)
Die Ausgabe kann aber auch für eine oder mehrere Funktionen definiert werden. In diesem Fall sind die Funktionen folgendermaßen zu definieren:
$ll_crm_debug[] = func1;
$ll_crm_debug[] = func2; 
ERROR-Handlung nicht mehr notwendig, ll_error steuert jetzt WP_DEBUG in wp_config.php => ACHTUNG dort bitte aktivieren!
//define('WP_DEBUG',false);
define('WP_DEBUG', !empty($_GET['ll_error']));
*/

// Dies sollte eigentlich überflüssig sein!
/*
if(!is_dir(wp_upload_dir()['basedir'].'/'.LL_PLUGIN_NAME.'/logs'))
	if(!is_dir(wp_upload_dir()['basedir'].'/'.LL_PLUGIN_NAME))
 		mkdir(wp_upload_dir()['basedir'].'/'.LL_PLUGIN_NAME.'/logs');
*/

/*
* $params = Wert, der ausgegeben werden soll, kann jeden Typ haben, wenn String und leer, dann "Debug ohne Parameter", "TRACE" gibt den debug_backtrace aus.
* $token = 	false: wird nicht ausgegeben
*			'print': wird in die Log-Datei geschrieben (Datei kann in $die übersteuert werden)
*			'display': wird immer ausgegeben (alte Form)
* $die =	true: Programmausführung wird nach Ausgabe des Wertes unterbrochen (nur bei Bildschirmausgabe wirksam)
*			'<filename>': übersteuert im Falle einer Printausgabe den File debug.txt
* $monitor = true: Ausgabe wird immer ausgeführt, auch wenn es sich um eine Produktivumgebung
*/

if(!function_exists('ll_crm_debug')) { //Dies ist nur für den Paralellbetrieb mit der alten Version!
	function ll_crm_debug($params=array(), $token = false, $die = false, $monitor = false) {
		if(empty($params) and is_string($params)) $params = "Debug ohne Parameter!";
		static $ll_crm_debug;
		if(!isset($ll_crm_debug)) {
			$ll_crm_debug = apply_filters('ll_crm_debug',array());
			if(isset($_REQUEST['ll_func']) and !in_array($_REQUEST['ll_func'],$ll_crm_debug)) $ll_crm_debug[] = $_REQUEST['ll_func'];
		}
		//Damit der Ausstieg übersteuert wird! (letzte Bedingung aus Kompatitibilitätsgründen)
		if((defined('LL_TOOLS_DEV') and LL_TOOLS_DEV) or isset($_REQUEST['ll_log']) or isset($_REQUEST['ll_func']) or ($token === 'display')) $monitor = true;
//		if(LL_TOOLS_DEV or isset($_REQUEST['ll_log']) or isset($_REQUEST['ll_func']) or ($token === 'display')) $monitor = true;
		if(empty($ll_crm_debug) and (($token === false) or !$monitor)) return true;
		if($params === "TRACE") $params = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS);

		$ll_crm_line = debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS)[0]["line"];
		$ll_crm_function = str_replace(WP_CONTENT_DIR,"",debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS)[0]["file"]);
		$ll_crm_function .= ":".debug_backtrace(DEBUG_BACKTRACE_IGNORE_ARGS)[1]["function"];

		if(isset($_REQUEST['ll_log'])) {
			static $ll_time;
			static $ll_start;
			if(!isset($ll_start)) $ll_start = microtime(true);
			$file = wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs/log_".current_time('Y-m-d_H-i').".txt";
			$text = str_pad($ll_crm_function." (Line:".$ll_crm_line.") - ",40) .str_pad((microtime(true)-$ll_time)." - ",20).
						(microtime(true)-$ll_start).chr(10);
			file_put_contents($file,$text, FILE_APPEND | LOCK_EX);

			$ll_time = microtime(true);
			return;				
		}		

		if($token === 'print') {
			$monitor = false; //Sonst wird bei 'print' Fehler immer angezeigt!""
			if(!is_string($die)) $die="debug.txt";
			$path = wp_upload_dir()['basedir']."/".LL_PLUGIN_NAME."/logs/";
			if(!is_dir($path)) return;

			file_put_contents($path.$die, $ll_crm_function." (Line:".$ll_crm_line.") - ".current_time('Y-m-d H:i:s').chr(10), FILE_APPEND | LOCK_EX);
			file_put_contents($path.$die, print_r($params,true).chr(10), FILE_APPEND | LOCK_EX);
		}

		if(in_array($ll_crm_function,$ll_crm_debug) or $token === true or $monitor === true) {
			echo $ll_crm_function." (Line:".$ll_crm_line.")<br>";
			echo "<pre>";
			print_r($params);
			echo "</pre>";
			if($die) die;
		}
		return;
	}	
}


/*
############# Einbau von Funktionsaufrufen auf der Seite
Aufruf mit <!-- ll_crm_func=<Name der Funktion> <Par1>=Value1 <Par2>Value2 ....-->
Implementierte Funktionen
* Project_Structure: zeigt die Struktur des Projektes (auch als Parameter zur Projektanzeige mit &structure)
* Flexlist: Parameter: $tag, $design, $text 
* LinkList: Wie Fexlist allerdings kann nur $tag übergeben werden, fixe Ausgabe
* List: Projektliste ACHTUNG nicht angepaßt!
* Project: Zeigt ein Projekt, muß die &uid= mit der ID-Nummer in der URL haben
* Map: zeichnet die Karte (derzeit werden keine Parameter übernommen)
* MailingList: Listet alle archivierten Mails auf: permalink = Permalink zur Seite auf der die Mail dargestellt werden soll (wenn leer, aufrufende Seite)
* Mailing: Stellt eine gewählte Mail dar: Parameter: id = Mail-ID, 
*/

function ll_tools_shortcode($params,$content=null,$tag = '') {
	ll_crm_debug(array($params,$content,$tag));
	//Im Gegensatz zur alten Methode über den $content wird diese Funktion für jeden Aufruf von Shortcode aufgerufen!
	if(!empty($params)) {
		if(!array_key_exists('func',$params)) $params['func'] = current($params);
		do_action('init_ll_functions',$params);
		if(strpos($params['func'],",") === false) {
			$type = 'LL_tools_';
			if(!function_exists($type.$params['func'])) {
				if(!function_exists('LL_crm_'.$params['func']))
					return 'Die Funktion LL_crm_'.$params['func'].'() oder LL_tools_'.$params['func'].'() ist nicht implementiert';
				$type = 'LL_crm_';
			} 
		 	return call_user_func($type.$params['func'] , $params);							
		} else {
			$func = explode(",",$func);
			$type = "LL_classes_";
			$func[1] = "short_".$func[1];
			if(!methode_exists($type.$func[0],$func[1])) {
				if(!methode_exists("ll_crm_".$func[0],"short_".$func[1]))
					return "Klasse LL_classes_/ll_crm_".$func[0]." oder Methode short_".$func[1]." nicht definiert";
			$type = "short_";		
			}
			return $type.$func[0]::$func[1]($args);	
		}
	}
	return "Kein Aufruf definiert!";
}

add_shortcode('ll-tools','ll_tools_shortcode');


//Dies ist für den Customizer notwendig, wenn Polylang installliert ist, da sonst Polylang auf die home_url umschaltet
//Gehört zu Domain-Mapping. Der File wird aber zu spät geladen, daher vorerst hier.
add_filter( 'pll_after_languages_cache', function($languages) {
	if(strpos($_SERVER['REQUEST_URI'],'customize_changeset_uuid') !== false) {
		if(defined('LL_CROSS_DOMAIN') and LL_CROSS_DOMAIN) {
			$home_url = home_url();
			$site_url = site_url();
			foreach($languages as $key => $language) {
				$languages[$key]->home_url = str_replace($home_url,$site_url,$language->home_url);
				$languages[$key]->search_url = str_replace($home_url,$site_url,$language->search_url);
			}
		}
		ll_crm_debug($_SERVER);
	}
	return $languages;
},10,4);


	/**
	 * Lädt die Komponentent. Array wird übergeben.
	 * Beim Laden stehen die arrays $components und $components_loaded zur Verfügung, um zu wissen,
	 * ob andere Komponenten geladen sind.
	 * 
	 * @since
	 *
	 * @param array $compoentes = Kompoenten
	 *
	 * 4.2.5.7 => $loaded_compoents
	 */


function ll_tools_load_components($components) {
	//ACHTUNG: $components wird gegenenfalls in den geladenen Files verwendet zur Schaltung von Filtern
	//daher nicht umbenennen!
	static $loaded_components;
	$loaded_components = array();
	if(is_array($components)) {
		foreach($components as $name => $component) {
			if(substr($component,-4) == ".php") {
				if(file_exists(LL_PLUGIN_DIR."components/".$component)) {
					require_once(LL_PLUGIN_DIR."components/".$component);
					$loaded_components[] = $name;
				}						
			} else {
				$component = str_replace(" ","-",$component);
				if(file_exists(LL_PLUGIN_DIR."components/ll-tools-".$component.".php")) {
					require_once(LL_PLUGIN_DIR."components/ll-tools-".$component.".php");
					$loaded_components[] = $component;				
				} elseif(is_dir(LL_PLUGIN_DIR."components/".$component)) {
					require_once(LL_PLUGIN_DIR."components/".$component."/ll-tools-".$component.".php");
					$loaded_components[] = $component;				
				} else {
					//new WP_Error("Komponente nicht installiert");
				}			
			}
		}
	}		
}


function ll_tools_file_loader($dir, $mask = "", $basedir = LL_PLUGIN_DIR) {
	$abs_dir=$basedir.$dir."/";
	$file_list = ll_list_dir($abs_dir, $mask);
	static $files_loaded;
	ll_crm_debug($file_list);
	if(is_array($file_list)) 
		foreach($file_list as $file) {
			if(empty($files_loaded[$abs_dir.$file])) {
				if(substr($file,-4) == ".php") require_once($abs_dir.$file);
				//Wird registriert, damit er nicht nochmal geprüft wird.
				$files_loaded[$abs_dir.$file] = $file;	
			}
		}
}

function ll_list_dir($dir,$mask="") {
	ll_crm_debug(array($dir,$mask));
	static $file_list;
	if(!isset($file_list[$dir])) $file_list[$dir] = scandir($dir); 
	$files=array();
	ll_crm_debug($file_list);
	foreach($file_list[$dir] as $dir_file) 
		if(is_file($dir."/".$dir_file) and (substr($dir_file,0,strlen($mask)) == $mask)) 
			$files[] = $dir_file; 
	return $files;
}

function ll_civi_deldir($dir,&$dirlist) {
	$files = scandir($dir);
	foreach($files as $file) {
		if(count_chars($file,3) != ".") {
			if(is_dir($dir.$file)) {
				ll_civi_deldir($dir.$file."/",$dirlist);
				if(rmdir($dir.$file));
			}
			if(is_file($dir.$file)) {
				unlink($dir.$file);
			}				
		}
	}
}

	/**
	 * Prüft, ob in den Default OptionOptions Sprachabhängige Felder enthalten sind
	 * 
	 * @since
	 *
	 * @param array $options ein eingetragens Datenset in optionOptions
	 * @param string $set Name des Optionsets, das konvertriert werden soll.
	 */

function ll_convert_optionOptions_lang($options,$set) {
	static $language_fields = array();
	static $languages;
	if(!isset($languages))
		$languages = apply_filters('ll_tools_languages',array());
	if(!empty($languages)) {
		if(!isset($language_fields[$set])) {
			$default = apply_filters('ll_tools_get_standardSet_'.$set,array(),'default');
			foreach($default as $def => $def_option) {	
				if(isset($def_option['language']))
					$language_fields[$set][] = $def;
			}
		}
		foreach($language_fields[$set] as $field) {
			$options[$field] = $options[$field][get_locale()];
		}
	}
	return $options;
}

?>
