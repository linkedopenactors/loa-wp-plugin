<?php
/**
* Plugin Name: LivingLines Tools
* Description: Kernfunktionalität für MU-Installationen, wenn LL-Tools im MainBlog nicht aktiviert ist!
* Version: 4.1.5
* Author: Hannes Zagar
* License:
* Plugin URI: http://livinglines.at/livinglines-tools/
*/
defined( 'ABSPATH' ) or die( 'No script kiddies please!' );
require_once(WP_PLUGIN_DIR.'/livinglines-tools/livinglines-tools.php');

?>
